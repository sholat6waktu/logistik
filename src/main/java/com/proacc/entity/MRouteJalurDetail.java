package com.proacc.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MRouteJalurDetailSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_route_jalur_detail")
@IdClass(MRouteJalurDetailSerializable.class)
public class MRouteJalurDetail {

	@Id
	@Column(name = "m_route_id")
	private Integer MRouteId;
	
	@Id
	@Column(name = "m_route_jalur_id")	
	private Integer MRouteJalurId;
	
	@Id
	@Column(name = "route_jalur_detail_id")
	private Integer routeJalurDetailId;
	
	@Column(name = "m_cabang_id")
	private Integer mCabangId;
	
	@Column(name = "m_cabangdetail_id")
	private Integer mCabangDetaild;
	
	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	@Column(name = "m_agendetail_id")
	private Integer mAgendetailId;
	
	@Column(name = "route_jalur_detail_status")
	private Boolean routeJalurStatus;
	
	@Id
	@Column(name = "route_jalur_detail_company_id")
	private String routeJalurDetailCompanyId;

	public Integer getMRouteId() {
		return MRouteId;
	}

	public void setMRouteId(Integer mRouteId) {
		MRouteId = mRouteId;
	}

	public Integer getMRouteJalurId() {
		return MRouteJalurId;
	}

	public void setMRouteJalurId(Integer mRouteJalurId) {
		MRouteJalurId = mRouteJalurId;
	}

	public Integer getRouteJalurDetailId() {
		return routeJalurDetailId;
	}

	public void setRouteJalurDetailId(Integer routeJalurDetailId) {
		this.routeJalurDetailId = routeJalurDetailId;
	}

	public Integer getmCabangId() {
		return mCabangId;
	}

	public void setmCabangId(Integer mCabangId) {
		this.mCabangId = mCabangId;
	}

	public Integer getmCabangDetaild() {
		return mCabangDetaild;
	}

	public void setmCabangDetaild(Integer mCabangDetaild) {
		this.mCabangDetaild = mCabangDetaild;
	}

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getmAgendetailId() {
		return mAgendetailId;
	}

	public void setmAgendetailId(Integer mAgendetailId) {
		this.mAgendetailId = mAgendetailId;
	}

	public Boolean getRouteJalurStatus() {
		return routeJalurStatus;
	}

	public void setRouteJalurStatus(Boolean routeJalurStatus) {
		this.routeJalurStatus = routeJalurStatus;
	}

	public String getRouteJalurDetailCompanyId() {
		return routeJalurDetailCompanyId;
	}

	public void setRouteJalurDetailCompanyId(String routeJalurDetailCompanyId) {
		this.routeJalurDetailCompanyId = routeJalurDetailCompanyId;
	}

	
	
	
	
	
	
}