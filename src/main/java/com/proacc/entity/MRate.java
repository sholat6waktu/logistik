package com.proacc.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MRateSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_rate")
@IdClass(MRateSerializable.class)
public class MRate {

	@Id
	@Column(name = "rate_id1")
	private Integer rateId1;
	
	@Id
	@Column(name = "rate_id2")
	private Integer rateId2;
	
	@Column(name = "m_kota_from_id")
	private Integer mKotaFromId;
	
	@Column(name = "m_province_from_id")
	private Integer mProvinceFromId;
	
	@Column(name = "m_kota_to_id")
	private Integer mKotaToId;
	
	@Column(name = "m_province_to_id")
	private Integer mProvinceToId;
	
	@Column(name = "m_service_id")
	private Integer mServiceId;
	
	@Column(name = "rate_vehicle")
	private String rateVehicle;
	
	@Column(name = "rate_price")
	private Float ratePrice;
	
	@Column(name = "rate_startdate")
	private LocalDate rateStartdate;
	
	@Column(name = "rate_enddate")
	private LocalDate rateEnddate;
	
	@Column(name = "rate_Draftstatus")
	private String rateDraftstatus;
	
	@Column(name = "rate_status")
	private boolean rateStatus;
	
	@Id
	@Column(name = "rate_company_id")
	private String rateCompanyId;

	public Integer getRateId1() {
		return rateId1;
	}

	public void setRateId1(Integer rateId1) {
		this.rateId1 = rateId1;
	}

	public Integer getRateId2() {
		return rateId2;
	}

	public void setRateId2(Integer rateId2) {
		this.rateId2 = rateId2;
	}

	public Integer getmKotaFromId() {
		return mKotaFromId;
	}

	public void setmKotaFromId(Integer mKotaFromId) {
		this.mKotaFromId = mKotaFromId;
	}

	public Integer getmProvinceFromId() {
		return mProvinceFromId;
	}

	public void setmProvinceFromId(Integer mProvinceFromId) {
		this.mProvinceFromId = mProvinceFromId;
	}

	public Integer getmKotaToId() {
		return mKotaToId;
	}

	public void setmKotaToId(Integer mKotaToId) {
		this.mKotaToId = mKotaToId;
	}

	public Integer getmProvinceToId() {
		return mProvinceToId;
	}

	public void setmProvinceToId(Integer mProvinceToId) {
		this.mProvinceToId = mProvinceToId;
	}

	public Integer getmServiceId() {
		return mServiceId;
	}

	public void setmServiceId(Integer mServiceId) {
		this.mServiceId = mServiceId;
	}

	public String getRateVehicle() {
		return rateVehicle;
	}

	public void setRateVehicle(String rateVehicle) {
		this.rateVehicle = rateVehicle;
	}

	public Float getRatePrice() {
		return ratePrice;
	}

	public void setRatePrice(Float ratePrice) {
		this.ratePrice = ratePrice;
	}

	public LocalDate getRateStartdate() {
		return rateStartdate;
	}

	public void setRateStartdate(LocalDate rateStartdate) {
		this.rateStartdate = rateStartdate;
	}

	public LocalDate getRateEnddate() {
		return rateEnddate;
	}

	public void setRateEnddate(LocalDate rateEnddate) {
		this.rateEnddate = rateEnddate;
	}

	public String getRateDraftstatus() {
		return rateDraftstatus;
	}

	public void setRateDraftstatus(String rateDraftstatus) {
		this.rateDraftstatus = rateDraftstatus;
	}

	public boolean isRateStatus() {
		return rateStatus;
	}

	public void setRateStatus(boolean rateStatus) {
		this.rateStatus = rateStatus;
	}

	public String getRateCompanyId() {
		return rateCompanyId;
	}

	public void setRateCompanyId(String rateCompanyId) {
		this.rateCompanyId = rateCompanyId;
	}
}