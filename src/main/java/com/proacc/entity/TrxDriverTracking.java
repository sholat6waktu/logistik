package com.proacc.entity;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxDriverTrackingSerializable;
import com.proacc.serializable.TrxDriverscheduleSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_drivertracking")
@IdClass(TrxDriverTrackingSerializable.class)
public class TrxDriverTracking {
	
	@Id
	@Column(name = "id")
	private Integer id;

	@Id
	@Column(name = "trx_order_id")
	private String tmTrxOrderId;
	
	@Column(name = "tracking_id")
	private Integer trackingId;
	
	@Column(name = "status")
	private Integer status;
	
	@Column(name = "m_driver_id")
	private Integer mDriverId;
	
	@Id
	@Column(name = "company_id")
	private String companyId;
	
	@Column(name = "date_tracking")
	private LocalDateTime dateTracking;

	@Column(name = "date_kantor")
	private LocalDateTime dateKantor;

	

	public LocalDateTime getDateKantor() {
		return dateKantor;
	}

	public void setDateKantor(LocalDateTime dateKantor) {
		this.dateKantor = dateKantor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(Integer trackingId) {
		this.trackingId = trackingId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(Integer mDriverId) {
		this.mDriverId = mDriverId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public LocalDateTime getDateTracking() {
		return dateTracking;
	}

	public void setDateTracking(LocalDateTime dateTracking) {
		this.dateTracking = dateTracking;
	}
	
		
}
