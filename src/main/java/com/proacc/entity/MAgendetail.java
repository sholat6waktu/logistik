package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MAgendetailSerializable;


@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_agendetail")
@IdClass(MAgendetailSerializable.class)
public class MAgendetail {
	@Id
	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	@Id
	@Column(name = "agendetail_id")
	private Integer agendetailId;
	
	@Column(name = "m_province_id")
	private Integer mProvinceId;
	
	@Column(name = "m_transferpoint_id")
	private Integer mTransferpointId;
	
	@Column(name = "m_kota_id")
	private Integer mKotaId;
	
	@Column(name = "agendetail_task")
	private String agendetailTask;
	
	@Column(name = "agendetail_created_by")
	private UUID agendetailCreatedBy;
	
	@Column(name="agendetail_created_at")
	private LocalDateTime agendetailCreatedAt;
	
	@Column(name = "agendetail_updated_by")
	private UUID agendetailUpdatedBy;
	
	@Column(name="agendetail_updated_at")
	private LocalDateTime agendetailUpdatedAt;
	
	@Column(name = "agendetail_status")
	private boolean agendetailStatus;
	
	@Id
	@Column(name = "agendetail_company_id")
	private String agendetailCompanyId;
	
	@Column(name = "kode")
	private String kode;
	
	@Column(name = "type_kode")
	private Integer typeKode;
	
	@Column(name = "m_cabang_id_kode")
	private Integer mCabangIdKode;
	
	@Column(name = "m_agen_id_kode")
	private Integer mAgenIdKode;
	
	@Column(name = "agendetail_id_kode")
	private Integer agendetailIdKode;
	
	

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public Integer getTypeKode() {
		return typeKode;
	}

	public void setTypeKode(Integer typeKode) {
		this.typeKode = typeKode;
	}

	public Integer getmCabangIdKode() {
		return mCabangIdKode;
	}

	public void setmCabangIdKode(Integer mCabangIdKode) {
		this.mCabangIdKode = mCabangIdKode;
	}

	public Integer getmAgenIdKode() {
		return mAgenIdKode;
	}

	public void setmAgenIdKode(Integer mAgenIdKode) {
		this.mAgenIdKode = mAgenIdKode;
	}

	public Integer getAgendetailIdKode() {
		return agendetailIdKode;
	}

	public void setAgendetailIdKode(Integer agendetailIdKode) {
		this.agendetailIdKode = agendetailIdKode;
	}

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getAgendetailId() {
		return agendetailId;
	}

	public void setAgendetailId(Integer agendetailId) {
		this.agendetailId = agendetailId;
	}

	public Integer getmProvinceId() {
		return mProvinceId;
	}

	public void setmProvinceId(Integer mProvinceId) {
		this.mProvinceId = mProvinceId;
	}

	public Integer getmTransferpointId() {
		return mTransferpointId;
	}

	public void setmTransferpointId(Integer mTransferpointId) {
		this.mTransferpointId = mTransferpointId;
	}

	public Integer getmKotaId() {
		return mKotaId;
	}

	public void setmKotaId(Integer mKotaId) {
		this.mKotaId = mKotaId;
	}

	public String getAgendetailTask() {
		return agendetailTask;
	}

	public void setAgendetailTask(String agendetailTask) {
		this.agendetailTask = agendetailTask;
	}

	public UUID getAgendetailCreatedBy() {
		return agendetailCreatedBy;
	}

	public void setAgendetailCreatedBy(UUID agendetailCreatedBy) {
		this.agendetailCreatedBy = agendetailCreatedBy;
	}

	public LocalDateTime getAgendetailCreatedAt() {
		return agendetailCreatedAt;
	}

	public void setAgendetailCreatedAt(LocalDateTime agendetailCreatedAt) {
		this.agendetailCreatedAt = agendetailCreatedAt;
	}

	public UUID getAgendetailUpdatedBy() {
		return agendetailUpdatedBy;
	}

	public void setAgendetailUpdatedBy(UUID agendetailUpdatedBy) {
		this.agendetailUpdatedBy = agendetailUpdatedBy;
	}

	public LocalDateTime getAgendetailUpdatedAt() {
		return agendetailUpdatedAt;
	}

	public void setAgendetailUpdatedAt(LocalDateTime agendetailUpdatedAt) {
		this.agendetailUpdatedAt = agendetailUpdatedAt;
	}

	public boolean isAgendetailStatus() {
		return agendetailStatus;
	}

	public void setAgendetailStatus(boolean agendetailStatus) {
		this.agendetailStatus = agendetailStatus;
	}

	public String getAgendetailCompanyId() {
		return agendetailCompanyId;
	}

	public void setAgendetailCompanyId(String agendetailCompanyId) {
		this.agendetailCompanyId = agendetailCompanyId;
	}
}