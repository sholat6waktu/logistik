package com.proacc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxNumbergenSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_numbergen")
@IdClass(TrxNumbergenSerializable.class)
public class TrxNumbergen {
	@Id
	@Column(name = "tm_trx_number_id")
	private Integer tmTrxNumberId;
	
	@Id
	@Column(name = "trx_numbergen_id")
	private String trxNumbergenId;
	
	@Id
	@Column(name = "trx_numbergen_Company_id")
	private String trxNumbergenCompanyId;

	public Integer getTmTrxNumberId() {
		return tmTrxNumberId;
	}

	public void setTmTrxNumberId(Integer tmTrxNumberId) {
		this.tmTrxNumberId = tmTrxNumberId;
	}

	public String getTrxNumbergenId() {
		return trxNumbergenId;
	}

	public void setTrxNumbergenId(String trxNumbergenId) {
		this.trxNumbergenId = trxNumbergenId;
	}

	public String getTrxNumbergenCompanyId() {
		return trxNumbergenCompanyId;
	}

	public void setTrxNumbergenCompanyId(String trxNumbergenCompanyId) {
		this.trxNumbergenCompanyId = trxNumbergenCompanyId;
	}
}
