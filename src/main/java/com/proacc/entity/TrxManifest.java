package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxManifestSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_manifest")
@IdClass(TrxManifestSerializable.class)
public class TrxManifest {

	@Id
	@Column(name = "id")
	private Integer id;
	
	
	
	@Column(name = "manifest_no")
	private String manifestNo;
	
	@Column(name = "driver_id")
	private Integer driverId;
	
	@Column(name = "cabang_id_from")
	private Integer cabangIdFrom;
	
	@Column(name = "agen_id_from")
	private Integer agenIdFrom;

	@Column(name = "agen_detail_id_from")
	private Integer agenDetailIdFrom;
	
	@Column(name = "cabang_id_to")
	private Integer  cabangIdTo;
	
	@Column(name = "agen_id_to")
	private Integer agenIdTo;

	@Column(name = "agen_detail_id_to")
	private Integer agenDetailIdTo;
	
	@Column(name = "tanggal")
	private Date tanggal;
	
	@Id
	@Column(name = "company_id")
	private String companyId;

	@Column(name = "created_by")
	private UUID createdBy;
	
	@Column(name="created_at")
	private LocalDateTime createdAt;
	
	@Column(name = "updated_by")
	private UUID updatedBy;
	
	@Column(name="updated_at")
	private LocalDateTime updatedAt;
	
	
	
	
	public UUID getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UUID createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public UUID getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(UUID updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getManifestNo() {
		return manifestNo;
	}

	public void setManifestNo(String manifestNo) {
		this.manifestNo = manifestNo;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public Integer getCabangIdFrom() {
		return cabangIdFrom;
	}

	public void setCabangIdFrom(Integer cabangIdFrom) {
		this.cabangIdFrom = cabangIdFrom;
	}

	public Integer getAgenIdFrom() {
		return agenIdFrom;
	}

	public void setAgenIdFrom(Integer agenIdFrom) {
		this.agenIdFrom = agenIdFrom;
	}

	public Integer getAgenDetailIdFrom() {
		return agenDetailIdFrom;
	}

	public void setAgenDetailIdFrom(Integer agenDetailIdFrom) {
		this.agenDetailIdFrom = agenDetailIdFrom;
	}

	public Integer getCabangIdTo() {
		return cabangIdTo;
	}

	public void setCabangIdTo(Integer cabangIdTo) {
		this.cabangIdTo = cabangIdTo;
	}

	public Integer getAgenIdTo() {
		return agenIdTo;
	}

	public void setAgenIdTo(Integer agenIdTo) {
		this.agenIdTo = agenIdTo;
	}

	public Integer getAgenDetailIdTo() {
		return agenDetailIdTo;
	}

	public void setAgenDetailIdTo(Integer agenDetailIdTo) {
		this.agenDetailIdTo = agenDetailIdTo;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	

}
