package com.proacc.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxNumberSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_number")
@IdClass(TrxNumberSerializable.class)
public class TrxNumber {

	@Id
	@Column(name = "trx_number_id")
	private Integer trxNumberId;
	
	@Column(name = "trx_number_prefix")
	private String trxNumberPrefix;
	
	@Column(name = "trx_number_period")
	private String trxNumberPeriod;
	
	@Column(name = "trx_number_runningnumber")
	private Integer trxNumberRunningnumber;
	
	@Column(name = "trx_number_date")
	private LocalDate trxNumberDate;
	
	@Id
	@Column(name = "trx_number_company_id")
	private String trxNumberCompanyId;

	public Integer getTrxNumberId() {
		return trxNumberId;
	}

	public void setTrxNumberId(Integer trxNumberId) {
		this.trxNumberId = trxNumberId;
	}

	public String getTrxNumberPrefix() {
		return trxNumberPrefix;
	}

	public void setTrxNumberPrefix(String trxNumberPrefix) {
		this.trxNumberPrefix = trxNumberPrefix;
	}

	public String getTrxNumberPeriod() {
		return trxNumberPeriod;
	}

	public void setTrxNumberPeriod(String trxNumberPeriod) {
		this.trxNumberPeriod = trxNumberPeriod;
	}

	public Integer getTrxNumberRunningnumber() {
		return trxNumberRunningnumber;
	}

	public void setTrxNumberRunningnumber(Integer trxNumberRunningnumber) {
		this.trxNumberRunningnumber = trxNumberRunningnumber;
	}

	public LocalDate getTrxNumberDate() {
		return trxNumberDate;
	}

	public void setTrxNumberDate(LocalDate trxNumberDate) {
		this.trxNumberDate = trxNumberDate;
	}

	public String getTrxNumberCompanyId() {
		return trxNumberCompanyId;
	}

	public void setTrxNumberCompanyId(String trxNumberCompanyId) {
		this.trxNumberCompanyId = trxNumberCompanyId;
	}
}
