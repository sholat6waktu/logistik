package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MCabangSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_cabang")
@IdClass(MCabangSerializable.class)
public class MCabang {
	@Id
	@Column(name = "cabang_id")
	private Integer cabangId;
	
	@Column(name = "cabang_name")
	private String cabangName;
	
	@Column(name = "m_province_id")
	private Integer mProvinceId;
	
	@Column(name = "m_kota_id")
	private Integer mKotaId;
	
	@Column(name = "cabang_created_by")
	private UUID cabangCreatedBy;
	
	@Column(name="cabang_created_at")
	private LocalDateTime cabangCreatedAt;
	
	@Column(name = "cabang_updated_by")
	private UUID cabangUpdatedBy;
	
	@Column(name="cabang_updated_at")
	private LocalDateTime cabangUpdatedAt;
	
	@Column(name = "cabang_status")
	private boolean cabangStatus;
	
	@Id
	@Column(name = "cabang_company_id")
	private String cabangCompanyId;
	
	@Column(name = "kode")
	private String kode;
	
	@Column(name = "type_kode")
	private Integer typeKode;
	
	@Column(name = "m_cabang_id_kode")
	private Integer mCabangIdKode;
	
	@Column(name = "m_agen_id_kode")
	private Integer mAgenIdKode;
	
	@Column(name = "agendetail_id_kode")
	private Integer agendetailIdKode;	
	
	

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public Integer getTypeKode() {
		return typeKode;
	}

	public void setTypeKode(Integer typeKode) {
		this.typeKode = typeKode;
	}

	public Integer getmCabangIdKode() {
		return mCabangIdKode;
	}

	public void setmCabangIdKode(Integer mCabangIdKode) {
		this.mCabangIdKode = mCabangIdKode;
	}

	public Integer getmAgenIdKode() {
		return mAgenIdKode;
	}

	public void setmAgenIdKode(Integer mAgenIdKode) {
		this.mAgenIdKode = mAgenIdKode;
	}

	public Integer getAgendetailIdKode() {
		return agendetailIdKode;
	}

	public void setAgendetailIdKode(Integer agendetailIdKode) {
		this.agendetailIdKode = agendetailIdKode;
	}

	public Integer getCabangId() {
		return cabangId;
	}

	public void setCabangId(Integer cabangId) {
		this.cabangId = cabangId;
	}

	public String getCabangName() {
		return cabangName;
	}

	public void setCabangName(String cabangName) {
		this.cabangName = cabangName;
	}

	public Integer getmProvinceId() {
		return mProvinceId;
	}

	public void setmProvinceId(Integer mProvinceId) {
		this.mProvinceId = mProvinceId;
	}

	public Integer getmKotaId() {
		return mKotaId;
	}

	public void setmKotaId(Integer mKotaId) {
		this.mKotaId = mKotaId;
	}

	public UUID getCabangCreatedBy() {
		return cabangCreatedBy;
	}

	public void setCabangCreatedBy(UUID cabangCreatedBy) {
		this.cabangCreatedBy = cabangCreatedBy;
	}

	public LocalDateTime getCabangCreatedAt() {
		return cabangCreatedAt;
	}

	public void setCabangCreatedAt(LocalDateTime cabangCreatedAt) {
		this.cabangCreatedAt = cabangCreatedAt;
	}

	public UUID getCabangUpdatedBy() {
		return cabangUpdatedBy;
	}

	public void setCabangUpdatedBy(UUID cabangUpdatedBy) {
		this.cabangUpdatedBy = cabangUpdatedBy;
	}

	public LocalDateTime getCabangUpdatedAt() {
		return cabangUpdatedAt;
	}

	public void setCabangUpdatedAt(LocalDateTime cabangUpdatedAt) {
		this.cabangUpdatedAt = cabangUpdatedAt;
	}

	public boolean isCabangStatus() {
		return cabangStatus;
	}

	public void setCabangStatus(boolean cabangStatus) {
		this.cabangStatus = cabangStatus;
	}

	public String getCabangCompanyId() {
		return cabangCompanyId;
	}

	public void setCabangCompanyId(String cabangCompanyId) {
		this.cabangCompanyId = cabangCompanyId;
	}
}