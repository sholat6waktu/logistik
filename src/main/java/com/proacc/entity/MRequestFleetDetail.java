package com.proacc.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MRequestFleetDetailSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_request_fleet_detail")
@IdClass(MRequestFleetDetailSerializable.class)
public class MRequestFleetDetail {
	@Id
	@Column(name = "id")
	private Integer id;
	
	@Id
	@Column(name = "detail_id")
	private Integer detailId;
	
	@Column(name = "order_id")
	private String orderId;
	
	@Id
	@Column(name = "company_id")
	private String companyId;

	@Column(name = "status")
	private Boolean status;
	
	

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDetailId() {
		return detailId;
	}

	public void setDetailId(Integer detailId) {
		this.detailId = detailId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	

	
}
