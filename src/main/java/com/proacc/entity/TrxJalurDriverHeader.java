package com.proacc.entity;

import java.sql.Time;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxJalurDriverHeaderSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_jalur_driver_header")
@IdClass(TrxJalurDriverHeaderSerializable.class)
public class TrxJalurDriverHeader {

	@Id
	@Column(name = "trx_driverjalurheader_id")
	private Integer trxDriverjalurHeaderId;
	
	@Column(name = "m_route_id")
	private Integer mRouteId;

	@Id
	@Column(name = "company_id")
	private String companyId;

	public Integer getTrxDriverjalurHeaderId() {
		return trxDriverjalurHeaderId;
	}

	public void setTrxDriverjalurHeaderId(Integer trxDriverjalurHeaderId) {
		this.trxDriverjalurHeaderId = trxDriverjalurHeaderId;
	}

	
	
	public Integer getmRouteId() {
		return mRouteId;
	}

	public void setmRouteId(Integer mRouteId) {
		this.mRouteId = mRouteId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
	
	
}
