package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MagenCoverAreaSerializable;


@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_agen_cover_area")
@IdClass(MagenCoverAreaSerializable.class)
public class MagenCoverArea {
	@Id
	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	@Id
	@Column(name = "m_agendetail_id")
	private Integer MAagendetailId;
	
	@Id
	@Column(name = "agen_cover_area_id")
	private Integer agenCoverAreaId;
	
	@Column(name = "m_province_id")
	private Integer mProvinceId;
	
	
	@Column(name = "m_kota_id")
	private Integer mKotaId;
	
	@Column(name = "agen_cover_area_status")
	private boolean agenCoverAreaStatus;
	
	@Id
	@Column(name = "agen_cover_area_company_id")
	private String agenCoverAreaCompany_id;

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getMAagendetailId() {
		return MAagendetailId;
	}

	public void setMAagendetailId(Integer mAagendetailId) {
		MAagendetailId = mAagendetailId;
	}

	public Integer getAgenCoverAreaId() {
		return agenCoverAreaId;
	}

	public void setAgenCoverAreaId(Integer agenCoverAreaId) {
		this.agenCoverAreaId = agenCoverAreaId;
	}

	public Integer getmProvinceId() {
		return mProvinceId;
	}

	public void setmProvinceId(Integer mProvinceId) {
		this.mProvinceId = mProvinceId;
	}

	public Integer getmKotaId() {
		return mKotaId;
	}

	public void setmKotaId(Integer mKotaId) {
		this.mKotaId = mKotaId;
	}

	public boolean isAgenCoverAreaStatus() {
		return agenCoverAreaStatus;
	}

	public void setAgenCoverAreaStatus(boolean agenCoverAreaStatus) {
		this.agenCoverAreaStatus = agenCoverAreaStatus;
	}

	public String getAgenCoverAreaCompany_id() {
		return agenCoverAreaCompany_id;
	}

	public void setAgenCoverAreaCompany_id(String agenCoverAreaCompany_id) {
		this.agenCoverAreaCompany_id = agenCoverAreaCompany_id;
	}

	

	
}