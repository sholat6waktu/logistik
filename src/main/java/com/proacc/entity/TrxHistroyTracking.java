package com.proacc.entity;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxHistroyTrackingSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_history_tracking")
@IdClass(TrxHistroyTrackingSerializable.class)
public class TrxHistroyTracking {

	@Id
	@Column(name = "order_id")
	private String orderId;
	
	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "cabang_id")
	private Integer cabangId;
	
	@Column(name = "agen_id")
	private Integer agenId;
	
	@Column(name = "agendetail_id")
	private Integer agendetailId;
	
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "waktu")
	private LocalDateTime waktu;

	@Id
	@Column(name = "company_id")
	private String companyId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCabangId() {
		return cabangId;
	}

	public void setCabangId(Integer cabangId) {
		this.cabangId = cabangId;
	}

	public Integer getAgenId() {
		return agenId;
	}

	public void setAgenId(Integer agenId) {
		this.agenId = agenId;
	}

	public Integer getAgendetailId() {
		return agendetailId;
	}

	public void setAgendetailId(Integer agendetailId) {
		this.agendetailId = agendetailId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDateTime getWaktu() {
		return waktu;
	}

	public void setWaktu(LocalDateTime waktu) {
		this.waktu = waktu;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	

	
}
