package com.proacc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxPartdetailSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_partdetail")
@IdClass(TrxPartdetailSerializable.class)
public class TrxPartdetail {

	@Id
	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Id
	@Column(name = "tm_trx_kolidetail_id")
	private Integer tmTrxKolidetailId;
	
	@Id
	@Column(name = "trx_partdetail_id")
	private Integer trxpartdetailId;
	
	@Column(name = "trx_partdetail_partnumber")
	private String trxpartdetailPartnumber;
	
	@Column(name = "trx_partdetail_name")
	private String trxpartdetailName;
	
	@Column(name = "trx_partdetail_quantity")
	private Integer trxpartdetailQuantity;
	
	@Column(name = "m_unit_id")
	private Integer mUnitId;
	
	@Column(name = "trx_partdetail_returquantity")
	private Integer trxpartdetailReturquantity;
	
	@Column(name = "trx_partdetail_returdescription")
	private String trxpartdetailReturdescription;
	
	@Column(name = "trx_partdetail_status")
	private boolean trxPartdetailStatus;
	
	@Id
	@Column(name = "trx_partdetail_company_id")
	private String trxPartdetailCompanyId;

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getTmTrxKolidetailId() {
		return tmTrxKolidetailId;
	}

	public void setTmTrxKolidetailId(Integer tmTrxKolidetailId) {
		this.tmTrxKolidetailId = tmTrxKolidetailId;
	}

	public Integer getTrxpartdetailId() {
		return trxpartdetailId;
	}

	public void setTrxpartdetailId(Integer trxpartdetailId) {
		this.trxpartdetailId = trxpartdetailId;
	}

	public String getTrxpartdetailPartnumber() {
		return trxpartdetailPartnumber;
	}

	public void setTrxpartdetailPartnumber(String trxpartdetailPartnumber) {
		this.trxpartdetailPartnumber = trxpartdetailPartnumber;
	}

	public String getTrxpartdetailName() {
		return trxpartdetailName;
	}

	public void setTrxpartdetailName(String trxpartdetailName) {
		this.trxpartdetailName = trxpartdetailName;
	}

	public Integer getTrxpartdetailQuantity() {
		return trxpartdetailQuantity;
	}

	public void setTrxpartdetailQuantity(Integer trxpartdetailQuantity) {
		this.trxpartdetailQuantity = trxpartdetailQuantity;
	}

	public Integer getmUnitId() {
		return mUnitId;
	}

	public void setmUnitId(Integer mUnitId) {
		this.mUnitId = mUnitId;
	}

	public Integer getTrxpartdetailReturquantity() {
		return trxpartdetailReturquantity;
	}

	public void setTrxpartdetailReturquantity(Integer trxpartdetailReturquantity) {
		this.trxpartdetailReturquantity = trxpartdetailReturquantity;
	}

	public String getTrxpartdetailReturdescription() {
		return trxpartdetailReturdescription;
	}

	public void setTrxpartdetailReturdescription(String trxpartdetailReturdescription) {
		this.trxpartdetailReturdescription = trxpartdetailReturdescription;
	}

	public boolean isTrxPartdetailStatus() {
		return trxPartdetailStatus;
	}

	public void setTrxPartdetailStatus(boolean trxPartdetailStatus) {
		this.trxPartdetailStatus = trxPartdetailStatus;
	}

	public String getTrxPartdetailCompanyId() {
		return trxPartdetailCompanyId;
	}

	public void setTrxPartdetailCompanyId(String trxPartdetailCompanyId) {
		this.trxPartdetailCompanyId = trxPartdetailCompanyId;
	}
}
