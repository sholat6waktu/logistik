package com.proacc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxDeliveryorderSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_deliveryorder")
@IdClass(TrxDeliveryorderSerializable.class)
public class TrxDeliveryorder {

	@Id
	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Column(name = "trx_deliveryorder_bukti")
	private String trxDeliveryorderBukti;
	
	@Id
	@Column(name = "trx_deliveryorder_company_id")
	private String trxDeliveryorderCompanyId;

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public String getTrxDeliveryorderBukti() {
		return trxDeliveryorderBukti;
	}

	public void setTrxDeliveryorderBukti(String trxDeliveryorderBukti) {
		this.trxDeliveryorderBukti = trxDeliveryorderBukti;
	}

	public String getTrxDeliveryorderCompanyId() {
		return trxDeliveryorderCompanyId;
	}

	public void setTrxDeliveryorderCompanyId(String trxDeliveryorderCompanyId) {
		this.trxDeliveryorderCompanyId = trxDeliveryorderCompanyId;
	}
}
