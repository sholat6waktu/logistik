package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MServiceSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_service")
@IdClass(MServiceSerializable.class)
public class MService {

	@Id
	@Column(name = "service_id")
	private Integer serviceId;
	
	@Column(name = "service_name")
	private String serviceName;
	
	@Column(name = "service_deliverytime1")
	private Integer serviceDeliverytime1;
	
	@Column(name = "service_deliverytime2")
	private Integer serviceDeliverytime2;
	
	@Column(name = "service_description")
	private String serviceDescription;
	
	@Column(name = "service_created_by")
	private UUID serviceCreatedBy;
	
	@Column(name="service_created_at")
	private LocalDateTime serviceCreatedAt;
	
	@Column(name = "service_updated_by")
	private UUID serviceUpdatedBy;
	
	@Column(name="service_updated_at")
	private LocalDateTime serviceUpdatedAt;
	
	@Column(name = "service_status")
	private boolean serviceStatus;
	
	@Id
	@Column(name = "service_company_id")
	private String serviceCompanyId;

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Integer getServiceDeliverytime1() {
		return serviceDeliverytime1;
	}

	public void setServiceDeliverytime1(Integer serviceDeliverytime1) {
		this.serviceDeliverytime1 = serviceDeliverytime1;
	}

	public Integer getServiceDeliverytime2() {
		return serviceDeliverytime2;
	}

	public void setServiceDeliverytime2(Integer serviceDeliverytime2) {
		this.serviceDeliverytime2 = serviceDeliverytime2;
	}

	public String getServiceDescription() {
		return serviceDescription;
	}

	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}

	public UUID getServiceCreatedBy() {
		return serviceCreatedBy;
	}

	public void setServiceCreatedBy(UUID serviceCreatedBy) {
		this.serviceCreatedBy = serviceCreatedBy;
	}

	public LocalDateTime getServiceCreatedAt() {
		return serviceCreatedAt;
	}

	public void setServiceCreatedAt(LocalDateTime serviceCreatedAt) {
		this.serviceCreatedAt = serviceCreatedAt;
	}

	public UUID getServiceUpdatedBy() {
		return serviceUpdatedBy;
	}

	public void setServiceUpdatedBy(UUID serviceUpdatedBy) {
		this.serviceUpdatedBy = serviceUpdatedBy;
	}

	public LocalDateTime getServiceUpdatedAt() {
		return serviceUpdatedAt;
	}

	public void setServiceUpdatedAt(LocalDateTime serviceUpdatedAt) {
		this.serviceUpdatedAt = serviceUpdatedAt;
	}

	public boolean isServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(boolean serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public String getServiceCompanyId() {
		return serviceCompanyId;
	}

	public void setServiceCompanyId(String serviceCompanyId) {
		this.serviceCompanyId = serviceCompanyId;
	}
}
