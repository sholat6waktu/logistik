package com.proacc.entity;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxOutboundSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_outbound")
@IdClass(TrxOutboundSerializable.class)
public class TrxOutbound {

	@Id
	@Column(name = "report_outbound_id")
	private Integer reportOutboundId;
	
	@Column(name = "trx_order_id")
	private String trxOrderId;

	@Column(name = "trx_driverschedule_id")
	private Integer trxDriverscheduleId;
	
	@Column(name = "date_outbound")
	private LocalDateTime dateOutbound;
	
	@Id
	@Column(name = "company_id")
	private String companyId;

	@Column(name = "trx_tracking_to_id")
	private Integer trxTrackingToId;
	
	@Column(name = "trx_tracking_from_id")
	private Integer trxTrackingFromId;

	

	public Integer getTrxTrackingToId() {
		return trxTrackingToId;
	}

	public void setTrxTrackingToId(Integer trxTrackingToId) {
		this.trxTrackingToId = trxTrackingToId;
	}

	public Integer getTrxTrackingFromId() {
		return trxTrackingFromId;
	}

	public void setTrxTrackingFromId(Integer trxTrackingFromId) {
		this.trxTrackingFromId = trxTrackingFromId;
	}

	public Integer getReportOutboundId() {
		return reportOutboundId;
	}

	public void setReportOutboundId(Integer reportOutboundId) {
		this.reportOutboundId = reportOutboundId;
	}

	public String getTrxOrderId() {
		return trxOrderId;
	}

	public void setTrxOrderId(String trxOrderId) {
		this.trxOrderId = trxOrderId;
	}

	public Integer getTrxDriverscheduleId() {
		return trxDriverscheduleId;
	}

	public void setTrxDriverscheduleId(Integer trxDriverscheduleId) {
		this.trxDriverscheduleId = trxDriverscheduleId;
	}

	

	public LocalDateTime getDateOutbound() {
		return dateOutbound;
	}

	public void setDateOutbound(LocalDateTime dateOutbound) {
		this.dateOutbound = dateOutbound;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	

	
}
