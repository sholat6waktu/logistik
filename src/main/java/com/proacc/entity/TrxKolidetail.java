package com.proacc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxKolidetailSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_kolidetail")
@IdClass(TrxKolidetailSerializable.class)
public class TrxKolidetail {

	@Id
	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Id
	@Column(name = "trx_kolidetail_id")
	private Integer trxKolidetailId;
	
	@Column(name = "trx_kolidetail_weight")
	private Float trxKolidetailWeight;
	
	@Column(name = "m_weight_id")
	private Integer mWeightId;
	
	@Column(name = "trx_kolidetail_height")
	private Float trxKolidetailHeight;
	
	@Column(name = "m_height_id")
	private Integer mHeightId;

	@Column(name = "trx_kolidetail_width")
	private Float trxKolidetailWidth;
	
	@Column(name = "m_width_id")
	private Integer mWidthId;
	
	@Column(name = "trx_kolidetail_length")
	private Float trxKolidetailLength;
	
	@Column(name = "m_length_id")
	private Integer mLengthId;
	
	@Column(name = "trx_kolidetail_declareditem")
	private String trxKolidetailDeclareditem;
	
	@Column(name = "trx_kolidetail_status")
	private boolean trxKolidetailStatus;
	
	@Id
	@Column(name = "trx_kolidetail_company_id")
	private String trxKolidetailCompanyId;

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getTrxKolidetailId() {
		return trxKolidetailId;
	}

	public void setTrxKolidetailId(Integer trxKolidetailId) {
		this.trxKolidetailId = trxKolidetailId;
	}

	public Float getTrxKolidetailWeight() {
		return trxKolidetailWeight;
	}

	public void setTrxKolidetailWeight(Float trxKolidetailWeight) {
		this.trxKolidetailWeight = trxKolidetailWeight;
	}

	public Integer getmWeightId() {
		return mWeightId;
	}

	public void setmWeightId(Integer mWeightId) {
		this.mWeightId = mWeightId;
	}

	public Float getTrxKolidetailHeight() {
		return trxKolidetailHeight;
	}

	public void setTrxKolidetailHeight(Float trxKolidetailHeight) {
		this.trxKolidetailHeight = trxKolidetailHeight;
	}

	public Integer getmHeightId() {
		return mHeightId;
	}

	public void setmHeightId(Integer mHeightId) {
		this.mHeightId = mHeightId;
	}

	public Float getTrxKolidetailWidth() {
		return trxKolidetailWidth;
	}

	public void setTrxKolidetailWidth(Float trxKolidetailWidth) {
		this.trxKolidetailWidth = trxKolidetailWidth;
	}

	public Integer getmWidthId() {
		return mWidthId;
	}

	public void setmWidthId(Integer mWidthId) {
		this.mWidthId = mWidthId;
	}

	public Float getTrxKolidetailLength() {
		return trxKolidetailLength;
	}

	public void setTrxKolidetailLength(Float trxKolidetailLength) {
		this.trxKolidetailLength = trxKolidetailLength;
	}

	public Integer getmLengthId() {
		return mLengthId;
	}

	public void setmLengthId(Integer mLengthId) {
		this.mLengthId = mLengthId;
	}

	public String getTrxKolidetailDeclareditem() {
		return trxKolidetailDeclareditem;
	}

	public void setTrxKolidetailDeclareditem(String trxKolidetailDeclareditem) {
		this.trxKolidetailDeclareditem = trxKolidetailDeclareditem;
	}

	public boolean isTrxKolidetailStatus() {
		return trxKolidetailStatus;
	}

	public void setTrxKolidetailStatus(boolean trxKolidetailStatus) {
		this.trxKolidetailStatus = trxKolidetailStatus;
	}

	public String getTrxKolidetailCompanyId() {
		return trxKolidetailCompanyId;
	}

	public void setTrxKolidetailCompanyId(String trxKolidetailCompanyId) {
		this.trxKolidetailCompanyId = trxKolidetailCompanyId;
	}
}
