package com.proacc.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MRequestFleetSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_request_fleet")
@IdClass(MRequestFleetSerializable.class)
public class MRequestFleet {
	@Id
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "no")
	private String no;
	
	@Column(name = "m_cabang_id")
	private Integer mCabangId;
	
	
	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	
	@Column(name = "m_agendetail_id")
	private Integer mAgendetailId;
	
	@Column(name = "m_cabang_id_to")
	private Integer mCabangIdTo;
	
	@Column(name = "m_agen_id_to")
	private Integer mAgenIdTo;
	
	@Column(name = "m_agendetail_id_to")
	private Integer mAgendetailIdTo;
	
	@Column(name = "date_request")
	private LocalDate dateRequest;
	
	@Column(name = "status")
	private boolean status;
	
	@Id
	@Column(name = "company_id")
	private String companyId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public Integer getmCabangId() {
		return mCabangId;
	}

	public void setmCabangId(Integer mCabangId) {
		this.mCabangId = mCabangId;
	}

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getmAgendetailId() {
		return mAgendetailId;
	}

	public void setmAgendetailId(Integer mAgendetailId) {
		this.mAgendetailId = mAgendetailId;
	}

	public Integer getmCabangIdTo() {
		return mCabangIdTo;
	}

	public void setmCabangIdTo(Integer mCabangIdTo) {
		this.mCabangIdTo = mCabangIdTo;
	}

	public Integer getmAgenIdTo() {
		return mAgenIdTo;
	}

	public void setmAgenIdTo(Integer mAgenIdTo) {
		this.mAgenIdTo = mAgenIdTo;
	}

	public Integer getmAgendetailIdTo() {
		return mAgendetailIdTo;
	}

	public void setmAgendetailIdTo(Integer mAgendetailIdTo) {
		this.mAgendetailIdTo = mAgendetailIdTo;
	}

	public LocalDate getDateRequest() {
		return dateRequest;
	}

	public void setDateRequest(LocalDate dateRequest) {
		this.dateRequest = dateRequest;
	}

	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
