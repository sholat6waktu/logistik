package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MCabangdetailSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_cabangdetail")
@IdClass(MCabangdetailSerializable.class)
public class MCabangdetail {
	@Id
	@Column(name = "m_cabang_id")
	private Integer mCabangId;
	
	@Id
	@Column(name = "cabangdetail_id")
	private Integer cabangdetailId;
	
	@Column(name = "m_province_id")
	private Integer mProvinceId;
	
	@Column(name = "m_kotadetail_id")
	private Integer mKotaId;
	
	@Column(name = "m_agendetail_id")
	private Integer mAgenDetailId;
	
	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	@Column(name = "m_parent_id")
	private Integer mParentId;
	
	@Column(name = "cabangdetail_priority")
	private Integer cabangdetailPriority;
	
	@Column(name = "cabangdetail_agenttype")
	private String cabangdetailAgenttype;
	
	@Column(name = "cabangdetail_created_by")
	private UUID cabangdetailCreatedBy;
	
	@Column(name="cabangdetail_created_at")
	private LocalDateTime cabangdetailCreatedAt;
	
	@Column(name = "cabangdetail_updated_by")
	private UUID cabangdetailUpdatedBy;
	
	@Column(name="cabangdetail_updated_at")
	private LocalDateTime cabangdetailUpdatedAt;
	
	@Column(name = "cabangdetail_status")
	private boolean cabangdetailStatus;
	
	@Id
	@Column(name = "cabangdetail_company_id")
	private String cabangdetailCompanyId;
	
	

	public Integer getmAgenDetailId() {
		return mAgenDetailId;
	}

	public void setmAgenDetailId(Integer mAgenDetailId) {
		this.mAgenDetailId = mAgenDetailId;
	}

	public Integer getmCabangId() {
		return mCabangId;
	}

	public void setmCabangId(Integer mCabangId) {
		this.mCabangId = mCabangId;
	}

	public Integer getCabangdetailId() {
		return cabangdetailId;
	}

	public void setCabangdetailId(Integer cabangdetailId) {
		this.cabangdetailId = cabangdetailId;
	}

	public Integer getmProvinceId() {
		return mProvinceId;
	}

	public void setmProvinceId(Integer mProvinceId) {
		this.mProvinceId = mProvinceId;
	}

	public Integer getmKotaId() {
		return mKotaId;
	}

	public void setmKotaId(Integer mKotaId) {
		this.mKotaId = mKotaId;
	}

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getmParentId() {
		return mParentId;
	}

	public void setmParentId(Integer mParentId) {
		this.mParentId = mParentId;
	}

	public Integer getCabangdetailPriority() {
		return cabangdetailPriority;
	}

	public void setCabangdetailPriority(Integer cabangdetailPriority) {
		this.cabangdetailPriority = cabangdetailPriority;
	}

	public String getCabangdetailAgenttype() {
		return cabangdetailAgenttype;
	}

	public void setCabangdetailAgenttype(String cabangdetailAgenttype) {
		this.cabangdetailAgenttype = cabangdetailAgenttype;
	}

	public UUID getCabangdetailCreatedBy() {
		return cabangdetailCreatedBy;
	}

	public void setCabangdetailCreatedBy(UUID cabangdetailCreatedBy) {
		this.cabangdetailCreatedBy = cabangdetailCreatedBy;
	}

	public LocalDateTime getCabangdetailCreatedAt() {
		return cabangdetailCreatedAt;
	}

	public void setCabangdetailCreatedAt(LocalDateTime cabangdetailCreatedAt) {
		this.cabangdetailCreatedAt = cabangdetailCreatedAt;
	}

	public UUID getCabangdetailUpdatedBy() {
		return cabangdetailUpdatedBy;
	}

	public void setCabangdetailUpdatedBy(UUID cabangdetailUpdatedBy) {
		this.cabangdetailUpdatedBy = cabangdetailUpdatedBy;
	}

	public LocalDateTime getCabangdetailUpdatedAt() {
		return cabangdetailUpdatedAt;
	}

	public void setCabangdetailUpdatedAt(LocalDateTime cabangdetailUpdatedAt) {
		this.cabangdetailUpdatedAt = cabangdetailUpdatedAt;
	}

	public boolean isCabangdetailStatus() {
		return cabangdetailStatus;
	}

	public void setCabangdetailStatus(boolean cabangdetailStatus) {
		this.cabangdetailStatus = cabangdetailStatus;
	}

	public String getCabangdetailCompanyId() {
		return cabangdetailCompanyId;
	}

	public void setCabangdetailCompanyId(String cabangdetailCompanyId) {
		this.cabangdetailCompanyId = cabangdetailCompanyId;
	}
}
