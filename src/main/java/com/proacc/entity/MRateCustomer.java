package com.proacc.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MRateCustomerSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_ratecustomer")
@IdClass(MRateCustomerSerializable.class)
public class MRateCustomer {

	@Id
	@Column(name = "ratecustomer_id")
	private Integer ratecustomerId;

	@Column(name = "m_customer_id")
	private Integer mCustomerId;
	
	@Column(name = "ratecustomer_startdate")
	private LocalDate ratecustomerStartdate;
	
	@Column(name = "ratecustomer_enddate")
	private LocalDate ratecustomerEnddate;
	
	@Column(name = "ratecustomer_created_by")
	private UUID ratecustomerCreatedBy;
	
	@Column(name="ratecustomer_created_at")
	private LocalDateTime ratecustomerCreatedAt;
	
	@Column(name = "ratecustomer_updated_by")
	private UUID ratecustomerUpdatedBy;
	
	@Column(name="ratecustomer_updated_at")
	private LocalDateTime ratecustomerUpdatedAt;
	
	@Column(name = "ratecustomer_status")
	private boolean ratecustomerStatus;
	
	@Id
	@Column(name = "ratecustomer_company_id")
	private String ratecustomerCompanyId;

	public Integer getRatecustomerId() {
		return ratecustomerId;
	}

	public void setRatecustomerId(Integer ratecustomerId) {
		this.ratecustomerId = ratecustomerId;
	}

	public Integer getmCustomerId() {
		return mCustomerId;
	}

	public void setmCustomerId(Integer mCustomerId) {
		this.mCustomerId = mCustomerId;
	}

	public LocalDate getRatecustomerStartdate() {
		return ratecustomerStartdate;
	}

	public void setRatecustomerStartdate(LocalDate ratecustomerStartdate) {
		this.ratecustomerStartdate = ratecustomerStartdate;
	}

	public LocalDate getRatecustomerEnddate() {
		return ratecustomerEnddate;
	}

	public void setRatecustomerEnddate(LocalDate ratecustomerEnddate) {
		this.ratecustomerEnddate = ratecustomerEnddate;
	}

	public UUID getRatecustomerCreatedBy() {
		return ratecustomerCreatedBy;
	}

	public void setRatecustomerCreatedBy(UUID ratecustomerCreatedBy) {
		this.ratecustomerCreatedBy = ratecustomerCreatedBy;
	}

	public LocalDateTime getRatecustomerCreatedAt() {
		return ratecustomerCreatedAt;
	}

	public void setRatecustomerCreatedAt(LocalDateTime ratecustomerCreatedAt) {
		this.ratecustomerCreatedAt = ratecustomerCreatedAt;
	}

	public UUID getRatecustomerUpdatedBy() {
		return ratecustomerUpdatedBy;
	}

	public void setRatecustomerUpdatedBy(UUID ratecustomerUpdatedBy) {
		this.ratecustomerUpdatedBy = ratecustomerUpdatedBy;
	}

	public LocalDateTime getRatecustomerUpdatedAt() {
		return ratecustomerUpdatedAt;
	}

	public void setRatecustomerUpdatedAt(LocalDateTime ratecustomerUpdatedAt) {
		this.ratecustomerUpdatedAt = ratecustomerUpdatedAt;
	}

	public boolean isRatecustomerStatus() {
		return ratecustomerStatus;
	}

	public void setRatecustomerStatus(boolean ratecustomerStatus) {
		this.ratecustomerStatus = ratecustomerStatus;
	}

	public String getRatecustomerCompanyId() {
		return ratecustomerCompanyId;
	}

	public void setRatecustomerCompanyId(String ratecustomerCompanyId) {
		this.ratecustomerCompanyId = ratecustomerCompanyId;
	}
}
