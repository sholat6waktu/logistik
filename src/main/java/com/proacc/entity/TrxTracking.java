package com.proacc.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxTrackingSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_tracking")
@IdClass(TrxTrackingSerializable.class)
public class TrxTracking {

	@Id
	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Id
	@Column(name = "trx_tracking_id")
	private Integer trxTrackingId;
	
	@Column(name = "m_contact_customer_from_id")
	private Integer mContactCustomerFromId;
	
	@Column(name = "m_cabang_id")
	private Integer mCabangId;
	
	@Column(name = "m_cabang_detail_id")
	private Integer mCabangDetailId;
	
	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	@Column(name = "m_parent_id")
	private Integer mParentId;
	
	@Column(name = "m_contact_customer_to_id")
	private Integer mContactCustomerToId;
	
	@Column(name = "trx_tracking_status")
	private String trxTrackingStatus;
	
	@Column(name = "trx_tracking_statusinbound")
	private String trxTrackingStatusinbound;
	
	@Column(name = "trx_tracking_statusoutbound")
	private String trxTrackingStatusoutbound;
	
	@Column(name = "trx_tracking_estimeted_time")
	private LocalDateTime trxTrackingEstimetedTime;
	
	@Column(name = "m_agendetail_id")
	private Integer mAgenDetailId;
	
	@Column(name = "trx_driverjalurheader_id")
	private Integer trxDriverjalurheaderId;
	
	@Column(name = "trx_driverjalur_id")
	private Integer trxDriverjalurId;
	
	@Column(name="type_tambahan")
	private String typeTambahan;
	
	
	
	@Id
	@Column(name = "trx_tracking_company_id")
	private String trxTrackingCompanyId;

	
	
	
	public String getTypeTambahan() {
		return typeTambahan;
	}

	public void setTypeTambahan(String typeTambahan) {
		this.typeTambahan = typeTambahan;
	}

	public Integer getTrxDriverjalurId() {
		return trxDriverjalurId;
	}

	public void setTrxDriverjalurId(Integer trxDriverjalurId) {
		this.trxDriverjalurId = trxDriverjalurId;
	}

	public Integer getTrxDriverjalurheaderId() {
		return trxDriverjalurheaderId;
	}

	public void setTrxDriverjalurheaderId(Integer trxDriverjalurheaderId) {
		this.trxDriverjalurheaderId = trxDriverjalurheaderId;
	}

	public Integer getmAgenDetailId() {
		return mAgenDetailId;
	}

	public void setmAgenDetailId(Integer mAgenDetailId) {
		this.mAgenDetailId = mAgenDetailId;
	}

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getTrxTrackingId() {
		return trxTrackingId;
	}

	public void setTrxTrackingId(Integer trxTrackingId) {
		this.trxTrackingId = trxTrackingId;
	}

	public Integer getmContactCustomerFromId() {
		return mContactCustomerFromId;
	}

	public void setmContactCustomerFromId(Integer mContactCustomerFromId) {
		this.mContactCustomerFromId = mContactCustomerFromId;
	}

	public Integer getmCabangId() {
		return mCabangId;
	}

	public void setmCabangId(Integer mCabangId) {
		this.mCabangId = mCabangId;
	}

	public Integer getmCabangDetailId() {
		return mCabangDetailId;
	}

	public void setmCabangDetailId(Integer mCabangDetailId) {
		this.mCabangDetailId = mCabangDetailId;
	}

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getmParentId() {
		return mParentId;
	}

	public void setmParentId(Integer mParentId) {
		this.mParentId = mParentId;
	}

	public Integer getmContactCustomerToId() {
		return mContactCustomerToId;
	}

	public void setmContactCustomerToId(Integer mContactCustomerToId) {
		this.mContactCustomerToId = mContactCustomerToId;
	}

	public String getTrxTrackingStatus() {
		return trxTrackingStatus;
	}

	public void setTrxTrackingStatus(String trxTrackingStatus) {
		this.trxTrackingStatus = trxTrackingStatus;
	}

	public String getTrxTrackingStatusinbound() {
		return trxTrackingStatusinbound;
	}

	public void setTrxTrackingStatusinbound(String trxTrackingStatusinbound) {
		this.trxTrackingStatusinbound = trxTrackingStatusinbound;
	}

	public String getTrxTrackingStatusoutbound() {
		return trxTrackingStatusoutbound;
	}

	public void setTrxTrackingStatusoutbound(String trxTrackingStatusoutbound) {
		this.trxTrackingStatusoutbound = trxTrackingStatusoutbound;
	}

	public LocalDateTime getTrxTrackingEstimetedTime() {
		return trxTrackingEstimetedTime;
	}

	public void setTrxTrackingEstimetedTime(LocalDateTime trxTrackingEstimetedTime) {
		this.trxTrackingEstimetedTime = trxTrackingEstimetedTime;
	}

	public String getTrxTrackingCompanyId() {
		return trxTrackingCompanyId;
	}

	public void setTrxTrackingCompanyId(String trxTrackingCompanyId) {
		this.trxTrackingCompanyId = trxTrackingCompanyId;
	}
}