package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MFleetSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_fleet")
@IdClass(MFleetSerializable.class)
public class MFleet {

	@Id
	@Column(name = "fleet_id")
	private Integer fleetId;
	
	@Column(name = "fleet_name")
	private String fleetName;
	
	@Column(name = "fleet_vehicle")
	private String fleetVehicle;
	
	@Column(name = "m_vehicletype_id")
	private Integer mVehicletypeId;
	
	@Column(name = "fleet_weight")
	private Float fleetWeight;
	
	@Column(name = "m_weight_id")
	private Integer mWeightId;
	
	@Column(name = "fleet_height")
	private Float fleetHeight;
	
	@Column(name = "m_height_id")
	private Integer mHeightId;
	
	@Column(name = "fleet_width")
	private Float fleetWidth;
	
	@Column(name = "m_width_id")
	private Integer mWidthId;
	
	@Column(name = "fleet_length")
	private Float fleetLength;
	
	@Column(name = "m_length_id")
	private Integer mLengthId;
	
	@Column(name = "fleet_volume")
	private Float fleetVolume;
	
	@Column(name = "m_volume_id")
	private Integer mVolumeId;
	
	@Column(name = "fleet_created_by")
	private UUID fleetCreatedBy;
	
	@Column(name="fleet_created_at")
	private LocalDateTime fleetCreatedAt;
	
	@Column(name = "fleet_updated_by")
	private UUID fleetUpdatedBy;
	
	@Column(name="fleet_updated_at")
	private LocalDateTime fleetUpdatedAt;
	
	@Column(name = "fleet_status")
	private boolean fleetStatus;
	
	@Id
	@Column(name = "fleet_company_id")
	private String fleetCompanyId;

	public Integer getFleetId() {
		return fleetId;
	}

	public void setFleetId(Integer fleetId) {
		this.fleetId = fleetId;
	}

	public String getFleetName() {
		return fleetName;
	}

	public void setFleetName(String fleetName) {
		this.fleetName = fleetName;
	}

	public String getFleetVehicle() {
		return fleetVehicle;
	}

	public void setFleetVehicle(String fleetVehicle) {
		this.fleetVehicle = fleetVehicle;
	}

	public Integer getmVehicletypeId() {
		return mVehicletypeId;
	}

	public void setmVehicletypeId(Integer mVehicletypeId) {
		this.mVehicletypeId = mVehicletypeId;
	}

	public Float getFleetWeight() {
		return fleetWeight;
	}

	public void setFleetWeight(Float fleetWeight) {
		this.fleetWeight = fleetWeight;
	}

	public Integer getmWeightId() {
		return mWeightId;
	}

	public void setmWeightId(Integer mWeightId) {
		this.mWeightId = mWeightId;
	}

	public Float getFleetHeight() {
		return fleetHeight;
	}

	public void setFleetHeight(Float fleetHeight) {
		this.fleetHeight = fleetHeight;
	}

	public Integer getmHeightId() {
		return mHeightId;
	}

	public void setmHeightId(Integer mHeightId) {
		this.mHeightId = mHeightId;
	}

	public Float getFleetWidth() {
		return fleetWidth;
	}

	public void setFleetWidth(Float fleetWidth) {
		this.fleetWidth = fleetWidth;
	}

	public Integer getmWidthId() {
		return mWidthId;
	}

	public void setmWidthId(Integer mWidthId) {
		this.mWidthId = mWidthId;
	}

	public Float getFleetLength() {
		return fleetLength;
	}

	public void setFleetLength(Float fleetLength) {
		this.fleetLength = fleetLength;
	}

	public Integer getmLengthId() {
		return mLengthId;
	}

	public void setmLengthId(Integer mLengthId) {
		this.mLengthId = mLengthId;
	}

	public Float getFleetVolume() {
		return fleetVolume;
	}

	public void setFleetVolume(Float fleetVolume) {
		this.fleetVolume = fleetVolume;
	}

	public Integer getmVolumeId() {
		return mVolumeId;
	}

	public void setmVolumeId(Integer mVolumeId) {
		this.mVolumeId = mVolumeId;
	}

	public UUID getFleetCreatedBy() {
		return fleetCreatedBy;
	}

	public void setFleetCreatedBy(UUID fleetCreatedBy) {
		this.fleetCreatedBy = fleetCreatedBy;
	}

	public LocalDateTime getFleetCreatedAt() {
		return fleetCreatedAt;
	}

	public void setFleetCreatedAt(LocalDateTime fleetCreatedAt) {
		this.fleetCreatedAt = fleetCreatedAt;
	}

	public UUID getFleetUpdatedBy() {
		return fleetUpdatedBy;
	}

	public void setFleetUpdatedBy(UUID fleetUpdatedBy) {
		this.fleetUpdatedBy = fleetUpdatedBy;
	}

	public LocalDateTime getFleetUpdatedAt() {
		return fleetUpdatedAt;
	}

	public void setFleetUpdatedAt(LocalDateTime fleetUpdatedAt) {
		this.fleetUpdatedAt = fleetUpdatedAt;
	}

	public boolean isFleetStatus() {
		return fleetStatus;
	}

	public void setFleetStatus(boolean fleetStatus) {
		this.fleetStatus = fleetStatus;
	}

	public String getFleetCompanyId() {
		return fleetCompanyId;
	}

	public void setFleetCompanyId(String fleetCompanyId) {
		this.fleetCompanyId = fleetCompanyId;
	}
}
