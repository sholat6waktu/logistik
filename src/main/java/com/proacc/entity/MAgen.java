package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MAgenSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_agen")
@IdClass(MAgenSerializable.class)
public class MAgen {
	@Id
	@Column(name = "agen_id")
	private Integer agenId;
	
	@Column(name = "agen_type")
	private Integer agenType;
	
	@Column(name = "agen_name")
	private String agenName;
	
	@Column(name = "agen_created_by")
	private UUID agenCreatedBy;
	
	@Column(name="agen_created_at")
	private LocalDateTime agenCreatedAt;
	
	@Column(name = "agen_updated_by")
	private UUID agenUpdatedBy;
	
	@Column(name="agen_updated_at")
	private LocalDateTime agenUpdatedAt;
	
	@Column(name = "agen_status")
	private boolean agenStatus;
	
	@Id
	@Column(name = "agen_company_id")
	private String agenCompanyId;

	public Integer getAgenId() {
		return agenId;
	}

	public void setAgenId(Integer agenId) {
		this.agenId = agenId;
	}

	public String getAgenName() {
		return agenName;
	}

	public void setAgenName(String agenName) {
		this.agenName = agenName;
	}

	public UUID getAgenCreatedBy() {
		return agenCreatedBy;
	}

	public void setAgenCreatedBy(UUID agenCreatedBy) {
		this.agenCreatedBy = agenCreatedBy;
	}

	public LocalDateTime getAgenCreatedAt() {
		return agenCreatedAt;
	}

	public void setAgenCreatedAt(LocalDateTime agenCreatedAt) {
		this.agenCreatedAt = agenCreatedAt;
	}

	public UUID getAgenUpdatedBy() {
		return agenUpdatedBy;
	}

	public void setAgenUpdatedBy(UUID agenUpdatedBy) {
		this.agenUpdatedBy = agenUpdatedBy;
	}

	public LocalDateTime getAgenUpdatedAt() {
		return agenUpdatedAt;
	}

	public void setAgenUpdatedAt(LocalDateTime agenUpdatedAt) {
		this.agenUpdatedAt = agenUpdatedAt;
	}

	public boolean isAgenStatus() {
		return agenStatus;
	}

	public void setAgenStatus(boolean agenStatus) {
		this.agenStatus = agenStatus;
	}

	public String getAgenCompanyId() {
		return agenCompanyId;
	}

	public void setAgenCompanyId(String agenCompanyId) {
		this.agenCompanyId = agenCompanyId;
	}

	public Integer getAgenType() {
		return agenType;
	}

	public void setAgenType(Integer agenType) {
		this.agenType = agenType;
	}
	
	
	
	
}
