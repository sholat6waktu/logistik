package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MJenispaketSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_jenispaket")
@IdClass(MJenispaketSerializable.class)
public class MJenispaket {
	@Id
	@Column(name = "jenispaket_id")
	private Integer jenispaketId;
	
	@Column(name = "jenispaket_name")
	private String jenispaketName;
	
	@Column(name = "jenispaket_description")
	private String jenispaketDescription;
	
	@Column(name = "jenispaket_created_by")
	private UUID jenispaketCreatedBy;
	
	@Column(name="jenispaket_created_at")
	private LocalDateTime jenispaketCreatedAt;
	
	@Column(name = "jenispaket_updated_by")
	private UUID jenispaketUpdatedBy;
	
	@Column(name="jenispaket_updated_at")
	private LocalDateTime jenispaketUpdatedAt;
	
	@Column(name = "jenispaket_status")
	private boolean jenispaketStatus;
	
	@Id
	@Column(name = "jenispaket_company_id")
	private String jenispaketCompanyId;

	public Integer getJenispaketId() {
		return jenispaketId;
	}

	public void setJenispaketId(Integer jenispaketId) {
		this.jenispaketId = jenispaketId;
	}

	public String getJenispaketName() {
		return jenispaketName;
	}

	public void setJenispaketName(String jenispaketName) {
		this.jenispaketName = jenispaketName;
	}

	public String getJenispaketDescription() {
		return jenispaketDescription;
	}

	public void setJenispaketDescription(String jenispaketDescription) {
		this.jenispaketDescription = jenispaketDescription;
	}

	public UUID getJenispaketCreatedBy() {
		return jenispaketCreatedBy;
	}

	public void setJenispaketCreatedBy(UUID jenispaketCreatedBy) {
		this.jenispaketCreatedBy = jenispaketCreatedBy;
	}

	public LocalDateTime getJenispaketCreatedAt() {
		return jenispaketCreatedAt;
	}

	public void setJenispaketCreatedAt(LocalDateTime jenispaketCreatedAt) {
		this.jenispaketCreatedAt = jenispaketCreatedAt;
	}

	public UUID getJenispaketUpdatedBy() {
		return jenispaketUpdatedBy;
	}

	public void setJenispaketUpdatedBy(UUID jenispaketUpdatedBy) {
		this.jenispaketUpdatedBy = jenispaketUpdatedBy;
	}

	public LocalDateTime getJenispaketUpdatedAt() {
		return jenispaketUpdatedAt;
	}

	public void setJenispaketUpdatedAt(LocalDateTime jenispaketUpdatedAt) {
		this.jenispaketUpdatedAt = jenispaketUpdatedAt;
	}

	public boolean isJenispaketStatus() {
		return jenispaketStatus;
	}

	public void setJenispaketStatus(boolean jenispaketStatus) {
		this.jenispaketStatus = jenispaketStatus;
	}

	public String getJenispaketCompanyId() {
		return jenispaketCompanyId;
	}

	public void setJenispaketCompanyId(String jenispaketCompanyId) {
		this.jenispaketCompanyId = jenispaketCompanyId;
	}
	
	
}
