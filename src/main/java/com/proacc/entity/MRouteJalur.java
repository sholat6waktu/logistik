package com.proacc.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MRouteJalurSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_route_jalur")
@IdClass(MRouteJalurSerializable.class)
public class MRouteJalur {

	@Id
	@Column(name = "m_route_id")
	private Integer MRouteId;
	
	@Id
	@Column(name = "route_jalur_id")
	private Integer routeJalurId;
	
	@Column(name = "m_cabang_id")
	private Integer mCabangId;
	
	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	@Column(name = "m_agendetail_id")
	private Integer mAgendetailId;
	
	@Column(name = "route_jalur_status")
	private Boolean routeJalurStatus;
	
	@Id
	@Column(name = "route_jalur_company_id")
	private String routeJalurCompanyId;

	public Integer getMRouteId() {
		return MRouteId;
	}

	public void setMRouteId(Integer mRouteId) {
		MRouteId = mRouteId;
	}

	public Integer getRouteJalurId() {
		return routeJalurId;
	}

	public void setRouteJalurId(Integer routeJalurId) {
		this.routeJalurId = routeJalurId;
	}

	public Integer getmCabangId() {
		return mCabangId;
	}

	public void setmCabangId(Integer mCabangId) {
		this.mCabangId = mCabangId;
	}

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getmAgendetailId() {
		return mAgendetailId;
	}

	public void setmAgendetailId(Integer mAgendetailId) {
		this.mAgendetailId = mAgendetailId;
	}

	public Boolean getRouteJalurStatus() {
		return routeJalurStatus;
	}

	public void setRouteJalurStatus(Boolean routeJalurStatus) {
		this.routeJalurStatus = routeJalurStatus;
	}

	public String getRouteJalurCompanyId() {
		return routeJalurCompanyId;
	}

	public void setRouteJalurCompanyId(String routeJalurCompanyId) {
		this.routeJalurCompanyId = routeJalurCompanyId;
	}

	
	
	
	
}