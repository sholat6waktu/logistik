package com.proacc.entity;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxDriverscheduleSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_Driverschedule")
@IdClass(TrxDriverscheduleSerializable.class)
public class TrxDriverschedule {

	@Id
	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Id
	@Column(name = "trx_driverschedule_id")
	private Integer trxDriverscheduleId;
	
	@Id
	@Column(name = "trx_driverschedule_company_id")
	private String trxDriverscheduleCompanyId;
	
	@Column(name = "m_driver_id")
	private Integer mDriverId;
	
	@Column(name = "tm_trx_tracking_id_from")
	private Integer tmTrxTrackingIdFrom;
	
	@Column(name = "tm_trx_tracking_id_to")
	private Integer tmTrxTrackingIdTo;
	
	@Column(name = "trx_driverschedule_time_from")
	private LocalTime trx_driverschedule_time_from;
	
	@Column(name = "trx_driverschedule_time_to")
	private LocalTime trx_driverschedule_time_to;
	
	@Column(name = "trx_driverschedule_weight")
	private Float trx_driverschedule_weight;
	
	@Column(name = "m_weight_id")
	private Integer mWeightId;
	
	@Column(name = "trx_driverschedule_volume")
	private Float trx_driverschedule_volume;
	
	@Column(name = "m_volume_id")
	private Integer mVolumeId;
	
	@Column(name = "trx_driverschedule_status")
	private String trxDriverscheduleStatus;
	
	@Column(name = "trx_driverschedule_statusoutbound")
	private String trxDriverscheduleStatusoutbound;
	
	@Column(name = "tgl_berangkat")
	private LocalDateTime tglBerangkat;
	
	@Column(name = "tgl_tiba")
	private LocalDateTime tglTiba;

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getTrxDriverscheduleId() {
		return trxDriverscheduleId;
	}

	public void setTrxDriverscheduleId(Integer trxDriverscheduleId) {
		this.trxDriverscheduleId = trxDriverscheduleId;
	}

	public String getTrxDriverscheduleCompanyId() {
		return trxDriverscheduleCompanyId;
	}

	public void setTrxDriverscheduleCompanyId(String trxDriverscheduleCompanyId) {
		this.trxDriverscheduleCompanyId = trxDriverscheduleCompanyId;
	}

	public Integer getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(Integer mDriverId) {
		this.mDriverId = mDriverId;
	}

	public Integer getTmTrxTrackingIdFrom() {
		return tmTrxTrackingIdFrom;
	}

	public void setTmTrxTrackingIdFrom(Integer tmTrxTrackingIdFrom) {
		this.tmTrxTrackingIdFrom = tmTrxTrackingIdFrom;
	}

	public Integer getTmTrxTrackingIdTo() {
		return tmTrxTrackingIdTo;
	}

	public void setTmTrxTrackingIdTo(Integer tmTrxTrackingIdTo) {
		this.tmTrxTrackingIdTo = tmTrxTrackingIdTo;
	}

	public LocalTime getTrx_driverschedule_time_from() {
		return trx_driverschedule_time_from;
	}

	public void setTrx_driverschedule_time_from(LocalTime trx_driverschedule_time_from) {
		this.trx_driverschedule_time_from = trx_driverschedule_time_from;
	}

	public LocalTime getTrx_driverschedule_time_to() {
		return trx_driverschedule_time_to;
	}

	public void setTrx_driverschedule_time_to(LocalTime trx_driverschedule_time_to) {
		this.trx_driverschedule_time_to = trx_driverschedule_time_to;
	}

	public Float getTrx_driverschedule_weight() {
		return trx_driverschedule_weight;
	}

	public void setTrx_driverschedule_weight(Float trx_driverschedule_weight) {
		this.trx_driverschedule_weight = trx_driverschedule_weight;
	}

	public Integer getmWeightId() {
		return mWeightId;
	}

	public void setmWeightId(Integer mWeightId) {
		this.mWeightId = mWeightId;
	}

	public Float getTrx_driverschedule_volume() {
		return trx_driverschedule_volume;
	}

	public void setTrx_driverschedule_volume(Float trx_driverschedule_volume) {
		this.trx_driverschedule_volume = trx_driverschedule_volume;
	}

	public Integer getmVolumeId() {
		return mVolumeId;
	}

	public void setmVolumeId(Integer mVolumeId) {
		this.mVolumeId = mVolumeId;
	}

	public String getTrxDriverscheduleStatus() {
		return trxDriverscheduleStatus;
	}

	public void setTrxDriverscheduleStatus(String trxDriverscheduleStatus) {
		this.trxDriverscheduleStatus = trxDriverscheduleStatus;
	}

	public String getTrxDriverscheduleStatusoutbound() {
		return trxDriverscheduleStatusoutbound;
	}

	public void setTrxDriverscheduleStatusoutbound(String trxDriverscheduleStatusoutbound) {
		this.trxDriverscheduleStatusoutbound = trxDriverscheduleStatusoutbound;
	}

	public LocalDateTime getTglBerangkat() {
		return tglBerangkat;
	}

	public void setTglBerangkat(LocalDateTime tglBerangkat) {
		this.tglBerangkat = tglBerangkat;
	}

	public LocalDateTime getTglTiba() {
		return tglTiba;
	}

	public void setTglTiba(LocalDateTime tglTiba) {
		this.tglTiba = tglTiba;
	}

	
	
}
