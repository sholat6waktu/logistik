package com.proacc.entity;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxJalurDriverSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_jalurdriver")
@IdClass(TrxJalurDriverSerializable.class)
public class TrxJalurDriver {

	@Id
	@Column(name = "trx_driverjalurheader_id")
	private Integer trxDriverjalurHeaderId;
	
	@Id
	@Column(name = "trx_driverjalur_id")
	private Integer trxDriverjalurId;

	@Column(name = "m_route_id")
	private Integer mRouteId;
	
	@Column(name = "m_routejalur_id")
	private Integer mRoutejalurId;
	
	@Column(name = "status_progress")
	private Integer statusProgress;
	
	
	@Column(name = "status")
	private Boolean status;

	@Id
	@Column(name = "company_id")
	private String companyId;
	
	@Column(name = "date_tracking")
	private LocalDateTime dateTracking;

	@Column(name = "date_kantor")
	private LocalDateTime dateKantor;
	
	

	

	public LocalDateTime getDateTracking() {
		return dateTracking;
	}

	public void setDateTracking(LocalDateTime dateTracking) {
		this.dateTracking = dateTracking;
	}

	public LocalDateTime getDateKantor() {
		return dateKantor;
	}

	public void setDateKantor(LocalDateTime dateKantor) {
		this.dateKantor = dateKantor;
	}

	public Integer getTrxDriverjalurId() {
		return trxDriverjalurId;
	}

	public void setTrxDriverjalurId(Integer trxDriverjalurId) {
		this.trxDriverjalurId = trxDriverjalurId;
	}

	public Integer getmRouteId() {
		return mRouteId;
	}

	public void setmRouteId(Integer mRouteId) {
		this.mRouteId = mRouteId;
	}

	public Integer getmRoutejalurId() {
		return mRoutejalurId;
	}

	public void setmRoutejalurId(Integer mRoutejalurId) {
		this.mRoutejalurId = mRoutejalurId;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Integer getTrxDriverjalurHeaderId() {
		return trxDriverjalurHeaderId;
	}

	public void setTrxDriverjalurHeaderId(Integer trxDriverjalurHeaderId) {
		this.trxDriverjalurHeaderId = trxDriverjalurHeaderId;
	}

	public Integer getStatusProgress() {
		return statusProgress;
	}

	public void setStatusProgress(Integer statusProgress) {
		this.statusProgress = statusProgress;
	}

	
	
}
