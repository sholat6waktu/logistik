package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MWeightSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_weight")
@IdClass(MWeightSerializable.class)
public class MWeight {

	@Id
	@Column(name = "weight_id")
	private Integer weightId;
	
	@Column(name = "weight_name")
	private String weightName;
	
	@Column(name = "weight_tokg")
	private Float weightTokg;
	
	@Column(name = "weight_description")
	private String weightDescription;
	
	@Column(name = "weight_created_by")
	private UUID weightCreatedBy;
	
	@Column(name="weight_created_at")
	private LocalDateTime weightCreatedAt;
	
	@Column(name = "weight_updated_by")
	private UUID weightUpdatedBy;
	
	@Column(name="weight_updated_at")
	private LocalDateTime weightUpdatedAt;
	
	@Column(name = "weight_status")
	private boolean weightStatus;
	
	@Id
	@Column(name = "weight_company_id")
	private String weightCompanyId;

	public Integer getWeightId() {
		return weightId;
	}

	public void setWeightId(Integer weightId) {
		this.weightId = weightId;
	}

	public String getWeightName() {
		return weightName;
	}

	public void setWeightName(String weightName) {
		this.weightName = weightName;
	}

	public Float getWeightTokg() {
		return weightTokg;
	}

	public void setWeightTokg(Float weightTokg) {
		this.weightTokg = weightTokg;
	}

	public String getWeightDescription() {
		return weightDescription;
	}

	public void setWeightDescription(String weightDescription) {
		this.weightDescription = weightDescription;
	}

	public UUID getWeightCreatedBy() {
		return weightCreatedBy;
	}

	public void setWeightCreatedBy(UUID weightCreatedBy) {
		this.weightCreatedBy = weightCreatedBy;
	}

	public LocalDateTime getWeightCreatedAt() {
		return weightCreatedAt;
	}

	public void setWeightCreatedAt(LocalDateTime weightCreatedAt) {
		this.weightCreatedAt = weightCreatedAt;
	}

	public UUID getWeightUpdatedBy() {
		return weightUpdatedBy;
	}

	public void setWeightUpdatedBy(UUID weightUpdatedBy) {
		this.weightUpdatedBy = weightUpdatedBy;
	}

	public LocalDateTime getWeightUpdatedAt() {
		return weightUpdatedAt;
	}

	public void setWeightUpdatedAt(LocalDateTime weightUpdatedAt) {
		this.weightUpdatedAt = weightUpdatedAt;
	}

	public boolean isWeightStatus() {
		return weightStatus;
	}

	public void setWeightStatus(boolean weightStatus) {
		this.weightStatus = weightStatus;
	}

	public String getWeightCompanyId() {
		return weightCompanyId;
	}

	public void setWeightCompanyId(String weightCompanyId) {
		this.weightCompanyId = weightCompanyId;
	}
}
