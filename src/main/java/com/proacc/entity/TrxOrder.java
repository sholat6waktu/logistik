package com.proacc.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxOrderSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_order")
@IdClass(TrxOrderSerializable.class)
public class TrxOrder {

	@Id
	@Column(name = "trx_order_id")
	private String trxOrderId;
	
	@Column(name = "trx_order_date")
	private LocalDate trxOrderDate;
	
	@Column(name = "m_contact_customer_from")
	private Integer mContactCustomerFrom;
	
	@Column(name = "m_contact_attedance_from")
	private Integer mContactAttedanceFrom;
	
	@Column(name = "m_contact_customer_from_city")
	private Integer mContactCustomerFromCity;
	
	@Column(name = "trx_kota_from")
	private String trxKotaFrom;
	
	@Column(name = "trx_order_pickup_address")
	private String trxOrderPickupAddress;
	
	@Column(name = "m_contact_attedance_pickup")
	private Integer mContactAttedancePickup;
	
	@Column(name = "m_contact_customer_to")
	private Integer mContactCustomerTo;
	
	@Column(name = "m_contact_attedance_to")
	private Integer mContactAttedanceTo;
	
	@Column(name = "m_contact_customer_to_city")
	private Integer mContactCustomerToCity;
	
	@Column(name = "trx_kota_to")
	private String trxKotaTo;
	
	@Column(name = "m_service_id")
	private Integer mServiceId;
	
	@Column(name = "trx_order_sendtype")
	private String trxOrderSendtype;
	
	@Column(name = "m_packing_id")
	private Integer mPackingId;
	
	@Column(name = "trx_order_nostt")
	private String trxOrderNostt;
	
	@Column(name = "trx_order_koli")
	private Integer trxOrderKoli;
	
	@Column(name = "trx_order_estimated_cost")
	private Float trxOrderEstimatedCost;
	
	@Column(name = "m_payment_id")
	private Integer mPaymentId;
	
	@Column(name = "trx_order_kolitype")
	private String trxOrderKolitype;
	
	@Column(name = "trx_order_weight")
	private Float trxOrderWeight;
	
	@Column(name = "m_weight_id")
	private Integer mWeightId;
	
	@Column(name = "trx_order_connocement")
	private String trxOrderConnocement;
	
	@Column(name = "trx_order_receiving_date")
	private LocalDate trxOrderReceivingDate;
	
	@Column(name = "trx_order_instruction")
	private String trxOrderInstruction;
	
	@Column(name = "trx_order_volume")
	private Float trxOrderVolume;
	
	@Column(name = "m_volume_id")
	private Integer mVolumeId;
	
	@Column(name = "trx_invoice_header_id")
	private Integer tmTrxInvoiceId;
	
	@Column(name = "trx_order_complete_date")
	private LocalDate trxOrderCompleteDate;
	
	@Column(name = "trx_order_nettprice")
	private Float trxOrderNettprice;
	
	@Column(name = "trx_order_ordertype")
	private String trxOrderOrdertype;
	
	@Column(name = "trx_order_solution")
	private String trxOrderSolution;
	
	@Column(name = "trx_order_returtype")
	private String trxOrderReturtype;
	
	@Column(name = "trx_order_status")
	private String trxOrderStatus;
	
	@Column(name = "trx_order_created_by")
	private UUID trxOrderCreatedBy;
	
	@Column(name="trx_order_created_at")
	private LocalDateTime trxOrderCreatedAt;
	
	@Id
	@Column(name = "trx_order_company_id")
	private String trxOrderCompanyId;
	
	@Column(name = "manifest_no")
	private String manifestNo;
	
	

	public String getManifestNo() {
		return manifestNo;
	}

	public void setManifestNo(String manifestNo) {
		this.manifestNo = manifestNo;
	}

	public String getTrxOrderId() {
		return trxOrderId;
	}

	public void setTrxOrderId(String trxOrderId) {
		this.trxOrderId = trxOrderId;
	}

	public LocalDate getTrxOrderDate() {
		return trxOrderDate;
	}

	public void setTrxOrderDate(LocalDate trxOrderDate) {
		this.trxOrderDate = trxOrderDate;
	}

	public Integer getmContactCustomerFrom() {
		return mContactCustomerFrom;
	}

	public void setmContactCustomerFrom(Integer mContactCustomerFrom) {
		this.mContactCustomerFrom = mContactCustomerFrom;
	}

	public Integer getmContactAttedanceFrom() {
		return mContactAttedanceFrom;
	}

	public void setmContactAttedanceFrom(Integer mContactAttedanceFrom) {
		this.mContactAttedanceFrom = mContactAttedanceFrom;
	}

	public Integer getmContactCustomerFromCity() {
		return mContactCustomerFromCity;
	}

	public void setmContactCustomerFromCity(Integer mContactCustomerFromCity) {
		this.mContactCustomerFromCity = mContactCustomerFromCity;
	}

	public String getTrxKotaFrom() {
		return trxKotaFrom;
	}

	public void setTrxKotaFrom(String trxKotaFrom) {
		this.trxKotaFrom = trxKotaFrom;
	}

	public String getTrxOrderPickupAddress() {
		return trxOrderPickupAddress;
	}

	public void setTrxOrderPickupAddress(String trxOrderPickupAddress) {
		this.trxOrderPickupAddress = trxOrderPickupAddress;
	}

	public Integer getmContactAttedancePickup() {
		return mContactAttedancePickup;
	}

	public void setmContactAttedancePickup(Integer mContactAttedancePickup) {
		this.mContactAttedancePickup = mContactAttedancePickup;
	}

	public Integer getmContactCustomerTo() {
		return mContactCustomerTo;
	}

	public void setmContactCustomerTo(Integer mContactCustomerTo) {
		this.mContactCustomerTo = mContactCustomerTo;
	}

	public Integer getmContactAttedanceTo() {
		return mContactAttedanceTo;
	}

	public void setmContactAttedanceTo(Integer mContactAttedanceTo) {
		this.mContactAttedanceTo = mContactAttedanceTo;
	}

	public Integer getmContactCustomerToCity() {
		return mContactCustomerToCity;
	}

	public void setmContactCustomerToCity(Integer mContactCustomerToCity) {
		this.mContactCustomerToCity = mContactCustomerToCity;
	}

	public String getTrxKotaTo() {
		return trxKotaTo;
	}

	public void setTrxKotaTo(String trxKotaTo) {
		this.trxKotaTo = trxKotaTo;
	}

	public Integer getmServiceId() {
		return mServiceId;
	}

	public void setmServiceId(Integer mServiceId) {
		this.mServiceId = mServiceId;
	}

	public String getTrxOrderSendtype() {
		return trxOrderSendtype;
	}

	public void setTrxOrderSendtype(String trxOrderSendtype) {
		this.trxOrderSendtype = trxOrderSendtype;
	}

	public Integer getmPackingId() {
		return mPackingId;
	}

	public void setmPackingId(Integer mPackingId) {
		this.mPackingId = mPackingId;
	}

	public String getTrxOrderNostt() {
		return trxOrderNostt;
	}

	public void setTrxOrderNostt(String trxOrderNostt) {
		this.trxOrderNostt = trxOrderNostt;
	}

	public Integer getTrxOrderKoli() {
		return trxOrderKoli;
	}

	public void setTrxOrderKoli(Integer trxOrderKoli) {
		this.trxOrderKoli = trxOrderKoli;
	}

	public Float getTrxOrderEstimatedCost() {
		return trxOrderEstimatedCost;
	}

	public void setTrxOrderEstimatedCost(Float trxOrderEstimatedCost) {
		this.trxOrderEstimatedCost = trxOrderEstimatedCost;
	}

	public Integer getmPaymentId() {
		return mPaymentId;
	}

	public void setmPaymentId(Integer mPaymentId) {
		this.mPaymentId = mPaymentId;
	}

	public String getTrxOrderKolitype() {
		return trxOrderKolitype;
	}

	public void setTrxOrderKolitype(String trxOrderKolitype) {
		this.trxOrderKolitype = trxOrderKolitype;
	}

	public Float getTrxOrderWeight() {
		return trxOrderWeight;
	}

	public void setTrxOrderWeight(Float trxOrderWeight) {
		this.trxOrderWeight = trxOrderWeight;
	}

	public Integer getmWeightId() {
		return mWeightId;
	}

	public void setmWeightId(Integer mWeightId) {
		this.mWeightId = mWeightId;
	}

	public String getTrxOrderConnocement() {
		return trxOrderConnocement;
	}

	public void setTrxOrderConnocement(String trxOrderConnocement) {
		this.trxOrderConnocement = trxOrderConnocement;
	}

	public LocalDate getTrxOrderReceivingDate() {
		return trxOrderReceivingDate;
	}

	public void setTrxOrderReceivingDate(LocalDate trxOrderReceivingDate) {
		this.trxOrderReceivingDate = trxOrderReceivingDate;
	}

	public String getTrxOrderInstruction() {
		return trxOrderInstruction;
	}

	public void setTrxOrderInstruction(String trxOrderInstruction) {
		this.trxOrderInstruction = trxOrderInstruction;
	}

	public Float getTrxOrderVolume() {
		return trxOrderVolume;
	}

	public void setTrxOrderVolume(Float trxOrderVolume) {
		this.trxOrderVolume = trxOrderVolume;
	}

	public Integer getmVolumeId() {
		return mVolumeId;
	}

	public void setmVolumeId(Integer mVolumeId) {
		this.mVolumeId = mVolumeId;
	}

	public Integer getTmTrxInvoiceId() {
		return tmTrxInvoiceId;
	}

	public void setTmTrxInvoiceId(Integer tmTrxInvoiceId) {
		this.tmTrxInvoiceId = tmTrxInvoiceId;
	}

	public LocalDate getTrxOrderCompleteDate() {
		return trxOrderCompleteDate;
	}

	public void setTrxOrderCompleteDate(LocalDate trxOrderCompleteDate) {
		this.trxOrderCompleteDate = trxOrderCompleteDate;
	}

	public Float getTrxOrderNettprice() {
		return trxOrderNettprice;
	}

	public void setTrxOrderNettprice(Float trxOrderNettprice) {
		this.trxOrderNettprice = trxOrderNettprice;
	}

	public String getTrxOrderOrdertype() {
		return trxOrderOrdertype;
	}

	public void setTrxOrderOrdertype(String trxOrderOrdertype) {
		this.trxOrderOrdertype = trxOrderOrdertype;
	}

	public String getTrxOrderSolution() {
		return trxOrderSolution;
	}

	public void setTrxOrderSolution(String trxOrderSolution) {
		this.trxOrderSolution = trxOrderSolution;
	}

	public String getTrxOrderReturtype() {
		return trxOrderReturtype;
	}

	public void setTrxOrderReturtype(String trxOrderReturtype) {
		this.trxOrderReturtype = trxOrderReturtype;
	}

	public String getTrxOrderStatus() {
		return trxOrderStatus;
	}

	public void setTrxOrderStatus(String trxOrderStatus) {
		this.trxOrderStatus = trxOrderStatus;
	}

	public UUID getTrxOrderCreatedBy() {
		return trxOrderCreatedBy;
	}

	public void setTrxOrderCreatedBy(UUID trxOrderCreatedBy) {
		this.trxOrderCreatedBy = trxOrderCreatedBy;
	}

	public LocalDateTime getTrxOrderCreatedAt() {
		return trxOrderCreatedAt;
	}

	public void setTrxOrderCreatedAt(LocalDateTime trxOrderCreatedAt) {
		this.trxOrderCreatedAt = trxOrderCreatedAt;
	}

	public String getTrxOrderCompanyId() {
		return trxOrderCompanyId;
	}

	public void setTrxOrderCompanyId(String trxOrderCompanyId) {
		this.trxOrderCompanyId = trxOrderCompanyId;
	}
}
