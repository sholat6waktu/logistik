package com.proacc.entity;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxInvoiceDetailSerailzable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_invoice_detail")
@IdClass(TrxInvoiceDetailSerailzable.class)
public class TrxInvoiceDetail {

	@Id
	@Column(name = "invoice_header_id")
	private Integer invoiceHeaderId;
	
	@Id
	@Column(name = "invoice_detail_id")
	private Integer invoiceDetailId;

	@Column(name = "order_id")
	private String orderId;
	
	
	
	@Column(name = "status")
	private Boolean status;
	
	
	@Id
	@Column(name = "company_id")
	private String companyId;


	public Integer getInvoiceHeaderId() {
		return invoiceHeaderId;
	}


	public void setInvoiceHeaderId(Integer invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}


	public Integer getInvoiceDetailId() {
		return invoiceDetailId;
	}


	public void setInvoiceDetailId(Integer invoiceDetailId) {
		this.invoiceDetailId = invoiceDetailId;
	}


	public String getOrderId() {
		return orderId;
	}


	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public Boolean getStatus() {
		return status;
	}


	public void setStatus(Boolean status) {
		this.status = status;
	}


	public String getCompanyId() {
		return companyId;
	}


	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
		
}
