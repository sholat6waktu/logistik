package com.proacc.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MDriverSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_driver")
@IdClass(MDriverSerializable.class)
public class MDriver {

	@Id
	@Column(name = "driver_id")
	private Integer driverId;
	
	@Column(name = "m_fleet_id")
	private Integer mFleetId;
	
	@Column(name = "m_contact_driver")
	private UUID mContactDriver;
	
	@Column(name = "driver_name")
	private String driverName;
	
	@Column(name = "m_contact_helper")
	private UUID mContactHelper;
	
	@Column(name = "driver_helper")
	private String driverHelper;
	
	@Column(name = "driver_date")
	private LocalDate driverDate;
	
	@Column(name = "driver_time_from")
	private LocalTime driverTimeFrom;
	
	@Column(name = "driver_time_to")
	private LocalTime driverTimeTo;
	
	@Column(name = "driver_created_by")
	private UUID driverCreatedBy;
	
	@Column(name="driver_created_at")
	private LocalDateTime driverCreatedAt;
	
	@Column(name = "driver_updated_by")
	private UUID driverUpdatedBy;
	
	@Column(name="driver_updated_at")
	private LocalDateTime driverUpdatedAt;
	
	@Column(name = "driver_status")
	private boolean driverStatus;
	
	@Id
	@Column(name = "driver_company_id")
	private String driverCompanyId;
	
	@Column(name = "trx_jalurdriverheader_id")
	private Integer trxJalurdriverheaderId;
	
	@Column(name = "position_cabang_id")
	private Integer positionCabangId;
	
	@Column(name = "position_agen_id")
	private Integer positionAgenId;
	
	@Column(name = "position_agendetail_id")
	private Integer positionAgenDetailId;
	
	@Column(name = "status_condition")
	private Integer statusCondition;
	
	

	public Integer getStatusCondition() {
		return statusCondition;
	}

	public void setStatusCondition(Integer statusCondition) {
		this.statusCondition = statusCondition;
	}

	public Integer getPositionCabangId() {
		return positionCabangId;
	}

	public void setPositionCabangId(Integer positionCabangId) {
		this.positionCabangId = positionCabangId;
	}

	public Integer getPositionAgenId() {
		return positionAgenId;
	}

	public void setPositionAgenId(Integer positionAgenId) {
		this.positionAgenId = positionAgenId;
	}

	public Integer getPositionAgenDetailId() {
		return positionAgenDetailId;
	}

	public void setPositionAgenDetailId(Integer positionAgenDetailId) {
		this.positionAgenDetailId = positionAgenDetailId;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public Integer getmFleetId() {
		return mFleetId;
	}

	public void setmFleetId(Integer mFleetId) {
		this.mFleetId = mFleetId;
	}

	public UUID getmContactDriver() {
		return mContactDriver;
	}

	public void setmContactDriver(UUID mContactDriver) {
		this.mContactDriver = mContactDriver;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public UUID getmContactHelper() {
		return mContactHelper;
	}

	public void setmContactHelper(UUID mContactHelper) {
		this.mContactHelper = mContactHelper;
	}

	public String getDriverHelper() {
		return driverHelper;
	}

	public void setDriverHelper(String driverHelper) {
		this.driverHelper = driverHelper;
	}

	public LocalDate getDriverDate() {
		return driverDate;
	}

	public void setDriverDate(LocalDate driverDate) {
		this.driverDate = driverDate;
	}

	public LocalTime getDriverTimeFrom() {
		return driverTimeFrom;
	}

	public void setDriverTimeFrom(LocalTime driverTimeFrom) {
		this.driverTimeFrom = driverTimeFrom;
	}

	public LocalTime getDriverTimeTo() {
		return driverTimeTo;
	}

	public void setDriverTimeTo(LocalTime driverTimeTo) {
		this.driverTimeTo = driverTimeTo;
	}

	public UUID getDriverCreatedBy() {
		return driverCreatedBy;
	}

	public void setDriverCreatedBy(UUID driverCreatedBy) {
		this.driverCreatedBy = driverCreatedBy;
	}

	public LocalDateTime getDriverCreatedAt() {
		return driverCreatedAt;
	}

	public void setDriverCreatedAt(LocalDateTime driverCreatedAt) {
		this.driverCreatedAt = driverCreatedAt;
	}

	public UUID getDriverUpdatedBy() {
		return driverUpdatedBy;
	}

	public void setDriverUpdatedBy(UUID driverUpdatedBy) {
		this.driverUpdatedBy = driverUpdatedBy;
	}

	public LocalDateTime getDriverUpdatedAt() {
		return driverUpdatedAt;
	}

	public void setDriverUpdatedAt(LocalDateTime driverUpdatedAt) {
		this.driverUpdatedAt = driverUpdatedAt;
	}

	public boolean isDriverStatus() {
		return driverStatus;
	}

	public void setDriverStatus(boolean driverStatus) {
		this.driverStatus = driverStatus;
	}

	public String getDriverCompanyId() {
		return driverCompanyId;
	}

	public void setDriverCompanyId(String driverCompanyId) {
		this.driverCompanyId = driverCompanyId;
	}

	public Integer getTrxJalurdriverheaderId() {
		return trxJalurdriverheaderId;
	}

	public void setTrxJalurdriverheaderId(Integer trxJalurdriverheaderId) {
		this.trxJalurdriverheaderId = trxJalurdriverheaderId;
	}
	
}
