package com.proacc.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MRateHargaSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_rateharga")
@IdClass(MRateHargaSerializable.class)
public class MRateHarga {
	@Id
	@Column(name = "m_rate_id1")
	private Integer mRateId1;
	
	@Id
	@Column(name = "m_rate_id2")
	private Integer mRateId2;
	
	@Id
	@Column(name = "rateharga_id1")
	private Integer ratehargaId1;
	
	@Id
	@Column(name = "rateharga_id2")
	private Integer ratehargaId2;
	
	@Id
	@Column(name = "m_ratecustomer_id")
	private Integer mRatecustomerId;
	
	@Column(name = "rateharga_discount")
	private Float ratehargaDiscount;
	
	@Column(name = "rateharga_nettprice")
	private Float ratehargaNettprice;
	
	@Column(name = "rateharga_startdate")
	private LocalDate ratehargaStartdate;
	
	@Column(name = "rateharga_enddate")
	private LocalDate ratehargaEnddate;
	
	@Column(name = "rateharga_status")
	private boolean ratehargaStatus;
	
	@Id
	@Column(name = "rateharga_company_id")
	private String ratehargaCompanyId;

	public Integer getmRateId1() {
		return mRateId1;
	}

	public void setmRateId1(Integer mRateId1) {
		this.mRateId1 = mRateId1;
	}

	public Integer getmRateId2() {
		return mRateId2;
	}

	public void setmRateId2(Integer mRateId2) {
		this.mRateId2 = mRateId2;
	}

	public Integer getRatehargaId1() {
		return ratehargaId1;
	}

	public void setRatehargaId1(Integer ratehargaId1) {
		this.ratehargaId1 = ratehargaId1;
	}

	public Integer getRatehargaId2() {
		return ratehargaId2;
	}

	public void setRatehargaId2(Integer ratehargaId2) {
		this.ratehargaId2 = ratehargaId2;
	}

	public Integer getmRatecustomerId() {
		return mRatecustomerId;
	}

	public void setmRatecustomerId(Integer mRatecustomerId) {
		this.mRatecustomerId = mRatecustomerId;
	}

	public Float getRatehargaDiscount() {
		return ratehargaDiscount;
	}

	public void setRatehargaDiscount(Float ratehargaDiscount) {
		this.ratehargaDiscount = ratehargaDiscount;
	}

	public Float getRatehargaNettprice() {
		return ratehargaNettprice;
	}

	public void setRatehargaNettprice(Float ratehargaNettprice) {
		this.ratehargaNettprice = ratehargaNettprice;
	}

	public LocalDate getRatehargaStartdate() {
		return ratehargaStartdate;
	}

	public void setRatehargaStartdate(LocalDate ratehargaStartdate) {
		this.ratehargaStartdate = ratehargaStartdate;
	}

	public LocalDate getRatehargaEnddate() {
		return ratehargaEnddate;
	}

	public void setRatehargaEnddate(LocalDate ratehargaEnddate) {
		this.ratehargaEnddate = ratehargaEnddate;
	}

	public boolean isRatehargaStatus() {
		return ratehargaStatus;
	}

	public void setRatehargaStatus(boolean ratehargaStatus) {
		this.ratehargaStatus = ratehargaStatus;
	}

	public String getRatehargaCompanyId() {
		return ratehargaCompanyId;
	}

	public void setRatehargaCompanyId(String ratehargaCompanyId) {
		this.ratehargaCompanyId = ratehargaCompanyId;
	}
}
