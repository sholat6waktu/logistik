package com.proacc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TMConfigSerializable;

@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "tm_config_numbering")
@IdClass(TMConfigSerializable.class)
@NamedQueries({@NamedQuery(name = "TmConfig.findByIdMenuAndCompanyCode", query = "FROM TmConfig e WHERE e.idMenu = ?1 and e.companyCode = ?2")})
public class TmConfig {
  @Id
  @Column(name = "id")
  private int id;
  
  @Column(name = "id_menu")
  private int idMenu;
  
  @Id
  @Column(name = "company_code")
  private int companyCode;
  
  @Column(name = "prefix_name")
  private String prefixName;
  
  @Column(name = "separator")
  private String separator;
  
  public int getId() {
    return this.id;
  }
  
  public void setId(int id) {
    this.id = id;
  }
  
  public int getIdMenu() {
    return this.idMenu;
  }
  
  public void setIdMenu(int idMenu) {
    this.idMenu = idMenu;
  }
  
  public int getCompanyCode() {
    return this.companyCode;
  }
  
  public void setCompanyCode(int companyCode) {
    this.companyCode = companyCode;
  }
  
  public String getPrefixName() {
    return this.prefixName;
  }
  
  public void setPrefixName(String prefixName) {
    this.prefixName = prefixName;
  }
  
  public String getSeparator() {
    return this.separator;
  }
  
  public void setSeparator(String separator) {
    this.separator = separator;
  }
}
