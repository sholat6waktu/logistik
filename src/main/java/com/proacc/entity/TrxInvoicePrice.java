package com.proacc.entity;

import java.sql.Time;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxInvoicePriceSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_invoice_price")
@IdClass(TrxInvoicePriceSerializable.class)
public class TrxInvoicePrice {

	@Id
	@Column(name = "invoice_header_id")
	private Integer invoiceHeaderId;
	
	@Id
	@Column(name = "invoice_detail_transaksi_id")
	private Integer invoiceDetailTransaksiId;

	@Column(name = "nominal")
	private Integer nominal;
	
	
	@Column(name = "status")
	private Boolean status;
	
	@Column(name = "keterangan")
	private String keterangan;
	
	@Column(name = "tanda")
	private String tanda;
	
	
	@Id
	@Column(name = "company_id")
	private String companyId;


	public Integer getInvoiceHeaderId() {
		return invoiceHeaderId;
	}


	public void setInvoiceHeaderId(Integer invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}


	public Integer getInvoiceDetailTransaksiId() {
		return invoiceDetailTransaksiId;
	}


	public void setInvoiceDetailTransaksiId(Integer invoiceDetailTransaksiId) {
		this.invoiceDetailTransaksiId = invoiceDetailTransaksiId;
	}


	public Integer getNominal() {
		return nominal;
	}


	public void setNominal(Integer nominal) {
		this.nominal = nominal;
	}


	public Boolean getStatus() {
		return status;
	}


	public void setStatus(Boolean status) {
		this.status = status;
	}


	public String getKeterangan() {
		return keterangan;
	}


	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}


	public String getTanda() {
		return tanda;
	}


	public void setTanda(String tanda) {
		this.tanda = tanda;
	}


	public String getCompanyId() {
		return companyId;
	}


	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
		
}
