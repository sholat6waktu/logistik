package com.proacc.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxManifestDetailSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_manifest_detail")
@IdClass(TrxManifestDetailSerializable.class)
public class TrxManifestDetail {

	@Id
	@Column(name = "id")
	private Integer id;
	
	
	@Id
	@Column(name = "detail_id")
	private Integer detailId;
	
	@Column(name = "order_id")
	private String orderId;
	
	@Column(name = "cabang_id_from")
	private Integer cabangIdFrom;
	
	@Column(name = "agen_id_from")
	private Integer agenIdFrom;

	@Column(name = "agen_detail_id_from")
	private Integer agenDetailIdFrom;
	
	@Column(name = "cabang_id_to")
	private Integer  cabangIdTo;
	
	@Column(name = "agen_id_to")
	private Integer agenIdTo;

	@Column(name = "agen_detail_id_to")
	private Integer agenDetailIdTo;
	
	@Id
	@Column(name = "company_id")
	private String companyId;
	
	@Column(name = "tanggal")
	private Date tanggal;
	
	

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDetailId() {
		return detailId;
	}

	public void setDetailId(Integer detailId) {
		this.detailId = detailId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getCabangIdFrom() {
		return cabangIdFrom;
	}

	public void setCabangIdFrom(Integer cabangIdFrom) {
		this.cabangIdFrom = cabangIdFrom;
	}

	public Integer getAgenIdFrom() {
		return agenIdFrom;
	}

	public void setAgenIdFrom(Integer agenIdFrom) {
		this.agenIdFrom = agenIdFrom;
	}

	public Integer getAgenDetailIdFrom() {
		return agenDetailIdFrom;
	}

	public void setAgenDetailIdFrom(Integer agenDetailIdFrom) {
		this.agenDetailIdFrom = agenDetailIdFrom;
	}

	public Integer getCabangIdTo() {
		return cabangIdTo;
	}

	public void setCabangIdTo(Integer cabangIdTo) {
		this.cabangIdTo = cabangIdTo;
	}

	public Integer getAgenIdTo() {
		return agenIdTo;
	}

	public void setAgenIdTo(Integer agenIdTo) {
		this.agenIdTo = agenIdTo;
	}

	public Integer getAgenDetailIdTo() {
		return agenDetailIdTo;
	}

	public void setAgenDetailIdTo(Integer agenDetailIdTo) {
		this.agenDetailIdTo = agenDetailIdTo;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

		

}
