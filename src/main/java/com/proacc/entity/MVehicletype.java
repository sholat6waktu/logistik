package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MVehicletypeSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_vehicletype")
@IdClass(MVehicletypeSerializable.class)
public class MVehicletype {

	@Id
	@Column(name = "vehicletype_id")
	private Integer vehicletypeId;
	
	@Column(name = "vehicletype_name")
	private String vehicletypeName;
	
	@Column(name = "vehicletype_type")
	private String vehicletypeType;
	
	@Column(name = "vehicletype_description")
	private String vehicletypeDescription;
	
	@Column(name = "vehicletype_created_by")
	private UUID vehicletypeCreatedBy;
	
	@Column(name="vehicletype_created_at")
	private LocalDateTime vehicletypeCreatedAt;
	
	@Column(name = "vehicletype_updated_by")
	private UUID vehicletypeUpdatedBy;
	
	@Column(name="vehicletype_updated_at")
	private LocalDateTime vehicletypeUpdatedAt;
	
	@Column(name = "vehicletype_status")
	private boolean vehicletypeStatus;
	
	@Id
	@Column(name = "vehicletype_company_id")
	private String vehicletypeCompanyId;

	public Integer getVehicletypeId() {
		return vehicletypeId;
	}

	public void setVehicletypeId(Integer vehicletypeId) {
		this.vehicletypeId = vehicletypeId;
	}

	public String getVehicletypeName() {
		return vehicletypeName;
	}

	public void setVehicletypeName(String vehicletypeName) {
		this.vehicletypeName = vehicletypeName;
	}

	public String getVehicletypeType() {
		return vehicletypeType;
	}

	public void setVehicletypeType(String vehicletypeType) {
		this.vehicletypeType = vehicletypeType;
	}

	public String getVehicletypeDescription() {
		return vehicletypeDescription;
	}

	public void setVehicletypeDescription(String vehicletypeDescription) {
		this.vehicletypeDescription = vehicletypeDescription;
	}

	public UUID getVehicletypeCreatedBy() {
		return vehicletypeCreatedBy;
	}

	public void setVehicletypeCreatedBy(UUID vehicletypeCreatedBy) {
		this.vehicletypeCreatedBy = vehicletypeCreatedBy;
	}

	public LocalDateTime getVehicletypeCreatedAt() {
		return vehicletypeCreatedAt;
	}

	public void setVehicletypeCreatedAt(LocalDateTime vehicletypeCreatedAt) {
		this.vehicletypeCreatedAt = vehicletypeCreatedAt;
	}

	public UUID getVehicletypeUpdatedBy() {
		return vehicletypeUpdatedBy;
	}

	public void setVehicletypeUpdatedBy(UUID vehicletypeUpdatedBy) {
		this.vehicletypeUpdatedBy = vehicletypeUpdatedBy;
	}

	public LocalDateTime getVehicletypeUpdatedAt() {
		return vehicletypeUpdatedAt;
	}

	public void setVehicletypeUpdatedAt(LocalDateTime vehicletypeUpdatedAt) {
		this.vehicletypeUpdatedAt = vehicletypeUpdatedAt;
	}

	public boolean isVehicletypeStatus() {
		return vehicletypeStatus;
	}

	public void setVehicletypeStatus(boolean vehicletypeStatus) {
		this.vehicletypeStatus = vehicletypeStatus;
	}

	public String getVehicletypeCompanyId() {
		return vehicletypeCompanyId;
	}

	public void setVehicletypeCompanyId(String vehicletypeCompanyId) {
		this.vehicletypeCompanyId = vehicletypeCompanyId;
	}
}
