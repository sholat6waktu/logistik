package com.proacc.entity;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.TrxInvoiceSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "trx_invoice_header")
@IdClass(TrxInvoiceSerializable.class)
public class TrxInvoiceHeader {

	@Id
	@Column(name = "invoice_header_id")
	private Integer invoiceHeaderId;
	
	@Column(name = "invoice_nomor")
	private String invoiceNomor;

	@Column(name = "customer_name")
	private Integer customerName;
	
	@Column(name = "date_tanggal")
	private LocalDate dateTanggal;
	
	
	
	@Column(name = "tax_percent")
	private Integer taxPercent;
	
	@Column(name = "discount")
	private Integer discount;
	
	@Column(name = "nett_price")
	private Integer nettPrice;
	
	@Column(name = "status")
	private Boolean status;
	
	@Column(name = "created_at")
	private LocalDateTime createdAt;
	
	@Column(name = "created_by")
	private UUID createdBy;
	
	@Column(name = "updated_at")
	private LocalDateTime updatedAt;
	
	@Column(name = "updated_by")
	private UUID updatedBy;
	
	@Id
	@Column(name = "company_id")
	private String companyId;
	
	@Column(name = "gross_price")
	private Integer grossPrice;
	
	@Column(name = "status_draft")
	private Integer statusDraft;
	
	@Column(name = "status_invoice")
	private Integer statusInvoice;
	
	@Column(name = "tax_nominal")
	private Integer taxNominal;
	
	@Column(name = "bank_name")
	private String bankName;
	
	@Column(name = "bank_account")
	private String bankAccount;
	
	@Column(name = "account_name")
	private String accountName;
	
	@Column(name = "name_signature")
	private String nameSignature;
	
	@Column(name = "position_signature")
	private String positionSignature;
	
	@Column(name = "text_signature")
	private String textSignature;

	public Integer getInvoiceHeaderId() {
		return invoiceHeaderId;
	}
	
	

	public String getBankName() {
		return bankName;
	}



	public void setBankName(String bankName) {
		this.bankName = bankName;
	}



	public String getBankAccount() {
		return bankAccount;
	}



	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}



	public String getAccountName() {
		return accountName;
	}



	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}



	public String getNameSignature() {
		return nameSignature;
	}



	public void setNameSignature(String nameSignature) {
		this.nameSignature = nameSignature;
	}



	public String getPositionSignature() {
		return positionSignature;
	}



	public void setPositionSignature(String positionSignature) {
		this.positionSignature = positionSignature;
	}



	public String getTextSignature() {
		return textSignature;
	}



	public void setTextSignature(String textSignature) {
		this.textSignature = textSignature;
	}



	public void setInvoiceHeaderId(Integer invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}

	public String getInvoiceNomor() {
		return invoiceNomor;
	}

	public void setInvoiceNomor(String invoiceNomor) {
		this.invoiceNomor = invoiceNomor;
	}

	

	public Integer getCustomerName() {
		return customerName;
	}

	public void setCustomerName(Integer customerName) {
		this.customerName = customerName;
	}

	

	public LocalDate getDateTanggal() {
		return dateTanggal;
	}

	public void setDateTanggal(LocalDate dateTanggal) {
		this.dateTanggal = dateTanggal;
	}

	public Integer getTaxPercent() {
		return taxPercent;
	}

	public void setTaxPercent(Integer taxPercent) {
		this.taxPercent = taxPercent;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Integer getNettPrice() {
		return nettPrice;
	}

	public void setNettPrice(Integer nettPrice) {
		this.nettPrice = nettPrice;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public UUID getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UUID createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public UUID getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(UUID updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Integer getGrossPrice() {
		return grossPrice;
	}

	public void setGrossPrice(Integer grossPrice) {
		this.grossPrice = grossPrice;
	}

	public Integer getStatusDraft() {
		return statusDraft;
	}

	public void setStatusDraft(Integer statusDraft) {
		this.statusDraft = statusDraft;
	}

	public Integer getStatusInvoice() {
		return statusInvoice;
	}

	public void setStatusInvoice(Integer statusInvoice) {
		this.statusInvoice = statusInvoice;
	}

	public Integer getTaxNominal() {
		return taxNominal;
	}

	public void setTaxNominal(Integer taxNominal) {
		this.taxNominal = taxNominal;
	}

	

	

	
}
