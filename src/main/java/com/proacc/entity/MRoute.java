package com.proacc.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MRouteSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_route")
@IdClass(MRouteSerializable.class)
public class MRoute {

	@Id
	@Column(name = "route_id")
	private Integer routeId;
	
	@Column(name = "route_name")
	private String routeName;
	
	@Column(name = "route_from_date")
	private LocalDate routeFromDate;
	
	@Column(name = "route_to_date")
	private LocalDate routeToDate;
	
	@Column(name = "route_status")
	private Boolean routeStatus;
	
	@Id
	@Column(name = "route_company_id")
	private String routeCompanyId;
	
	@Column(name = "route_created_by")
	private UUID routeCreatedBy;
	
	@Column(name = "route_created_at")
	private LocalDateTime routeCreatedAt;

	@Column(name = "route_updated_by")
	private UUID routeUpdatedBy;
	
	@Column(name = "route_updated_at")
	private LocalDateTime routeUpdatedAt;

	public Integer getRouteId() {
		return routeId;
	}

	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public LocalDate getRouteFromDate() {
		return routeFromDate;
	}

	public void setRouteFromDate(LocalDate routeFromDate) {
		this.routeFromDate = routeFromDate;
	}

	public LocalDate getRouteToDate() {
		return routeToDate;
	}

	public void setRouteToDate(LocalDate routeToDate) {
		this.routeToDate = routeToDate;
	}

	public Boolean getRouteStatus() {
		return routeStatus;
	}

	public void setRouteStatus(Boolean routeStatus) {
		this.routeStatus = routeStatus;
	}

	public String getRouteCompanyId() {
		return routeCompanyId;
	}

	public void setRouteCompanyId(String routeCompanyId) {
		this.routeCompanyId = routeCompanyId;
	}

	public UUID getRouteCreatedBy() {
		return routeCreatedBy;
	}

	public void setRouteCreatedBy(UUID routeCreatedBy) {
		this.routeCreatedBy = routeCreatedBy;
	}

	public LocalDateTime getRouteCreatedAt() {
		return routeCreatedAt;
	}

	public void setRouteCreatedAt(LocalDateTime routeCreatedAt) {
		this.routeCreatedAt = routeCreatedAt;
	}

	public UUID getRouteUpdatedBy() {
		return routeUpdatedBy;
	}

	public void setRouteUpdatedBy(UUID routeUpdatedBy) {
		this.routeUpdatedBy = routeUpdatedBy;
	}

	public LocalDateTime getRouteUpdatedAt() {
		return routeUpdatedAt;
	}

	public void setRouteUpdatedAt(LocalDateTime routeUpdatedAt) {
		this.routeUpdatedAt = routeUpdatedAt;
	}

	
	
	
}