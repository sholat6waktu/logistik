package com.proacc.entity;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MPackingSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_packing")
@IdClass(MPackingSerializable.class)
public class MPacking {
	@Id
	@Column(name = "packing_id")
	private Integer packingId;
	
	@Column(name = "packing_name")
	private String packingName;
	
	@Column(name = "packing_description")
	private String packingDescription;
	
	@Column(name = "packing_created_by")
	private UUID packingCreatedBy;
	
	@Column(name="packing_created_at")
	private LocalDateTime packingCreatedAt;
	
	@Column(name = "packing_updated_by")
	private UUID packingUpdatedBy;
	
	@Column(name="packing_updated_at")
	private LocalDateTime packingUpdatedAt;
	
	@Column(name = "packing_status")
	private boolean packingStatus;
	
	@Id
	@Column(name = "packing_company_id")
	private String packingCompanyId;

	public Integer getPackingId() {
		return packingId;
	}

	public void setPackingId(Integer packingId) {
		this.packingId = packingId;
	}

	public String getPackingName() {
		return packingName;
	}

	public void setPackingName(String packingName) {
		this.packingName = packingName;
	}

	public String getPackingDescription() {
		return packingDescription;
	}

	public void setPackingDescription(String packingDescription) {
		this.packingDescription = packingDescription;
	}

	public UUID getPackingCreatedBy() {
		return packingCreatedBy;
	}

	public void setPackingCreatedBy(UUID packingCreatedBy) {
		this.packingCreatedBy = packingCreatedBy;
	}

	public LocalDateTime getPackingCreatedAt() {
		return packingCreatedAt;
	}

	public void setPackingCreatedAt(LocalDateTime packingCreatedAt) {
		this.packingCreatedAt = packingCreatedAt;
	}

	public UUID getPackingUpdatedBy() {
		return packingUpdatedBy;
	}

	public void setPackingUpdatedBy(UUID packingUpdatedBy) {
		this.packingUpdatedBy = packingUpdatedBy;
	}

	public LocalDateTime getPackingUpdatedAt() {
		return packingUpdatedAt;
	}

	public void setPackingUpdatedAt(LocalDateTime packingUpdatedAt) {
		this.packingUpdatedAt = packingUpdatedAt;
	}

	public boolean isPackingStatus() {
		return packingStatus;
	}

	public void setPackingStatus(boolean packingStatus) {
		this.packingStatus = packingStatus;
	}

	public String getPackingCompanyId() {
		return packingCompanyId;
	}

	public void setPackingCompanyId(String packingCompanyId) {
		this.packingCompanyId = packingCompanyId;
	}
}
