package com.proacc.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.proacc.serializable.MShareDriverSerializable;

@Entity
@DynamicUpdate
@DynamicInsert
@Table(name = "m_share_driver")
@IdClass(MShareDriverSerializable.class)
public class MShareDriver {
	@Id
	@Column(name = "id")
	private Integer id;
	
	@Id
	@Column(name = "m_driver_id")
	private Integer mDriverId;
	
	@Column(name = "no")
	private String no;
	
	@Column(name = "m_cabang_id")
	private Integer mCabangId;
	
	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	@Column(name = "m_agendetail_id")
	private Integer mAgenDetailId;
	
	@Column(name = "start_date")
	private LocalDate startDate;
	
	@Column(name = "end_date")
	private LocalDate endDate;
	
	@Column(name = "status")
	private boolean status;
	
	@Id
	@Column(name = "company_id")
	private String companyId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(Integer mDriverId) {
		this.mDriverId = mDriverId;
	}

	public Integer getmCabangId() {
		return mCabangId;
	}

	public void setmCabangId(Integer mCabangId) {
		this.mCabangId = mCabangId;
	}

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getmAgenDetailId() {
		return mAgenDetailId;
	}

	public void setmAgenDetailId(Integer mAgenDetailId) {
		this.mAgenDetailId = mAgenDetailId;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	
}