package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MRequestFleetDetail;
import com.proacc.serializable.MRequestFleetDetailSerializable;

public interface MRequestFleetDetailRepository extends JpaRepository<MRequestFleetDetail, MRequestFleetDetailSerializable>{

}
