package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MRouteJalurDetail;
import com.proacc.serializable.MRouteJalurDetailSerializable;

public interface MRouteJalurDetailRepository extends JpaRepository<MRouteJalurDetail, MRouteJalurDetailSerializable>{

}
