package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MRoute;
import com.proacc.serializable.MRouteSerializable;

public interface MRouteRepository extends JpaRepository<MRoute, MRouteSerializable>{

}
