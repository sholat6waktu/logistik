package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MRouteJalur;
import com.proacc.serializable.MRouteJalurSerializable;

public interface MRouteJalurRepository extends JpaRepository<MRouteJalur, MRouteJalurSerializable>{

}
