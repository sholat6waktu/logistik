package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxDriverschedule;
import com.proacc.serializable.TrxDriverscheduleSerializable;

public interface TrxDriverscheduleRepository extends JpaRepository<TrxDriverschedule, TrxDriverscheduleSerializable>{

}
