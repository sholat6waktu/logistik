package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxJalurDriver;
import com.proacc.serializable.TrxJalurDriverSerializable;

public interface TrxJalurDriverRepository extends JpaRepository<TrxJalurDriver, TrxJalurDriverSerializable>{

}
