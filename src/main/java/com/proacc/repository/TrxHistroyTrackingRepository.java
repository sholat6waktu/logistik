package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxHistroyTracking;
import com.proacc.serializable.TrxHistroyTrackingSerializable;

public interface TrxHistroyTrackingRepository extends JpaRepository<TrxHistroyTracking, TrxHistroyTrackingSerializable>{

}
