package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MAgendetail;
import com.proacc.serializable.MAgendetailSerializable;

public interface MAgendetailRepository extends JpaRepository<MAgendetail, MAgendetailSerializable>{

}
