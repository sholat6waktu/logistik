package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MCabang;
import com.proacc.serializable.MCabangSerializable;

public interface MCabangRepository extends JpaRepository<MCabang, MCabangSerializable>{

}
