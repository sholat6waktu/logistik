package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MRequestFleet;
import com.proacc.serializable.MRequestFleetSerializable;

public interface MRequestFleetRepository extends JpaRepository<MRequestFleet, MRequestFleetSerializable>{

}
