package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MRate;
import com.proacc.serializable.MRateSerializable;

public interface MRateRepository extends JpaRepository<MRate, MRateSerializable>{

}
