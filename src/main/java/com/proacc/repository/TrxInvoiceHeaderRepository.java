package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxInvoiceHeader;
import com.proacc.serializable.TrxInvoiceSerializable;

public interface TrxInvoiceHeaderRepository extends JpaRepository<TrxInvoiceHeader, TrxInvoiceSerializable>{

}
