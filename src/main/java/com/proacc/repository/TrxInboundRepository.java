package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxInbound;
import com.proacc.serializable.TrxInboundSerializable;

public interface TrxInboundRepository extends JpaRepository<TrxInbound, TrxInboundSerializable>{

}
