package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxDeliveryorder;
import com.proacc.serializable.TrxDeliveryorderSerializable;

public interface TrxDeliveryorderRepository extends JpaRepository<TrxDeliveryorder, TrxDeliveryorderSerializable>{

}
