package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MRateHarga;
import com.proacc.serializable.MRateHargaSerializable;

public interface MRateHargaRepository extends JpaRepository<MRateHarga, MRateHargaSerializable>{

}
