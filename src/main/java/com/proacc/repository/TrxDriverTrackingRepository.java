package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxDriverTracking;
import com.proacc.serializable.TrxDriverTrackingSerializable;

public interface TrxDriverTrackingRepository extends JpaRepository<TrxDriverTracking, TrxDriverTrackingSerializable>{

}
