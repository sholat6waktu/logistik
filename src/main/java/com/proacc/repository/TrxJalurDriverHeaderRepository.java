package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxJalurDriverHeader;
import com.proacc.serializable.TrxJalurDriverHeaderSerializable;

public interface TrxJalurDriverHeaderRepository extends JpaRepository<TrxJalurDriverHeader, TrxJalurDriverHeaderSerializable>{

}
