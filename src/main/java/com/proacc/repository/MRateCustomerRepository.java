package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MRateCustomer;
import com.proacc.serializable.MRateCustomerSerializable;

public interface MRateCustomerRepository extends JpaRepository<MRateCustomer, MRateCustomerSerializable>{

}
