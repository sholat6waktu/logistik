package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxOutbound;
import com.proacc.serializable.TrxOutboundSerializable;

public interface TrxOutboundRepository extends JpaRepository<TrxOutbound, TrxOutboundSerializable>{

}
