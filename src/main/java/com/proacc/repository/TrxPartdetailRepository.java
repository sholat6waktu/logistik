package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxPartdetail;
import com.proacc.serializable.TrxPartdetailSerializable;

public interface TrxPartdetailRepository extends JpaRepository<TrxPartdetail, TrxPartdetailSerializable>{

}
