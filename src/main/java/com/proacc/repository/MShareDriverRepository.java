package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MShareDriver;
import com.proacc.serializable.MShareDriverSerializable;

public interface MShareDriverRepository extends JpaRepository<MShareDriver, MShareDriverSerializable>{

}
