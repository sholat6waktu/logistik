package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxManifest;
import com.proacc.serializable.TrxManifestSerializable;

public interface TrxManifestRepository extends JpaRepository<TrxManifest, TrxManifestSerializable>{

}
