package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MagenCoverArea;
import com.proacc.serializable.MagenCoverAreaSerializable;

public interface MagenDetailCoverAreaRepository extends JpaRepository<MagenCoverArea, MagenCoverAreaSerializable>{

}
