package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxInvoicePrice;
import com.proacc.serializable.TrxInvoicePriceSerializable;

public interface TrxInvoicePriceRepository extends JpaRepository<TrxInvoicePrice, TrxInvoicePriceSerializable>{

}
