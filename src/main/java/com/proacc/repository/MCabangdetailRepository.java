package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MCabangdetail;
import com.proacc.serializable.MCabangdetailSerializable;

public interface MCabangdetailRepository extends JpaRepository<MCabangdetail, MCabangdetailSerializable>{

}
