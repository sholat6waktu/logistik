package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxTracking;
import com.proacc.serializable.TrxTrackingSerializable;

public interface TrxTrackingRepository extends JpaRepository<TrxTracking, TrxTrackingSerializable>{

}
