package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxNumbergen;
import com.proacc.serializable.TrxNumbergenSerializable;

public interface TrxNumbergenRepository extends JpaRepository<TrxNumbergen, TrxNumbergenSerializable>{

}
