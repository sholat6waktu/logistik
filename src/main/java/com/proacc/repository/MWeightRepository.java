package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MWeight;
import com.proacc.serializable.MWeightSerializable;

public interface MWeightRepository extends JpaRepository<MWeight, MWeightSerializable>{

}
