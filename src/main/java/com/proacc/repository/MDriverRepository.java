package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MDriver;
import com.proacc.serializable.MDriverSerializable;

public interface MDriverRepository extends JpaRepository<MDriver, MDriverSerializable>{

}
