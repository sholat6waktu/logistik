package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MService;
import com.proacc.serializable.MServiceSerializable;

public interface MServiceRepository extends JpaRepository<MService, MServiceSerializable>{

}
