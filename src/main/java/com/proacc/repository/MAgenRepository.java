package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MAgen;
import com.proacc.serializable.MAgenSerializable;

public interface MAgenRepository extends JpaRepository<MAgen, MAgenSerializable>{
}
