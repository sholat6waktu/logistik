package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxOrder;
import com.proacc.serializable.TrxOrderSerializable;

public interface TrxOrderRepository extends JpaRepository<TrxOrder, TrxOrderSerializable>{

}
