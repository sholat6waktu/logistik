package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxManifestDetail;
import com.proacc.serializable.TrxManifestDetailSerializable;

public interface TrxManifestDetailRepository extends JpaRepository<TrxManifestDetail, TrxManifestDetailSerializable>{

}
