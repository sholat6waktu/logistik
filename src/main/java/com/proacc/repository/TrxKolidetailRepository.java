package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxKolidetail;
import com.proacc.serializable.TrxKolidetailSerializable;

public interface TrxKolidetailRepository extends JpaRepository<TrxKolidetail, TrxKolidetailSerializable>{

}
