package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MFleet;
import com.proacc.serializable.MFleetSerializable;

public interface MFleetRepository extends JpaRepository<MFleet, MFleetSerializable>{

}
