package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxInvoiceDetail;
import com.proacc.serializable.TrxInvoiceDetailSerailzable;

public interface TrxInvoiceDetailRepository extends JpaRepository<TrxInvoiceDetail, TrxInvoiceDetailSerailzable>{

}
