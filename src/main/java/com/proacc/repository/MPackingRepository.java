package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MPacking;
import com.proacc.serializable.MPackingSerializable;

public interface MPackingRepository extends JpaRepository<MPacking, MPackingSerializable>{

}
