package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MVehicletype;
import com.proacc.serializable.MVehicletypeSerializable;

public interface MVehicletypeRepository extends JpaRepository<MVehicletype, MVehicletypeSerializable>{

}
