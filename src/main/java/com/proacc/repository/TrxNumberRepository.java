package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.TrxNumber;
import com.proacc.serializable.TrxNumberSerializable;

public interface TrxNumberRepository extends JpaRepository<TrxNumber, TrxNumberSerializable>{

}
