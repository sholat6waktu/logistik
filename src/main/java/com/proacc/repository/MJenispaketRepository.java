package com.proacc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proacc.entity.MJenispaket;
import com.proacc.serializable.MJenispaketSerializable;

public interface MJenispaketRepository extends JpaRepository<MJenispaket, MJenispaketSerializable>{

}
