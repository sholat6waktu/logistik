package com.proacc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.service.MLwhService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MLwhController {

	private static final Logger logger = LoggerFactory.getLogger(MLwhController.class);
	
	@Autowired
	MLwhService mLwhService;
	
	@PostMapping("/getAllLwh/{paramAktif}")
	public String getAllLwh(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> lwhs = mLwhService.getallLwh(paramAktif.toString().toLowerCase(), dataRequest.getString("lwhCompanyId"));
			lwhs.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("lwhId", column[0]);
				data.put("lwhName", column[1] == null ? "" : column[1].toString());
				data.put("lwhTocm", column[2] == null ? "" : column[2].toString());
				data.put("lwhDescription", column[3] == null ? "" : column[3].toString());
				data.put("lwhCreatedBy", column[4].toString());
				data.put("lwhCreatedAt", column[5].toString());
				data.put("lwhUpdatedBy", column[6] == null ? "" : column[6].toString());
				data.put("lwhUpdatedAt", column[7] == null ? "" : column[7].toString());
				data.put("lwhStatus", column[8].toString());
				data.put("lwhCompanyId", column[9]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data lwh berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data lwh gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getLwh/{paramAktif}")
	public String getLwh(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> lwhs = mLwhService.getLwh(paramAktif.toString().toLowerCase(), dataRequest.getInt("lwhId"), dataRequest.getString("lwhCompanyId"));
			lwhs.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("lwhId", column[0]);
				data.put("lwhName", column[1] == null ? "" : column[1].toString());
				data.put("lwhTocm", column[2] == null ? "" : column[2].toString());
				data.put("lwhDescription", column[3] == null ? "" : column[3].toString());
				data.put("lwhCreatedBy", column[4].toString());
				data.put("lwhCreatedAt", column[5].toString());
				data.put("lwhUpdatedBy", column[6] == null ? "" : column[6].toString());
				data.put("lwhUpdatedAt", column[7] == null ? "" : column[7].toString());
				data.put("lwhStatus", column[8].toString());
				data.put("lwhCompanyId", column[9]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data lwh berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data lwh gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
