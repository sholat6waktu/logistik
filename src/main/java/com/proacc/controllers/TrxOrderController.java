package com.proacc.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MCabang;
import com.proacc.entity.TrxHistroyTracking;
import com.proacc.entity.TrxKolidetail;
import com.proacc.entity.TrxNumber;
import com.proacc.entity.TrxNumbergen;
import com.proacc.entity.TrxOrder;
import com.proacc.entity.TrxPartdetail;
import com.proacc.entity.TrxTracking;
import com.proacc.helper.Helper;
import com.proacc.service.AllOrderService;
import com.proacc.service.MAgendetailService;
import com.proacc.service.MCabangdetailService;
import com.proacc.service.TrxDriverscheduleService;
import com.proacc.service.TrxHistroyTrackingService;
import com.proacc.service.TrxKolidetailService;
import com.proacc.service.TrxNumberService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxPartdetailService;
import com.proacc.service.TrxTrackingService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TrxOrderController {
	private static final Logger logger = LoggerFactory.getLogger(TrxOrderController.class);
	
	@Autowired
	TrxOrderService trxOrderService;
	@Autowired
	TrxNumberService trxNumberService;
	@Autowired
	TrxKolidetailService trxKolidetailService;
	@Autowired
	TrxPartdetailService trxPartdetailService;
	@Autowired
	TrxTrackingService trxTrackingService;
	@Autowired
	AllOrderService allOrderService;
	@Autowired
	TrxDriverscheduleService trxDriverscheduleService;
	@Autowired
	TrxHistroyTrackingService TrxHistroyTrackingService;
	
	@Autowired
	MAgendetailService MAgendetailService;
	@Autowired
	MCabangdetailService MCabangdetailService;
	
	@PostMapping("/getallOrder/{paramAktif}")
	public String getAllOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> orders = trxOrderService.getallOrder(paramAktif.toString().toLowerCase(), dataRequest.getString("orderCompanyId"), dataRequest.getString("orderStatus"), UUID.fromString(Helper.getUserId(header)));
			orders.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("orderId", column[0]);
				data.put("orderdate", column[1].toString());
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("customer", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("customerid", column[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("pickupAddress", "");
							data.put("customerid", "");
						}
					}
				}
				else
				{
					data.put("pickupAddress", "");
					data.put("customerid", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("contactToName", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactTo", column[4]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("receipantAddress", "");
							data.put("contactTo", "");
						}
					}
				}
				else
				{
					data.put("receipantAddress", "");
					data.put("contactTo", "");
				}
				data.put("service", column[5].toString());
				data.put("koli", column[6]);
				data.put("status", column[7].toString());
				data.put("companyId", column[8].toString());
				data.put("berat", column[9].toString());
				data.put("beratNominal", column[10]);
				data.put("beratSatuan", column[11]);
				data.put("beratSatuanLabel", column[12].toString());
				data.put("volume", column[13].toString());
				data.put("volumeNominal", column[14]);
				data.put("volumeSatuan", column[15]);
				data.put("volumenSatuanLabel", column[16].toString());
				data.put("trxTrackingId", column[17]);
				data.put("trxTrackingFromId", column[18]);
				data.put("trxTrackingToId", column[19]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getOrderArray/{paramAktif}")
	public String getOrderArray(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
	    	JSONArray orderArray = dataRequest.getJSONArray("orderArray");
	    	if(orderArray.size() > 0)
	    	{
	    		for(int a = 0; a < orderArray.size(); a++)
	    		{
	    			List<Object[]> orders = trxOrderService.getOrder(paramAktif.toString().toLowerCase(), orderArray.getJSONObject(a).getString("orderId"), dataRequest.getString("orderCompanyId"));
	    			orders.stream().forEach(column -> {
	    				List<JSONObject> dataskolidetails = new ArrayList<>();
	    				JSONObject data = new JSONObject();
	    				data.put("orderId", column[0]);
	    				data.put("orderdate", column[1].toString());
	    				if(column[2] != null)
	    				{
	    					for(int i = 0; i < datacustomer.size(); i++)
	    					{
	    						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
	    						{
	    							data.put("contactNameFrom", datacustomer.getJSONObject(i).getString("contact_name"));
	    							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_address"));
	    							data.put("contactFrom", column[2]);
	    							if(column[3] != null)
	    							{
	    								for(int j = 0; j < datacustomer.getJSONObject(i).getJSONArray("contact_persons").size(); j++)
	    								{
	    									if(column[3].equals(datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getInt("id")))
	    									{
	    										data.put("attendanceNameFrom", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_name"));
	    										data.put("attendancephoneFrom", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_phone"));
	    										data.put("attendaceFrom", column[3]);
	    										break;
	    									}
	    									else
	    									{
	    										data.put("attendanceNameFrom", "");
	    										data.put("attendancephoneFrom", "");
	    										data.put("attendaceFrom", "");
	    									}
	    								}
	    							}
	    							else
	    							{
	    								data.put("attendanceNameFrom", "");
	    								data.put("attendancephoneFrom", "");
	    								data.put("attendaceFrom", "");
	    							}
	    							if(column[38] != null)
	    							{
	    								for(int j = 0; j < datacustomer.getJSONObject(i).getJSONArray("contact_persons").size(); j++)
	    								{
	    									if(column[38].equals(datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getInt("id")))
	    									{
	    										data.put("pickupAddresspickup", column[5].toString());
	    										data.put("attendanceNamePickup", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_name"));
	    										data.put("attendancephonePickup", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_phone"));
	    										data.put("attendacePickup", column[38]);
	    										break;
	    									}
	    									else
	    									{
	    										data.put("pickupAddresspickup", "");
	    										data.put("attendanceNamePickup", "");
	    										data.put("attendancephonePickup", "");
	    									}
	    								}
	    							}
	    							else
	    							{
	    								data.put("pickupAddresspickup", "");
	    								data.put("attendanceNamePickup", "");
	    								data.put("attendancephonePickup", "");
	    								data.put("attendacePickup", "");
	    							}
	    							break;
	    						}
	    						else if(datacustomer.size() == i + 1)
	    						{
	    							data.put("pickupAddresspickup", "");
	    							data.put("contactNameFrom", "");
	    							data.put("pickupAddress", "");
	    							data.put("contactFrom", "");
	    							data.put("attendanceNameFrom", "");
	    							data.put("attendancephoneFrom", "");
	    							data.put("attendaceFrom", "");
	    							data.put("attendanceNamePickup", "");
	    							data.put("attendancephonePickup", "");
	    							data.put("attendacePickup", "");
	    						}
	    					}
	    				}
	    				else
	    				{
	    					data.put("pickupAddresspickup", "");
	    					data.put("contactNameFrom", "");
	    					data.put("pickupAddress", "");
	    					data.put("contactFrom", "");
	    					data.put("attendanceNameFrom", "");
	    					data.put("attendancephoneFrom", "");
	    					data.put("attendaceFrom", "");
	    					data.put("attendanceNamePickup", "");
	    					data.put("attendancephonePickup", "");
	    					data.put("attendacePickup", "");
	    				}
	    				if(column[6] != null)
	    				{
	    					for(int i = 0; i < datacustomer.size(); i++)
	    					{
	    						if(column[6].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
	    						{
	    							data.put("contactNameTo", datacustomer.getJSONObject(i).getString("contact_name"));
	    							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_address"));
	    							data.put("contactTo", column[6]);
	    							if(column[7] != null)
	    							{
	    								for(int j = 0; j < datacustomer.getJSONObject(i).getJSONArray("contact_persons").size(); j++)
	    								{
	    									if(column[7].equals(datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getInt("id")))
	    									{
	    										data.put("attendanceNameTo", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_name"));
	    										data.put("attendancephoneTo", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_phone"));
	    										data.put("attendaceTo", column[7]);
	    										break;
	    									}
	    									else
	    									{
	    										data.put("attendanceNameTo", "");
	    										data.put("attendancephoneTo", "");
	    										data.put("attendaceTo", "");
	    									}
	    								}
	    							}
	    							else
	    							{
	    								data.put("attendanceNameTo", "");
	    								data.put("attendancephoneTo", "");
	    								data.put("attendaceTo", "");
	    							}
	    							break;
	    						}
	    						else if(datacustomer.size() == i + 1)
	    						{
	    							data.put("contactNameTo", "");
	    							data.put("receipantAddress", "");
	    							data.put("contactTo", "");
	    							data.put("attendanceNameTo", "");
	    							data.put("attendancephoneTo", "");
	    							data.put("attendaceTo", "");
	    						}
	    					}
	    				}
	    				else
	    				{
	    					data.put("contactNameTo", "");
	    					data.put("receipantAddress", "");
	    					data.put("contactTo", "");
	    					data.put("attendanceNameTo", "");
	    					data.put("attendancephoneTo", "");
	    					data.put("attendaceTo", "");
	    				}
	    				data.put("service", column[9] == null ? "" : column[9]);
	    				data.put("serviceName", column[36] == null ? "": column[36].toString());
	    				data.put("vehicle", column[10] == null ? "" : column[10].toString());
	    				data.put("packing", column[11] == null ? "" : column[11]);
	    				data.put("packingName", column[37] == null ? "": column[37].toString());
	    				data.put("no STT", column[12] == null ? "" : column[12]);
	    				data.put("koli", column[13] == null ? "" : column[13]);
	    				data.put("estimatedCost", column[14] == null ? "" : column[14]);
	    				data.put("mPayment", column[15] == null ? "" : column[15]);
	    				data.put("kolitype", column[16] == null ? "" : column[16]);
	    				data.put("weight", column[17] == null ? "" : column[17]);
	    				data.put("weighttype", column[18] == null ? "" : column[18]);
	    				data.put("connocement", column[19] == null ? "" : column[19]);
	    				data.put("receivingdate", column[20] == null ? "" : column[20].toString());
	    				data.put("instruction", column[21] == null ? "" : column[21]);
	    				data.put("volume", column[22] == null ? "" : column[22]);
	    				data.put("volumetype", column[23] == null ? "" : column[23]);
	    				data.put("invoice", column[24] == null ? "" : column[24]);
	    				data.put("completedate", column[25] == null ? "" : column[25].toString());
	    				data.put("nettprice", column[26] == null ? "" : column[26]);
	    				data.put("ordertype", column[27] == null ? "" : column[27]);
	    				data.put("solution", column[28] == null ? "" : column[28]);
	    				data.put("returtype", column[29] == null ? "" : column[29]);
	    				data.put("status", column[30] == null ? "": column[30].toString());
	    				data.put("companyId", column[31] == null ? "": column[31].toString());
	    				data.put("createdby", column[32] == null ? "": column[32].toString());
	    				data.put("createdat", column[33] == null ? "": column[33].toString());
	    				data.put("kotafrom", column[34] == null ? "": column[34].toString());
	    				data.put("kotato", column[35] == null ? "": column[35].toString());
	    				List<Object[]> kolis = trxKolidetailService.getKoli(paramAktif.toString().toLowerCase(), column[0].toString(), dataRequest.getString("orderCompanyId"));
	    				kolis.stream().forEach(columnkoli -> {
	    					List<JSONObject> dataspartdetails = new ArrayList<>();
	    					JSONObject datakoli = new JSONObject();
	    					datakoli.put("orderId", columnkoli[0]);
	    					datakoli.put("trxKolidetailId", columnkoli[1]);
	    					datakoli.put("trxKolidetailWeight", columnkoli[2]);
	    					datakoli.put("trxKolidetailHeight", columnkoli[3]);
	    					datakoli.put("trxKolidetailWidth", columnkoli[4]);
	    					datakoli.put("trxKolidetailLength", columnkoli[5]);
	    					datakoli.put("trxKolidetailDeclareditem", columnkoli[6]);
	    					datakoli.put("trxKolidetailStatus", columnkoli[7]);
	    					datakoli.put("trxKolidetailCompanyId", columnkoli[8]);
	    					List<Object[]> parts = trxPartdetailService.getPart(paramAktif.toString().toLowerCase(), column[0].toString(), Integer.parseInt(columnkoli[1].toString()), dataRequest.getString("orderCompanyId"));
	    					parts.stream().forEach(columnpart -> {
	    						JSONObject datapart = new JSONObject();
	    						datapart.put("orderId", columnpart[0]);
	    						datapart.put("tmTrxKolidetailId", columnpart[1]);
	    						datapart.put("trxPartdetailId", columnpart[2]);
	    						datapart.put("trxPartdetailPartnumber", columnpart[3]);
	    						datapart.put("trxPartdetailName", columnpart[10].toString());
	    						datapart.put("trxPartdetailQuantity", columnpart[4]);
	    						datapart.put("weight", columnpart[5]);
	    						datapart.put("trxPartdetailPartReturquantity", columnpart[6] == null ? "" : columnpart[6]);
	    						datapart.put("trxPartdetailPartReturdescription", columnpart[7]== null ? "" : columnpart[7]);
	    						datapart.put("trxPartdetailPartStatus", columnpart[8]);
	    						datapart.put("trxPartdetailPartCompanyId", columnpart[9]);
	    						dataspartdetails.add(datapart);
	    					});
	    					datakoli.put("partdetailArray", dataspartdetails);
	    					dataskolidetails.add(datakoli);
	    				});
	    				data.put("kolidetailArray", dataskolidetails);
	    				datas.add(data);
	    			});
	    		}
	    	}
		response.put("responseCode", "00");
		response.put("responseDesc", "data Cabang berhasil didapat");
		response.put("responseData", datas);
		}
	catch(Exception e)
	{
    	response.put("responseCode", "99");
	    response.put("responseDesc", "data Cabang gagal didapat");
	    response.put("responseError", e.getMessage());
	    logger.info("ERROR #### " + e.getMessage());
	}
	    return response.toString();
	}
	@PostMapping("/getOrder/{paramAktif}")
	public String getOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> orders = trxOrderService.getOrder(paramAktif.toString().toLowerCase(), dataRequest.getString("orderId"), dataRequest.getString("orderCompanyId"));
			orders.stream().forEach(column -> {
				List<JSONObject> dataskolidetails = new ArrayList<>();
				JSONObject data = new JSONObject();
				data.put("orderId", column[0]);
				data.put("orderdate", column[1].toString());
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("contactNameFrom", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactFrom", column[2]);
							if(column[3] != null)
							{
								for(int j = 0; j < datacustomer.getJSONObject(i).getJSONArray("contact_persons").size(); j++)
								{
									if(column[3].equals(datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getInt("id")))
									{
										data.put("attendanceNameFrom", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_name"));
										data.put("attendancephoneFrom", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_phone"));
										data.put("attendaceFrom", column[3]);
										break;
									}
									else
									{
										data.put("attendanceNameFrom", "");
										data.put("attendancephoneFrom", "");
										data.put("attendaceFrom", "");
									}
								}
							}
							else
							{
								data.put("attendanceNameFrom", "");
								data.put("attendancephoneFrom", "");
								data.put("attendaceFrom", "");
							}
							if(column[38] != null)
							{
								for(int j = 0; j < datacustomer.getJSONObject(i).getJSONArray("contact_persons").size(); j++)
								{
									if(column[38].equals(datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getInt("id")))
									{
										data.put("pickupAddresspickup", column[5].toString());
										data.put("attendanceNamePickup", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_name"));
										data.put("attendancephonePickup", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_phone"));
										data.put("attendacePickup", column[38]);
										break;
									}
									else
									{
										data.put("pickupAddresspickup", "");
										data.put("attendanceNamePickup", "");
										data.put("attendancephonePickup", "");
									}
								}
							}
							else
							{
								data.put("pickupAddresspickup", "");
								data.put("attendanceNamePickup", "");
								data.put("attendancephonePickup", "");
								data.put("attendacePickup", "");
							}
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("pickupAddresspickup", "");
							data.put("contactNameFrom", "");
							data.put("pickupAddress", "");
							data.put("contactFrom", "");
							data.put("attendanceNameFrom", "");
							data.put("attendancephoneFrom", "");
							data.put("attendaceFrom", "");
							data.put("attendanceNamePickup", "");
							data.put("attendancephonePickup", "");
							data.put("attendacePickup", "");
						}
					}
				}
				else
				{
					data.put("pickupAddresspickup", "");
					data.put("contactNameFrom", "");
					data.put("pickupAddress", "");
					data.put("contactFrom", "");
					data.put("attendanceNameFrom", "");
					data.put("attendancephoneFrom", "");
					data.put("attendaceFrom", "");
					data.put("attendanceNamePickup", "");
					data.put("attendancephonePickup", "");
					data.put("attendacePickup", "");
				}
				if(column[6] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[6].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("contactNameTo", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactTo", column[6]);
							if(column[7] != null)
							{
								for(int j = 0; j < datacustomer.getJSONObject(i).getJSONArray("contact_persons").size(); j++)
								{
									if(column[7].equals(datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getInt("id")))
									{
										data.put("attendanceNameTo", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_name"));
										data.put("attendancephoneTo", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_phone"));
										data.put("attendaceTo", column[7]);
										break;
									}
									else
									{
										data.put("attendanceNameTo", "");
										data.put("attendancephoneTo", "");
										data.put("attendaceTo", "");
									}
								}
							}
							else
							{
								data.put("attendanceNameTo", "");
								data.put("attendancephoneTo", "");
								data.put("attendaceTo", "");
							}
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("contactNameTo", "");
							data.put("receipantAddress", "");
							data.put("contactTo", "");
							data.put("attendanceNameTo", "");
							data.put("attendancephoneTo", "");
							data.put("attendaceTo", "");
						}
					}
				}
				else
				{
					data.put("contactNameTo", "");
					data.put("receipantAddress", "");
					data.put("contactTo", "");
					data.put("attendanceNameTo", "");
					data.put("attendancephoneTo", "");
					data.put("attendaceTo", "");
				}
				data.put("service", column[9] == null ? "" : column[9]);
				data.put("serviceName", column[36] == null ? "": column[36].toString());
				data.put("vehicle", column[10] == null ? "" : column[10].toString());
				data.put("packing", column[11] == null ? "" : column[11]);
				data.put("packingName", column[37] == null ? "": column[37].toString());
				data.put("no STT", column[12] == null ? "" : column[12]);
				data.put("koli", column[13] == null ? "" : column[13]);
				data.put("estimatedCost", column[14] == null ? "" : column[14]);
				data.put("mPayment", column[15] == null ? "" : column[15]);
				data.put("kolitype", column[16] == null ? "" : column[16]);
				data.put("weight", column[17] == null ? "" : column[17]);
				data.put("weighttype", column[18] == null ? "" : column[18]);
				data.put("connocement", column[19] == null ? "" : column[19]);
				data.put("receivingdate", column[20] == null ? "" : column[20].toString());
				data.put("instruction", column[21] == null ? "" : column[21]);
				data.put("volume", column[22] == null ? "" : column[22]);
				data.put("volumetype", column[23] == null ? "" : column[23]);
				data.put("invoice", column[24] == null ? "" : column[24]);
				data.put("completedate", column[25] == null ? "" : column[25].toString());
				data.put("nettprice", column[26] == null ? "" : column[26]);
				data.put("ordertype", column[27] == null ? "" : column[27]);
				data.put("solution", column[28] == null ? "" : column[28]);
				data.put("returtype", column[29] == null ? "" : column[29]);
				data.put("status", column[30] == null ? "": column[30].toString());
				data.put("companyId", column[31] == null ? "": column[31].toString());
				data.put("createdby", column[32] == null ? "": column[32].toString());
				data.put("createdat", column[33] == null ? "": column[33].toString());
				data.put("kotafrom", column[34] == null ? "": column[34].toString());
				data.put("kotato", column[35] == null ? "": column[35].toString());
				List<Object[]> kolis = trxKolidetailService.getKoli(paramAktif.toString().toLowerCase(), dataRequest.getString("orderId"), dataRequest.getString("orderCompanyId"));
				kolis.stream().forEach(columnkoli -> {
					List<JSONObject> dataspartdetails = new ArrayList<>();
					JSONObject datakoli = new JSONObject();
					datakoli.put("orderId", columnkoli[0]);
					datakoli.put("trxKolidetailId", columnkoli[1]);
					datakoli.put("trxKolidetailWeight", columnkoli[2]);
					datakoli.put("trxKolidetailHeight", columnkoli[3]);
					datakoli.put("trxKolidetailWidth", columnkoli[4]);
					datakoli.put("trxKolidetailLength", columnkoli[5]);
					datakoli.put("trxKolidetailDeclareditem", columnkoli[6]);
					datakoli.put("trxKolidetailStatus", columnkoli[7]);
					datakoli.put("trxKolidetailCompanyId", columnkoli[8]);
					List<Object[]> parts = trxPartdetailService.getPart(paramAktif.toString().toLowerCase(), dataRequest.getString("orderId"), Integer.parseInt(columnkoli[1].toString()), dataRequest.getString("orderCompanyId"));
					parts.stream().forEach(columnpart -> {
						JSONObject datapart = new JSONObject();
						datapart.put("orderId", columnpart[0]);
						datapart.put("tmTrxKolidetailId", columnpart[1]);
						datapart.put("trxPartdetailId", columnpart[2]);
						datapart.put("trxPartdetailPartnumber", columnpart[3]);
						datapart.put("trxPartdetailName", columnpart[10].toString());
						datapart.put("trxPartdetailQuantity", columnpart[4]);
						datapart.put("weight", columnpart[5]);
						datapart.put("trxPartdetailPartReturquantity", columnpart[6] == null ? "" : columnpart[6]);
						datapart.put("trxPartdetailPartReturdescription", columnpart[7]== null ? "" : columnpart[7]);
						datapart.put("trxPartdetailPartStatus", columnpart[8]);
						datapart.put("trxPartdetailPartCompanyId", columnpart[9]);
						dataspartdetails.add(datapart);
					});
					datakoli.put("partdetailArray", dataspartdetails);
					dataskolidetails.add(datakoli);
				});
				data.put("kolidetailArray", dataskolidetails);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/getComplete")
	public String getComplete(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("API === getComplete " );
		logger.info("INPUT === " + dataRequest.toString() );
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> getComplete = trxOrderService.getComplete(dataRequest.getString("companyId"), user_uuid);
			if(getComplete.size() > 0)
			{
				getComplete.stream().forEach(col->{
					JSONObject data = new JSONObject();
					data.put("customerId", col[0] == null ? "" : col[0]);
					data.put("orderId", col[1]  == null ? "" : col[1]);
					data.put("date", col[2]  == null ? "" : col[2].toString());
					data.put("estimatedCost", col[3]  == null ? "" : col[3]);
					data.put("mPaymentId", col[4]  == null ? "" : col[4]);
					data.put("mPaymentId", Integer.parseInt(col[4].toString())  == 1 ? "Tunai" : "Non Tunai");
					data.put("status", col[5]  == null ? "" : col[5]);
					datas.add(data);
				});
			}
			response.put("responseCode", "00");
			response.put("responseDesc", "List Order Success");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "List Order Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/getTrackingdetail/{paramAktif}")
	public String getTrackingdetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
		
		final String url2 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> orders = trxOrderService.getIncomingOrder(paramAktif.toString().toLowerCase(), dataRequest.getString("orderId"), dataRequest.getString("orderCompanyId"));
			orders.stream().forEach(columndetail -> {
				List<JSONObject> datastracking = new ArrayList<>();
				List<JSONObject> datasdriverschedule = new ArrayList<>();
				JSONObject datadetail = new JSONObject();
				datadetail.put("orderId", columndetail[0] == null ? "" : columndetail[0]);
				datadetail.put("orderdate", columndetail[1] == null ? "" : columndetail[1].toString());
				if(columndetail[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(columndetail[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							datadetail.put("contactfromname", datacustomer.getJSONObject(i).getString("contact_name"));
							datadetail.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_address"));
							datadetail.put("phonefrom",  datacustomer.getJSONObject(i).getJSONArray("contact_phones").size() == 0 ? "" : datacustomer.getJSONObject(i).getJSONArray("contact_phones").getJSONObject(0).getString("contact_phone"));
							datadetail.put("contactFrom", columndetail[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							datadetail.put("contactfromname", "");
							datadetail.put("pickupAddress", "");
							datadetail.put("phonefrom", "");
							datadetail.put("contactFrom", "");
						}
					}
				}
				else
				{
					datadetail.put("contactfromname", "");
					datadetail.put("pickupAddress", "");
					datadetail.put("phonefrom", "");
					datadetail.put("contactFrom", "");
				}
				if(columndetail[3] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						
							if(columndetail[3].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
							{
								datadetail.put("contacttoName", datacustomer.getJSONObject(i).getString("contact_name"));
								datadetail.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_address"));
//								datadetail.put("phoneto", datacustomer.getJSONObject(i).getJSONArray("contact_phones").getJSONObject(0).getString("contact_phone"));
								datadetail.put("phoneto",  datacustomer.getJSONObject(i).getJSONArray("contact_phones").size() == 0 ? "" : datacustomer.getJSONObject(i).getJSONArray("contact_phones").getJSONObject(0).getString("contact_phone"));
								
								datadetail.put("contactTo", columndetail[3]);
								break;
							}
							else if(datacustomer.size() == i + 1)
							{
								datadetail.put("contacttoName", "");
								datadetail.put("receipantAddress", "");
								datadetail.put("phoneto", "");
								datadetail.put("contactTo", "");
							}
						
						
					}
				}
				else
				{
					datadetail.put("contacttoName", "");
					datadetail.put("receipantAddress", "");
					datadetail.put("phoneto", "");
					datadetail.put("contactTo", "");
				}
//				datadetail.put("contactTo", columndetail[3] == null ? "" : columndetail[3]);
				datadetail.put("noStt", columndetail[4] == null ? "" : columndetail[4].toString());
				datadetail.put("koli", columndetail[5] == null ? "" : columndetail[5]);
				datadetail.put("weight", columndetail[6] == null ? "" : columndetail[6].toString());
				datadetail.put("volume", columndetail[7] == null ? "" : columndetail[7].toString());
				datadetail.put("OrderCompanyId", columndetail[8].toString());
				datadetail.put("statusorder", columndetail[9].toString());
				List<Object[]> tracking = trxTrackingService.getTrackingByOrder(paramAktif.toString().toLowerCase(), columndetail[0].toString(), dataRequest.getString("orderCompanyId"));
				tracking.stream().forEach(column ->{
					JSONObject data = new JSONObject();
					data.put("orderId", column[0] == null ? "" : column[0]);
					data.put("trackingId", column[1] == null ? "" :column[1]);
					if(column[2] != null)
					{
						for(int i = 0; i < datacustomer.size(); i++)
						{
							if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
							{
								data.put("contactNameFrom", datacustomer.getJSONObject(i).getString("contact_name"));
								data.put("contactFrom", column[2]);
								break;
							}
							else if(datacustomer.size() == i + 1)
							{
								data.put("contactNameFrom", "");
								data.put("contactFrom", "");
							}
						}
					}
					else
					{
						data.put("contactNameFrom", "");
						data.put("contactFrom", "");
					}
					data.put("cabang", column[3] == null ? "" : column[3]);
					data.put("cabangdetail", column[4] == null ? "" : column[4]);
					if(column[5] != null)
					{
						for(int i = 0; i < datacities.size(); i++)
						{
							if(column[5].equals(datacities.getJSONObject(i).getInt("city_id")))
							{
								data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
								data.put("mKotaIdFrom", column[5]);
								break;
							}
							else if(datacities.size() == i + 1)
							{
								data.put("kotaNameFrom", "");
								data.put("mKotaIdFrom", "");
							}
						}
					}
					else
					{
						data.put("kotaNameFrom", "");
						data.put("mKotaIdFrom", "");
					}
					data.put("agen", column[6] == null ? "" : column[6]);
					data.put("parent", column[7] == null ? "" : column[7]);
					data.put("cabangname", column[8] == null ? "" : column[8]);
					data.put("agenname", column[12] == null ? "" : column[12]);
					if(column[9] != null)
					{
						for(int i = 0; i < datacustomer.size(); i++)
						{
							if(column[9].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
							{
								data.put("contactNameTo", datacustomer.getJSONObject(i).getString("contact_name"));
								data.put("contactTo", column[9]);
								break;
							}
							else if(datacustomer.size() == i + 1)
							{
								data.put("contactNameTo", "");
								data.put("contactTo", "");
							}
						}
					}
					else
					{
						data.put("contactNameTo", "");
						data.put("contactTo", "");
					}
					data.put("tanggal", column[13] == null ? "" : column[13].toString().split(" ")[0]);
					data.put("time", column[13] == null ? "" : column[13].toString().split(" ")[1]);
					data.put("day", column[14] == null ? "" : column[14].toString().replaceAll(" ", ""));
					data.put("daytanggal", column[15] == null ? "" : column[15].toString().replaceAll(" ", ""));
					data.put("month", column[16] == null ? "" : column[16].toString().replaceAll(" ", ""));
					data.put("year", column[17] == null ? "" : column[17].toString().replaceAll(" ", ""));
					data.put("date", column[14] == null && column[15] == null && column[16] == null && column[17] == null ? "" :  column[14].toString().replaceAll(" ", "") + ", " + column[15].toString().replaceAll(" ", "") + " " + column[16].toString().replaceAll(" ", "") + " " + column[17].toString().replaceAll(" ", ""));
					data.put("status", column[10] == null ? "" : column[10]);
					data.put("company", column[11] == null ? "" : column[11]);
					data.put("statusInbound", column[18] == null ? "" : column[18]);
					datastracking.add(data);
				});
				List<Object[]> driverschedule = trxDriverscheduleService.getDriverschedulebyorder(paramAktif.toString().toLowerCase(), columndetail[0].toString(), dataRequest.getString("orderCompanyId"));
				driverschedule.stream().forEach(column -> {
					JSONObject data = new JSONObject();
					data.put("orderId", column[0]);
					data.put("trxDriverschedule", column[1]);
					data.put("trackingIdFrom", column[2]);
					data.put("trackingIdTo", column[3]);
					data.put("schedule", column[4]);
//					data.put("weight", column[5]);
//					data.put("weightName", column[6]);
//					data.put("volume", column[7]);
//					data.put("VolumeName", column[8]);
					data.put("companyId", column[10] == null ? "" : column[10].toString());
					data.put("driverId", column[11] == null ? "" : column[11]);
					data.put("fleetVehicle", column[12] == null ? "" : column[12].toString());
					data.put("contactDriver", column[13] == null ? "" : column[13].toString());
					data.put("tglBerangkat", column[14] == null ? "" : column[14].toString());
					data.put("tglTiba", column[15] == null ? "" : column[15].toString());
					datasdriverschedule.add(data);
				});
				datadetail.put("sender", datasdriverschedule);
				datadetail.put("tracking", datastracking);
				datas.add(datadetail);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getmyOrder/{paramAktif}")
	public String getmyOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> orders = trxOrderService.getmyOrder(paramAktif.toString().toLowerCase(), user_uuid, dataRequest.getString("orderCompanyId"));
			orders.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("orderId", column[0]);
				data.put("orderdate", column[1].toString());
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactFrom", column[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("pickupAddress", "");
							data.put("contactFrom", "");
						}
					}
				}
				else
				{
					data.put("pickupAddress", "");
					data.put("contactFrom", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactTo", column[4]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("receipantAddress", "");
							data.put("contactTo", "");
						}
					}
				}
				else
				{
					data.put("receipantAddress", "");
					data.put("contactTo", "");
				}
				data.put("service", column[5].toString());
				data.put("koli", column[6]);
				data.put("status", column[7].toString());
				data.put("companyId", column[8].toString());
				data.put("createdby", column[9].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
//	@PostMapping("/getmyOutbound/{paramAktif}")
//	public String getmyOutbound(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
//	{
//		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
//		RestTemplate restTemplate = new RestTemplate();
//	    HttpHeaders headers = new HttpHeaders();
//	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//	    headers.set("x-access-code", header);
//	    HttpEntity<String> entity = new HttpEntity<String>(headers);
//	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
//	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
//		JSONObject response = new JSONObject();
//		List<JSONObject> datas = new ArrayList<>();
//		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
//		try
//		{
//			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
//			List<Object[]> orders = trxOrderService.getmyinboundoutboundornotif("a", user_uuid, "0", dataRequest.getString("orderCompanyId"));
//			orders.stream().forEach(column -> {
//				JSONObject data = new JSONObject();
//				data.put("orderId", column[0]);
//				data.put("orderdate", column[1].toString());
//				if(column[2] != null)
//				{
//					for(int i = 0; i < datacustomer.size(); i++)
//					{
//						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
//						{
//							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
//							data.put("contactFromname", datacustomer.getJSONObject(i).getString("contact_name"));
//							data.put("contactFrom", column[2]);
//							
//							break;
//						}
//						else if(datacustomer.size() == i + 1)
//						{
//							data.put("contactFrom", "");
//							data.put("pickupAddress", "");
//							data.put("contactFrom", "");
//						}
//					}
//				}
//				else
//				{
//					data.put("contactFrom", "");
//					data.put("pickupAddress", "");
//					data.put("contactFrom", "");
//				}
//				if(column[4] != null)
//				{
//					for(int i = 0; i < datacustomer.size(); i++)
//					{
//						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
//						{
//							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
//							data.put("contactToName", datacustomer.getJSONObject(i).getString("contact_name"));
//							data.put("contactTo", column[4]);
//							break;
//						}
//						else if(datacustomer.size() == i + 1)
//						{
//							data.put("contactToName", "");
//							data.put("receipantAddress", "");
//							data.put("contactTo", "");
//						}
//					}
//				}
//				else
//				{
//					data.put("contactToName", "");
//					data.put("receipantAddress", "");
//					data.put("contactTo", "");
//				}
//				data.put("service", column[5].toString());
//				data.put("koli", column[6]);
//				data.put("status", column[7].toString());
//				data.put("companyId", column[8].toString());
//				data.put("createdby", column[9].toString());
//				datas.add(data);
//			});
//			response.put("responseCode", "00");
//			response.put("responseDesc", "data Cabang berhasil didapat");
//			response.put("responseData", datas);
//		}
//		catch(Exception e)
//		{
//	    	response.put("responseCode", "99");
//		    response.put("responseDesc", "data Cabang gagal didapat");
//		    response.put("responseError", e.getMessage());
//		    logger.info("ERROR #### " + e.getMessage());
//		}
//	    return response.toString();
//	}
	
	@PostMapping("/estimatingcost/{paramAktif}")
	public String estimatingcost(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
    		Integer kotafrom = dataRequest.getInt("customerFrom");
    		Integer fromkota = 0;
			for(int i = 0; i < datacustomer.size(); i++)
			{
				if(kotafrom.equals(datacustomer.getJSONObject(i).getInt("contact_id")))
				{
					fromkota = datacustomer.getJSONObject(i).getInt("contact_cityid");
					break;
				}
			}
    		Integer kotato = dataRequest.getInt("customerTo");
    		Integer tokota = 0;
			for(int i = 0; i < datacustomer.size(); i++)
			{
				if(kotato.equals(datacustomer.getJSONObject(i).getInt("contact_id")))
				{
					tokota = datacustomer.getJSONObject(i).getInt("contact_cityid");
					break;
				}
			}
			logger.info("from = " + fromkota);
			logger.info("to = " + tokota);
			List<Object[]> orders = trxOrderService.estcost(dataRequest.getInt("customerFrom"), fromkota, tokota, dataRequest.getInt("mServiceId"), dataRequest.getString("Sendtype"), Float.parseFloat(dataRequest.getString("orderWeight")), dataRequest.getString("orderCompanyId"));
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", orders.get(0)[0]);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	Integer resultId = null;
	@PostMapping("/saveOrder")
	public String saveOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		final String url1 = "http://54.169.109.123:3004/api/v1/cities?limit=all";
	    ResponseEntity<String> responsecities = restTemplate.exchange(url1, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	List<Object[]> numbers = trxNumberService.getNumber("a", 1,  dataRequest.getString("orderCompanyId"));
	    	numbers.stream().forEach(column-> {
		    	Optional <TrxNumber> dataUpdateNumber = trxNumberService.getdetail(Integer.parseInt(column[0].toString()), dataRequest.getString("orderCompanyId"));
		    	if(dataUpdateNumber.isPresent())
		    	{
		    		
		    		
		    		
		    		
		    		ArrayList <TrxKolidetail> dataskolidetail = new ArrayList<>();
		    		ArrayList <TrxPartdetail> dataspartdetail = new ArrayList<>();
		    		ArrayList <TrxTracking> datastracking = new ArrayList<>();
			    	TrxNumbergen datanumgen = new TrxNumbergen();
			    	TrxOrder dataorder = new TrxOrder();
			    	
			    	datanumgen.setTmTrxNumberId(Integer.parseInt(column[0].toString()));
			    	
			    	datanumgen.setTrxNumbergenCompanyId(dataRequest.getString("orderCompanyId"));
		    		((TrxNumber)dataUpdateNumber.get()).setTrxNumberRunningnumber(Integer.parseInt(column[3].toString()) + 1);
//		    		dataorder.setTrxOrderId(column[6].toString());
		    		dataorder.setTrxOrderDate(LocalDate.now());
		    		dataorder.setmContactCustomerFrom(dataRequest.getInt("customerFrom"));
		    		dataorder.setmContactAttedanceFrom(dataRequest.getInt("attendanceFrom"));
		    		Integer kotafrom = dataRequest.getInt("customerFrom");
		    		Integer fromkota = 0;
		    		Integer tokota = 0;
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(kotafrom.equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							fromkota = datacustomer.getJSONObject(i).getInt("contact_cityid");
							dataorder.setmContactCustomerFromCity(datacustomer.getJSONObject(i).getInt("contact_cityid"));
							for(int j = 0; j < datacities.size(); j++)
							{
								if(datacustomer.getJSONObject(i).getInt("contact_cityid") == datacities.getJSONObject(j).getInt("city_id"))
								{
									dataorder.setTrxKotaFrom(datacities.getJSONObject(j).getString("city_name"));
									break;
								}
							}
							break;
						}
					}
		    		dataorder.setmContactAttedancePickup(dataRequest.get("customerPickup")  == "" ? null :dataRequest.getInt("customerPickup"));
		    		dataorder.setTrxOrderPickupAddress(dataRequest.get("pickupaddress")  == "" ? null :dataRequest.getString("pickupaddress"));
		    		dataorder.setmContactCustomerTo(dataRequest.getInt("customerTo"));
		    		dataorder.setmContactAttedanceTo(dataRequest.getInt("attendanceTo"));
		    		Integer kotato = dataRequest.getInt("customerTo");
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(kotato.equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							tokota = datacustomer.getJSONObject(i).getInt("contact_cityid");
							dataorder.setmContactCustomerToCity(datacustomer.getJSONObject(i).getInt("contact_cityid"));
							for(int j = 0; j < datacities.size(); j++)
							{
								if(datacustomer.getJSONObject(i).getInt("contact_cityid") == datacities.getJSONObject(j).getInt("city_id"))
								{
									dataorder.setTrxKotaTo(datacities.getJSONObject(j).getString("city_name"));
									break;
								}
							}
							break;
						}
					}
					String orderId = "";
					List<Object[]> getAgen2 = trxTrackingService.getAgen(fromkota, tokota, dataRequest.getString("orderCompanyId"));
		    		if(getAgen2.size() <= 0)
		    		{
		    			List<Object[]> getCabang = trxTrackingService.getCabang(fromkota, tokota, dataRequest.getString("orderCompanyId"));
		    			if(getCabang.size() <= 0)
		    			{
		    				response.put("responseCode", "99");
			    	    	response.put("responseDesc", "Save Order Error");
			    	    	response.put("responseError", "Please set agen and branch !");
			    		    return;
		    			}
		    			else
		    			{
		    				orderId = "LA-"+ getCabang.get(0)[9].toString()+"-"+column[6].toString();
		    				dataorder.setTrxOrderId(getCabang.get(0)[9].toString()+column[6].toString());
//		    				datanumgen.setTrxNumbergenId(getCabang.get(0)[9].toString()+column[6].toString());
//	    					TrxTracking datatracking = new TrxTracking();
//	    					id++;
//				    		datatracking.setTmTrxOrderId(column[6].toString());
//				    		datatracking.setTrxTrackingId(id); 
//				    		datatracking.setTrxTrackingStatus("data Entry");
//				    		datatracking.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
//	    					datatracking.setmCabangId(Integer.parseInt(getCabang.get(0)[0].toString()));
//	    					datatracking.setmCabangDetailId(Integer.parseInt(getCabang.get(0)[1].toString()));
//	    					datatracking.setmParentId(Integer.parseInt(getCabang.get(0)[0].toString()));
//			    			datastracking.add(datatracking);
		    				
			    		}
		    		}
		    		else
		    		{
		    			orderId = "LA-"+getAgen2.get(0)[9].toString()+"-"+column[6].toString();
		    			dataorder.setTrxOrderId(getAgen2.get(0)[9].toString()+column[6].toString());
//		    			TrxTracking datato = new TrxTracking();
//			    		id++;
//			    		datato.setTmTrxOrderId(column[6].toString());
//			    		datato.setTrxTrackingId(id);
//			    		datato.setTrxTrackingStatus("data Entry");
//			    		datato.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
//			    		datato.setmAgenId(Integer.parseInt(getAgen.get(0)[2].toString()));
//			    		datato.setmAgenDetailId(Integer.parseInt(getAgen.get(0)[3].toString()));
//			    		datastracking.add(datato);
		    		}
					
		    		logger.info("orderId == " + orderId);
		    		if(!orderId.equals(""))
		    		{
		    			dataorder.setTrxOrderId(orderId);
		    		}
		    		else
		    		{
		    			response.put("responseCode", "99");
		    	    	response.put("responseDesc", "Save Order Error");
		    	    	response.put("responseError", "Please set agen and branch !");
		    		    return;
		    		}
		    		datanumgen.setTrxNumbergenId(orderId);
		    		List<Object[]> getLatestId = TrxHistroyTrackingService.getLatestId(orderId, dataRequest.getString("orderCompanyId"));
		    		TrxHistroyTracking dataHistory = new TrxHistroyTracking();
		    		dataHistory.setOrderId(orderId);
		    		dataHistory.setId(getLatestId.size() > 0 ? Integer.parseInt(getLatestId.get(0)[0].toString()) + 1 : 0);
		    		dataHistory.setStatus("Order Created");
		    		dataHistory.setWaktu(localDateTime2);
		    		dataHistory.setCompanyId(dataRequest.getString("orderCompanyId"));
		    		TrxHistroyTrackingService.save(dataHistory);
		    		
		    		dataorder.setmServiceId(dataRequest.getInt("mServiceId"));
		    		dataorder.setTrxOrderSendtype(dataRequest.getString("Sendtype"));
		    		dataorder.setmPackingId(dataRequest.getInt("mPackingId"));
			    	if(dataRequest.has("orderstt"))
			    	{
			    		dataorder.setTrxOrderNostt(dataRequest.getString("orderstt"));
			    	}
			    	dataorder.setTrxOrderKoli(dataRequest.getInt("koli"));
			    	dataorder.setTrxOrderEstimatedCost(Float.parseFloat(dataRequest.getString("cost")));
			    	dataorder.setmPaymentId(dataRequest.getInt("payment"));
			    	dataorder.setTrxOrderKolitype(dataRequest.getString("koliType"));
			    	dataorder.setTrxOrderWeight(Float.parseFloat(dataRequest.getString("orderWeight")));
			    	dataorder.setmWeightId(dataRequest.getInt("weightType"));
			    	if(dataRequest.has("connocement"))
			    	{
				    	dataorder.setTrxOrderConnocement(dataRequest.getString("connocement"));	
			    	}
			    	dataorder.setTrxOrderReceivingDate(LocalDate.parse(dataRequest.getString("ReceivingDate")));
			    	if(dataRequest.has("Instruction"))
			    	{
				    	dataorder.setTrxOrderInstruction(dataRequest.getString("Instruction"));	
			    	}
			    	dataorder.setTrxOrderVolume(Float.parseFloat(dataRequest.getString("orderVolume")));
			    	dataorder.setmVolumeId(dataRequest.getInt("volumeType"));
			    	if(dataRequest.has("invoice"))
			    	{
			    		dataorder.setTmTrxInvoiceId(dataRequest.getInt("invoice"));
			    	}
			    	if(dataRequest.has("CompletedDate"))
			    	{
				    	dataorder.setTrxOrderCompleteDate(LocalDate.parse(dataRequest.getString("CompletedDate")));
			    	}
			    	if(dataRequest.has("nettprice"))
			    	{
				    	dataorder.setTrxOrderCompleteDate(LocalDate.parse(dataRequest.getString("CompletedDate")));
			    	}
			    	if(dataRequest.has("ordertype"))
			    	{
				    	dataorder.setTrxOrderOrdertype(dataRequest.getString("ordertype"));
			    	}
			    		dataorder.setTrxOrderCreatedBy(user_uuid);
			    		dataorder.setTrxOrderCreatedAt(localDateTime2);
				    	dataorder.setTrxOrderStatus(dataRequest.getString("status"));
				    	dataorder.setTrxOrderCompanyId(dataRequest.getString("orderCompanyId"));
				    	JSONArray kolidetailArray = dataRequest.getJSONArray("kolidetailArray");
				    	if(kolidetailArray.size() > 0)
				    	{
				    		for(int i = 0; i < kolidetailArray.size(); i++)
				    		{
				    			TrxKolidetail datakoli = new TrxKolidetail();
				    			datakoli.setTmTrxOrderId(column[6].toString());
				    			datakoli.setTrxKolidetailId(kolidetailArray.getJSONObject(i).getInt("trxKolidetailId"));
				    			datakoli.setTrxKolidetailWeight(Float.parseFloat(kolidetailArray.getJSONObject(i).getString("trxKolidetailWeight")));
				    			datakoli.setmWeightId(kolidetailArray.getJSONObject(i).getInt("mWeightId"));
				    			datakoli.setTrxKolidetailHeight(Float.parseFloat(kolidetailArray.getJSONObject(i).getString("trxKolidetailHeight")));
				    			datakoli.setmHeightId(kolidetailArray.getJSONObject(i).getInt("mHeightId"));
				    			datakoli.setTrxKolidetailWidth(Float.parseFloat(kolidetailArray.getJSONObject(i).getString("trxKolidetailWidth")));
				    			datakoli.setmWidthId(kolidetailArray.getJSONObject(i).getInt("mWidthId"));
				    			datakoli.setTrxKolidetailLength(Float.parseFloat(kolidetailArray.getJSONObject(i).getString("trxKolidetailLength")));
				    			datakoli.setmLengthId(kolidetailArray.getJSONObject(i).getInt("mLengthId"));
				    			datakoli.setTrxKolidetailDeclareditem(kolidetailArray.getJSONObject(i).getString("trxKolidetailDeclareditem"));
				    			datakoli.setTrxKolidetailStatus(kolidetailArray.getJSONObject(i).getBoolean("trxKolidetailStatus"));
				    			datakoli.setTrxKolidetailCompanyId(kolidetailArray.getJSONObject(i).getString("trxKolidetailCompanyId"));
				    			dataskolidetail.add(datakoli);
				    		}
				    		JSONArray partdetailArray = dataRequest.getJSONArray("partdetailArray");
				    		if(partdetailArray.size() > 0)
				    		{
					    		for(int j = 0; j < partdetailArray.size(); j++)
					    		{
					    			TrxPartdetail datapart = new TrxPartdetail();
					    			datapart.setTmTrxOrderId(column[6].toString());
					    			datapart.setTmTrxKolidetailId(partdetailArray.getJSONObject(j).getInt("trxKolidetailId"));
					    			datapart.setTrxpartdetailId(partdetailArray.getJSONObject(j).getInt("trxPartdetailId"));
					    			datapart.setTrxpartdetailPartnumber(partdetailArray.getJSONObject(j).getString("trxPartdetailPartnumber"));
					    			datapart.setTrxpartdetailName(partdetailArray.getJSONObject(j).getString("partname"));
					    			datapart.setTrxpartdetailQuantity(partdetailArray.getJSONObject(j).getInt("trxPartdetailQuantity"));
					    			datapart.setmUnitId(partdetailArray.getJSONObject(j).getInt("mUnitId"));
					    			datapart.setTrxPartdetailStatus(partdetailArray.getJSONObject(j).getBoolean("trxPartdetailStatus"));
					    			datapart.setTrxPartdetailCompanyId(partdetailArray.getJSONObject(j).getString("trxPartdetailCompanyId"));
					    			dataspartdetail.add(datapart);
					    		}
				    		}
				    	}
				    	TrxTracking datafrom = new TrxTracking();
			    		Integer id = 1;
			    		datafrom.setTmTrxOrderId(orderId);
			    		datafrom.setTrxTrackingId(id);
			    		datafrom.setmContactCustomerFromId(dataRequest.getInt("customerFrom"));
			    		datafrom.setTrxTrackingStatus("Data Entry");
			    		datafrom.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
			    		datafrom.setTrxTrackingEstimetedTime(LocalDateTime.now());
			    		datastracking.add(datafrom);
			    		// 1. masukkan customer from sebagai tracking pertama.
			    		// 2. baru panggil tracking yang cover area nya sama dengan kota customer from
			    		logger.info("kota to ambil dari contact id = " + tokota);
			    		logger.info("kota from ambil dari contact id = " + fromkota);
			    		
			    		List<Object[]> getAgen = trxTrackingService.getAgen(fromkota, tokota, dataRequest.getString("orderCompanyId"));
			    		if(getAgen.size() <= 0)
			    		{
			    			List<Object[]> getCabang = trxTrackingService.getCabang(fromkota, tokota, dataRequest.getString("orderCompanyId"));
			    			if(getCabang.size() <= 0)
			    			{
			    				response.put("responseCode", "99");
				    	    	response.put("responseDesc", "Save Order Error");
				    	    	response.put("responseError", "Please set agen and branch !");
				    		    return;
			    			}
			    			else
			    			{
		    					TrxTracking datatracking = new TrxTracking();
		    					id++;
					    		datatracking.setTmTrxOrderId(orderId);
					    		datatracking.setTrxTrackingId(id); 
					    		datatracking.setTrxTrackingStatus("data Entry");
					    		datatracking.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
		    					datatracking.setmCabangId(Integer.parseInt(getCabang.get(0)[0].toString()));
		    					datatracking.setmCabangDetailId(Integer.parseInt(getCabang.get(0)[1].toString()));
		    					datatracking.setmParentId(Integer.parseInt(getCabang.get(0)[0].toString()));
				    			datastracking.add(datatracking);
			    				
				    		}
			    		}
			    		else
			    		{
			    			TrxTracking datato = new TrxTracking();
				    		id++;
				    		datato.setTmTrxOrderId(orderId);
				    		datato.setTrxTrackingId(id);
				    		datato.setTrxTrackingStatus("data Entry");
				    		datato.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
				    		datato.setmAgenId(Integer.parseInt(getAgen.get(0)[2].toString()));
				    		datato.setmAgenDetailId(Integer.parseInt(getAgen.get(0)[3].toString()));
				    		datastracking.add(datato);
			    		}
			    		
			    		List<Object[]> getAgenTo = trxTrackingService.getAgen(tokota, tokota, dataRequest.getString("orderCompanyId"));
			    		if(getAgenTo.size() <= 0)
			    		{
			    			List<Object[]> getCabang = trxTrackingService.getCabang(tokota, tokota, dataRequest.getString("orderCompanyId"));
			    			if(getCabang.size() <= 0)
			    			{
			    				response.put("responseCode", "99");
				    	    	response.put("responseDesc", "Save Order Error");
				    	    	response.put("responseError", "Please set agen and branch !");
				    		    return;
			    			}
			    			else
			    			{
		    					TrxTracking datatracking = new TrxTracking();
		    					id++;
					    		datatracking.setTmTrxOrderId(orderId);
					    		datatracking.setTrxTrackingId(id); 
					    		datatracking.setTrxTrackingStatus("data Entry");
					    		datatracking.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
		    					datatracking.setmCabangId(Integer.parseInt(getCabang.get(0)[0].toString()));
		    					datatracking.setmCabangDetailId(Integer.parseInt(getCabang.get(0)[1].toString()));
		    					datatracking.setmParentId(Integer.parseInt(getCabang.get(0)[0].toString()));
				    			datastracking.add(datatracking);
			    				
				    		}
			    		}
			    		else
			    		{
			    			TrxTracking datato = new TrxTracking();
				    		id++;
				    		datato.setTmTrxOrderId(orderId);
				    		datato.setTrxTrackingId(id);
				    		datato.setTrxTrackingStatus("data Entry");
				    		datato.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
				    		datato.setmAgenId(Integer.parseInt(getAgenTo.get(0)[2].toString()));
				    		datato.setmAgenDetailId(Integer.parseInt(getAgenTo.get(0)[3].toString()));
				    		datastracking.add(datato);
			    		}
			    		
			    		TrxTracking datato2 = new TrxTracking();
			    		id++;
			    		datato2.setTmTrxOrderId(orderId);
			    		datato2.setTrxTrackingId(id);
			    		datato2.setmContactCustomerToId(dataRequest.getInt("customerTo"));
			    		datato2.setTrxTrackingStatus("data Entry");
			    		datato2.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
			    		datastracking.add(datato2);
			    		
//			    		List<Object[]> trackings = trxTrackingService.getRoute(fromkota, tokota, dataRequest.getString("orderCompanyId"));
//			    		logger.info("trackings = " + trackings.size());
//			    		 disini bisa terjadi error karena ada kondisi jika tracking belum diset sehingga membuat return null
//			    		if(trackings == null || trackings.isEmpty())
//			    		{
//			    			//ini belum nyeleksi yang online atau offline.
//			    			//klo offline nanti yang bertanggung jawab itu cabang setelahnya
//			    			
//			    			List<Object[]> getAgen = MAgendetailService.getAgenByName(user_uuid.toString());
//			    			logger.info("getAgen = " + getAgen.size());
////			    			logger.info("user_uuid.toString() = " + user_uuid.toString());
//			    			if(getAgen.size()>0)
//			    			{
//			    				int agenId = Integer.parseInt(getAgen.get(0)[0].toString());
//			    				//untuk mencari apakah kota bekasi itu sebagai agent ?
//			    				List<Object[]> getCustomerCityByAgen = MAgendetailService.getAgenDetailByIdAndMKotaId(Integer.parseInt(getAgen.get(0)[0].toString()), fromkota);
//			    				
//			    				if(getCustomerCityByAgen.size() > 0)
//			    				{
//			    					
//			    					//percabangan kalo seumpama yang order dari kantor agen nya. bukan di cover area agen cirebon
//			    					List<Object[]> getCabangIdWithCityAndAgenId = MCabangdetailService.getCabangIdWithCityAndAgenId(agenId, fromkota);
//			    					
//			    					if(getCabangIdWithCityAndAgenId.size() > 0)
//			    					{
//			    						Optional<MCabang> getcabang = MCabangdetailService.getDetailCabang(Integer.parseInt(getCabangIdWithCityAndAgenId.get(0)[0].toString()),"1");
//
//			    						if(!getcabang.get().getmKotaId().equals("") || !getcabang.get().getmKotaId().equals(null))
//			    						{
//			    							// ini hati hati karena ada error disini ketika agent ke cover area agent ( kota cirebon ke kabupaten nganjuk ( ada errornya )
//			    							// awal nya pakek getRoute biasa
//			    							List<Object[]> trackingsAgen = trxTrackingService.getRouteKhususGetFromCoverAreaAgent(getcabang.get().getmKotaId(), tokota, dataRequest.getString("orderCompanyId"));
//			    							if(!trackingsAgen.isEmpty()) 
//			    							{
//			    								
//			    								ArrayList<TrxTracking> dataTracking = (ArrayList<TrxTracking>) setTrackingKhususCoverAreaAgent(trackingsAgen, column[6].toString(), dataRequest.getString("orderCompanyId"),dataRequest.getInt("customerTo"), datastracking,agenId,1);
//			    								if(Float.parseFloat(dataRequest.getString("cost")) == 0)
//			    					    		{
//			    					    	    	response.put("responseCode", "99");
//			    					    	    	response.put("responseDesc", "Save Order Error, cost has not been filled");
//			    					    		    return;
//			    					    		}
//			    					    		else
//			    					    		{
//			    							    	allOrderService.saveallOrder(dataUpdateNumber.get(), datanumgen, dataorder, dataskolidetail, dataspartdetail,dataTracking);
//			    							    	response.put("responseCode", "00");
//			    								    response.put("responseDesc", "Save Order success");
//			    								    return;
//			    					    		} 	
//			    							}
//			    						}
//			    					}
//			    				}
//			    				else if (getCustomerCityByAgen.size() == 0)
//			    				{
//			    					logger.info("masuk ke sini = " + getAgen.get(0)[0].toString());
//			    					logger.info("from  kota = " + fromkota);
////			    					percabangan kalo seumpama ada orderan di area cover si agen cirebon
//			    					List<Object[]> getCustomerCityByAgenTransferPoint = MAgendetailService.getCustomerCityByAgenTransferPoint(getAgen.get(0)[0].toString(), fromkota); 
////			    					//cek di cover area agen itu ada gak bekasi. ternyata yang mengcover area bekasi itu ada 2 yaitu ciamis dan cirebon
//			    					if(getCustomerCityByAgenTransferPoint.size() > 0)
//			    					{
//			    						
//			    						//setelah itu looping ciamis dan cirebon untuk di cek di cabang mana dia dicover.
//			    						resultId = null;
//			    						getCustomerCityByAgenTransferPoint.stream().forEach(col->{
//			    							List<Object[]> getCabangIdWithCityAndAgenId = MCabangdetailService.getCabangIdWithCityAndAgenId(agenId, Integer.parseInt(col[1].toString()));
//			    							logger.info("lebih dari 1 = " + getCabangIdWithCityAndAgenId.size());
//			    							logger.info("Integer.parseInt(getCabangIdWithCityAndAgenId.get(0)[0].toString() = " + Integer.parseInt(getCabangIdWithCityAndAgenId.get(0)[0].toString()) );
//			    							if(getCabangIdWithCityAndAgenId.size()>0)
//			    							{
//			    								//masuk sini jika kantor agen ada yang doble mengcover nya. jadi pilih salah satu
//			    								resultId = Integer.parseInt(getCabangIdWithCityAndAgenId.get(0)[0].toString());
//			    								return;
//			    							}
//			    						});
//			    						logger.info("resultId = " + resultId);
//			    						
//			    						//lalu cek apakah kota agen (cirebon) itu sudah dicover oleh cabang.
//				    					if(resultId != null)
//				    					{
//				    						
//				    						//jika iya maka ambil cabang id untuk mendapatkan city id dari cabang yang mengcover untuk mendapatkan rute nya ( ambil kota semarang )
//				    						Optional<MCabang> getcabang = MCabangdetailService.getDetailCabang(resultId,"1");
//				    						
//				    						if(!getcabang.get().getmKotaId().equals("") || !getcabang.get().getmKotaId().equals(null))
//				    						{
//				    							List<Object[]> trackingsAgen = trxTrackingService.getRouteKhususGetFromCoverAreaAgent(getcabang.get().getmKotaId(), tokota, dataRequest.getString("orderCompanyId"));
//				    							if(!trackingsAgen.isEmpty()) {
//				    								
//				    								ArrayList<TrxTracking> dataTracking = (ArrayList<TrxTracking>) setTrackingKhususCoverAreaAgent(trackingsAgen, column[6].toString(), dataRequest.getString("orderCompanyId"),dataRequest.getInt("customerTo"), datastracking,agenId,1);
//				    								//jika sudah ada rute nya maka tambahkan agen sebelum cabang yang mengantar ke rutenya agar agen bisa mengambil barang dari customer yang kemudian
//				    								// dilanjutkan agar orderan bisa berjalan 
////				    								logger.info("dataTracking = " + dataTracking.size());
//				    								for (TrxTracking trxTracking : dataTracking) {
//														logger.info("trxTracking = " + trxTracking.getmParentId());
//													}
//				    								if(Float.parseFloat(dataRequest.getString("cost")) == 0)
//				    					    		{
//				    					    	    	response.put("responseCode", "99");
//				    					    		    response.put("responseDesc", "Save Order Error, cost has not been filled");
//				    					    		    return;
//				    					    		}
//				    					    		else
//				    					    		{
//				    							    	allOrderService.saveallOrder(dataUpdateNumber.get(), datanumgen, dataorder, dataskolidetail, dataspartdetail,dataTracking);
//				    							    	response.put("responseCode", "00");
//				    								    response.put("responseDesc", "Save Order success");
//				    								    return;
//				    					    		} 	
//				    							}
//				    						}
//				    					}
//			    					}
//			    					else
//				    				{
//				    					response.put("responseCode", "99");
//						    		    response.put("responseDesc", "Save Order Error, Agen does not cover customer city");
//						    		    return;
//				    				}
//			    				}
//			    				else
//			    				{
//			    					response.put("responseCode", "99");
//					    		    response.put("responseDesc", "Save Order Error");
//					    		    return;
//			    				}
//			    			}
//			    			else
//			    			{
//
//			    				
//			    				List<Object[]> getCabangHeaderIdByCityId = MCabangdetailService.getCabangHeaderIdByCityId(fromkota, dataRequest.getString("orderCompanyId"));
//			    				List<Object[]> searchCabangByCityId = MCabangdetailService.getCabangIdByCityId(fromkota, dataRequest.getString("orderCompanyId"));
//			    				logger.info("fromkota asdfasdf= " + fromkota);
//			    				logger.info("searchCabangByCityId = = "+ searchCabangByCityId.size());
//			    				if(getCabangHeaderIdByCityId.size() > 0)
//			    				{
//			    					// masuk ke sini ketika cabang memiliki order
//			    					List<Object[]> trackingsAgen = trxTrackingService.getRouteKhususGetFromCoverAreaAgent(Integer.parseInt(getCabangHeaderIdByCityId.get(0)[0].toString()), tokota, dataRequest.getString("orderCompanyId"));
////			    					List<Object[]> getAgenDetail = MAgendetailService.getAgenByCityId(fromkota, dataRequest.getString("orderCompanyId"));
//			    					List<Object[]> getAgenDetail = MAgendetailService.getAgenByCityId(fromkota, dataRequest.getString("orderCompanyId"));
//				    				if(!trackingsAgen.isEmpty()) {
//				    					ArrayList<TrxTracking> dataTracking = (ArrayList<TrxTracking>) setTrackingKhususCoverAreaCabang(trackingsAgen, column[6].toString(), dataRequest.getString("orderCompanyId"),dataRequest.getInt("customerTo"), datastracking,Integer.parseInt(getCabangHeaderIdByCityId.get(0)[0].toString()), getAgenDetail);
//	    								for (TrxTracking trxTracking : dataTracking) {
//											logger.info("trxTracking = " + trxTracking.getmParentId());
//										}
//	    								if(Float.parseFloat(dataRequest.getString("cost")) == 0)
//	    					    		{
//	    					    	    	response.put("responseCode", "99");
//	    					    	    	response.put("responseDesc", "Save Order Error, cost has not been filled");
//	    					    		    return;
//	    					    		}
//	    					    		else
//	    					    		{
//	    							    	allOrderService.saveallOrder(dataUpdateNumber.get(), datanumgen, dataorder, dataskolidetail, dataspartdetail,dataTracking);
//	    							    	response.put("responseCode", "00");
//	    								    response.put("responseDesc", "Save Order success");
//	    								    return;
//	    					    		} 	
//				    				}
//				    				else
//				    				{
//				    					response.put("responseCode", "99");
//    					    		    response.put("responseDesc", "Save Order Error, There is no tracking");
//    					    		    return;
//				    				}
//				    				
//			    				}
//			    				else if (searchCabangByCityId.size() > 0)
//			    				{
//			    					logger.info("masuk sini");
//			    					
//			    					// masuk ke sini ketika cover area ( entah itu agen atau kota biasa ) cabang memiliki order
//			    					
//			    					int branchId =Integer.parseInt(searchCabangByCityId.get(0)[0].toString());
//			    					List<Object[]> trackingsAgen = trxTrackingService.getRouteKhususGetFromCoverAreaAgent(Integer.parseInt(searchCabangByCityId.get(0)[0].toString()), tokota, dataRequest.getString("orderCompanyId"));
////			    					logger.info("searchCabangByCityId = " + searchCabangByCityId.get(0)[0]);
//			    					List<Object[]> getAgenDetail = MAgendetailService.getAgenByCityId(fromkota, dataRequest.getString("orderCompanyId"));
////			    					List<Object[]> getAgenDetail = null;
////			    					logger.info("cabang membuat order");
//			    					logger.info("size tracking = " + trackingsAgen.size());
//			    					logger.info("from = " + Integer.parseInt(searchCabangByCityId.get(0)[0].toString()));
//			    					logger.info("tokota = " + tokota);
//			    					logger.info("column[6].toString() = " + column[6].toString());
//				    				if(!trackingsAgen.isEmpty()) {
//				    					ArrayList<TrxTracking> dataTracking = (ArrayList<TrxTracking>) setTrackingKhususCoverAreaCabangkeCabang(trackingsAgen, column[6].toString(), dataRequest.getString("orderCompanyId"),dataRequest.getInt("customerTo"), datastracking,branchId, getAgenDetail);
//	    								for (TrxTracking trxTracking : dataTracking) {
//											logger.info("trxTracking = " + trxTracking.getmParentId());
//										}
//	    								if(Float.parseFloat(dataRequest.getString("cost")) == 0)
//	    					    		{
//	    					    	    	response.put("responseCode", "99");
//	    					    	    	response.put("responseDesc", "Save Order Error, cost has not been filled");
//	    					    		    return;
//	    					    		}
//	    					    		else
//	    					    		{
//	    							    	allOrderService.saveallOrder(dataUpdateNumber.get(), datanumgen, dataorder, dataskolidetail, dataspartdetail,dataTracking);
//	    							    	response.put("responseCode", "00");
//	    								    response.put("responseDesc", "Save Order success");
//	    								    return;
//	    					    		} 	
//				    				}
//
//			    				}
//			    				else
//			    				{
//			    					
//			    					List<Object[]> getAgenDetail = MAgendetailService.getAgenByCityId(fromkota, dataRequest.getString("orderCompanyId"));
//			    					//dari agen cirebon
//			    					// terus yang buatkan cabang
//			    					//kota cirebon di cari di m_agendetail
//			    					//apakah ada disana
//			    					//jika ada maka ambil agen id dan kota id ( agen nya apa dan kantor kota agen nya dimana )
//			    					// 
//			    					
//			    					// masuk kesini jika cabang membuatkan orderan untuk agen
//			    					//buat 2 percabangan 
//			    					//1. jika yang order ada di kota si agen
//			    					//2. jika orderan ada di cover area si agen
//			    					
//			    					//rute nya customer -> agen -> cabang
//			    						
//			    					//fungsi dibawah untuk mengecek kota cabang mana yang mengcover agen cirebon
////			    					List<Object[]> getCabangIdWithCityAndAgenId = MCabangdetailService.getCabangIdWithCityAndAgenId(Integer.parseInt(getAgenDetail.get(0)[0].toString()), Integer.parseInt(getAgenDetail.get(0)[1].toString()));
//			    					resultId = null;
//			    					getAgenDetail.stream().forEach(col->{
//		    							List<Object[]> getCabangIdWithCityAndAgenId = MCabangdetailService.getCabangIdWithCityAndAgenId(Integer.parseInt(col[0].toString()), Integer.parseInt(col[1].toString()));
//		    							if(getCabangIdWithCityAndAgenId.size()>0)
//		    							{
//		    								resultId = Integer.parseInt(getCabangIdWithCityAndAgenId.get(0)[0].toString());
//		    								return;
//		    							}
//		    						});
//			    					if(resultId != null)
//			    					{
//			    						
//			    						//jika iya maka ambil cabang id untuk mendapatkan city id dari cabang yang mengcover untuk mendapatkan rute nya ( ambil kota semarang )
//			    						Optional<MCabang> getcabang = MCabangdetailService.getDetailCabang(resultId,"1");
//			    						
//			    						if(!getcabang.get().getmKotaId().equals("") || !getcabang.get().getmKotaId().equals(null))
//			    						{
//			    							List<Object[]> trackingsAgen = trxTrackingService.getRouteKhususGetFromCoverAreaAgent(getcabang.get().getmKotaId(), tokota, dataRequest.getString("orderCompanyId"));
//			    							if(!trackingsAgen.isEmpty()) {
//			    								
//			    								ArrayList<TrxTracking> dataTracking = (ArrayList<TrxTracking>) setTrackingKhususCoverAreaAgent(trackingsAgen, column[6].toString(), dataRequest.getString("orderCompanyId"),dataRequest.getInt("customerTo"), datastracking,Integer.parseInt(getAgenDetail.get(0)[0].toString()),Integer.parseInt(getAgenDetail.get(0)[2].toString()));
//
//						    					
//			    								//jika sudah ada rute nya maka tambahkan agen sebelum cabang yang mengantar ke rutenya agar agen bisa mengambil barang dari customer yang kemudian
//			    								// dilanjutkan agar orderan bisa berjalan 
////			    								logger.info("dataTracking = " + dataTracking.size());
//			    								for (TrxTracking trxTracking : dataTracking) {
//													logger.info("trxTracking = " + trxTracking.getmParentId());
//												}
//			    								if(Float.parseFloat(dataRequest.getString("cost")) == 0)
//			    					    		{
//			    					    	    	response.put("responseCode", "99");
//			    					    	    	response.put("responseDesc", "Save Order Error, cost has not been filled");
//			    					    		    return;
//			    					    		}
//			    					    		else
//			    					    		{
//			    							    	allOrderService.saveallOrder(dataUpdateNumber.get(), datanumgen, dataorder, dataskolidetail, dataspartdetail,dataTracking);
//			    							    	response.put("responseCode", "00");
//			    								    response.put("responseDesc", "Save Order success");
//			    								    return;
//			    					    		} 	
//			    							}
//			    						}
//			    					}
//			    				}
//			    				
//			    				
//				    	    	
//			    			}
//			    		}
//			    		else
//			    		{
//			    			String listrute = trackings.get(0)[2].toString().replace("{", "").replace("}", "");
//				    		String[] rute = listrute.split(",");
//				    		for(String hasil: rute)
//				    		{
//				    			String tempat = hasil.split("/")[0];
//				    			String detail = hasil.split("/")[1];
//				    			String[] input = detail.split("-");
//			    				if(tempat.equals("customer"))
//			    				{
//			    					
//			    				}
//			    				else 
//			    				{
//			    					TrxTracking datatracking = new TrxTracking();
//			    					id++;
//						    		datatracking.setTmTrxOrderId(column[6].toString());
//						    		datatracking.setTrxTrackingId(id); 
//						    		datatracking.setTrxTrackingStatus("Data Entry");
//						    		datatracking.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
//					    			for(String inputan: input)
//					    			{
//						    				String tipedata = inputan.split("=")[0];
//						    				String isi = inputan.split("=")[1];
//						    				if(tipedata.equals("id"))
//						    				{
//						    					datatracking.setmCabangId(Integer.parseInt(isi));
//						    				}
//						    				else if(tipedata.equals("detail_id"))
//						    				{
//						    					datatracking.setmCabangDetailId(Integer.parseInt(isi));
//						    				}
//						    				else if(tipedata.equals("parent_id"))
//						    				{
//						    					datatracking.setmParentId(Integer.parseInt(isi));
//						    				}
//						    				else if(tipedata.equals("agen_id"))
//						    				{
//						    					datatracking.setmAgenId(Integer.parseInt(isi));
//						    				}
//					    			}
//					    			datastracking.add(datatracking);
//			    				}
//				    		}
//				    		TrxTracking datato = new TrxTracking();
//				    		id++;
//				    		datato.setTmTrxOrderId(column[6].toString());
//				    		datato.setTrxTrackingId(id);
//				    		datato.setmContactCustomerToId(dataRequest.getInt("customerTo"));
//				    		datato.setTrxTrackingStatus("data Entry");
//				    		datato.setTrxTrackingCompanyId(dataRequest.getString("orderCompanyId"));
//				    		datastracking.add(datato);
				    		if(Float.parseFloat(dataRequest.getString("cost")) == 0)
				    		{
				    	    	response.put("responseCode", "99");
				    	    	response.put("responseDesc", "Save Order Error, cost has not been filled");
				    		}
				    		else
				    		{
						    	allOrderService.saveallOrder(dataUpdateNumber.get(), datanumgen, dataorder, dataskolidetail, dataspartdetail,datastracking);
						    	response.put("responseCode", "00");
							    response.put("responseDesc", "Save Order success");
				    		} 	
//			    		}
		    	}
	    	});
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Order failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	public static Object setTrackingKhususCoverAreaCabangkeCabang(List<Object[]> data, String orderId, String companyId, Integer customerTo,ArrayList <TrxTracking> datastracking, Integer branchId, List<Object[]> getAgenDetail)
	{
		String listrute = data.get(0)[1].toString().replace("{", "").replace("}", "");
		String[] rute = listrute.split(",");
		int id=1;
		
		
		if(getAgenDetail.size() > 0)
		{
			id++;

			String detail2 = rute[0].split("/")[1];
			TrxTracking datatoAwal = new TrxTracking();
			datatoAwal.setTmTrxOrderId(orderId);
			datatoAwal.setTrxTrackingId(id);
			
			datatoAwal.setTrxTrackingStatus("data Entry");
			datatoAwal.setTrxTrackingCompanyId(companyId);
			logger.info("lebih dari satu " + getAgenDetail.get(0));
			datatoAwal.setmAgenId(Integer.parseInt(getAgenDetail.get(0)[0].toString()));
			if(getAgenDetail.get(0)[2].equals(0))
			{
				String[] input = detail2.split("-");
				for(String inputan: input)
				{
					String tipedata = inputan.split("=")[0];
					String isi = inputan.split("=")[1];
					if(tipedata.equals("id"))
					{
						datatoAwal.setmCabangId(Integer.parseInt(isi));
					}
					else if(tipedata.equals("detail_id"))
					{
						datatoAwal.setmCabangDetailId(Integer.parseInt(isi));
					}
					else if(tipedata.equals("parent_id"))
					{
						datatoAwal.setmParentId(Integer.parseInt(isi));
					}
					else if(tipedata.equals("agen_id"))
					{
						datatoAwal.setmAgenId(Integer.parseInt(isi));
					}
				}
			}
			datastracking.add(datatoAwal);
		}
		Integer index = 0;
		for(String hasil: rute)
		{
//			String tempat = null;
//			String detail = null;
//			String[] input = null;
//			
//			if(detail != null)
//			{
//				 input = detail.split("-");
//			}
			String tempat = hasil.split("/")[0];
			String detail = hasil.split("/")[1];
			String[] input = detail.split("-");
//			if(!hasil.equals("NULL"))
//			{
//				tempat= hasil.split("/")[0];
//				detail = hasil.split("/")[1];
//			}
//			logger.info("input = " + input.equals(null));
			if(tempat.equals("customer"))
			{
				
			}
//			
			else 
			{
				TrxTracking datatracking = new TrxTracking();
				id++;
	    		datatracking.setTmTrxOrderId(orderId);
	    		datatracking.setTrxTrackingId(id); 
	    		datatracking.setTrxTrackingStatus("Data Entry");
	    		datatracking.setTrxTrackingCompanyId(companyId);
	    		
    			for(String inputan: input)
    			{
	    				String tipedata = inputan.split("=")[0];
	    				String isi = inputan.split("=")[1];
	    				if(tipedata.equals("id"))
	    				{
	    					datatracking.setmCabangId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("detail_id"))
	    				{
	    					datatracking.setmCabangDetailId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("parent_id"))
	    				{
	    					datatracking.setmParentId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("agen_id"))
	    				{
	    					datatracking.setmAgenId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("agen_type"))
	    				{
	    					if(Integer.parseInt(isi) == 0)
	    					{
	    						String detail1 = rute[index-1].split("/")[1];
	    						String[] input1 = detail1.split("-");
	    						for(String inputan1: input1)
	    						{
	    							String tipedata1 = inputan1.split("=")[0];
	    							String isi1 = inputan1.split("=")[1];
	    							if(tipedata1.equals("id"))
	    							{
	    								datatracking.setmCabangId(Integer.parseInt(isi1));
	    							}
	    							else if(tipedata1.equals("detail_id"))
	    							{
	    								datatracking.setmCabangDetailId(Integer.parseInt(isi1));
	    							}
	    							else if(tipedata1.equals("parent_id"))
	    							{
	    								datatracking.setmParentId(Integer.parseInt(isi1));
	    							}
	    							else if(tipedata1.equals("agen_id"))
	    							{
	    								datatracking.setmAgenId(Integer.parseInt(isi1));
	    							}
	    						}
	    					}
	    				}
	    				
    			}
    			datastracking.add(datatracking);
			}
			index++;
		}
		TrxTracking datato = new TrxTracking();
		id++;
		datato.setTmTrxOrderId(orderId);
		datato.setTrxTrackingId(id);
		datato.setmContactCustomerToId(customerTo);
		datato.setTrxTrackingStatus("data Entry");
		datato.setTrxTrackingCompanyId(companyId);
		datastracking.add(datato); 
		
		
		
		return datastracking;
	}
	
	public static Object setTrackingKhususCoverAreaCabang(List<Object[]> data, String orderId, String companyId, Integer customerTo,ArrayList <TrxTracking> datastracking, Integer branchId, List<Object[]> getAgenDetail)
	{
		String listrute = data.get(0)[1].toString().replace("{", "").replace("}", "");
		String[] rute = listrute.split(",");
		int id=1;
		//ketika yang insert itu agen ofline. berarti ini diisi semua
		
			
			
			if(getAgenDetail.size() > 0)
			{
				String detail2 = rute[0].split("/")[1];
				id++;
				TrxTracking datatoAwal = new TrxTracking();
				datatoAwal.setTmTrxOrderId(orderId);
				datatoAwal.setTrxTrackingId(id);
				
				datatoAwal.setTrxTrackingStatus("data Entry");
				datatoAwal.setTrxTrackingCompanyId(companyId);
				datatoAwal.setmAgenId(Integer.parseInt(getAgenDetail.get(0)[0].toString()));
				if(getAgenDetail.get(0)[2].equals(0))
				{
					String[] input = detail2.split("-");
					for(String inputan: input)
					{
						String tipedata = inputan.split("=")[0];
						String isi = inputan.split("=")[1];
						if(tipedata.equals("id"))
						{
							datatoAwal.setmCabangId(Integer.parseInt(isi));
						}
						else if(tipedata.equals("detail_id"))
						{
							datatoAwal.setmCabangDetailId(Integer.parseInt(isi));
						}
						else if(tipedata.equals("parent_id"))
						{
							datatoAwal.setmParentId(Integer.parseInt(isi));
						}
						else if(tipedata.equals("agen_id"))
						{
							datatoAwal.setmAgenId(Integer.parseInt(isi));
						}
					}
				}
				datastracking.add(datatoAwal);
			}
		Integer index = 0;
		for(String hasil: rute)
		{
			String tempat = hasil.split("/")[0];
			String detail = hasil.split("/")[1];
			
			String[] input = detail.split("-");
			logger.info("input = " + input);
			
			
			if(tempat.equals("customer"))
			{
				
			}
			
			else 
			{
				TrxTracking datatracking = new TrxTracking();
				id++;
	    		datatracking.setTmTrxOrderId(orderId);
	    		datatracking.setTrxTrackingId(id); 
	    		datatracking.setTrxTrackingStatus("Data Entry");
	    		datatracking.setTrxTrackingCompanyId(companyId);
	    		
    			for(String inputan: input)
    			{
	    				String tipedata = inputan.split("=")[0];
	    				String isi = inputan.split("=")[1];
	    				if(tipedata.equals("id"))
	    				{
	    					datatracking.setmCabangId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("detail_id"))
	    				{
	    					datatracking.setmCabangDetailId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("parent_id"))
	    				{
	    					datatracking.setmParentId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("agen_id"))
	    				{
	    					datatracking.setmAgenId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("agen_type"))
	    				{
	    					if(Integer.parseInt(isi) == 0)
	    					{
	    						String detail1 = rute[index-1].split("/")[1];
	    						String[] input1 = detail1.split("-");
	    						for(String inputan1: input1)
	    						{
	    							String tipedata1 = inputan1.split("=")[0];
	    							String isi1 = inputan1.split("=")[1];
	    							if(tipedata1.equals("id"))
	    							{
	    								datatracking.setmCabangId(Integer.parseInt(isi1));
	    							}
	    							else if(tipedata1.equals("detail_id"))
	    							{
	    								datatracking.setmCabangDetailId(Integer.parseInt(isi1));
	    							}
	    							else if(tipedata1.equals("parent_id"))
	    							{
	    								datatracking.setmParentId(Integer.parseInt(isi1));
	    							}
	    							else if(tipedata1.equals("agen_id"))
	    							{
	    								datatracking.setmAgenId(Integer.parseInt(isi1));
	    							}
	    						}
	    					}
	    				}
	    				
    			}
    			datastracking.add(datatracking);
			}
			index++;
		}
		TrxTracking datato = new TrxTracking();
		id++;
		datato.setTmTrxOrderId(orderId);
		datato.setTrxTrackingId(id);
		datato.setmContactCustomerToId(customerTo);
		datato.setTrxTrackingStatus("data Entry");
		datato.setTrxTrackingCompanyId(companyId);
		datastracking.add(datato); 
		
		
		
		return datastracking;
	}
	
	public static Object setTrackingKhususCoverAreaAgent(List<Object[]> data, String orderId, String companyId, Integer customerTo,ArrayList <TrxTracking> datastracking, Integer agenId, Integer agenType)
	{
		String listrute = data.get(0)[1].toString().replace("{", "").replace("}", "");
		String[] rute = listrute.split(",");
		int id=2;
		TrxTracking datatoAwal = new TrxTracking();
		datatoAwal.setTmTrxOrderId(orderId);
		datatoAwal.setTrxTrackingId(id);
		datatoAwal.setmAgenId(agenId);
		if(agenType.equals(0))
		{
			String detail = rute[0].split("/")[1];
			String[] input = detail.split("-");
			for(String inputan: input)
			{
				String tipedata = inputan.split("=")[0];
				String isi = inputan.split("=")[1];
				if(tipedata.equals("id"))
				{
					datatoAwal.setmCabangId(Integer.parseInt(isi));
				}
				else if(tipedata.equals("detail_id"))
				{
					datatoAwal.setmCabangDetailId(Integer.parseInt(isi));
				}
				else if(tipedata.equals("parent_id"))
				{
					datatoAwal.setmParentId(Integer.parseInt(isi));
				}
				else if(tipedata.equals("agen_id"))
				{
					datatoAwal.setmAgenId(Integer.parseInt(isi));
				}
			}
		}
		datatoAwal.setTrxTrackingStatus("data Entry");
		datatoAwal.setTrxTrackingCompanyId(companyId);
		datastracking.add(datatoAwal);
		Integer index = 0;
		for(String hasil: rute)
		{
			String tempat = hasil.split("/")[0];
			String detail = hasil.split("/")[1];
			
			String[] input = detail.split("-");
			logger.info("input = " + input);
			
			
			if(tempat.equals("customer"))
			{
				
			}
			
			else 
			{
				TrxTracking datatracking = new TrxTracking();
				id++;
	    		datatracking.setTmTrxOrderId(orderId);
	    		datatracking.setTrxTrackingId(id); 
	    		datatracking.setTrxTrackingStatus("Data Entry");
	    		datatracking.setTrxTrackingCompanyId(companyId);
	    		
    			for(String inputan: input)
    			{
	    				String tipedata = inputan.split("=")[0];
	    				String isi = inputan.split("=")[1];
//	    				logger.info("isi = " + isi);
//	    				logger.info("tipe data = " + tipedata);
	    				if(tipedata.equals("id"))
	    				{
	    					datatracking.setmCabangId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("detail_id"))
	    				{
	    					datatracking.setmCabangDetailId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("parent_id"))
	    				{
	    					datatracking.setmParentId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("agen_id"))
	    				{
	    					datatracking.setmAgenId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("agen_type"))
	    				{
	    					if(Integer.parseInt(isi) == 0)
	    					{
	    						String detail1 = rute[index-1].split("/")[1];
	    						String[] input1 = detail1.split("-");
	    						for(String inputan1: input1)
	    						{
	    							String tipedata1 = inputan1.split("=")[0];
	    							String isi1 = inputan1.split("=")[1];
	    							if(tipedata1.equals("id"))
	    							{
	    								datatracking.setmCabangId(Integer.parseInt(isi1));
	    							}
	    							else if(tipedata1.equals("detail_id"))
	    							{
	    								datatracking.setmCabangDetailId(Integer.parseInt(isi1));
	    							}
	    							else if(tipedata1.equals("parent_id"))
	    							{
	    								datatracking.setmParentId(Integer.parseInt(isi1));
	    							}
	    							else if(tipedata1.equals("agen_id"))
	    							{
	    								datatracking.setmAgenId(Integer.parseInt(isi1));
	    							}
	    						}
	    					}
	    				}
    			}
    			datastracking.add(datatracking);
			}
			index++;
		}
		TrxTracking datato = new TrxTracking();
		id++;
		datato.setTmTrxOrderId(orderId);
		datato.setTrxTrackingId(id);
		datato.setmContactCustomerToId(customerTo);
		datato.setTrxTrackingStatus("data Entry");
		datato.setTrxTrackingCompanyId(companyId);
		datastracking.add(datato); 
		
//		logger.info("datastracking = " + datastracking.get(datastracking.size()-2).getmAgenId());
		return datastracking;
	}
	    
	
	public static Object setTracking(List<Object[]> data, String orderId, String companyId, Integer customerTo,ArrayList <TrxTracking> datastracking, Integer agenId)
	{
		String listrute = data.get(0)[2].toString().replace("{", "").replace("}", "");
		String[] rute = listrute.split(",");
		int id=2;
		TrxTracking datatoAwal = new TrxTracking();
		datatoAwal.setTmTrxOrderId(orderId);
		datatoAwal.setTrxTrackingId(id);
		datatoAwal.setmAgenId(agenId);
		datatoAwal.setTrxTrackingStatus("data Entry");
		datatoAwal.setTrxTrackingCompanyId(companyId);
		datastracking.add(datatoAwal);
		for(String hasil: rute)
		{
			String tempat = hasil.split("/")[0];
			String detail = hasil.split("/")[1];
			logger.info("tempat  = " + tempat);
			logger.info("detail = " + detail);
			
			String[] input = detail.split("-");
			logger.info("input = " + input);
			
			
			if(tempat.equals("customer"))
			{
				
			}
			else 
			{
				TrxTracking datatracking = new TrxTracking();
				id++;
	    		datatracking.setTmTrxOrderId(orderId);
	    		datatracking.setTrxTrackingId(id); 
	    		datatracking.setTrxTrackingStatus("Data Entry");
	    		datatracking.setTrxTrackingCompanyId(companyId);
    			for(String inputan: input)
    			{
	    				String tipedata = inputan.split("=")[0];
	    				String isi = inputan.split("=")[1];
	    				logger.info("isi = " + isi);
	    				logger.info("tipe data = " + tipedata);
	    				if(tipedata.equals("id"))
	    				{
	    					datatracking.setmCabangId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("detail_id"))
	    				{
	    					datatracking.setmCabangDetailId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("parent_id"))
	    				{
	    					datatracking.setmParentId(Integer.parseInt(isi));
	    				}
	    				else if(tipedata.equals("agen_id"))
	    				{
	    					datatracking.setmAgenId(Integer.parseInt(isi));
	    				}
    			}
    			datastracking.add(datatracking);
			}
		}
		TrxTracking datato = new TrxTracking();
		id++;
		datato.setTmTrxOrderId(orderId);
		datato.setTrxTrackingId(id);
		datato.setmContactCustomerToId(customerTo);
		datato.setTrxTrackingStatus("data Entry");
		datato.setTrxTrackingCompanyId(companyId);
		datastracking.add(datato);
//		
		return datastracking;
	}
	
	@PostMapping("/claimOrder")
	public String claimOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	JSONArray trackingArray = dataRequest.getJSONArray("driverscheduleArray");
			ArrayList <TrxTracking> datastracking = new ArrayList<>();
    		for(int j = 0; j < trackingArray.size(); j++)
    		{
    			Optional <TrxTracking> dataUpdatetrackingto = trxTrackingService.getdetail(trackingArray.getJSONObject(j).getString("OrderId"), trackingArray.getJSONObject(j).getInt("DriverscheduleId"), dataRequest.getString("driverschedulecompanyId"));
    			Optional <TrxTracking> dataUpdatetrackingfrom = trxTrackingService.getdetail(trackingArray.getJSONObject(j).getString("OrderId"), trackingArray.getJSONObject(j).getInt("DriverscheduleId") - 1, dataRequest.getString("driverschedulecompanyId"));
    			if(dataUpdatetrackingto.isPresent())
    			{
    				List<Object[]> trackings = trxDriverscheduleService.getDriverschedulebytrackingtocheckstat("a", trackingArray.getJSONObject(j).getString("OrderId"), trackingArray.getJSONObject(j).getInt("DriverscheduleId"), dataRequest.getString("driverschedulecompanyId"));
    				if(trackings.isEmpty())
    				{
    					((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingStatusinbound("waiting response");
						((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingStatus(null);
						((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingStatusoutbound(null);
    				}
    				else
    				{
						((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingStatusinbound("ware house");
						((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingEstimetedTime(LocalDateTime.now());
						((TrxTracking)dataUpdatetrackingfrom.get()).setTrxTrackingStatusinbound(null);
						((TrxTracking)dataUpdatetrackingfrom.get()).setTrxTrackingStatusoutbound("complete");
	    				trxDriverscheduleService.updatedriver(trackingArray.getJSONObject(j).getString("OrderId"), trackingArray.getJSONObject(j).getInt("DriverscheduleId"), dataRequest.getString("driverschedulecompanyId"), "complete", null);	
    				}
    				datastracking.add(dataUpdatetrackingto.get());
    				datastracking.add(dataUpdatetrackingfrom.get());
    			}
    		}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Claim success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Claim failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/returOrder")
	public String returOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	Optional <TrxOrder> dataUpdateorder = trxOrderService.getdetail(dataRequest.getString("orderId"), dataRequest.getString("orderCompanyId"));
	    	Optional <TrxTracking> dataUpdateTracking = trxTrackingService.getdetail(dataRequest.getString("orderId"), dataRequest.getInt("trackingId"), dataRequest.getString("orderCompanyId"));
    		ArrayList <TrxTracking> datastracking = new ArrayList<>();
	    	if(dataUpdateorder.isPresent())
	    	{
	    		((TrxOrder)dataUpdateorder.get()).setTrxOrderStatus("complete");
	    	}
	    	if(dataUpdateTracking.isPresent())
	    	{
	    		((TrxTracking)dataUpdateTracking.get()).setTrxTrackingStatus("0");
	    		datastracking.add(dataUpdateTracking.get());
	    	}
	    	trxOrderService.saveOrder(dataUpdateorder.get());
	    	trxTrackingService.saveAllTracking(datastracking);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Claim success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Retur failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getDeliveryOrder/{paramAktif}")
	public String getDeliveryOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> orders = trxOrderService.getdeliveryOrder("a", dataRequest.getString("orderCompanyId"));
			orders.stream().forEach(column->{
				JSONObject data = new JSONObject();	
				data.put("orderId", column[0] == null ? "" : column[0]);
				data.put("orderDate", column[1] == null ? "" : column[1].toString());
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("contactNameFrom", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactFrom", column[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("contactNameFrom", "");
							data.put("pickupAddress", "");
							data.put("contactFrom", "");
						}
					}
				}
				else
				{
					data.put("contactNameFrom", "");
					data.put("pickupAddress", "");
					data.put("contactFrom", "");
				}
				if(column[3] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[3].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("contactNameTo", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactTo", column[3]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("contactNameTo", "");
							data.put("receipantAddress", "");
							data.put("contactTo", "");
						}
					}
				}
				else
				{
					data.put("contactNameTo", "");
					data.put("receipantAddress", "");
					data.put("contactTo", "");
				}
				data.put("nostt", column[4] == null ? "" : column[4]);
				data.put("service", column[5] == null ? "" : column[5]);
				data.put("koli", column[6] == null ? "" : column[6]);
				data.put("statusinbound", column[7] == null ? "" : column[7]);
				if(column[7] != null)
				{
					if(column[9] != null)
					{
						for(int i = 0; i < datacities.size(); i++)
						{
							if(column[9].equals(datacities.getJSONObject(i).getInt("city_id")))
							{
								data.put("status", column[7].toString() + " " + datacities.getJSONObject(i).get("city_name").toString().toLowerCase());
								break;
							}
							else if(datacities.size() == i + 1)
							{
								data.put("status", "");
							}
						}
					}
					else if(column[10] != null)
					{
						data.put("status", column[7].toString() + " " + column[10].toString());
					}
				}
				else
				{
					data.put("status", "");
				}
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data order berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data order gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
}
