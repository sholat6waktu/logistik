package com.proacc.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.TrxDeliveryorder;
import com.proacc.entity.TrxHistroyTracking;
import com.proacc.entity.TrxOrder;
import com.proacc.entity.TrxTracking;
import com.proacc.service.AllOrderService;
import com.proacc.service.TrxDeliveryorderService;
import com.proacc.service.TrxDriverscheduleService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxTrackingService;
import com.proacc.service.TrxHistroyTrackingService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TrxDeliveryorderController {
	private static final Logger logger = LoggerFactory.getLogger(TrxDeliveryorderController.class);
	
	@Autowired
	TrxOrderService trxOrderService;
	@Autowired
	TrxTrackingService trxTrackingService;
	@Autowired
	TrxDriverscheduleService trxDriverscheduleService;
	@Autowired
	TrxDeliveryorderService trxDeliveryorderService;
	@Autowired
	AllOrderService allOrderService;
	@Autowired
	TrxHistroyTrackingService TrxHistroyTrackingService;
	
	@PostMapping("/deliveryOrder/{paramAktif}")
	public String deliveryOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> deliverys = trxDeliveryorderService.getallDeliveryorder("a", dataRequest.getString("compId"));
			deliverys.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				data.put("orderId", column[0] == null ? "" : column[0].toString());
				data.put("orderDate", column[1] == null ? "" : column[1].toString());
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("companyFrom", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("customerid", column[2]);
							if(column[3] != null)
							{
								for(int j = 0; j < datacustomer.getJSONObject(i).getJSONArray("contact_persons").size(); j++)
								{
									if(column[3].equals(datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getInt("id")))
									{
										data.put("attendanceNameFrom", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_name"));
										data.put("attendancephoneFrom", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_phone"));
										data.put("attendaceFrom", column[3]);
										break;
									}
									else
									{
										data.put("attendanceNameFrom", "");
										data.put("attendancephoneFrom", "");
										data.put("attendaceFrom", "");
									}
								}
							}
							else
							{
								data.put("attendanceNameFrom", "");
								data.put("attendancephoneFrom", "");
								data.put("attendaceFrom", "");
							}
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("attendanceNameFrom", "");
							data.put("attendancephoneFrom", "");
							data.put("attendaceFrom", "");
							data.put("companyFrom", "");
							data.put("pickupAddress", "");
							data.put("customerid", "");
						}
					}
				}
				else
				{
					data.put("attendanceNameFrom", "");
					data.put("attendancephoneFrom", "");
					data.put("attendaceFrom", "");
					data.put("companyFrom", "");
					data.put("pickupAddress", "");
					data.put("customerid", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("companyTo", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactTo", column[4]);
							if(column[5] != null)
							{
								for(int j = 0; j < datacustomer.getJSONObject(i).getJSONArray("contact_persons").size(); j++)
								{
									if(column[5].equals(datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getInt("id")))
									{
										data.put("attendanceNameTo", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_name"));
										data.put("attendancephoneTo", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_phone"));
										data.put("attendaceTo", column[5]);
										break;
									}
									else
									{
										data.put("attendanceNameTo", "");
										data.put("attendancephoneTo", "");
										data.put("attendaceTo", "");
									}
								}
							}
							else
							{
								data.put("attendanceNameTo", "");
								data.put("attendancephoneTo", "");
								data.put("attendaceTo", "");
							}
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("attendanceNameTo", "");
							data.put("attendancephoneTo", "");
							data.put("attendaceTo", "");
							data.put("companyTo", "");
							data.put("receipantAddress", "");
							data.put("contactTo", "");
						}
					}
				}
				else
				{
					data.put("attendanceNameTo", "");
					data.put("attendancephoneTo", "");
					data.put("attendaceTo", "");
					data.put("companyTo", "");
					data.put("receipantAddress", "");
					data.put("contactTo", "");
				}
				data.put("connocement", column[6] == null ? "" : column[6].toString());
				data.put("koli", column[7] == null ? "" : column[7].toString());
				data.put("satuan", column[8] == null ? "" : column[8].toString());
				data.put("nominalsatuan", column[9] == null ? "" : column[9].toString());
				data.put("berat", column[10] == null ? "" : column[10].toString());
				data.put("bukti", column[11] == null ? "" : column[11].toString());
				data.put("companyId", column[12] == null ? "" : column[12].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/searchDoneOrder/{paramAktif}")
	public String searchDoneOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> deliverys = trxDeliveryorderService.searchDoneOrder(dataRequest.getString("orderId") ,  dataRequest.getString("compId"));
			deliverys.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("customerFromId", col[0] == null ? "" : col[0]);
				for(int i = 0; i < datacustomer.size(); i++)
				{
					if(col[0].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
					{
						data.put("customerFromName", datacustomer.getJSONObject(i).getString("contact_name"));
						
					}
					if(col[2] != null)
					{
						for(int j = 0; j < datacustomer.getJSONObject(i).getJSONArray("contact_persons").size(); j++)
						{
							if(col[2].equals(datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getInt("id")))
							{
								data.put("customerFromUserName", datacustomer.getJSONObject(i).getJSONArray("contact_persons").getJSONObject(j).getString("contact_name"));
								break;
							}
						}
						
					}
				}
				data.put("date", col[1] == null ? "" : col[1].toString());
				data.put("customerFromUserId", col[2] == null ? "" : col[2]);
				
				datas.add(data);
			});
			
			response.put("responseCode", "00");
			response.put("responseDesc", "Search Data Done");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Search Data Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	
	@PostMapping("/saveDeliveryorder")
	public String saveDeliveryorder(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			
			LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
			
			TrxDeliveryorder data = new TrxDeliveryorder();
			data.setTmTrxOrderId(dataRequest.getString("orderId"));
			data.setTrxDeliveryorderCompanyId(dataRequest.getString("compId"));
			data.setTrxDeliveryorderBukti(dataRequest.getString("bukti"));
			Optional <TrxOrder> dataupdateorder = trxOrderService.getdetail(dataRequest.getString("orderId"), dataRequest.getString("compId"));
			if(dataupdateorder.isPresent())
			{
				((TrxOrder)dataupdateorder.get()).setTrxOrderStatus("complete");
			}
			List<Object[]> lastTrackingId = trxTrackingService.getLastTrackingId(dataRequest.getString("orderId"), dataRequest.getString("compId"));
			if(lastTrackingId.size()>0)
			{
				Optional<TrxTracking> get = trxTrackingService.getdetail(dataRequest.getString("orderId"), Integer.parseInt(lastTrackingId.get(0)[0].toString()), dataRequest.getString("compId"));
				if(get.isPresent())
				{
					((TrxTracking)get.get()).setTrxTrackingEstimetedTime(localDateTime2);
				}
				trxTrackingService.saveTracking(get.get());
				
				
    			List<Object[]> getLatestId = TrxHistroyTrackingService.getLatestId(dataRequest.getString("orderId"), dataRequest.getString("compId"));
	    		TrxHistroyTracking dataHistory = new TrxHistroyTracking();
	    		dataHistory.setOrderId(dataRequest.getString("orderId"));
	    		dataHistory.setId(getLatestId.size() > 0 ? Integer.parseInt(getLatestId.get(0)[0].toString()) + 1 : 0);
	    		dataHistory.setStatus("Order Completed");				    		
	    		dataHistory.setWaktu(localDateTime2);
	    		dataHistory.setCompanyId(dataRequest.getString("compId"));
	    		TrxHistroyTrackingService.save(dataHistory);
			}
			
			
			
			
			allOrderService.savealldeliveryorder(data, dataupdateorder.get());
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
}
