package com.proacc.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.entity.MVehicletype;
import com.proacc.helper.Helper;
import com.proacc.service.MVehicletypeService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MVehicletypeController {
	private static final Logger logger = LoggerFactory.getLogger(MVehicletypeController.class);
	
	@Autowired
	MVehicletypeService mVehicletypeService;
	
	@PostMapping("/getAllVehicletype/{paramAktif}")
	public String getAllVehicletype(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> vehicletypes = mVehicletypeService.getallVehicletype(paramAktif.toString().toLowerCase(), dataRequest.getString("vehicletypeCompanyId"));
			vehicletypes.stream().forEach(column ->{
				JSONObject data = new JSONObject();
				data.put("vehicletypeId", column[0]);
				data.put("vehicletypeName", column[1] == null ? "" : column[1].toString());
				data.put("vehicletypeType", column[2] == null ? "" : column[2].toString());
				data.put("vehicletypeDescription", column[3] == null ? "" : column[3].toString());
				data.put("vehicletypeCreatedBy", column[4].toString());
				data.put("vehicletypeCreatedAt", column[5].toString());
				data.put("vehicletypeUpdatedBy", column[6] == null ? "" : column[6].toString());
				data.put("vehicletypeUpdatedAt", column[7] == null ? "" : column[7].toString());
				data.put("vehicletypeStatus", column[8].toString());
				data.put("vehicletypeCompanyId", column[9]);
				datas.add(data);
				
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data vehicletype berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data vehicletype gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/getVehicletype/{paramAktif}")
	public String getVehicletype(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> vehicletypes = mVehicletypeService.getVehicletype(paramAktif.toString().toLowerCase(), dataRequest.getInt("vehicletypeId"), dataRequest.getString("vehicletypeCompanyId"));
			vehicletypes.stream().forEach(column ->{
				JSONObject data = new JSONObject();
				data.put("vehicletypeId", column[0]);
				data.put("vehicletypeName", column[1] == null ? "" : column[1].toString());
				data.put("vehicletypeType", column[2] == null ? "" : column[2].toString());
				data.put("vehicletypeDescription", column[3] == null ? "" : column[3].toString());
				data.put("vehicletypeCreatedBy", column[4].toString());
				data.put("vehicletypeCreatedAt", column[5].toString());
				data.put("vehicletypeUpdatedBy", column[6] == null ? "" : column[6].toString());
				data.put("vehicletypeUpdatedAt", column[7] == null ? "" : column[7].toString());
				data.put("vehicletypeStatus", column[8].toString());
				data.put("vehicletypeCompanyId", column[9]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data vehicletype berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data vehicletype gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/saveVehicletype")
	public String saveVehicletype(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	int id = 0;
	    	List<Object[]> dataNext = mVehicletypeService.nextvalMVehicletype();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	MVehicletype data = new MVehicletype();
	    	data.setVehicletypeId(id);
	    	data.setVehicletypeName(dataRequest.getString("vehicletypeName"));
	    	data.setVehicletypeType(dataRequest.getString("vehicletypeType"));
	    	if(dataRequest.has("vehicletypeDescription"))
	    	{
	    		data.setVehicletypeDescription(dataRequest.getString("vehicletypeDescription"));		
	    	}
	    	data.setVehicletypeCreatedBy(user_uuid);
	    	data.setVehicletypeCreatedAt(localDateTime2);
	    	data.setVehicletypeStatus(dataRequest.getBoolean("vehicletypeStatus"));
	    	data.setVehicletypeCompanyId(dataRequest.getString("vehicletypeCompanyId"));
	    	mVehicletypeService.saveVehicletype(data);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save vehicletype success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save vehicletype failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateVehicletype")
	public String updateVehicletype(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	Optional <MVehicletype> dataupdate = mVehicletypeService.getdetail(dataRequest.getInt("vehicletypeId"), dataRequest.getString("vehicletypeCompanyId"));
	    	if(dataupdate.isPresent())
	    	{
	    		if(dataRequest.containsValue(dataRequest.getString("vehicletypeName")))
	    		{
	    			((MVehicletype)dataupdate.get()).setVehicletypeName(dataRequest.getString("vehicletypeName"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getString("vehicletypeType")))
	    		{
	    			((MVehicletype)dataupdate.get()).setVehicletypeType(dataRequest.getString("vehicletypeType"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getString("vehicletypeDescription")))
	    		{
	    			((MVehicletype)dataupdate.get()).setVehicletypeDescription(dataRequest.getString("vehicletypeDescription"));
	    		}
	    		((MVehicletype)dataupdate.get()).setVehicletypeUpdatedBy(user_uuid);
	    		((MVehicletype)dataupdate.get()).setVehicletypeUpdatedAt(localDateTime2);
	    		((MVehicletype)dataupdate.get()).setVehicletypeStatus(dataRequest.getBoolean("vehicletypeStatus"));
	    		mVehicletypeService.saveVehicletype(dataupdate.get());
	    	}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update vehicletype success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Update vehicletype failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
