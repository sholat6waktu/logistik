package com.proacc.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.entity.MAgendetail;
import com.proacc.entity.TrxInvoiceDetail;
import com.proacc.entity.TrxInvoiceHeader;
import com.proacc.entity.TrxInvoicePrice;
import com.proacc.entity.TrxNumber;
import com.proacc.entity.TrxOrder;
import com.proacc.helper.Helper;
import com.proacc.service.MDriverService;
import com.proacc.service.MVolumeService;
import com.proacc.service.MWeightService;
import com.proacc.service.TrxDriverTrackingService;
import com.proacc.service.TrxDriverscheduleService;
import com.proacc.service.TrxHistroyTrackingService;
import com.proacc.service.TrxInboundService;
import com.proacc.service.TrxInvoiceHeaderService;
import com.proacc.service.TrxInvoicePriceService;
import com.proacc.service.TrxJalurDriverService;
import com.proacc.service.TrxNumberService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxInvoiceDetailService;
import com.proacc.service.TrxTrackingService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TrxInvoiceController {
	private static final Logger logger = LoggerFactory.getLogger(TrxInvoiceController.class);
	
	@Autowired
	TrxInvoiceHeaderService TrxInvoiceHeaderService;
	@Autowired
	TrxInvoiceDetailService TrxInvoiceDetailService;
	
	@Autowired
	TrxOrderService TrxOrderService;
	
	@Autowired
	TrxNumberService trxNumberService;
	
	@Autowired
	TrxInvoicePriceService TrxInvoicePriceService;
	
	
	@PostMapping("/listOpenInvoice/{paramAktif}")
	public String listOpenInvoice(@RequestBody String request
			, @RequestHeader(value = "User-Access") String header
			,@PathVariable("paramAktif") String paramAktif
			)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		UUID user_uuid = UUID.fromString(Helper.getUserId(header));
		logger.info("API == listOpenInvoice");
		logger.info("INPUT == " + dataRequest);
		try
		{
			List<Object[]> list = TrxInvoiceHeaderService.listOpenInvoice(user_uuid,paramAktif, dataRequest.getInt("statusDraft"), dataRequest.getString("companyId"));
			if(list.size()>0)
			{
				list.stream().forEach(col->{
					JSONObject data = new JSONObject();
					data.put("noInvoice", col[0] == null ? "" : col[0].toString());
					data.put("customerId", col[1] == null ? "" : col[1].toString());
					data.put("date", col[2] == null ? "" : col[2].toString());
					data.put("grossPrice", col[3] == null ? "" : col[3]);
					data.put("tax", col[4] == null ? "" : col[4]);
					data.put("nettPrice", col[5] == null ? "" : col[5]);
					data.put("createdBy", col[6] == null ? "" : col[6]);
					data.put("status", col[7] == null ? "" : col[7]);
					data.put("taxPercent", col[8] == null ? "" : col[8]);
					data.put("bankName", col[9] == null ? "" : col[9]);
					data.put("bankAccount", col[10] == null ? "" : col[10]);
					data.put("accountName", col[11] == null ? "" : col[11]);
					data.put("nameSignature", col[12] == null ? "" : col[12]);
					data.put("positionSignature", col[13] == null ? "" : col[13]);
					data.put("textSignature", col[14] == null ? "" : col[14]);
					data.put("invoiceId", col[15] == null ? "" : col[15]);
					datas.add(data);
				});
			}
			response.put("responseCode", "00");
		    response.put("responseDesc", "List Open Invoice Success" );
		    response.put("responseData", datas );
		}
		catch(Exception e)
		{
			 response.put("responseCode", "99");
		     response.put("responseDesc", "List Open Invoice Failed" );
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/listPaidInvoice/{paramAktif}")
	public String listPaidInvoice(@RequestBody String request
			, @RequestHeader(value = "User-Access") String header
			,@PathVariable("paramAktif") String paramAktif
			)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		UUID user_uuid = UUID.fromString(Helper.getUserId(header));
		logger.info("API == listPaidInvoice");
		logger.info("INPUT == " + dataRequest);
		try
		{
			List<Object[]> list = TrxInvoiceHeaderService.listPaidInvoice(user_uuid,paramAktif, dataRequest.getString("companyId"));
			if(list.size()>0)
			{
				list.stream().forEach(col->{
					JSONObject data = new JSONObject();
					data.put("noInvoice", col[0] == null ? "" : col[0].toString());
					data.put("customerId", col[1] == null ? "" : col[1].toString());
					data.put("date", col[2] == null ? "" : col[2].toString());
					data.put("grossPrice", col[3] == null ? "" : col[3]);
					data.put("tax", col[4] == null ? "" : col[4]);
					data.put("nettPrice", col[5] == null ? "" : col[5]);
					data.put("createdBy", col[6] == null ? "" : col[6]);
					data.put("status", col[7] == null ? "" : col[7]);
					data.put("taxPercent", col[8] == null ? "" : col[8]);
					data.put("bankName", col[9] == null ? "" : col[9]);
					data.put("bankAccount", col[10] == null ? "" : col[10]);
					data.put("accountName", col[11] == null ? "" : col[11]);
					data.put("nameSignature", col[12] == null ? "" : col[12]);
					data.put("positionSignature", col[13] == null ? "" : col[13]);
					data.put("textSignature", col[14] == null ? "" : col[14]);
					data.put("invoiceId", col[15] == null ? "" : col[15]);
					datas.add(data);
				});
			}
			response.put("responseCode", "00");
		    response.put("responseDesc", "List Paid Invoice Success" );
		    response.put("responseData", datas );
		}
		catch(Exception e)
		{
			 response.put("responseCode", "99");
		     response.put("responseDesc", "List Paid Invoice Failed" );
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/saveInvoice")
	public String saveInvoice(@RequestBody String request
			, @RequestHeader(value = "User-Access") String header
			
			)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		UUID user_uuid = UUID.fromString(Helper.getUserId(header));
		logger.info("API == saveInvoice");
		logger.info("INPUT == " + dataRequest);
		try
		{
			LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
			
	    	ArrayList<TrxInvoiceDetail> dataDetail = new ArrayList<>();
	    	ArrayList<TrxInvoicePrice> dataDetailPrice = new ArrayList<>();
	    	
			List<Object[]> numbers = trxNumberService.getNumber("a", 6,  dataRequest.getString("companyId"));
	    	numbers.stream().forEach(column-> {
		    	Optional <TrxNumber> dataUpdateNumber = trxNumberService.getdetail(Integer.parseInt(column[0].toString()), dataRequest.getString("companyId"));
		    	Integer id = 0;
				List<Object[]> dataNext = TrxInvoiceHeaderService.getId();
		    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
				TrxInvoiceHeader data= new TrxInvoiceHeader();
				data.setInvoiceHeaderId(id);
				data.setInvoiceNomor(column[6].toString());
				data.setCustomerName(dataRequest.getInt("customerId"));
				data.setDateTanggal(LocalDate.parse(dataRequest.getString("dateTanggal")));
				data.setTaxPercent(dataRequest.getInt("taxPercent"));
				data.setNettPrice(dataRequest.getInt("nettPrice"));
				data.setStatus(dataRequest.getBoolean("status"));
				data.setCreatedBy(user_uuid);
				data.setCreatedAt(localDateTime2);
				data.setCompanyId(dataRequest.getString("companyId"));
				data.setGrossPrice(dataRequest.getInt("grossPrice"));
				data.setStatusDraft(dataRequest.getInt("statusDraft"));
				data.setStatusInvoice(dataRequest.getInt("statusInvoice"));
				data.setTaxNominal(dataRequest.getInt("taxNominal"));
				data.setBankName(dataRequest.getString("bankName"));
				data.setBankAccount(dataRequest.getString("bankAccount"));
				data.setAccountName(dataRequest.getString("accountName"));
				data.setNameSignature(dataRequest.getString("nameSignature"));
				data.setPositionSignature(dataRequest.getString("positionSignature"));
				data.setTextSignature(dataRequest.getString("textSignature"));
				
				
				
				JSONArray coverareaArray = dataRequest.getJSONArray("detail");
    	    	if(coverareaArray.size() > 0)
    	    	{
    	    		for(int k = 0; k < coverareaArray.size(); k++)
    	    		{
    	    			TrxInvoiceDetail data2 = new TrxInvoiceDetail();
    	    			data2.setInvoiceHeaderId(id);
    	    			data2.setInvoiceDetailId(coverareaArray.getJSONObject(k).getInt("invoiceDetailId"));
    	    			data2.setCompanyId(dataRequest.getString("companyId"));
    	    			data2.setStatus(coverareaArray.getJSONObject(k).getBoolean("status"));
    	    			data2.setOrderId(coverareaArray.getJSONObject(k).getString("orderId"));
    	    			dataDetail.add(data2);
    	    			
    	    			Optional <TrxOrder> updateOrder = TrxOrderService.getdetail(coverareaArray.getJSONObject(k).getString("orderId"), dataRequest.getString("companyId"));
    	    			((TrxOrder)updateOrder.get()).setTmTrxInvoiceId(id);
    	    			TrxOrderService.saveOrder(updateOrder.get());
    	    		}
    	    	}
    	    	
    	    	JSONArray detailPrice = dataRequest.getJSONArray("detailPrice");
    	    	if(detailPrice.size() > 0)
    	    	{
    	    		for(int k = 0; k < detailPrice.size(); k++)
    	    		{
    	    			TrxInvoicePrice data2 = new TrxInvoicePrice();
    	    			data2.setInvoiceHeaderId(id);
    	    			data2.setInvoiceDetailTransaksiId(detailPrice.getJSONObject(k).getInt("invoiceDetailPriceId"));
    	    			data2.setCompanyId(dataRequest.getString("companyId"));
    	    			data2.setNominal(detailPrice.getJSONObject(k).getInt("nominal"));
    	    			data2.setKeterangan(detailPrice.getJSONObject(k).getString("keterangan"));
    	    			data2.setTanda(detailPrice.getJSONObject(k).getString("tanda"));
    	    			data2.setStatus(detailPrice.getJSONObject(k).getBoolean("status"));
    	    			dataDetailPrice.add(data2);
    	    		}
    	    	}
    	    	logger.info("masuk lur");
    	    	((TrxNumber)dataUpdateNumber.get()).setTrxNumberRunningnumber(Integer.parseInt(column[3].toString()) + 1);
    	    	trxNumberService.saveNumber(dataUpdateNumber.get());
    	    	TrxInvoiceHeaderService.save(data);
    	    	TrxInvoiceDetailService.saveDetail(dataDetail);
    	    	TrxInvoicePriceService.saveDetail(dataDetailPrice);
    	    	
//				data.setTaxPercent(dataRequest.getInt("discount"));
	    	});
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Invoice Success" );
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		     response.put("responseDesc", "Save Invoice Failed" );
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/changeStatusDraftAndInvoice")
	public String changeStatusDraft(@RequestBody String request
			, @RequestHeader(value = "User-Access") String header
			
			)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		UUID user_uuid = UUID.fromString(Helper.getUserId(header));
		logger.info("API == changeStatusDraft");
		logger.info("INPUT == " + dataRequest);
		try
		{
			LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
			
	    	ArrayList<TrxInvoiceDetail> dataDetail = new ArrayList<>();
	    	
	    	Optional<TrxInvoiceHeader> detailHeader = TrxInvoiceHeaderService.getDetail(dataRequest.getInt("invoiceHeader"),dataRequest.getString("companyId"));
	    	((TrxInvoiceHeader)detailHeader.get()).setStatusDraft(dataRequest.getInt("statusDraft"));
	    	((TrxInvoiceHeader)detailHeader.get()).setStatusInvoice(dataRequest.getInt("statusInvoice"));
	    	TrxInvoiceHeaderService.save(detailHeader.get());
	    	
	    	response.put("responseCode", "00");
		     response.put("responseDesc", "Change Status Draft Success" );
		}
		
		catch(Exception e)
		{
			response.put("responseCode", "99");
		     response.put("responseDesc", "Change Status Draft Failed" );
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/updateInvoice")
	public String updateInvoice(@RequestBody String request
			, @RequestHeader(value = "User-Access") String header
			
			)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		UUID user_uuid = UUID.fromString(Helper.getUserId(header));
		logger.info("API == updateInvoice");
		logger.info("INPUT == " + dataRequest);
		try
		{
			LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
			
	    	ArrayList<TrxInvoiceDetail> dataDetail = new ArrayList<>();
	    	ArrayList<TrxInvoicePrice> dataDetailPrice = new ArrayList<>();
	    	
	    	Optional<TrxInvoiceHeader> detailHeader = TrxInvoiceHeaderService.getDetail(dataRequest.getInt("invoiceHeader"),dataRequest.getString("companyId"));
	    	((TrxInvoiceHeader)detailHeader.get()).setCustomerName(dataRequest.getInt("customerId"));
	    	((TrxInvoiceHeader)detailHeader.get()).setDateTanggal(LocalDate.parse(dataRequest.getString("dateTanggal")));
	    	((TrxInvoiceHeader)detailHeader.get()).setTaxPercent(dataRequest.getInt("taxPercent"));
	    	((TrxInvoiceHeader)detailHeader.get()).setNettPrice(dataRequest.getInt("nettPrice"));
	    	((TrxInvoiceHeader)detailHeader.get()).setStatus(dataRequest.getBoolean("status"));
	    	((TrxInvoiceHeader)detailHeader.get()).setUpdatedBy(user_uuid);
	    	((TrxInvoiceHeader)detailHeader.get()).setUpdatedAt(localDateTime2);
	    	((TrxInvoiceHeader)detailHeader.get()).setGrossPrice(dataRequest.getInt("grossPrice"));
	    	((TrxInvoiceHeader)detailHeader.get()).setStatusDraft(dataRequest.getInt("statusDraft"));
	    	((TrxInvoiceHeader)detailHeader.get()).setStatusInvoice(dataRequest.getInt("statusInvoice"));
	    	((TrxInvoiceHeader)detailHeader.get()).setTaxNominal(dataRequest.getInt("taxNominal"));
	    	((TrxInvoiceHeader)detailHeader.get()).setBankName(dataRequest.getString("bankName"));
	    	((TrxInvoiceHeader)detailHeader.get()).setBankAccount(dataRequest.getString("bankAccount"));
	    	((TrxInvoiceHeader)detailHeader.get()).setAccountName(dataRequest.getString("accountName"));
	    	((TrxInvoiceHeader)detailHeader.get()).setNameSignature(dataRequest.getString("nameSignature"));
	    	((TrxInvoiceHeader)detailHeader.get()).setPositionSignature(dataRequest.getString("positionSignature"));
	    	((TrxInvoiceHeader)detailHeader.get()).setTextSignature(dataRequest.getString("textSignature"));
	    	JSONArray coverareaArray = dataRequest.getJSONArray("detail");
	    	if(coverareaArray.size() > 0)
	    	{
	    		List<Object[]> list = TrxInvoiceDetailService.detailInvoice("y", dataRequest.getString("companyId"), dataRequest.getInt("invoiceHeader"));
	    		if(list.size()>0)
				{
					list.stream().forEach(col->{
						
						Optional<TrxInvoiceDetail> getDetail = TrxInvoiceDetailService.getDetail(Integer.parseInt(col[7].toString()),Integer.parseInt(col[8].toString()),dataRequest.getString("companyId") );
						((TrxInvoiceDetail)getDetail.get()).setStatus(false);
						TrxInvoiceDetailService.save(getDetail.get());
						
						Optional <TrxOrder> updateOrder = TrxOrderService.getdetail(col[3].toString(), dataRequest.getString("companyId"));
    	    			((TrxOrder)updateOrder.get()).setTmTrxInvoiceId(null);
    	    			TrxOrderService.saveOrder(updateOrder.get());
					});
				}
	    		
	    		for(int k = 0; k < coverareaArray.size(); k++)
	    		{
	    			TrxInvoiceDetail data2 = new TrxInvoiceDetail();
	    			data2.setInvoiceHeaderId(dataRequest.getInt("invoiceHeader"));
	    			data2.setInvoiceDetailId(coverareaArray.getJSONObject(k).getInt("invoiceDetailId"));
	    			data2.setCompanyId(dataRequest.getString("companyId"));
	    			data2.setStatus(coverareaArray.getJSONObject(k).getBoolean("status"));
	    			data2.setOrderId(coverareaArray.getJSONObject(k).getString("orderId"));
	    			dataDetail.add(data2);
	    			
	    			Optional <TrxOrder> updateOrder = TrxOrderService.getdetail(coverareaArray.getJSONObject(k).getString("orderId"), dataRequest.getString("companyId"));
	    			((TrxOrder)updateOrder.get()).setTmTrxInvoiceId(dataRequest.getInt("invoiceHeader"));
	    			TrxOrderService.saveOrder(updateOrder.get());
	    		}
	    		
	    		JSONArray detailPrice = dataRequest.getJSONArray("detailPrice");
    	    	if(detailPrice.size() > 0)
    	    	{
    	    		for(int k = 0; k < detailPrice.size(); k++)
    	    		{
    	    			TrxInvoicePrice data2 = new TrxInvoicePrice();
    	    			data2.setInvoiceHeaderId(dataRequest.getInt("invoiceHeader"));
    	    			data2.setInvoiceDetailTransaksiId(detailPrice.getJSONObject(k).getInt("invoiceDetailPriceId"));
    	    			data2.setCompanyId(dataRequest.getString("companyId"));
    	    			data2.setNominal(detailPrice.getJSONObject(k).getInt("nominal"));
    	    			data2.setKeterangan(detailPrice.getJSONObject(k).getString("keterangan"));
    	    			data2.setTanda(detailPrice.getJSONObject(k).getString("tanda"));
    	    			data2.setStatus(detailPrice.getJSONObject(k).getBoolean("status"));
    	    			dataDetailPrice.add(data2);
    	    		}
    	    	}
	    	}
	    	TrxInvoiceHeaderService.save(detailHeader.get());
	    	TrxInvoiceDetailService.saveDetail(dataDetail);
	    	TrxInvoicePriceService.saveDetail(dataDetailPrice);
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Invoice Success" );
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		     response.put("responseDesc", "Update Invoice Failed" );
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/detailInvoice/{paramAktif}")
	public String detailInvoice(@RequestBody String request
			, @RequestHeader(value = "User-Access") String header
			,@PathVariable("paramAktif") String paramAktif
			)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		UUID user_uuid = UUID.fromString(Helper.getUserId(header));
		logger.info("API == detailInvoice");
		logger.info("INPUT == " + dataRequest);
		try
		{
			List<Object[]> list = TrxInvoiceDetailService.detailInvoice(paramAktif, dataRequest.getString("companyId"), dataRequest.getInt("invoiceHeader"));
			if(list.size()>0)
			{
				list.stream().forEach(col->{
					JSONObject data = new JSONObject();
					data.put("customerId", col[0] == null ? "" : col[0].toString());
					data.put("customerKotaId", col[1] == null ? "" : col[1].toString());
					data.put("date", col[2] == null ? "" : col[2].toString());
					data.put("orderId", col[3] == null ? "" : col[3]);
					data.put("price", col[4] == null ? "" : col[4]);
					data.put("payment", col[5] != null ?  Integer.parseInt(col[5].toString()) == 1 ? "Tunai" : "Non Tunai" : "Tunai");
					data.put("orderStatus", col[6] == null ? "" : col[6]);
					data.put("invoiceId", col[7] == null ? "" : col[7]);
					data.put("invoiceDetailId", col[8] == null ? "" : col[8]);
					data.put("status", col[9] == null ? "" : col[9]);
					data.put("customerKotaIdTo", col[10] == null ? "" : col[10]);
					datas.add(data);
				});
			}
			response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Invoice Success" );
		    response.put("responseData", datas );
		}
		catch(Exception e)
		{
			 response.put("responseCode", "99");
		     response.put("responseDesc", "Detail Invoice Failed" );
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/detailPrice/{paramAktif}")
	public String detailPrice(@RequestBody String request
			, @RequestHeader(value = "User-Access") String header
			,@PathVariable("paramAktif") String paramAktif
			)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		UUID user_uuid = UUID.fromString(Helper.getUserId(header));
		logger.info("API == detailPrice");
		logger.info("INPUT == " + dataRequest);
		try
		{
			List<Object[]> list = TrxInvoicePriceService.detailPrice(paramAktif, dataRequest.getString("companyId"), dataRequest.getInt("invoiceHeader"));
			if(list.size()>0)
			{
				list.stream().forEach(col->{
					
					
					JSONObject data = new JSONObject();
					data.put("invoiceHeaderId", col[0] == null ? "" : col[0].toString());
					data.put("invoiceDetailTransaksiId", col[1] == null ? "" : col[1].toString());
					data.put("nominal", col[2] == null ? "" : col[2]);
					data.put("keterangan", col[3] == null ? "" : col[3]);
					data.put("tanda", col[4] == null ? "" : col[4]);
					data.put("companyId", col[5] == null ? "" : col[5]);
					data.put("status", col[6] == null ? "" : col[6]);
					datas.add(data);
				});
			}
			response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Invoice Price Success" );
		    response.put("responseData", datas );
		}
		catch(Exception e)
		{
			 response.put("responseCode", "99");
		     response.put("responseDesc", "Detail Invoice Price Failed" );
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	


}
