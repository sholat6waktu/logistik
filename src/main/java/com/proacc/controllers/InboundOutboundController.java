package com.proacc.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MDriver;
import com.proacc.entity.TrxDriverTracking;
import com.proacc.entity.TrxDriverschedule;
import com.proacc.entity.TrxHistroyTracking;
import com.proacc.entity.TrxInbound;
import com.proacc.entity.TrxJalurDriver;
import com.proacc.entity.TrxOutbound;
import com.proacc.entity.TrxTracking;
import com.proacc.helper.Helper;
import com.proacc.service.MCabangdetailService;
import com.proacc.service.MDriverService;
import com.proacc.service.MVolumeService;
import com.proacc.service.MWeightService;
import com.proacc.service.TrxDriverTrackingService;
import com.proacc.service.TrxDriverscheduleService;
import com.proacc.service.TrxJalurDriverService;
import com.proacc.service.TrxKolidetailService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxPartdetailService;
import com.proacc.service.TrxTrackingService;
import com.proacc.service.TrxHistroyTrackingService;
import com.proacc.service.TrxInboundService;
import com.proacc.service.TrxOutboundService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class InboundOutboundController {
	private static final Logger logger = LoggerFactory.getLogger(InboundOutboundController.class);
	@Autowired
	TrxOrderService trxOrderService;
	@Autowired
	TrxKolidetailService trxKolidetailService;
	@Autowired
	TrxPartdetailService trxPartdetailService;
	@Autowired
	TrxTrackingService trxTrackingService;
	@Autowired
	MDriverService mDriverService;
	@Autowired
	TrxDriverscheduleService trxDriverscheduleService;
	@Autowired
	MWeightService mWeightService;
	@Autowired
	MVolumeService mVolumeService;
	@Autowired
	MCabangdetailService MCabangdetailService;
	@Autowired
	TrxJalurDriverService TrxJalurDriverService;
	@Autowired
	TrxDriverTrackingService TrxDriverTrackingService;
	@Autowired
	TrxHistroyTrackingService TrxHistroyTrackingService;
	
	@Autowired
	TrxInboundService TrxInboundService;
	
	@Autowired
	MDriverService MDriverService;
	
	@Autowired
	TrxOutboundService TrxOutboundService;
	
	@PostMapping("/depart")
	public String depart(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT = " + request);
		logger.info("API = depart");
		
		LocalDateTime localDateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        String formatDateTime = localDateTime.format(formatter);
        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
		try
		{
			Optional<MDriver> updateDepart = MDriverService.getdetail(dataRequest.getInt("driverId"), dataRequest.getString("companyId"));
			((MDriver)updateDepart.get()).setStatusCondition(dataRequest.getInt("statusCondition"));
			MDriverService.saveDriver(updateDepart.get());
			response.put("responseCode", "00");
		    response.put("responseDesc", "Driver Depart Success");	
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		     response.put("responseDesc", "Driver Depart Failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/finishDriver")
	public String finishDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT = " + request);
		logger.info("API = depart");
		
		LocalDateTime localDateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        String formatDateTime = localDateTime.format(formatter);
        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
		try
		{

			Optional<MDriver> updateDepart = MDriverService.getdetail(dataRequest.getInt("driverId"), dataRequest.getString("companyId"));
			((MDriver)updateDepart.get()).setPositionCabangId(null);
			((MDriver)updateDepart.get()).setPositionAgenId(null);
			((MDriver)updateDepart.get()).setPositionAgenDetailId(null);
			((MDriver)updateDepart.get()).setStatusCondition(0);
			((MDriver)updateDepart.get()).setTrxJalurdriverheaderId(null);;
			MDriverService.saveDriver(updateDepart.get());
			
			response.put("responseCode", "00");
		     response.put("responseDesc", "Finish Driver Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		     response.put("responseDesc", "Finish Driver Failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/acceptinbound")
	public String acceptinbound(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT = " + request);
		logger.info("API = acceptinbound");
		
		LocalDateTime localDateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        String formatDateTime = localDateTime.format(formatter);
        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
		try
		{
			List<Object[]> drivers = mDriverService.getDriverAlltime("a", UUID.fromString(Helper.getUserId(header)), dataRequest.getString("driverschedulecompanyId"));
			logger.info("drivers = " + drivers.size());
			if(!drivers.isEmpty())
			{
				JSONArray driverscheduleArray = dataRequest.getJSONArray("driverscheduleArray");
				if(driverscheduleArray.size() > 0)
				{
					ArrayList <TrxDriverschedule> datasdriverschedule = new ArrayList<>();
		    		for(int j = 0; j < driverscheduleArray.size(); j++)
		    		{
						Optional <TrxDriverschedule> dataUpdatedriverschedule = trxDriverscheduleService.getdetail(driverscheduleArray.getJSONObject(j).getString("OrderId"), driverscheduleArray.getJSONObject(j).getInt("DriverscheduleId"), dataRequest.getString("driverschedulecompanyId"));
				    	if(dataUpdatedriverschedule.isPresent())
				    	{
				    		((TrxDriverschedule)dataUpdatedriverschedule.get()).setTrxDriverscheduleStatus(null);
				    		((TrxDriverschedule)dataUpdatedriverschedule.get()).setTrxDriverscheduleStatusoutbound("on Proggress");
				    		((TrxDriverschedule)dataUpdatedriverschedule.get()).setTglBerangkat(localDateTime2);
				    		
				    		
				    		
				    		//from
				    		Optional<TrxTracking> getDetailFrom = trxTrackingService.getdetail(driverscheduleArray.getJSONObject(j).getString("OrderId"), dataUpdatedriverschedule.get().getTmTrxTrackingIdFrom(), dataUpdatedriverschedule.get().getTrxDriverscheduleCompanyId());
				    		logger.info("getDetailFrom = " + getDetailFrom.isPresent());
				    		if(getDetailFrom.isPresent())
				    		{
				    			Optional<TrxJalurDriver> getDetailJalurDriver = TrxJalurDriverService.getDetail(getDetailFrom.get().getTrxDriverjalurheaderId(), getDetailFrom.get().getTrxDriverjalurId(), getDetailFrom.get().getTrxTrackingCompanyId());
				    			if(getDetailJalurDriver.isPresent())
				    			{
				    				
				    				((TrxJalurDriver)getDetailJalurDriver.get()).setStatusProgress(2);
				    				((TrxJalurDriver)getDetailJalurDriver.get()).setDateTracking(localDateTime2);
				    			}
				    			
		    					Optional<MDriver> getDetailDriver = MDriverService.getdetail(dataUpdatedriverschedule.get().getmDriverId(), dataRequest.getString("driverschedulecompanyId"));
			    				
		    					if(getDetailFrom.get().getmCabangId() != null)
		    					{
		    						((MDriver)getDetailDriver.get()).setPositionCabangId(Integer.parseInt(getDetailFrom.get().getmCabangId().toString()));
		    					}
		    					
		    					if(getDetailFrom.get().getmAgenId() != null)
		    					{
		    						((MDriver)getDetailDriver.get()).setPositionAgenId(getDetailFrom.get().getmAgenId());
		    					}
		    					
		    					if(getDetailFrom.get().getmAgenDetailId() != null)
		    					{
		    						((MDriver)getDetailDriver.get()).setPositionAgenDetailId(getDetailFrom.get().getmAgenDetailId());
		    					}
		    					
		    					((MDriver)getDetailDriver.get()).setStatusCondition(1);
		    					MDriverService.saveDriver(getDetailDriver.get());
				    				
			    				
				    		}
				    		
				    		//to
				    		Optional<TrxTracking> getDetailTo = trxTrackingService.getdetail(driverscheduleArray.getJSONObject(j).getString("OrderId"), dataUpdatedriverschedule.get().getTmTrxTrackingIdTo(), dataUpdatedriverschedule.get().getTrxDriverscheduleCompanyId());
				    		if(getDetailTo.isPresent())
				    		{
				    			Optional<TrxJalurDriver> getDetailJalurDriverTo = TrxJalurDriverService.getDetail(getDetailTo.get().getTrxDriverjalurheaderId(), getDetailTo.get().getTrxDriverjalurId(), getDetailTo.get().getTrxTrackingCompanyId());
				    			if(getDetailJalurDriverTo.isPresent())
				    			{
				    				((TrxJalurDriver)getDetailJalurDriverTo.get()).setStatusProgress(2);
				    			}
				    			List<Object[]> getLastId = trxTrackingService.getLastTrackingId(driverscheduleArray.getJSONObject(j).getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
				    			List<Object[]> getLatestId = TrxHistroyTrackingService.getLatestId(driverscheduleArray.getJSONObject(j).getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
					    		TrxHistroyTracking dataHistory = new TrxHistroyTracking();
					    		dataHistory.setOrderId(driverscheduleArray.getJSONObject(j).getString("OrderId"));
					    		dataHistory.setId(getLatestId.size() > 0 ? Integer.parseInt(getLatestId.get(0)[0].toString()) + 1 : 0);
					    		dataHistory.setStatus(Integer.parseInt(getLastId.get(0)[0].toString()) == getDetailTo.get().getTrxTrackingId() ? "On The Way Customer" : "On The Way");
					    		if(Integer.parseInt(getLastId.get(0)[0].toString()) != getDetailTo.get().getTrxTrackingId())
								{
					    			dataHistory.setCabangId(getDetailTo.get().getmCabangId());
						    		dataHistory.setAgenId(getDetailTo.get().getmAgenId());
						    		dataHistory.setAgendetailId(getDetailTo.get().getmAgenDetailId());
								}					    		
					    		dataHistory.setWaktu(localDateTime2);
					    		dataHistory.setCompanyId(dataRequest.getString("driverschedulecompanyId"));
					    		TrxHistroyTrackingService.save(dataHistory);
				    		}
				    		
				    		//from driver schedule
				    		Optional<TrxDriverTracking> getDetailTrackingFrom = TrxDriverTrackingService.getDetail(driverscheduleArray.getJSONObject(j).getString("OrderId"), dataUpdatedriverschedule.get().getTmTrxTrackingIdFrom(), dataUpdatedriverschedule.get().getTrxDriverscheduleCompanyId());
				    		if(getDetailTrackingFrom.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingFrom.get()).setStatus(2);
				    			((TrxDriverTracking)getDetailTrackingFrom.get()).setDateTracking(localDateTime2);
				    		}
				    		
				    		//to driver schedule
				    		Optional<TrxDriverTracking> getDetailTrackingTo = TrxDriverTrackingService.getDetail(driverscheduleArray.getJSONObject(j).getString("OrderId"), dataUpdatedriverschedule.get().getTmTrxTrackingIdTo(), dataUpdatedriverschedule.get().getTrxDriverscheduleCompanyId());
				    		if(getDetailTrackingTo.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingTo.get()).setStatus(2);
				    			
				    		}
				    		
				    		TrxOutbound dataOutbound = new TrxOutbound();
					    	List<Object[]> NoBaru = TrxOutboundService.getId();
			    			int id = 0;
			    	    	id = Integer.parseInt(String.valueOf(NoBaru.get(0)));
			    	    	
			    	    	dataOutbound.setReportOutboundId(id);
					    	dataOutbound.setDateOutbound(localDateTime2);
//					    	dataOutbound.setTrxDriverscheduleId(driverscheduleArray.getJSONObject(j).getInt("DriverscheduleId"));
					    	dataOutbound.setTrxTrackingFromId(dataUpdatedriverschedule.get().getTmTrxTrackingIdFrom());
					    	dataOutbound.setTrxTrackingToId(dataUpdatedriverschedule.get().getTmTrxTrackingIdTo());
					    	dataOutbound.setTrxOrderId(driverscheduleArray.getJSONObject(j).getString("OrderId"));
					    	dataOutbound.setCompanyId(dataRequest.getString("driverschedulecompanyId"));
					    	TrxOutboundService.save(dataOutbound);
					    	
				    		datasdriverschedule.add(dataUpdatedriverschedule.get());
				    	}
				    	
		    		}
			    	trxDriverscheduleService.savealldriverschedule(datasdriverschedule);
				}
		    	response.put("responseCode", "00");
			    response.put("responseDesc", "accept pickup success");	
			}
			else
			{
				logger.info("masuk ke cabang");
				JSONArray trackingArray = dataRequest.getJSONArray("driverscheduleArray");
				ArrayList <TrxTracking> datastracking = new ArrayList<>();
	    		for(int j = 0; j < trackingArray.size(); j++)
	    		{
	    			Optional <TrxTracking> dataUpdatetrackingto = trxTrackingService.getdetail(trackingArray.getJSONObject(j).getString("OrderId"), trackingArray.getJSONObject(j).getInt("DriverscheduleId"), dataRequest.getString("driverschedulecompanyId"));
	    			Optional <TrxTracking> dataUpdatetrackingfrom = trxTrackingService.getdetail(trackingArray.getJSONObject(j).getString("OrderId"), trackingArray.getJSONObject(j).getInt("DriverscheduleId") - 1, dataRequest.getString("driverschedulecompanyId"));
	    			if(dataUpdatetrackingto.isPresent())
	    			{
	    				
	    				List<Object[]> trackings = trxDriverscheduleService.getDriverschedulebytrackingtocheckstat("a", trackingArray.getJSONObject(j).getString("OrderId"), trackingArray.getJSONObject(j).getInt("DriverscheduleId"), dataRequest.getString("driverschedulecompanyId"));
	    				if(trackings.isEmpty())
	    				{
	    					((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingStatusinbound("waiting response");
							((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingStatus(null);
							((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingStatusoutbound(null);
							
							//from
			    			Optional<TrxJalurDriver> getDetailJalurDriverFrom = TrxJalurDriverService.getDetail(dataUpdatetrackingfrom.get().getTrxDriverjalurheaderId(), dataUpdatetrackingfrom.get().getTrxDriverjalurId(), dataUpdatetrackingfrom.get().getTrxTrackingCompanyId());
			    			if(getDetailJalurDriverFrom.isPresent())
			    			{
			    				((TrxJalurDriver)getDetailJalurDriverFrom.get()).setStatusProgress(2);
			    			}
			    			
				    		//to
			    			Optional<TrxJalurDriver> getDetailJalurDriver = TrxJalurDriverService.getDetail(dataUpdatetrackingto.get().getTrxDriverjalurheaderId(), dataUpdatetrackingto.get().getTrxDriverjalurId(), dataUpdatetrackingto.get().getTrxTrackingCompanyId());
			    			if(getDetailJalurDriver.isPresent())
			    			{
			    				((TrxJalurDriver)getDetailJalurDriver.get()).setStatusProgress(2);
			    			}
			    			
			    			//from driver tracking
				    		Optional<TrxDriverTracking> getDetailTrackingFrom = TrxDriverTrackingService.getDetail(trackingArray.getJSONObject(j).getString("OrderId"), dataUpdatetrackingfrom.get().getTrxTrackingId(), dataUpdatetrackingfrom.get().getTrxTrackingCompanyId());
				    		if(getDetailTrackingFrom.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingFrom.get()).setStatus(3);
				    		}
				    		
				    		//to driver schedule
				    		Optional<TrxDriverTracking> getDetailTrackingTo = TrxDriverTrackingService.getDetail(trackingArray.getJSONObject(j).getString("OrderId"), dataUpdatetrackingto.get().getTrxTrackingId(), dataUpdatetrackingto.get().getTrxTrackingCompanyId());
				    		if(getDetailTrackingTo.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingTo.get()).setStatus(3);
				    		}
	    				}
	    				else
	    				{
							((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingStatusinbound("ware house");
							((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingEstimetedTime(LocalDateTime.now());
							((TrxTracking)dataUpdatetrackingfrom.get()).setTrxTrackingStatusinbound(null);
							((TrxTracking)dataUpdatetrackingfrom.get()).setTrxTrackingStatusoutbound("complete");
							
							List<Object[]> getLatestId = TrxHistroyTrackingService.getLatestId(trackingArray.getJSONObject(j).getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
				    		TrxHistroyTracking dataHistory = new TrxHistroyTracking();
				    		dataHistory.setOrderId(trackingArray.getJSONObject(j).getString("OrderId"));
				    		dataHistory.setId(getLatestId.size() > 0 ? Integer.parseInt(getLatestId.get(0)[0].toString()) + 1 : 0);
				    		dataHistory.setStatus("Inbound");
				    		dataHistory.setCabangId(dataUpdatetrackingto.get().getmCabangId());
				    		dataHistory.setAgenId(dataUpdatetrackingto.get().getmAgenId());
				    		dataHistory.setAgendetailId(dataUpdatetrackingto.get().getmAgenDetailId());
				    		dataHistory.setWaktu(localDateTime2);
				    		dataHistory.setCompanyId(dataRequest.getString("driverschedulecompanyId"));
				    		TrxHistroyTrackingService.save(dataHistory);
							
							//from driver tracking
				    		Optional<TrxDriverTracking> getDetailTrackingFrom = TrxDriverTrackingService.getDetail(trackingArray.getJSONObject(j).getString("OrderId"), dataUpdatetrackingfrom.get().getTrxTrackingId(), dataUpdatetrackingfrom.get().getTrxTrackingCompanyId());
				    		if(getDetailTrackingFrom.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingFrom.get()).setStatus(3);
				    		}
				    		
				    		//to driver schedule
				    		Optional<TrxDriverTracking> getDetailTrackingTo = TrxDriverTrackingService.getDetail(trackingArray.getJSONObject(j).getString("OrderId"), dataUpdatetrackingto.get().getTrxTrackingId(), dataUpdatetrackingto.get().getTrxTrackingCompanyId());
				    		if(getDetailTrackingTo.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingTo.get()).setStatus(3);
				    			((TrxDriverTracking)getDetailTrackingTo.get()).setDateKantor(localDateTime2);
				    			
				    		}
				    		
				    		//from
			    			Optional<TrxJalurDriver> getDetailJalurDriverFrom = TrxJalurDriverService.getDetail(dataUpdatetrackingfrom.get().getTrxDriverjalurheaderId(), dataUpdatetrackingfrom.get().getTrxDriverjalurId(), dataUpdatetrackingfrom.get().getTrxTrackingCompanyId());
			    			if(getDetailJalurDriverFrom.isPresent())
			    			{
			    				((TrxJalurDriver)getDetailJalurDriverFrom.get()).setStatusProgress(3);
			    			}
			    			
				    		//to
			    			Optional<TrxJalurDriver> getDetailJalurDriver = TrxJalurDriverService.getDetail(dataUpdatetrackingto.get().getTrxDriverjalurheaderId(), dataUpdatetrackingto.get().getTrxDriverjalurId(), dataUpdatetrackingto.get().getTrxTrackingCompanyId());
			    			if(getDetailJalurDriver.isPresent())
			    			{
			    				((TrxJalurDriver)getDetailJalurDriver.get()).setStatusProgress(3);
				    			((TrxJalurDriver)getDetailJalurDriver.get()).setDateKantor(localDateTime2);
				    			
			    			}
			    			
			    			List<Object[]> NoBaru = TrxInboundService.getId();
			    			int id = 0;
			    	    	id = Integer.parseInt(String.valueOf(NoBaru.get(0)));
			    			TrxInbound dataInboundHistory = new TrxInbound();
			    			dataInboundHistory.setTrxOrderId(trackingArray.getJSONObject(j).getString("OrderId"));
//			    			dataInboundHistory.setTrxDriverscheduleId(trackingArray.getJSONObject(j).getInt("DriverscheduleId") - 1);
			    			dataInboundHistory.setTrxTrackingFromId(dataUpdatetrackingfrom.get().getTrxTrackingId());
			    			dataInboundHistory.setTrxTrackingToId(dataUpdatetrackingto.get().getTrxTrackingId());
			    			dataInboundHistory.setCompanyId(dataRequest.getString("driverschedulecompanyId"));
			    			dataInboundHistory.setDateInbound(localDateTime2);
				    		dataInboundHistory.setReportInboundId(id);
				    		
				    		TrxInboundService.save(dataInboundHistory);
				    		
		    				trxDriverscheduleService.updatedriver(trackingArray.getJSONObject(j).getString("OrderId"), trackingArray.getJSONObject(j).getInt("DriverscheduleId"), dataRequest.getString("driverschedulecompanyId"), "complete", null);	
	    				}
	    				
	    				
	    				
	    				datastracking.add(dataUpdatetrackingto.get());
	    				datastracking.add(dataUpdatetrackingfrom.get());
	    			}
	    		}
	    		trxTrackingService.saveAllTracking(datastracking);
		    	response.put("responseCode", "00");
			    response.put("responseDesc", "accept order success");
			}
		}
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "accept pickup failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/driverupdateoutbound")
	public String driverupdateoutbound(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		
		LocalDateTime localDateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        String formatDateTime = localDateTime.format(formatter);
        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
		try
		{
			JSONArray driverscheduleArray = dataRequest.getJSONArray("driverscheduleArray");
			if(driverscheduleArray.size() > 0)
			{
				ArrayList <TrxTracking> datastracking = new ArrayList<>();
				ArrayList <TrxDriverschedule> datasdriverschedulue = new ArrayList<>();
	    		for(int j = 0; j < driverscheduleArray.size(); j++)
	    		{
	    			Optional <TrxDriverschedule> dataUpdatedriverschedule = trxDriverscheduleService.getdetail(driverscheduleArray.getJSONObject(j).getString("OrderId"), driverscheduleArray.getJSONObject(j).getInt("DriverscheduleId"), dataRequest.getString("driverschedulecompanyId"));
	    			if(dataUpdatedriverschedule.isPresent())
	    			{
	    				Optional <TrxTracking> dataUpdatetrackingto = trxTrackingService.getdetail(driverscheduleArray.getJSONObject(j).getString("OrderId"),((TrxDriverschedule)dataUpdatedriverschedule.get()).getTmTrxTrackingIdTo(), dataRequest.getString("driverschedulecompanyId"));
	    				Optional <TrxTracking> dataUpdatetrackingfrom = trxTrackingService.getdetail(driverscheduleArray.getJSONObject(j).getString("OrderId"),((TrxDriverschedule)dataUpdatedriverschedule.get()).getTmTrxTrackingIdFrom(), dataRequest.getString("driverschedulecompanyId"));
	    				if(((TrxTracking)dataUpdatetrackingto.get()).getTrxTrackingStatusinbound().equals("waiting response"))
	    				{
	    					((TrxDriverschedule)dataUpdatedriverschedule.get()).setTrxDriverscheduleStatusoutbound("complete");
	    					((TrxDriverschedule)dataUpdatedriverschedule.get()).setTrxDriverscheduleStatus(null);
	    					((TrxDriverschedule)dataUpdatedriverschedule.get()).setTglTiba(localDateTime2);
	    					((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingStatusinbound("ware house");
	    					((TrxTracking)dataUpdatetrackingto.get()).setTrxTrackingEstimetedTime(LocalDateTime.now());
	    					((TrxTracking)dataUpdatetrackingfrom.get()).setTrxTrackingStatusinbound(null);
	    					((TrxTracking)dataUpdatetrackingfrom.get()).setTrxTrackingStatusoutbound("complete");
	    					
	    					
	    					
	    					
	    					
	    					//from
			    			Optional<TrxJalurDriver> getDetailJalurDriverFrom = TrxJalurDriverService.getDetail(dataUpdatetrackingfrom.get().getTrxDriverjalurheaderId(), dataUpdatetrackingfrom.get().getTrxDriverjalurId(), dataUpdatetrackingfrom.get().getTrxTrackingCompanyId());
			    			if(getDetailJalurDriverFrom.isPresent())
			    			{
			    				((TrxJalurDriver)getDetailJalurDriverFrom.get()).setStatusProgress(3);
			    			}
			    			
				    		//to
			    			Optional<TrxJalurDriver> getDetailJalurDriver = TrxJalurDriverService.getDetail(dataUpdatetrackingto.get().getTrxDriverjalurheaderId(), dataUpdatetrackingto.get().getTrxDriverjalurId(), dataUpdatetrackingto.get().getTrxTrackingCompanyId());
			    			if(getDetailJalurDriver.isPresent())
			    			{
			    				((TrxJalurDriver)getDetailJalurDriver.get()).setStatusProgress(3);
			    				((TrxJalurDriver)getDetailJalurDriver.get()).setDateKantor(localDateTime2);
			    			}
			    			
			    			//from driver schedule
				    		Optional<TrxDriverTracking> getDetailTrackingFrom = TrxDriverTrackingService.getDetail(dataUpdatetrackingfrom.get().getTmTrxOrderId(), dataUpdatetrackingfrom.get().getTrxTrackingId(), dataUpdatetrackingfrom.get().getTrxTrackingCompanyId());
				    		if(getDetailTrackingFrom.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingFrom.get()).setStatus(3);
				    		}
				    		
				    		//to driver schedule
				    		Optional<TrxDriverTracking> getDetailTrackingTo = TrxDriverTrackingService.getDetail(dataUpdatetrackingto.get().getTmTrxOrderId(), dataUpdatetrackingto.get().getTrxTrackingId(), dataUpdatetrackingto.get().getTrxTrackingCompanyId());
				    		if(getDetailTrackingTo.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingTo.get()).setStatus(3);
				    			((TrxDriverTracking)getDetailTrackingTo.get()).setDateKantor(localDateTime2);
				    		}
	    				}
	    				else
	    				{
	    					((TrxDriverschedule)dataUpdatedriverschedule.get()).setTrxDriverscheduleStatusoutbound("waiting response");
	    					((TrxDriverschedule)dataUpdatedriverschedule.get()).setTrxDriverscheduleStatus(null);	
	    					((TrxDriverschedule)dataUpdatedriverschedule.get()).setTglTiba(localDateTime2);
	    					//from driver schedule
				    		Optional<TrxDriverTracking> getDetailTrackingFrom = TrxDriverTrackingService.getDetail(dataUpdatetrackingfrom.get().getTmTrxOrderId(), dataUpdatetrackingfrom.get().getTrxTrackingId(), dataUpdatetrackingfrom.get().getTrxTrackingCompanyId());
				    		if(getDetailTrackingFrom.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingFrom.get()).setStatus(2);
				    		}
				    		
				    		//to driver schedule
				    		Optional<TrxDriverTracking> getDetailTrackingTo = TrxDriverTrackingService.getDetail(dataUpdatetrackingto.get().getTmTrxOrderId(), dataUpdatetrackingto.get().getTrxTrackingId(), dataUpdatetrackingto.get().getTrxTrackingCompanyId());
				    		if(getDetailTrackingTo.isPresent())
				    		{
				    			((TrxDriverTracking)getDetailTrackingTo.get()).setStatus(2);
				    		}
	    				}
	    				datastracking.add(dataUpdatetrackingfrom.get());
	    				datastracking.add(dataUpdatetrackingto.get());
	    				datasdriverschedulue.add(dataUpdatedriverschedule.get());
	    				
	    				if(dataUpdatetrackingto.isPresent())
	    				{
	    					Optional<MDriver> getDetailDriver = MDriverService.getdetail(dataUpdatedriverschedule.get().getmDriverId(), dataRequest.getString("driverschedulecompanyId"));
		    				
	    					if(dataUpdatetrackingto.get().getmCabangId() != null)
	    					{
	    						((MDriver)getDetailDriver.get()).setPositionCabangId(Integer.parseInt(dataUpdatetrackingto.get().getmCabangId().toString()));
	    						((MDriver)getDetailDriver.get()).setPositionAgenId(null);
	    						((MDriver)getDetailDriver.get()).setPositionAgenDetailId(null);
	    					}
	    					
	    					if(dataUpdatetrackingto.get().getmAgenId() != null)
	    					{
	    						((MDriver)getDetailDriver.get()).setPositionCabangId(null);
	    						((MDriver)getDetailDriver.get()).setPositionAgenId(dataUpdatetrackingto.get().getmAgenId());
	    					}
	    					
	    					if(dataUpdatetrackingto.get().getmAgenDetailId() != null)
	    					{
	    						((MDriver)getDetailDriver.get()).setPositionCabangId(null);
	    						((MDriver)getDetailDriver.get()).setPositionAgenDetailId(dataUpdatetrackingto.get().getmAgenDetailId());
	    					}
	    					((MDriver)getDetailDriver.get()).setStatusCondition(0);
	    					MDriverService.saveDriver(getDetailDriver.get());
		    				
	    				}
	    				
	    				
	    			}
	    		}
	    		trxDriverscheduleService.savealldriverschedule(datasdriverschedulue);
	    		trxTrackingService.saveAllTracking(datastracking);
			}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "update order success");
		}
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "update order failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/getInboundDriver")
	public String getInboundDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			
		}
		catch(Exception e)
		{
			
		}
		return response.toString();
	}
	
	@PostMapping("/getInbound/{paramAktif}")
	public String getInbound(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> orders; 
			orders = trxOrderService.getmyinbound(paramAktif.toString().toLowerCase(), UUID.fromString(Helper.getUserId(header)), dataRequest.getString("orderCompanyId"), dataRequest.getString("destination"), dataRequest.getString("drivername"), dataRequest.getString("date"), dataRequest.getString("conno"));
			logger.info("orders = " + orders.size());
			if(orders.isEmpty() || orders.size() <= 0)
			{
				orders = trxDriverscheduleService.getmyinbound(paramAktif.toString().toLowerCase(), UUID.fromString(Helper.getUserId(header)), dataRequest.getString("orderCompanyId"), dataRequest.getString("destination"), dataRequest.getString("drivername"), dataRequest.getString("date"), dataRequest.getString("conno"));
			}
			orders.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				List<JSONObject> childrenkoli = new ArrayList<>();
				data.put("manifestNo", column[0]);
				data.put("manifestDate", column[1].toString());
				data.put("DriverscheduleId", column[10]);
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactFrom", column[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("pickupAddress", "");
							data.put("contactFrom", "");
						}
					}
				}
				else
				{
					data.put("pickupAddress", "");
					data.put("contactFrom", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactTo", column[4]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("receipantAddress", "");
							data.put("contactTo", "");
						}
					}
				}
				else
				{
					data.put("receipantAddress", "");
					data.put("contactTo", "");
				}
				data.put("serviceName", column[5].toString());
				data.put("koli", column[6]);
				data.put("status", column[7].toString());
				data.put("companyId", column[8].toString());
				data.put("createdby", column[9].toString());
				data.put("to_kota_cabang", column[12] == null ? "" : column[12]);
				data.put("to_provinsi_cabang", column[13] == null ? "" : column[13]);
				data.put("to_kota_agen", column[14] == null ? "" : column[14]);
				data.put("to_provinsi_agen", column[15] == null ? "" : column[15]);
				data.put("to_cust_from", column[16] == null ? "" : column[16]);
				data.put("to_cust_to", column[17] == null ? "" : column[17]);
				data.put("from_kota_cabang", column[18] == null ? "" : column[18]);
				data.put("from_provinsi_cabang", column[19] == null ? "" : column[19]);
				data.put("from_kota_agen", column[20] == null ? "" : column[20]);
				data.put("from_provinsi_agen", column[21] == null ? "" : column[21]);
				data.put("from_cust_from", column[22] == null ? "" : column[22]);
				data.put("from_cust_to", column[23] == null ? "" : column[23]);
				
				
				List<Object[]> kolis = trxKolidetailService.getKoli(paramAktif.toString().toLowerCase(), column[0].toString(), dataRequest.getString("orderCompanyId"));
				kolis.stream().forEach(columnkoli -> {
					List<JSONObject> dataspartdetails = new ArrayList<>();
					JSONObject datakoli = new JSONObject();
					datakoli.put("manifestNo", columnkoli[0]);
					datakoli.put("key", columnkoli[1]);
					datakoli.put("berat", columnkoli[2]);
					datakoli.put("beratNominal", columnkoli[9]);
					datakoli.put("beratLabel", columnkoli[10]);
					datakoli.put("height", columnkoli[3]);
					datakoli.put("heightNominal", columnkoli[11]);
					datakoli.put("heightLabelNama", columnkoli[12]);
					datakoli.put("trxKolidetailWidth", columnkoli[4]);
					datakoli.put("widthNominal", columnkoli[13]);
					datakoli.put("widthLabelNama", columnkoli[14]);
					datakoli.put("length", columnkoli[5]);
					datakoli.put("lengthNominal", columnkoli[15]);
					datakoli.put("lengthLabelNama", columnkoli[16]);
					datakoli.put("trxKolidetailDeclareditem", columnkoli[6]);
					datakoli.put("trxKolidetailStatus", columnkoli[7]);
					datakoli.put("trxKolidetailCompanyId", columnkoli[8]);
					List<Object[]> parts = trxPartdetailService.getPart(paramAktif.toString().toLowerCase(), column[0].toString(), Integer.parseInt(columnkoli[1].toString()), dataRequest.getString("orderCompanyId"));
					parts.stream().forEach(columnpart -> {
						JSONObject datapart = new JSONObject();
						datapart.put("manifestNo", columnpart[0]);
						datapart.put("no", columnpart[1]);
						datapart.put("key", columnpart[2]);
						datapart.put("Partnumber", columnpart[3]);
						datapart.put("Partname", columnpart[10]);
						datapart.put("trxPartdetailQuantity", columnpart[4]);
						datapart.put("weight", columnpart[5]);
						datapart.put("returquantity", columnpart[6] == null ? "" : columnpart[6]);
						datapart.put("returPartReturdescription", columnpart[7]== null ? "" : columnpart[7]);
						datapart.put("PartStatus", columnpart[8]);
						datapart.put("PartCompanyId", columnpart[9]);
						dataspartdetails.add(datapart);
					});
					datakoli.put("childrenKoliDetail", dataspartdetails);
					childrenkoli.add(datakoli);
				});
				data.put("childrenkoli", childrenkoli);
				datas.add(data);
				
			});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "List Inbound success");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "List Inbound failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getOutbound/{paramAktif}")
	public String getOutbound(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> orders; 
			orders = trxOrderService.getmyoutbound(paramAktif.toString().toLowerCase(), UUID.fromString(Helper.getUserId(header)), dataRequest.getString("orderCompanyId"), dataRequest.getString("destination"), dataRequest.getString("drivername"), dataRequest.getString("date"), dataRequest.getString("conno"));
			if(orders.isEmpty() || orders.size() < 0)
			{
				orders = trxDriverscheduleService.getmyoutbound(paramAktif.toString().toLowerCase(), UUID.fromString(Helper.getUserId(header)), dataRequest.getString("orderCompanyId"), dataRequest.getString("destination"), dataRequest.getString("drivername"), dataRequest.getString("date"), dataRequest.getString("conno"));
			}
			orders.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("manifestNo", column[0]);
				data.put("DriverscheduleId", column[10]);
				data.put("manifestDate", column[1].toString());
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactFrom", column[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("pickupAddress", "");
							data.put("contactFrom", "");
						}
					}
				}
				else
				{
					data.put("pickupAddress", "");
					data.put("contactFrom", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactTo", column[4]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("receipantAddress", "");
							data.put("contactTo", "");
						}
					}
				}
				else
				{
					data.put("receipantAddress", "");
					data.put("contactTo", "");
				}
				data.put("serviceName", column[5].toString());
				data.put("koli", column[6]);
				data.put("status", column[7].toString());
				data.put("companyId", column[8].toString());
				data.put("createdby", column[9].toString());
				data.put("to_kota_cabang", column[12] == null ? "" : column[12]);
				data.put("to_provinsi_cabang", column[13] == null ? "" : column[13]);
				data.put("to_kota_agen", column[14] == null ? "" : column[14]);
				data.put("to_provinsi_agen", column[15] == null ? "" : column[15]);
				data.put("to_cust_from", column[16] == null ? "" : column[16]);
				data.put("to_cust_to", column[17] == null ? "" : column[17]);
				data.put("from_kota_cabang", column[18] == null ? "" : column[18]);
				data.put("from_provinsi_cabang", column[19] == null ? "" : column[19]);
				data.put("from_kota_agen", column[20] == null ? "" : column[20]);
				data.put("from_provinsi_agen", column[21] == null ? "" : column[21]);
				data.put("from_cust_from", column[22] == null ? "" : column[22]);
				data.put("from_cust_to", column[23] == null ? "" : column[23]);
				List<Object[]> checkActor = MCabangdetailService.checkActor(dataRequest.getString("orderCompanyId"), UUID.fromString(Helper.getUserId(header)));
				logger.info("checkActor = " + checkActor.size());
				data.put("actor", checkActor.size() > 0 ? "agen": "driver");
				datas.add(data);
				
			});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "List Inbound success");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "List Inbound failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getNotif/{paramAktif}")
	public String getNotif(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> orders; 
			orders = trxOrderService.getmynotif(paramAktif.toString().toLowerCase(), UUID.fromString(Helper.getUserId(header)), dataRequest.getString("orderCompanyId"), dataRequest.getString("destination"), dataRequest.getString("drivername"), dataRequest.getString("date"), dataRequest.getString("conno"));
			if(orders.isEmpty() || orders.size() < 0)
			{
				orders = trxDriverscheduleService.getmynotif(paramAktif.toString().toLowerCase(), UUID.fromString(Helper.getUserId(header)), dataRequest.getString("orderCompanyId"),dataRequest.getString("destination"), dataRequest.getString("drivername"), dataRequest.getString("date"), dataRequest.getString("conno"));
			}
			orders.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("manifestNo", column[0]);
				data.put("DriverscheduleId", column[10]);
				data.put("manifestDate", column[1].toString());
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("pickupAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactFrom", column[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("pickupAddress", "");
							data.put("contactFrom", "");
						}
					}
				}
				else
				{
					data.put("pickupAddress", "");
					data.put("contactFrom", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("receipantAddress", datacustomer.getJSONObject(i).getString("contact_name") + " " + datacustomer.getJSONObject(i).getString("contact_address"));
							data.put("contactTo", column[4]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("receipantAddress", "");
							data.put("contactTo", "");
						}
					}
				}
				else
				{
					data.put("receipantAddress", "");
					data.put("contactTo", "");
				}
				data.put("serviceName", column[5].toString());
				data.put("koli", column[6]);
				data.put("status", column[7].toString());
				data.put("companyId", column[8].toString());
				data.put("createdby", column[9].toString());
				datas.add(data);
				
			});
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "List Inbound success");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "List Inbound failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
