package com.proacc.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.entity.MRequestFleet;
import com.proacc.entity.MRequestFleetDetail;
import com.proacc.entity.MShareDriver;
import com.proacc.entity.TrxNumber;
import com.proacc.helper.Helper;
import com.proacc.service.MServiceService;
import com.proacc.service.MShareDriverService;
import com.proacc.service.TrxNumberService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MShareDriverController {
	private static final Logger logger = LoggerFactory.getLogger(MShareDriverController.class);
	
	@Autowired
	MShareDriverService MShareDriverService;
	
	@Autowired
	TrxNumberService TrxNumberService;
	
	@PostMapping("/detailShareDriver/{paramAktif}")
	public String detailShareDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT #### " + dataRequest.toString());
		logger.info("API #### detailShareDriver ()");
		try
		{
			List<Object[]> detailShareDriver = MShareDriverService.detailShareDriver(dataRequest.getInt("mDriverId"), dataRequest.getString("companyId"), paramAktif);
			detailShareDriver.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("no", col[0]);
				data.put("id", col[1]);
				data.put("driverId", col[2]);
				data.put("cabangId", col[3] == null ? "" : col[3]);
				data.put("cabangName", col[4] == null ? "" : col[4]);
				data.put("mProvinceIdCabang", col[5] == null ? "" : col[5]);
				data.put("mKotaIdCabang", col[6] == null ? "" : col[6]);
				data.put("mAgenId", col[7] == null ? "" : col[7]);
				data.put("mAgenDetailId", col[8] == null ? "" : col[8]);
				data.put("mAgenName", col[9] == null ? "" : col[9]);
				data.put("mProvinceIdAgen", col[10] == null ? "" : col[10]);
				data.put("mKotaIdAgen", col[11] == null ? "" : col[11]);
				data.put("startAgen", col[12] == null ? "" : col[12].toString());
				data.put("endAgen", col[13] == null ? "" : col[13].toString());
				data.put("status", col[14] == null ? "" : col[14]);
				datas.add(data);
			});
			
			response.put("responseCode", "00");
			response.put("responseDesc", "Detail Share Driver Success");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
	    	response.put("responseDesc", "Detail Share Driver Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/saveShareDriver")
	public String saveShareDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT #### " + dataRequest.toString());
		logger.info("API #### saveShareDriver()");
		try
		{
			ArrayList<MShareDriver> dataDetailRequest = new ArrayList<>();
	    	
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	
	    	List<Object[]> getShareDriverAll = MShareDriverService.detailShareDriver(dataRequest.getInt("driverId"), dataRequest.getString("companyId"), "a");
	    	if(getShareDriverAll.size()>0)
	    	{
	    		
	    		getShareDriverAll.stream().forEach(col->{
	    			System.out.print("col[1]" + col[1]);
		    		System.out.print("col[1]" + col[1]);
	    			MShareDriverService.updateStatusFalse(Integer.parseInt(col[1].toString()), Integer.parseInt(col[2].toString()), dataRequest.getString("companyId"));
	    		});
	    	}
	    	
	    	
	    	JSONArray requestDetail = dataRequest.getJSONArray("all");
	    	if(requestDetail.size() > 0)
	    	{
	    		for (int j = 0; j < requestDetail.size(); j++) {
	    			MShareDriver data = new MShareDriver();
	    	    	data.setId(requestDetail.getJSONObject(j).getInt("id"));
	    	    	if(requestDetail.getJSONObject(j).has("no"))
	    	    	{
	    	    		data.setNo( requestDetail.getJSONObject(j).getString("no"));
	    	    	}
	    	    	else
	    	    	{
	    	    		Optional <TrxNumber> dataUpdateNumber = TrxNumberService.getdetail(4, requestDetail.getJSONObject(j).getString("companyId"));
	    		    	List<Object[]> numbers = TrxNumberService.getNumber("a", 4,  requestDetail.getJSONObject(j).getString("companyId"));
	    		    	((TrxNumber)dataUpdateNumber.get()).setTrxNumberRunningnumber(dataUpdateNumber.get().getTrxNumberRunningnumber() + 1);
	    	    		data.setNo(numbers.get(0)[6].toString());
	    	    	}
	    	    	data.setmDriverId(requestDetail.getJSONObject(j).getInt("driverId"));
	    	    	
	    	    	if( requestDetail.getJSONObject(j).has("cabangId"))
	    	    	{
	    	    		data.setmCabangId( requestDetail.getJSONObject(j).getInt("cabangId"));
	    	    	}
	    	    	if( requestDetail.getJSONObject(j).has("agenId"))
	    	    	{
	    	    		data.setmAgenId( requestDetail.getJSONObject(j).getInt("agenId"));
	    	    	}
	    	    	if( requestDetail.getJSONObject(j).has("agenDetailId"))
	    	    	{
	    	    		data.setmAgenDetailId( requestDetail.getJSONObject(j).getInt("agenDetailId"));
	    	    	}
	    	    	
	    	    	if( requestDetail.getJSONObject(j).has("startDate"))
	    	    	{
	    	    		data.setStartDate(LocalDate.parse(requestDetail.getJSONObject(j).getString("startDate")));
	    	    	}
	    	    	
	    	    	if( requestDetail.getJSONObject(j).has("endDate"))
	    	    	{
	    	    		data.setEndDate(LocalDate.parse(requestDetail.getJSONObject(j).getString("endDate")));
	    	    	}
	    	    	
//	    	    	data.setStartDate(requestDetail.getJSONObject(j).getString("startDate") == "" ? null : LocalDate.parse( requestDetail.getJSONObject(j).getString("startDate")));
//	    	    	data.setEndDate(requestDetail.getJSONObject(j).getString("endDate") == "" ? null : LocalDate.parse( requestDetail.getJSONObject(j).getString("endDate")));
	    	    	data.setCompanyId(requestDetail.getJSONObject(j).getString("companyId"));
	    	    	data.setStatus(requestDetail.getJSONObject(j).getBoolean("status"));
	    	    	dataDetailRequest.add(data);
	    		}
	    	}
	    	
	    	MShareDriverService.saveAll(dataDetailRequest);
	    	response.put("responseCode", "00");
			response.put("responseDesc", "Save Share Driver Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
	    	response.put("responseDesc", "Save Share Driver Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}

}
