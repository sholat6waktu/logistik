package com.proacc.controllers;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MVehicletype;
import com.proacc.entity.TrxOrder;
import com.proacc.entity.TrxTracking;
import com.proacc.helper.Helper;
import com.proacc.service.MAgendetailService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxOutboundService;
import com.proacc.service.TrxTrackingService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TrxTrackingController {

	private static final Logger logger = LoggerFactory.getLogger(TrxTrackingController.class);
	
	@Autowired
	TrxTrackingService trxTrackingService;
	
	@Autowired
	MAgendetailService MAgendetailService;
	
	@Autowired
	com.proacc.service.TrxDriverscheduleService TrxDriverscheduleService;
	
	@Autowired
	TrxOrderService TrxOrderService;
	
	@Autowired
	TrxOutboundService TrxOutboundService;
	
	@PostMapping("/getallTracking/{paramAktif}")
	public String getAllTracking(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> tracking = trxTrackingService.getallTracking(paramAktif.toString().toLowerCase(), dataRequest.getString("trackingCompanyId"));
			tracking.stream().forEach(column ->{
				JSONObject data = new JSONObject();
				data.put("orderId", column[0]);
				data.put("trackingId", column[1]);
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("contactNameFrom", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("contactFrom", column[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("contactNameFrom", "");
							data.put("contactFrom", "");
						}
					}
				}
				else
				{
					data.put("contactNameFrom", "");
					data.put("contactFrom", "");
				}
				data.put("cabang", column[3] == null ? "" : column[3]);
				data.put("cabangdetail", column[4] == null ? "" : column[4]);
				if(column[5] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[5].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[5]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				data.put("agen", column[6] == null ? "" : column[6]);
				data.put("parent", column[7] == null ? "" : column[7]);
				data.put("cabangname", column[8] == null ? "" : column[8]);
				if(column[9] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[9].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameTo", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdTo", column[9]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameTo", "");
							data.put("mKotaIdTo", "");
						}
					}
				}
				else
				{
					data.put("kotaNameTo", "");
					data.put("mKotaIdTo", "");
				}
				data.put("status", column[10] == null ? "" : column[10]);
				data.put("company", column[11] == null ? "" : column[11]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate Harga berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate Harga gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/getTracking/{paramAktif}")
	public String getTracking(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> tracking = trxTrackingService.getTracking(paramAktif.toString().toLowerCase(), dataRequest.getString("orderId"), dataRequest.getInt("trackingId"), dataRequest.getString("trackingCompanyId"));
			tracking.stream().forEach(column ->{
				JSONObject data = new JSONObject();
				data.put("orderId", column[0]);
				data.put("trackingId", column[1]);
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("contactNameFrom", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("contactFrom", column[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("contactNameFrom", "");
							data.put("contactFrom", "");
						}
					}
				}
				else
				{
					data.put("contactNameFrom", "");
					data.put("contactFrom", "");
				}
				data.put("cabang", column[3] == null ? "" : column[3]);
				data.put("cabangdetail", column[4] == null ? "" : column[4]);
				if(column[5] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[5].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[5]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				data.put("agen", column[6] == null ? "" : column[6]);
				data.put("parent", column[7] == null ? "" : column[7]);
				data.put("cabangname", column[8] == null ? "" : column[8]);
				if(column[9] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[9].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameTo", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdTo", column[9]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameTo", "");
							data.put("mKotaIdTo", "");
						}
					}
				}
				else
				{
					data.put("kotaNameTo", "");
					data.put("mKotaIdTo", "");
				}
				data.put("status", column[10] == null ? "" : column[10]);
				data.put("company", column[11] == null ? "" : column[11]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate Harga berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate Harga gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/getTrackingByOrder/{paramAktif}")
	public String getTrackingByOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> tracking = trxTrackingService.getTrackingByOrder(paramAktif.toString().toLowerCase(), dataRequest.getString("orderId"), dataRequest.getString("trackingCompanyId"));
			tracking.stream().forEach(column ->{
				JSONObject data = new JSONObject();
				data.put("orderId", column[0]);
				data.put("trackingId", column[1]);
				if(column[2] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[2].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("contactNameFrom", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("contactFrom", column[2]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("contactNameFrom", "");
							data.put("contactFrom", "");
						}
					}
				}
				else
				{
					data.put("contactNameFrom", "");
					data.put("contactFrom", "");
				}
				data.put("cabang", column[3] == null ? "" : column[3]);
				data.put("cabangdetail", column[4] == null ? "" : column[4]);
				if(column[5] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[5].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[5]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				data.put("agen", column[6] == null ? "" : column[6]);
				data.put("parent", column[7] == null ? "" : column[7]);
				data.put("cabangname", column[8] == null ? "" : column[8]);
				if(column[9] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[9].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameTo", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdTo", column[9]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameTo", "");
							data.put("mKotaIdTo", "");
						}
					}
				}
				else
				{
					data.put("kotaNameTo", "");
					data.put("mKotaIdTo", "");
				}
				data.put("status", column[10] == null ? "" : column[10]);
				data.put("company", column[11] == null ? "" : column[11]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate Harga berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate Harga gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getTrackingViewOngoing/{paramAktif}")
	public String getTrackingViewOngoing(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
	    final String urlUser = "http://54.169.109.123:3003/api/users/all";
	    ResponseEntity<String> responseuser= restTemplate.exchange(urlUser, HttpMethod.GET, entity, String.class);
	    JSONObject objdatauser = (JSONObject)JSONSerializer.toJSON(responseuser.getBody());
	    net.sf.json.JSONArray datauser = objdatauser.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> tracking = trxTrackingService.getTrackingViewOngoing("a", dataRequest.getString("trackingCompanyId"));
			tracking.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("orderId", column[0] == null ? "" : column[0]);
				data.put("orderDate", column[1] == null ? "" : column[1].toString());
				data.put("kotaFrom", column[2] == null ? "" : column[2].toString().toLowerCase());
				data.put("kotaTo", column[3] == null ? "" : column[3].toString().toLowerCase());
				data.put("cabangParent", column[4] == null ? "" : column[4].toString());
				if(column[9] != null)
				{
					// ini transit
					data.put("status", column[9].toString());
				}
//				else if ( column[6] != null)
//				{
//					String kota="";
//					Integer kotaId=null;
//					logger.info("masuk sini lur");
//					logger.info("column11 = " + column[11] );
//					//ini harusnya klo seumpama ada agen ditengah jalan bisa bahaya klo agen nya offline
//					if(column[10].equals(2))
//					{
//						kota = column[2].toString();
//						final String urlSearchCityByName = "http://54.169.109.123:3004/api/v1/cities-with?column=city_name&value="+kota;
//					    ResponseEntity<String> responsecitiesId = restTemplate.exchange(urlSearchCityByName, HttpMethod.GET, entity, String.class);
//					    JSONObject objdatacitiesId = (JSONObject)JSONSerializer.toJSON(responsecitiesId.getBody());
//					    net.sf.json.JSONArray datacitiesId = objdatacitiesId.getJSONArray("data");
//					    
//					    if(datacitiesId.size()>0)
//						{
//							kotaId = datacitiesId.getJSONObject(0).getInt("city_id");
//						}
//					}
//					else if (Integer.parseInt(column[11].toString()) != -1)
//					{
//						final String urlSearchCityByName = "http://54.169.109.123:3004/api/v1/cities-with?column=city_id&value="+Integer.parseInt(column[11].toString());
//					    ResponseEntity<String> responsecitiesId = restTemplate.exchange(urlSearchCityByName, HttpMethod.GET, entity, String.class);
//					    JSONObject objdatacitiesId = (JSONObject)JSONSerializer.toJSON(responsecitiesId.getBody());
//					    net.sf.json.JSONArray datacitiesId = objdatacitiesId.getJSONArray("data");
//					    
//					    data.put("status", column[8].toString() + " " + datacitiesId.getJSONObject(0).getString("city_name"));
//					}
//					else
//					{
//						kota = column[3].toString();
//						final String urlSearchCityByName = "http://54.169.109.123:3004/api/v1/cities-with?column=city_name&value="+kota;
//					    ResponseEntity<String> responsecitiesId = restTemplate.exchange(urlSearchCityByName, HttpMethod.GET, entity, String.class);
//					    JSONObject objdatacitiesId = (JSONObject)JSONSerializer.toJSON(responsecitiesId.getBody());
//					    net.sf.json.JSONArray datacitiesId = objdatacitiesId.getJSONArray("data");
//					    
//						
//						if(datacitiesId.size()>0)
//						{
//							kotaId = datacitiesId.getJSONObject(0).getInt("city_id");
//						}
//					}
//					
//					if(Integer.parseInt(column[11].toString()) == -1) {
//						//ini untuk nyari m_kota_id
//						List<Object[]> searchMKotaId = MAgendetailService.getAgenDetailByIdAndMKotaId(Integer.parseInt(column[6].toString()), kotaId);
//						
//						//ini untuk nyari m_transfer_point
//						List<Object[]> searchTransferPoint = MAgendetailService.getCustomerCityByAgenTransferPoint(column[6].toString(), kotaId);
//						
//						if(searchMKotaId.size()>0)
//						{
////							jika ternyata ada di m_kota_id nya berarti agen yang dituju adalah kota agen asal
//							final String urlSearchCityByName = "http://54.169.109.123:3004/api/v1/cities-with?column=city_id&value="+kotaId;
//						    ResponseEntity<String> responsecitiesId = restTemplate.exchange(urlSearchCityByName, HttpMethod.GET, entity, String.class);
//						    JSONObject objdatacitiesId = (JSONObject)JSONSerializer.toJSON(responsecitiesId.getBody());
//						    net.sf.json.JSONArray datacitiesId = objdatacitiesId.getJSONArray("data");
//						    
//							data.put("status", column[8].toString() + " " + datacitiesId.getJSONObject(0).getString("city_name"));
//						}
//						else
//						{
////							jika ternyata ada di m_transferpoint_id nya berarti agen yang dituju adalah cover agen maka kita ambil m_kota_id, contoh 
////							ternyata banjar cover_area nya maka diambil m_kota_id yang namanya adalah kabupaten bandung barat
//							final String urlSearchCityByName = "http://54.169.109.123:3004/api/v1/cities-with?column=city_id&value="+searchTransferPoint.get(0)[1];
//						    ResponseEntity<String> responsecitiesId = restTemplate.exchange(urlSearchCityByName, HttpMethod.GET, entity, String.class);
//						    JSONObject objdatacitiesId = (JSONObject)JSONSerializer.toJSON(responsecitiesId.getBody());
//						    net.sf.json.JSONArray datacitiesId = objdatacitiesId.getJSONArray("data");
//							
//							
//							data.put("status", column[8].toString() + " " + datacitiesId.getJSONObject(0).getString("city_name"));
//						}
//					}
//					
//					
//					logger.info("kota id = " + kotaId);
//					logger.info("agen id = " + column[6]);
//					logger.info("tracking id = " + column[10]);
//				}
				
				else if (column[12] != null)
				{
//				ini cabang
					String kota="";
					
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[5].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							kota = datacities.getJSONObject(i).getString("city_name");
							break;
						}
						else if(datacities.size() == i + 1)
						{
							kota="";
						}
					}
					
					for(int i = 0; i < datauser.size(); i++)
					{
						
							if(column[12].equals(datauser.getJSONObject(i).getString("user_uuid")))
							{
								data.put("status", column[12] == null ? "" : datauser.getJSONObject(i).get("fullname") + " " + column[8].toString() + " " +kota);
								data.put("cabangid", column[4]);
								break;
							}
							else if(datauser.size() == i + 1)
							{

								data.put("status", column[12] == null ? "" : column[12].toString() + " " + column[8].toString() + " " + kota);
								data.put("cabangid", column[4]);
							}
//						
						
//						logger.info("column[7] = " + column[7]);
//						logger.info("datauser.getJSONObject(i).getString(\"user_uuid\") = " + datauser.getJSONObject(i).getString("user_uuid"));
						
					}
				}
				//--------------mulai
				else if (column[7] != null)
				{
					// ini agen
					String kota="";
					
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[11].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							kota = datacities.getJSONObject(i).getString("city_name");
							break;
						}
						else if(datacities.size() == i + 1)
						{
							kota="";
						}
					}
					
					for(int i = 0; i < datauser.size(); i++)
					{
						if(column[7] != null)
						{
							if(column[7].equals(datauser.getJSONObject(i).getString("user_uuid")))
							{
								data.put("status", column[7] == null ? "" : datauser.getJSONObject(i).get("fullname") + " " + column[8].toString() + " " +kota);
								data.put("agenid", column[6]);
								break;
							}
							else if(datauser.size() == i + 1)
							{

								data.put("status", column[7] == null ? "" : column[7].toString() + " " + column[8].toString() + " " + kota);
								data.put("agenid", column[6]);
							}
						}
						
//						else 
//						{
//							if(column[12].equals(datauser.getJSONObject(i).getString("user_uuid")))
//							{
//								data.put("status", column[12] == null ? "" : datauser.getJSONObject(i).get("fullname") + " " + column[8].toString() + " " +kota);
//								data.put("cabangid", column[4]);
//								break;
//							}
//							else if(datauser.size() == i + 1)
//							{
//
//								data.put("status", column[12] == null ? "" : column[12].toString() + " " + column[8].toString() + " " + kota);
//								data.put("cabangid", column[4]);
//							}
//						}
						
//						logger.info("column[7] = " + column[7]);
//						logger.info("datauser.getJSONObject(i).getString(\"user_uuid\") = " + datauser.getJSONObject(i).getString("user_uuid"));
						
					}

				}
				else
				{
					data.put("status", "");
				}
				//--------------------------
//				else
//				{
//					String coba =column[8] == null ? "" : column[8].toString();
//					String semua = column[7] == null? null : column[7].toString();
//					String uuid = ""; //define it!
//					if(semua != null)
//					{
//						if ( semua.matches("[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}")) {
//								for(int i = 0; i < datauser.size(); i++)
//								{
//									logger.info("datauser = " + datauser.getJSONObject(i));
//									if(semua.equals(datauser.getJSONObject(i).getString("user_uuid")))
//									{
//										uuid = datauser.getJSONObject(i).getString("fullname");
////										data.put("contactNameFrom", datacustomer.getJSONObject(i).getString("contact_name"));
////										data.put("contactFrom", column[2]);
//										break;
//									}
////									else if(datacustomer.size() == i + 1)
////									{
////										data.put("contactNameFrom", "");
////										data.put("contactFrom", "");
////									}
//								}
//							}
//							else
//							{
//								uuid = semua;
//							}
//					
//					}
////					
//					logger.info("uuid = " + uuid);
//					data.put("status", coba  + " " + uuid );
//					data.put("agenid", column[6]);
//				}
				if(column[8] != null)
				{
					// jika inbound
					if(column[8].toString().equals("intransit"))
					{
						data.put("status", column[8].toString());
					}	
				}
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data tracking berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data tracking gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/addingTracking")
	public String addingTracking(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
//		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
//		RestTemplate restTemplate = new RestTemplate();
//	    HttpHeaders headers = new HttpHeaders();
//	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//	    headers.set("x-access-code", header);
//	    HttpEntity<String> entity = new HttpEntity<String>(headers);
//	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
//	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
//	    
//		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
//	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
//	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		ArrayList<TrxTracking> dataTracking = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			DateTimeFormatter formatter11 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss a");
			
			
			List<Object[]> getDetailTracking = trxTrackingService.getTrackingByOrder("a", dataRequest.getString("orderId"), dataRequest.getString("compId"));
			getDetailTracking.stream().forEach(col->{
				LocalDateTime startTime=null;
				if(col[20] != null) {

					String replace = col[20].toString().replaceAll(" ", "T");
					startTime = LocalDateTime.parse(replace);	
				}
				TrxTracking data = new TrxTracking ();
				data.setTmTrxOrderId(col[0].toString());
				data.setmContactCustomerFromId(col[2] == null ? null : Integer.parseInt(col[2].toString()));
				data.setmCabangId(col[3] == null ? null : Integer.parseInt(col[3].toString()));
				data.setmCabangDetailId(col[4] == null ? null : Integer.parseInt(col[4].toString()));
				data.setmAgenId(col[6] == null ? null : Integer.parseInt(col[6].toString()));
				data.setmParentId(col[7] == null ? null : Integer.parseInt(col[7].toString()));
				data.setmContactCustomerToId(col[9] == null ? null : Integer.parseInt(col[9].toString()));
				data.setTrxTrackingStatus(col[10] == null ? null : col[10].toString());
				data.setTrxTrackingEstimetedTime(col[20] == null ? null :startTime);
				data.setTrxTrackingCompanyId(dataRequest.getString("compId"));
				data.setTrxTrackingStatusinbound(col[18] == null ? null : col[18].toString());
				data.setTrxTrackingStatusoutbound(col[19] == null ? null : col[19].toString());
				data.setmAgenDetailId(col[21] == null ? null : Integer.parseInt(col[21].toString()));
//				data.setTrxTrackingId(col[1] == null ? null : Integer.parseInt(col[1].toString()));
//				
//				if(dataRequest.getInt("trackingId") == Integer.parseInt(col[1].toString()))
//				{
//					data.setTrxTrackingId(Integer.parseInt(col[1].toString()));
//					data.setmAgenId(dataRequest.getInt("mAgenId"));
//					data.setmAgenDetailId(dataRequest.getInt("mAgenDetailId"));
//					data.setTmTrxOrderId(col[0].toString());
//					data.setTrxTrackingCompanyId(dataRequest.getString("compId"));
//					data.setmCabangId(null);
//					data.setmCabangDetailId(null);
//					data.setmParentId(null);
//				}
				if(dataRequest.getInt("trackingId") <= Integer.parseInt(col[1].toString()))
				{
					data.setTrxTrackingId(Integer.parseInt(col[1].toString())+1 );
				}
				else
				{
					data.setTrxTrackingId(Integer.parseInt(col[1].toString()) );
				}
				
				
				
				
				dataTracking.add(data);
			});
			
			Optional<TrxOrder> getDetail = TrxOrderService.getdetail(dataRequest.getString("orderId"), dataRequest.getString("compId"));
			
			TrxTracking data2 = new TrxTracking ();
			data2.setmAgenId(dataRequest.getInt("mAgenId"));
			data2.setTrxTrackingId(dataRequest.getInt("trackingId"));
			data2.setmAgenId(dataRequest.getInt("mAgenId"));
			data2.setmAgenDetailId(dataRequest.getInt("mAgenDetailId"));
			data2.setTmTrxOrderId(dataRequest.getString("orderId"));
			data2.setTrxTrackingCompanyId(dataRequest.getString("compId"));
			data2.setTrxTrackingStatus(getDetail.get().getTrxOrderStatus());
			data2.setmCabangId(null);
			data2.setmCabangDetailId(null);
			data2.setmParentId(null);
			dataTracking.add((dataRequest.getInt("trackingId")-1), data2);
			trxTrackingService.saveAllTracking(dataTracking);
			
			response.put("responseCode", "00");
		    response.put("responseDesc", "Adding Schedule Success");
//		    response.put("responseData", dataTracking);
			
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Adding Schedule Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	int a = 1;
	@PostMapping("/cancelTracking")
	public String cancelTracking(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
//		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
//		RestTemplate restTemplate = new RestTemplate();
//	    HttpHeaders headers = new HttpHeaders();
//	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//	    headers.set("x-access-code", header);
//	    HttpEntity<String> entity = new HttpEntity<String>(headers);
//	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
//	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
//	    
//		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
//	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
//	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		ArrayList<TrxTracking> dataTracking = new ArrayList<>();
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		 a = 1;
		try
		{
			
			if(dataRequest.getInt("trackingFrom") > 1)
			{
				logger.info("masuk sini ");
				Optional<TrxTracking> getTracking = trxTrackingService.getdetail(dataRequest.getString("orderId"), dataRequest.getInt("trackingFrom"), dataRequest.getString("companyId"));
				
				Optional<TrxTracking> getTracking2 = trxTrackingService.getdetail(dataRequest.getString("orderId"), dataRequest.getInt("trackingTo"), dataRequest.getString("companyId"));
				
				((TrxTracking)getTracking2.get()).setTrxTrackingStatusinbound(null);
				((TrxTracking)getTracking2.get()).setTrxTrackingStatusoutbound(null);
				((TrxTracking)getTracking2.get()).setTrxTrackingStatus("on Proggress");
				((TrxTracking)getTracking2.get()).setTrxTrackingEstimetedTime(null);
				trxTrackingService.saveTracking(getTracking2.get());
				
				((TrxTracking)getTracking.get()).setTrxTrackingStatusinbound("ware house");
				((TrxTracking)getTracking.get()).setTrxTrackingStatusoutbound(null);
				((TrxTracking)getTracking.get()).setTrxTrackingStatus(null);
				((TrxTracking)getTracking.get()).setTrxTrackingEstimetedTime(null);
				trxTrackingService.saveTracking(getTracking.get());
				if(getTracking2.get().getTypeTambahan()!=null)
				{
					if(!getTracking2.get().getTypeTambahan().isEmpty())
					{
						
						if(getTracking2.get().getTypeTambahan().equals("1"))
						{
							trxTrackingService.deleteTracking(dataRequest.getString("orderId"), getTracking2.get().getTrxTrackingId(), dataRequest.getString("companyId"));
							
							List<Object[]> getDetailTracking = trxTrackingService.getTrackingByOrder("a", dataRequest.getString("orderId"), dataRequest.getString("companyId"));
							getDetailTracking.stream().forEach(col->{
								trxTrackingService.updateId(a, Integer.parseInt(col[1].toString()), dataRequest.getString("companyId"), dataRequest.getString("orderId"));
								a++;
							});
						}
					}
				}
				
				Optional<TrxOrder> getOrder = TrxOrderService.getdetail(dataRequest.getString("orderId"), dataRequest.getString("companyId"));
//				((TrxOrder)getOrder.get()).setTrxOrderStatus("data Entry");
				((TrxOrder)getOrder.get()).setManifestNo(null);
				TrxOrderService.saveOrder(getOrder.get());
				
				TrxDriverscheduleService.delete(dataRequest.getInt("driverscheduleId"), dataRequest.getString("orderId"), dataRequest.getString("companyId"));
				
			}
			
			else
			{
				Optional<TrxOrder> getOrder = TrxOrderService.getdetail(dataRequest.getString("orderId"), dataRequest.getString("companyId"));
				((TrxOrder)getOrder.get()).setTrxOrderStatus("data Entry");
				((TrxOrder)getOrder.get()).setManifestNo(null);
				TrxOrderService.saveOrder(getOrder.get());
//				Optional<TrxTracking> getTracking = trxTrackingService.getdetail(dataRequest.getString("orderId"), dataRequest.getInt("trackingFrom"), dataRequest.getString("companyId"));
//				((TrxTracking)getTracking.get()).setTrxTrackingStatusinbound(null);
//				((TrxTracking)getTracking.get()).setTrxTrackingStatusoutbound(null);
//				((TrxTracking)getTracking.get()).setTrxTrackingStatus("Data Entry");
//				((TrxTracking)getTracking.get()).setTrxTrackingEstimetedTime(null);
//				trxTrackingService.saveTracking(getTracking.get());
//				
//				Optional<TrxTracking> getTracking2 = trxTrackingService.getdetail(dataRequest.getString("orderId"), dataRequest.getInt("trackingTo"), dataRequest.getString("companyId"));
//				((TrxTracking)getTracking2.get()).setTrxTrackingStatusinbound(null);
//				((TrxTracking)getTracking2.get()).setTrxTrackingStatusoutbound(null);
//				((TrxTracking)getTracking2.get()).setTrxTrackingStatus("Data Entry");
//				((TrxTracking)getTracking2.get()).setTrxTrackingEstimetedTime(null);
//				trxTrackingService.saveTracking(getTracking2.get());
				
				trxTrackingService.updatestrackingtatusbyorderCancell(dataRequest.getString("orderId"), dataRequest.getString("companyId"));
				
				TrxDriverscheduleService.delete(dataRequest.getInt("driverscheduleId"), dataRequest.getString("orderId"), dataRequest.getString("companyId"));
			}
			TrxOutboundService.delete(dataRequest.getString("orderId"), dataRequest.getInt("trackingFrom"),dataRequest.getInt("trackingTo"), dataRequest.getString("companyId"));
			
			
			response.put("responseCode", "00");
		    response.put("responseDesc", "Cancel Schedule Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Cancel Schedule Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/getTrackingViewDone/{paramAktif}")
	public String getTrackingViewDone(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> tracking = trxTrackingService.getTrackingViewDone("a", dataRequest.getString("trackingCompanyId"));
			tracking.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("orderId", column[0] == null ? "" : column[0]);
				data.put("orderDate", column[1] == null ? "" : column[1].toString());
				data.put("kotaFrom", column[2] == null ? "" : column[2].toString().toLowerCase());
				data.put("kotaTo", column[3] == null ? "" : column[3].toString().toLowerCase());
				data.put("status", column[8].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data tracking berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data tracking gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getTrackingSettingOngoing/{paramAktif}")
	public String getTrackingSettingOngoing(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> tracking = trxTrackingService.getTrackingSettingOngoing("a", dataRequest.getString("trackingCompanyId"), user_uuid.toString());
			tracking.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("orderId", column[0] == null ? "" : column[0]);
				data.put("orderDate", column[1] == null ? "" : column[1].toString());
				data.put("kotaFrom", column[2] == null ? "" : column[2].toString().toLowerCase());
				data.put("kotaTo", column[3] == null ? "" : column[3].toString().toLowerCase());
				data.put("status", column[4] == null ? "" : column[4].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data tracking berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data tracking gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
}