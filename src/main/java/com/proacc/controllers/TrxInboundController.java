package com.proacc.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.helper.Helper;
import com.proacc.service.MDriverService;
import com.proacc.service.MVolumeService;
import com.proacc.service.MWeightService;
import com.proacc.service.TrxDriverTrackingService;
import com.proacc.service.TrxDriverscheduleService;
import com.proacc.service.TrxHistroyTrackingService;
import com.proacc.service.TrxInboundService;
import com.proacc.service.TrxJalurDriverService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxTrackingService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TrxInboundController {
	private static final Logger logger = LoggerFactory.getLogger(TrxInboundController.class);
	
	@Autowired
	TrxOrderService trxOrderService;
	@Autowired
	TrxTrackingService trxTrackingService;
	@Autowired
	MDriverService mDriverService;
	@Autowired
	TrxDriverscheduleService trxDriverscheduleService;
	@Autowired
	MWeightService mWeightService;
	@Autowired
	MVolumeService mVolumeService;
	@Autowired
	TrxDriverTrackingService TrxDriverTrackingService;
	@Autowired
	TrxJalurDriverService TrxJalurDriverService;
	
	@Autowired
	TrxHistroyTrackingService TrxHistroyTrackingService;
	
	@Autowired
	TrxInboundService TrxInboundService;
	
	@PostMapping("/listInboundReport")
	public String listInboundReport(@RequestBody String request
			, @RequestHeader(value = "User-Access") String header
			)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		UUID user_uuid = UUID.fromString(Helper.getUserId(header));
		logger.info("API == listInboundReport");
		logger.info("INPUT == " + dataRequest);
		try
		{
			List<Object[]> list = TrxInboundService.getList(dataRequest.getString("date"), dataRequest.getString("order"), dataRequest.getString("koli"), dataRequest.getString("weight"), user_uuid.toString());
			if(list.size()>0)
			{
				list.stream().forEach(col->{
					JSONObject data = new JSONObject();
					data.put("date", col[0] == null ? "" : col[0].toString());
					data.put("noOrder", col[1] == null ? "" : col[1].toString());
					data.put("weight", col[2] == null ? "" : col[2].toString());
					data.put("koli", col[3] == null ? "" : col[3]);
					data.put("FromCustomerFrom", col[4] == null ? "" : col[4]);
					data.put("FromCustomerTo", col[5] == null ? "" : col[5]);
					data.put("FromAgenName", col[6] == null ? "" : col[6]);
					data.put("FromAgenKotaId", col[7] == null ? "" : col[7]);
					data.put("FromAgenProvinceId", col[8] == null ? "" : col[8]);
					data.put("FromCabangName", col[9] == null ? "" : col[9]);
					data.put("FromCabangProvinceId", col[10] == null ? "" : col[10]);
					data.put("FromCabangKotaId", col[11] == null ? "" : col[11]);
					
					data.put("ToCustomerFrom", col[12] == null ? "" : col[12]);
					data.put("ToCustomerTo", col[13] == null ? "" : col[13]);
					data.put("ToAgenName", col[14] == null ? "" : col[14]);
					data.put("ToAgenKotaId", col[15] == null ? "" : col[15]);
					data.put("ToAgenProvinceId", col[16] == null ? "" : col[16]);
					data.put("ToCabangName", col[17] == null ? "" : col[17]);
					data.put("ToCabangProvinceId", col[18] == null ? "" : col[18]);
					data.put("ToCabangKotaId", col[19] == null ? "" : col[19]);
					
					data.put("orderCustomerFrom", col[22] == null ? "" : col[22]);
					data.put("orderCustomerTo", col[23] == null ? "" : col[23]);

					data.put("driverId", col[24] == null ? "" : col[24]);
					data.put("driverHelperId", col[25] == null ? "" : col[25]);
					datas.add(data);
				});
			}
			response.put("responseCode", "00");
		    response.put("responseDesc", "List Inbound Success" );
		    response.put("responseData", datas );
		}
		catch(Exception e)
		{
			 response.put("responseCode", "99");
		     response.put("responseDesc", "List Inbound Failed" );
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}

}
