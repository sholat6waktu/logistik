package com.proacc.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MRate;
import com.proacc.entity.MRateCustomer;
import com.proacc.entity.MRateHarga;
import com.proacc.helper.Helper;
import com.proacc.service.AllRateCustomerService;
import com.proacc.service.MRateCustomerService;
import com.proacc.service.MRateHargaService;
import com.proacc.service.MRateService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MRateHargaController {

	private static final Logger logger = LoggerFactory.getLogger(MRateHargaController.class);
	
	@Autowired
	MRateService mRateService;
	@Autowired
	MRateHargaService mRateHargaService;
	@Autowired
	MRateCustomerService mRateCustomerService;
	@Autowired
	AllRateCustomerService allRateCustomerService;

	@PostMapping("/getAllRateCustomer/{paramAktif}")
	public String getAllRateCustomer(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateCustomerService.getallRateCustomer(paramAktif.toString().toLowerCase(), dataRequest.getString("rateCustomerCompanyId"));
			rates.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				data.put("ratecustomerId", column[0]);
				if(column[1] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[1].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("customerName", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("customerId", column[1]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("customerName", "");
							data.put("customerId", "");
						}
					}
				}
				else
				{
					data.put("customerName", "");
					data.put("customerId", "");
				}
				data.put("startDate", column[2].toString());
				data.put("endDate", column[3].toString());
				data.put("LastUpdate", column[7].toString());
				data.put("status", column[8]);
				data.put("ratecustomerCompanyId", column[9]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate Harga berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate Harga gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/getRateCustomer/{paramAktif}")
	public String getRateCustomer(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateCustomerService.getRateCustomer(paramAktif.toString().toLowerCase(), dataRequest.getInt("ratecustomerId"), dataRequest.getString("rateCustomerCompanyId"));
			rates.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				data.put("ratecustomerId", column[0]);
				if(column[1] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[1].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("customerName", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("customerId", column[1]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("customerName", "");
							data.put("customerId", "");
						}
					}
				}
				else
				{
					data.put("customerName", "");
					data.put("customerId", "");
				}
				data.put("startDate", column[2].toString());
				data.put("endDate", column[3].toString());
				data.put("LastUpdate", column[7].toString());
				data.put("status", column[8]);
				data.put("ratecustomerCompanyId", column[9]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate Harga berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate Harga gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/getAllRateHarga/{paramAktif}")
	public String getAllRateHarga(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateHargaService.getallRateHarga(paramAktif.toString().toLowerCase(), dataRequest.getString("rateHargaCompanyId"));
			rates.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("mRateId1", column[0]);
				data.put("mRateId2", column[1]);
				data.put("ratehargaId1", column[2]);
				data.put("ratehargaId2", column[3]);
				if(column[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("customerName", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("customerId", column[4]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("customerName", "");
							data.put("customerId", "");
						}
					}
				}
				else
				{
					data.put("customerName", "");
					data.put("customerId", "");
				}
				if(column[5] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[5].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[5]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				if(column[6] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[6].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdFrom", column[6]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameFrom", "");
							data.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					data.put("provinceNameFrom", "");
					data.put("mProvinceIdFrom", "");
				}
				if(column[7] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[7].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameTo", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdTo", column[7]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameTo", "");
							data.put("mKotaIdTo", "");
						}
					}
				}
				else
				{
					data.put("kotaNameTo", "");
					data.put("mKotaIdTo", "");
				}
				if(column[8] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[8].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameTo", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdTo", column[8]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameTo", "");
							data.put("mProvinceIdTo", "");
						}
					}
				}
				else
				{
					data.put("provinceNameTo", "");
					data.put("mProvinceIdTo", "");
				}
				data.put("service_id", column[9] == null ? "" : column[9]);
				data.put("rateServiceName", column[10] == null ? "" : column[10].toString());
				data.put("rateVehicle", column[11] == null ? "" : column[11].toString());
				data.put("ratePrice", column[12] == null ? "" : column[12].toString());
				data.put("rateHargaDiscount", column[13] == null ? "" : column[13].toString());
				data.put("rateHargaNettprice", column[14] == null ? "" : String.format("%.0f",  column[14]));
				data.put("startDate", column[15] == null ? "" : column[15].toString());
				data.put("endDate", column[16] == null ? "" : column[16].toString());
				data.put("status", column[17] == null ? "" : column[17]);
				data.put("ratehargaCompanyId", column[18]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate Harga berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate Harga gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getRateHarga/{paramAktif}")
	public String getRateHarga(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateHargaService.getRateHarga(paramAktif.toString().toLowerCase(), dataRequest.getInt("ratehargaId1"), dataRequest.getString("rateHargaCompanyId"));
			rates.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("mRateId1", column[0]);
				data.put("mRateId2", column[1]);
				data.put("ratehargaId1", column[2]);
				data.put("ratehargaId2", column[3]);
				if(column[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("customerName", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("customerId", column[4]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("customerName", "");
							data.put("customerId", "");
						}
					}
				}
				else
				{
					data.put("customerName", "");
					data.put("customerId", "");
				}
				if(column[5] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[5].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[5]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				if(column[6] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[6].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdFrom", column[6]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameFrom", "");
							data.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					data.put("provinceNameFrom", "");
					data.put("mProvinceIdFrom", "");
				}
				if(column[7] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[7].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameTo", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdTo", column[7]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameTo", "");
							data.put("mKotaIdTo", "");
						}
					}
				}
				else
				{
					data.put("kotaNameTo", "");
					data.put("mKotaIdTo", "");
				}
				if(column[8] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[8].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameTo", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdTo", column[8]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameTo", "");
							data.put("mProvinceIdTo", "");
						}
					}
				}
				else
				{
					data.put("provinceNameTo", "");
					data.put("mProvinceIdTo", "");
				}
				data.put("service_id", column[9] == null ? "" : column[9]);
				data.put("rateServiceName", column[10] == null ? "" : column[10].toString());
				data.put("rateVehicle", column[11] == null ? "" : column[11].toString());
				data.put("ratePrice", column[12] == null ? "" : column[12].toString());
				data.put("rateHargaDiscount", column[13] == null ? "" : column[13].toString());
				data.put("rateHargaNettprice", column[14] == null ? "" : String.format("%.0f",  column[14]));
				data.put("startDate", column[15] == null ? "" : column[15].toString());
				data.put("endDate", column[16] == null ? "" : column[16].toString());
				data.put("status", column[17] == null ? "" : column[17]);
				data.put("ratehargaCompanyId", column[18]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate Harga berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate Harga gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/nettdisc/{paramAktif}")
	public String nettdisc(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			if(dataRequest.get("diskon").equals(""))
			{
				List<Object[]> rateprice = mRateHargaService.nettdisc(Float.parseFloat(dataRequest.getString("price")), null, dataRequest.getString("nettprice"));
				response.put("responseCode", "00");
				response.put("responseDesc", "data diskon berhasil didapat");
				response.put("diskon", rateprice.get(0)[0]);
			}
			else if(dataRequest.get("nettprice").equals(""))
			{
				List<Object[]> rateprice = mRateHargaService.nettdisc(Float.parseFloat(dataRequest.getString("price")), dataRequest.getString("diskon"), null);
				response.put("responseCode", "00");
				response.put("responseDesc", "data nettprice berhasil didapat");
				response.put("nettprice", rateprice.get(0)[0]);
			}
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data diskon/nettprice gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getRateHargabyCustomer/{paramAktif}")
	public String getRateHargabyCustomer(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateCustomerService.getRateCustomer(paramAktif.toString().toLowerCase(), dataRequest.getInt("ratecustomerId"), dataRequest.getString("rateCustomerCompanyId"));
			rates.stream().forEach(column-> {
				List<JSONObject> datashargas = new ArrayList<>();
				JSONObject data = new JSONObject();
				data.put("ratecustomerId", column[0]);
				if(column[1] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[1].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("customerName", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("customerId", column[1]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("customerName", "");
							data.put("customerId", "");
						}
					}
				}
				else
				{
					data.put("customerName", "");
					data.put("customerId", "");
				}
				data.put("startDate", column[2].toString());
				data.put("endDate", column[3].toString());
				data.put("LastUpdate", column[7].toString());
				data.put("status", column[8]);
				data.put("ratecustomerCompanyId", column[9]);
				List<Object[]> ratehargas = mRateHargaService.getbyCustomerRateHarga("a", Integer.parseInt(column[0].toString()), column[9].toString());
				ratehargas.stream().forEach(columndetail-> {
				JSONObject datadetail = new JSONObject();
				datadetail.put("mRateId1", columndetail[0]);
				datadetail.put("mRateId2", columndetail[1]);
				datadetail.put("ratehargaId1", columndetail[2] == null ? "" : columndetail[2]);
				datadetail.put("ratehargaId2", columndetail[3] == null ? "" : columndetail[3]);
				if(columndetail[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(columndetail[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							datadetail.put("customerName", datacustomer.getJSONObject(i).getString("contact_name"));
							datadetail.put("customerId", columndetail[4]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							datadetail.put("customerName", "");
							datadetail.put("customerId", "");
						}
					}
				}
				else
				{
					datadetail.put("customerName", "");
					datadetail.put("customerId", "");
				}
				if(columndetail[5] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(columndetail[5].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							datadetail.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							datadetail.put("mKotaIdFrom", columndetail[5]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							datadetail.put("kotaNameFrom", "");
							datadetail.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					datadetail.put("kotaNameFrom", "");
					datadetail.put("mKotaIdFrom", "");
				}
				if(columndetail[6] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(columndetail[6].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							datadetail.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							datadetail.put("mProvinceIdFrom", columndetail[6]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							datadetail.put("provinceNameFrom", "");
							datadetail.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					datadetail.put("provinceNameFrom", "");
					datadetail.put("mProvinceIdFrom", "");
				}
				if(columndetail[7] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(columndetail[7].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							datadetail.put("kotaNameTo", datacities.getJSONObject(i).get("city_name"));
							datadetail.put("mKotaIdTo", columndetail[7]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							datadetail.put("kotaNameTo", "");
							datadetail.put("mKotaIdTo", "");
						}
					}
				}
				else
				{
					datadetail.put("kotaNameTo", "");
					datadetail.put("mKotaIdTo", "");
				}
				if(columndetail[8] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(columndetail[8].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							datadetail.put("provinceNameTo", dataprovinces.getJSONObject(i).get("prov_name"));
							datadetail.put("mProvinceIdTo", columndetail[8]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							datadetail.put("provinceNameTo", "");
							datadetail.put("mProvinceIdTo", "");
						}
					}
				}
				else
				{
					datadetail.put("provinceNameTo", "");
					datadetail.put("mProvinceIdTo", "");
				}
				datadetail.put("service_id", columndetail[9] == null ? "" : columndetail[9]);
				datadetail.put("rateServiceName", columndetail[10] == null ? "" : columndetail[10].toString());
				datadetail.put("rateVehicle", columndetail[11] == null ? "" : columndetail[11].toString());
				datadetail.put("ratePrice", columndetail[12] == null ? "" : columndetail[12].toString());
				datadetail.put("rateHargaDiscount", columndetail[13] == null ? "" : columndetail[13].toString());
				datadetail.put("rateHargaNettprice", columndetail[14] == null ? "" : String.format("%.0f",  columndetail[14]));
				datadetail.put("startDate", columndetail[15] == null ? "" : columndetail[15].toString());
				datadetail.put("endDate", columndetail[16] == null ? "" : columndetail[16].toString());
				datadetail.put("status", columndetail[17] == null ? "" : columndetail[17]);
				datadetail.put("ratehargaCompanyId", columndetail[18]);
				datashargas.add(datadetail);
				});
				data.put("hargaArray", datashargas);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate Harga berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate Harga gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/getRateHargaDetail/{paramAktif}")
	public String getRateHargaDetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateHargaService.getRateDetail(paramAktif.toString().toLowerCase(), dataRequest.getInt("ratehargaId1"), dataRequest.getInt("ratehargaId2"), dataRequest.getString("rateHargaCompanyId"));
			rates.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("mRateId1", column[0]);
				data.put("mRateId2", column[1]);
				data.put("ratehargaId1", column[2]);
				data.put("ratehargaId2", column[3]);
				if(column[4] != null)
				{
					for(int i = 0; i < datacustomer.size(); i++)
					{
						if(column[4].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
						{
							data.put("customerName", datacustomer.getJSONObject(i).getString("contact_name"));
							data.put("customerId", column[4]);
							break;
						}
						else if(datacustomer.size() == i + 1)
						{
							data.put("customerName", "");
							data.put("customerId", "");
						}
					}
				}
				else
				{
					data.put("customerName", "");
					data.put("customerId", "");
				}
				if(column[5] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[5].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[5]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				if(column[6] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[6].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdFrom", column[6]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameFrom", "");
							data.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					data.put("provinceNameFrom", "");
					data.put("mProvinceIdFrom", "");
				}
				if(column[7] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[7].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameTo", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdTo", column[7]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameTo", "");
							data.put("mKotaIdTo", "");
						}
					}
				}
				else
				{
					data.put("kotaNameTo", "");
					data.put("mKotaIdTo", "");
				}
				if(column[8] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[8].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameTo", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdTo", column[8]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameTo", "");
							data.put("mProvinceIdTo", "");
						}
					}
				}
				else
				{
					data.put("provinceNameTo", "");
					data.put("mProvinceIdTo", "");
				}
				data.put("service_id", column[9] == null ? "" : column[9]);
				data.put("rateServiceName", column[10] == null ? "" : column[10].toString());
				data.put("rateVehicle", column[11] == null ? "" : column[11].toString());
				data.put("ratePrice", column[12] == null ? "" : column[12].toString());
				data.put("rateHargaDiscount", column[13] == null ? "" : column[13].toString());
				data.put("rateHargaNettprice", column[14] == null ? "" : String.format("%.0f",  column[14]));
				data.put("startDate", column[15] == null ? "" : column[15].toString());
				data.put("endDate", column[16] == null ? "" : column[16].toString());
				data.put("status", column[17] == null ? "" : column[17]);
				data.put("ratehargaCompanyId", column[18]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate Harga berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate Harga gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/saveRateharga")
	public String saveRateharga(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
    	LocalDateTime localDateTime = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
    	String formatDateTime = localDateTime.format(formatter);
    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
    	int id = 0;
    	
    	
    	if(dataRequest.has("mRatecustomerId"))
    	{
    		id = dataRequest.getInt("mRatecustomerId");
    	}
    	else
    	{
    		List<Object[]> dataNext = mRateCustomerService.nextvalMRateCustomer();
    		id = Integer.parseInt(String.valueOf(dataNext.get(0)));
    	}
    	
    	
	    try
	    {
	    	ArrayList <MRateHarga> datasRateHargas = new ArrayList<>();
	    	MRateCustomer datacust = new MRateCustomer();
	    	datacust.setRatecustomerId(id);
	    	datacust.setmCustomerId(dataRequest.getInt("mCustomerId"));
	    	if(dataRequest.get("startDate").equals(""))
	    	{
	    		datacust.setRatecustomerStartdate(null);	
	    	}
	    	else
	    	{
	    		datacust.setRatecustomerStartdate(LocalDate.parse(dataRequest.getString("startDate")));
	    	}
	    	if(dataRequest.get("endDate").equals(""))
	    	{
	    		datacust.setRatecustomerEnddate(null);
	    	}
	    	else
	    	{
	    		datacust.setRatecustomerEnddate(LocalDate.parse(dataRequest.getString("endDate")));
	    	}
	    	datacust.setRatecustomerCreatedBy(user_uuid);
	    	datacust.setRatecustomerCreatedAt(localDateTime2);
	    	datacust.setRatecustomerUpdatedBy(user_uuid);
	    	datacust.setRatecustomerUpdatedAt(localDateTime2);
	    	datacust.setRatecustomerStatus(dataRequest.getBoolean("status"));
	    	datacust.setRatecustomerCompanyId(dataRequest.getString("rateCustomerCompanyId"));
	    	JSONArray ratehargaArray = dataRequest.getJSONArray("ratehargaArray");
			if(ratehargaArray.size() > 0)
			{
	    		for(int i = 0; i < ratehargaArray.size(); i++)
	    		{
	    			MRateHarga data = new MRateHarga();
	    			data.setmRateId1(ratehargaArray.getJSONObject(i).getInt("mRateId1"));
	    			data.setmRateId2(ratehargaArray.getJSONObject(i).getInt("mRateId2"));
	    			data.setRatehargaId1(ratehargaArray.getJSONObject(i).getInt("ratehargaId1"));
	    			data.setRatehargaId2(ratehargaArray.getJSONObject(i).getInt("ratehargaId2"));
	    			data.setmRatecustomerId(id);
	    			data.setRatehargaDiscount(ratehargaArray.getJSONObject(i).getString("ratehargaDiscount").equals("") ? null : Float.parseFloat(ratehargaArray.getJSONObject(i).getString("ratehargaDiscount")));
	    			data.setRatehargaNettprice(ratehargaArray.getJSONObject(i).getString("ratehargaNettprice").equals("") ? null : Float.parseFloat(ratehargaArray.getJSONObject(i).getString("ratehargaNettprice")));
	    			data.setRatehargaStartdate(ratehargaArray.getJSONObject(i).getString("startDateHarga").equals("") ? null : LocalDate.parse(ratehargaArray.getJSONObject(i).getString("startDateHarga")));
	    			data.setRatehargaEnddate(ratehargaArray.getJSONObject(i).getString("endDateHarga").equals("") ? null : LocalDate.parse(ratehargaArray.getJSONObject(i).getString("endDateHarga")));
	    			data.setRatehargaStatus(ratehargaArray.getJSONObject(i).getBoolean("ratehargaStatus"));
	    			data.setRatehargaCompanyId(dataRequest.getString("rateCustomerCompanyId"));
	    			datasRateHargas.add(data);
	    		}
			}
			allRateCustomerService.saveallRateCustomer(datacust, datasRateHargas);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Rate success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Rate failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateallRateharga")
	public String updateallRateharga(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
    	LocalDateTime localDateTime = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
    	String formatDateTime = localDateTime.format(formatter);
    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    try
	    {
	    	ArrayList <MRateHarga> datasRateHargas = new ArrayList<>();
	    	Optional <MRateCustomer> dataUpdateRateCustomer = mRateCustomerService.getDetail(dataRequest.getInt("mRatecustomerId"), dataRequest.getString("rateCustomerCompanyId"));
	    	if(dataUpdateRateCustomer.isPresent())
	    	{
	    		if(dataRequest.has("mCustomerId"))
	    		{
	    			((MRateCustomer)dataUpdateRateCustomer.get()).setmCustomerId(dataRequest.getInt("mCustomerId"));	
	    		}
		    	if(dataRequest.get("startDate").equals(""))
		    	{
		    		((MRateCustomer)dataUpdateRateCustomer.get()).setRatecustomerStartdate(null);	
		    	}
		    	else
		    	{
		    		((MRateCustomer)dataUpdateRateCustomer.get()).setRatecustomerStartdate(LocalDate.parse(dataRequest.getString("startDate")));
		    	}
		    	if(dataRequest.get("endDate").equals(""))
		    	{
		    		((MRateCustomer)dataUpdateRateCustomer.get()).setRatecustomerEnddate(null);
		    	}
		    	else
		    	{
		    		((MRateCustomer)dataUpdateRateCustomer.get()).setRatecustomerEnddate(LocalDate.parse(dataRequest.getString("endDate")));
		    	}
	    		((MRateCustomer)dataUpdateRateCustomer.get()).setRatecustomerUpdatedBy(user_uuid);
	    		((MRateCustomer)dataUpdateRateCustomer.get()).setRatecustomerUpdatedAt(localDateTime2);
		    	JSONArray ratehargaArray = dataRequest.getJSONArray("ratehargaArray");
				if(ratehargaArray.size() > 0)
				{
		    		for(int i = 0; i < ratehargaArray.size(); i++)
		    		{
		    	    	Optional <MRateHarga> dataUpdateRateHarga = mRateHargaService.getDetail(ratehargaArray.getJSONObject(i).getInt("mRateId1"), ratehargaArray.getJSONObject(i).getInt("mRateId2"), ratehargaArray.getJSONObject(i).getInt("ratehargaId1"), ratehargaArray.getJSONObject(i).getInt("ratehargaId2") , dataRequest.getInt("mRatecustomerId"), dataRequest.getString("rateCustomerCompanyId"));
				    	if(dataUpdateRateHarga.isPresent())
				    	{
				    		((MRateHarga)dataUpdateRateHarga.get()).setmRateId1(ratehargaArray.getJSONObject(i).getInt("mRateId1"));
				    		((MRateHarga)dataUpdateRateHarga.get()).setmRateId2(ratehargaArray.getJSONObject(i).getInt("mRateId2"));
				    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaId1(ratehargaArray.getJSONObject(i).getInt("ratehargaId1"));
				    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaId2(ratehargaArray.getJSONObject(i).getInt("ratehargaId2"));
				    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaDiscount(ratehargaArray.getJSONObject(i).getString("ratehargaDiscount").equals("") ? null : Float.parseFloat(ratehargaArray.getJSONObject(i).getString("ratehargaDiscount")));
				    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaNettprice(ratehargaArray.getJSONObject(i).getString("ratehargaNettprice").equals("") ? null : Float.parseFloat(ratehargaArray.getJSONObject(i).getString("ratehargaNettprice")));
				    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaStartdate(ratehargaArray.getJSONObject(i).getString("startDateHarga").equals("") ? null : LocalDate.parse(ratehargaArray.getJSONObject(i).getString("startDateHarga")));
				    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaEnddate(ratehargaArray.getJSONObject(i).getString("endDateHarga").equals("") ? null : LocalDate.parse(ratehargaArray.getJSONObject(i).getString("endDateHarga")));
				    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaStatus(ratehargaArray.getJSONObject(i).getBoolean("ratehargaStatus"));
				    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaCompanyId(dataRequest.getString("rateCustomerCompanyId"));
			    			datasRateHargas.add(dataUpdateRateHarga.get());	
				    	}
		    		}
				}
				allRateCustomerService.saveallRateCustomer(dataUpdateRateCustomer.get(), datasRateHargas);
	    	}	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Rate success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Rate failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateRateharga")
	public String updateRateHarga(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
    	LocalDateTime localDateTime = LocalDateTime.now();
    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
    	String formatDateTime = localDateTime.format(formatter);
    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    try
	    {
	    	Optional <MRateCustomer> dataUpdateRateCustomer = mRateCustomerService.getDetail(0, dataRequest.getString("ratehargaCompanyId"));
	    	Optional <MRateHarga> dataUpdateRateHarga = mRateHargaService.getDetail(dataRequest.getInt("mRateId1"), dataRequest.getInt("mRateId2"), dataRequest.getInt("ratehargaId1"), 0 , 0, dataRequest.getString("ratehargaCompanyId"));
	    	if(dataRequest.has("mRatecustomerId"))
	    	{
	    		dataUpdateRateCustomer = mRateCustomerService.getDetail(dataRequest.getInt("mRatecustomerId"), dataRequest.getString("ratehargaCompanyId"));
	    		dataUpdateRateHarga = mRateHargaService.getDetail(dataRequest.getInt("mRateId1"), dataRequest.getInt("mRateId2"), dataRequest.getInt("ratehargaId1"), dataRequest.getInt("ratehargaId2") , dataRequest.getInt("mRatecustomerId"), dataRequest.getString("ratehargaCompanyId"));
	    	}
	    	if(dataUpdateRateCustomer.isPresent())
	    	{
	    		((MRateCustomer)dataUpdateRateCustomer.get()).setRatecustomerUpdatedBy(user_uuid);
	    		((MRateCustomer)dataUpdateRateCustomer.get()).setRatecustomerUpdatedAt(localDateTime2);
		    	if(dataUpdateRateHarga.isPresent())
		    	{
    		    	if(dataRequest.has("ratehargaDiscount"))
    		    	{
    		    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaDiscount(Float.parseFloat(dataRequest.getString("ratehargaDiscount")));
    		    	}
    		    	if(dataRequest.has("ratehargaNettprice"))
    		    	{
    		    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaNettprice(Float.parseFloat(dataRequest.getString("ratehargaNettprice")));
    		    	}
    		    	if(dataRequest.get("startDate").equals(""))
    		    	{
    		    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaStartdate(null);
    		    	}
    		    	else
    		    	{
    		    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaStartdate(LocalDate.parse(dataRequest.getString("startDate")));
    		    	}
    		    	if(dataRequest.get("endDate").equals(""))
    		    	{
    		    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaEnddate(null);
    		    	}
    		    	else
    		    	{
    		    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaEnddate(LocalDate.parse(dataRequest.getString("endDate")));
    		    	}
    		    	if(dataRequest.has("ratehargaStatus"))
    		    	{
    		    		((MRateHarga)dataUpdateRateHarga.get()).setRatehargaStatus(dataRequest.getBoolean("ratehargaStatus"));
    		    	}
    		    	mRateHargaService.saverateharga(dataUpdateRateHarga.get());
		    	}
		    	mRateCustomerService.saveallrateharga(dataUpdateRateCustomer.get());
    	    	response.put("responseCode", "00");
    		    response.put("responseDesc", "Save Rate success");
	    	}
	    	else
	    	{
	        	int id = 0;
	        	List<Object[]> dataNext = mRateCustomerService.nextvalMRateCustomer();
	        	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    		MRateCustomer datacust = new MRateCustomer();
	    		datacust.setRatecustomerId(id);
	    		datacust.setmCustomerId(dataRequest.getInt("mCustomerId"));
				datacust.setRatecustomerStartdate(LocalDate.parse(dataRequest.getString("startDateCustomer")));
				datacust.setRatecustomerEnddate(LocalDate.parse(dataRequest.getString("endDateCustomer")));
		    	datacust.setRatecustomerCreatedBy(user_uuid);
		    	datacust.setRatecustomerCreatedAt(localDateTime2);
		    	datacust.setRatecustomerUpdatedBy(user_uuid);
		    	datacust.setRatecustomerUpdatedAt(localDateTime2);
		    	datacust.setRatecustomerStatus(dataRequest.getBoolean("status"));
		    	datacust.setRatecustomerCompanyId(dataRequest.getString("ratehargaCompanyId"));
		    	mRateCustomerService.saveallrateharga(datacust);
	    		MRateHarga data = new MRateHarga();
    			data.setmRateId1(dataRequest.getInt("mRateId1"));
    			data.setmRateId2(dataRequest.getInt("mRateId2"));
    			data.setRatehargaId1(dataRequest.getInt("ratehargaId1"));
    			data.setRatehargaId2(dataRequest.getInt("ratehargaId2"));
    			data.setmRatecustomerId(id);
    			data.setRatehargaDiscount(dataRequest.getString("ratehargaDiscount").equals("") ? null : Float.parseFloat(dataRequest.getString("ratehargaDiscount")));
    			data.setRatehargaNettprice(dataRequest.getString("ratehargaNettprice").equals("") ? null : Float.parseFloat(dataRequest.getString("ratehargaNettprice")));
    			data.setRatehargaStartdate(dataRequest.getString("startDate").equals("") ? null : LocalDate.parse(dataRequest.getString("startDate")));
    			data.setRatehargaEnddate(dataRequest.getString("endDate").equals("") ? null : LocalDate.parse(dataRequest.getString("endDate")));
    			data.setRatehargaStatus(dataRequest.getBoolean("ratehargaStatus"));
    			data.setRatehargaCompanyId(dataRequest.getString("ratehargaCompanyId"));
    			mRateHargaService.saverateharga(data);
    	    	response.put("responseCode", "00");
    		    response.put("responseDesc", "Save Rate success");
    		    response.put("responsemratecustid", id);
	    	}
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Rate failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
