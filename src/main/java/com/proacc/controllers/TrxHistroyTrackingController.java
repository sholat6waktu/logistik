package com.proacc.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.service.MDriverService;
import com.proacc.service.MVolumeService;
import com.proacc.service.MWeightService;
import com.proacc.service.TrxDriverTrackingService;
import com.proacc.service.TrxDriverscheduleService;
import com.proacc.service.TrxHistroyTrackingService;
import com.proacc.service.TrxJalurDriverService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxTrackingService;
import com.proacc.service.TrxHistroyTrackingService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TrxHistroyTrackingController {
	private static final Logger logger = LoggerFactory.getLogger(TrxHistroyTrackingController.class);
	
	@Autowired
	TrxOrderService trxOrderService;
	@Autowired
	TrxTrackingService trxTrackingService;
	@Autowired
	MDriverService mDriverService;
	@Autowired
	TrxDriverscheduleService trxDriverscheduleService;
	@Autowired
	MWeightService mWeightService;
	@Autowired
	MVolumeService mVolumeService;
	@Autowired
	TrxDriverTrackingService TrxDriverTrackingService;
	@Autowired
	TrxJalurDriverService TrxJalurDriverService;
	
	@Autowired
	TrxHistroyTrackingService TrxHistroyTrackingService;
	
	@PostMapping("/getHistroyTracking")
	public String getHistroyTracking(@RequestBody String request
//			, @RequestHeader(value = "User-Access") String header
			)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		
		logger.info("API == getHistroyTracking");
		logger.info("INPUT == " + dataRequest);
		try
		{
			List<Object[]> getHistroy = TrxHistroyTrackingService.getByOrderId(dataRequest.getString("orderId"), dataRequest.getString("companyId"));
			if(getHistroy.size() >0)
			{
				getHistroy.stream().forEach(col->{
					JSONObject data = new JSONObject();
					data.put("orderId", col[0]);
					data.put("id", col[1]);
					data.put("status", col[2]);
//					data.put("cabangName", col[3] == null ? "" : col[3].toString());
					data.put("mProvinceIdCabang", col[4] == null ? "" : col[4]);
					data.put("mKotaIdCabang", col[5] == null ? "" : col[5]);
//					data.put("agenName", col[6] == null ? "" : col[6].toString());
					data.put("mProvinceIdAgen", col[7] == null ? "" : col[7]);
					data.put("mKotaIdAgen", col[8] == null ? "" : col[8]);
					data.put("waktu", col[9] == null ? "" : col[9].toString());
					datas.add(data);
				});
			}
			//tambahin satu header value nya order id nya. akan di by pass. key nya,
			//valid-invoice
			
			response.put("responseCode", "00");
			response.put("responseDesc", "Get Detail History Tracking Detail Success");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Get Detail History Tracking Detail Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	

}
