package com.proacc.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MRate;
import com.proacc.service.MRateService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MRateController {
	private static final Logger logger = LoggerFactory.getLogger(MRateController.class);
	
	@Autowired
	MRateService mRateService;
	@PostMapping("/getAllRate/{paramAktif}")
	public String getAllRate(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateService.getallRate(paramAktif.toString().toLowerCase(), dataRequest.getString("rateCompanyId"));
			rates.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("rateId1", column[0]);
				data.put("rateId2", column[1]);
				if(column[2] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[2].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[2]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				if(column[3] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[3].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdFrom", column[3]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameFrom", "");
							data.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					data.put("provinceNameFrom", "");
					data.put("mProvinceIdFrom", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[4].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameTo", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdTo", column[4]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameTo", "");
							data.put("mKotaIdTo", "");
						}
					}
				}
				else
				{
					data.put("kotaNameTo", "");
					data.put("mKotaIdTo", "");
				}
				if(column[5] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[5].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameTo", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdTo", column[5]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameTo", "");
							data.put("mProvinceIdTo", "");
						}
					}
				}
				else
				{
					data.put("provinceNameTo", "");
					data.put("mProvinceIdTo", "");
				}
				data.put("service_id", column[14] == null ? "" : column[14]);
				data.put("rateServiceName", column[6] == null ? "" : column[6].toString());
				data.put("rateVehicle", column[7] == null ? "" : column[7].toString());
				data.put("ratePrice", column[8] == null ? "" : String.format("%.0f",  column[8]));
				data.put("rateStartdate", column[9] == null ? "" : column[9].toString());
				data.put("rateEnddate", column[10] == null ? "" : column[10].toString());
				data.put("rateDraftstatus", column[11] == null ? "" : column[11].toString());
				data.put("rateStatus", column[12] == null ? "" : column[12].toString());
				data.put("rateCompanyId", column[13].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getAllRatebyStats/{paramAktif}")
	public String getAllbyStats(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateService.getallRatebystats(paramAktif.toString().toLowerCase(), dataRequest.getString("rateCompanyId"), dataRequest.getString("rateDraftstatus"));
			rates.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("rateId1", column[0]);
				data.put("rateId2", column[1]);
				if(column[2] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[2].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[2]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				if(column[3] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[3].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdFrom", column[3]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameFrom", "");
							data.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					data.put("provinceNameFrom", "");
					data.put("mProvinceIdFrom", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[4].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameTo", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdTo", column[4]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameTo", "");
							data.put("mKotaIdTo", "");
						}
					}
				}
				else
				{
					data.put("kotaNameTo", "");
					data.put("mKotaIdTo", "");
				}
				if(column[5] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[5].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameTo", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdTo", column[5]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameTo", "");
							data.put("mProvinceIdTo", "");
						}
					}
				}
				else
				{
					data.put("provinceNameTo", "");
					data.put("mProvinceIdTo", "");
				}
				data.put("service_id", column[14] == null ? "" : column[14]);
				data.put("rateServiceName", column[6] == null ? "" : column[6].toString());
				data.put("rateVehicle", column[7] == null ? "" : column[7].toString());
				data.put("ratePrice", column[8] == null ? "" :  String.format("%.0f",  column[8]));
				data.put("rateStartdate", column[9] == null ? "" : column[9].toString());
				data.put("rateEnddate", column[10] == null ? "" : column[10].toString());
				data.put("rateDraftstatus", column[11] == null ? "" : column[11].toString());
				data.put("rateStatus", column[12] == null ? "" : column[12].toString());
				data.put("rateCompanyId", column[13].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getRate/{paramAktif}")
	public String getRate(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateService.getRate(paramAktif.toString().toLowerCase(), dataRequest.getInt("rateId1"), dataRequest.getString("rateCompanyId"));
			rates.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("rateId1", column[0]);
				data.put("rateId2", column[1]);
				if(column[2] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[2].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[2]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				if(column[3] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[3].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdFrom", column[3]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameFrom", "");
							data.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					data.put("provinceNameFrom", "");
					data.put("mProvinceIdFrom", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[4].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[4]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				if(column[5] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[5].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdFrom", column[5]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameFrom", "");
							data.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					data.put("provinceNameFrom", "");
					data.put("mProvinceIdFrom", "");
				}
				data.put("service_id", column[14] == null ? "" : column[14]);
				data.put("rateServiceName", column[6] == null ? "" : column[6].toString());
				data.put("rateVehicle", column[7] == null ? "" : column[7].toString());
				data.put("ratePrice", column[8] == null ? "" :  String.format("%.0f",  column[8]));
				data.put("rateStartdate", column[9] == null ? "" : column[9].toString());
				data.put("rateEnddate", column[10] == null ? "" : column[10].toString());
				data.put("rateDraftstatus", column[11] == null ? "" : column[11].toString());
				data.put("rateStatus", column[12] == null ? "" : column[12].toString());
				data.put("rateCompanyId", column[13].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getRateDetail/{paramAktif}")
	public String getRateDetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> rates = mRateService.getRateDetail(paramAktif.toString().toLowerCase(), dataRequest.getInt("rateId1"), dataRequest.getInt("rateId2"), dataRequest.getString("rateCompanyId"));
			rates.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("rateId1", column[0]);
				data.put("rateId2", column[1]);
				if(column[2] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[2].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[2]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				if(column[3] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[3].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdFrom", column[3]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameFrom", "");
							data.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					data.put("provinceNameFrom", "");
					data.put("mProvinceIdFrom", "");
				}
				if(column[4] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[4].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaNameFrom", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaIdFrom", column[4]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaNameFrom", "");
							data.put("mKotaIdFrom", "");
						}
					}
				}
				else
				{
					data.put("kotaNameFrom", "");
					data.put("mKotaIdFrom", "");
				}
				if(column[5] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column[5].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							data.put("provinceNameFrom", dataprovinces.getJSONObject(i).get("prov_name"));
							data.put("mProvinceIdFrom", column[5]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							data.put("provinceNameFrom", "");
							data.put("mProvinceIdFrom", "");
						}
					}
				}
				else
				{
					data.put("provinceNameFrom", "");
					data.put("mProvinceIdFrom", "");
				}
				data.put("service_id", column[14] == null ? "" : column[14]);
				data.put("rateServiceName", column[6] == null ? "" : column[6].toString());
				data.put("rateVehicle", column[7] == null ? "" : column[7].toString());
				data.put("ratePrice", column[8] == null ? "" :  String.format("%.0f",  column[8]));
				data.put("rateStartdate", column[9] == null ? "" : column[9].toString());
				data.put("rateEnddate", column[10] == null ? "" : column[10].toString());
				data.put("rateDraftstatus", column[11] == null ? "" : column[11].toString());
				data.put("rateStatus", column[12] == null ? "" : column[12].toString());
				data.put("rateCompanyId", column[13].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Rate berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Rate gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/saveRate")
	public String saveRate(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	ArrayList <MRate> datasRates = new ArrayList <MRate>();
	    	int id = 0;
	    	List<Object[]> dataNext = mRateService.nextvalMRate();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	int id2 = 0;
			JSONArray rateArray = dataRequest.getJSONArray("rateArray");
	    	JSONArray serviceArray = dataRequest.getJSONArray("serviceArray");
	    	if(serviceArray.size() > 0)
	    	{
	    		for(int i = 0; i < serviceArray.size(); i++)
	    		{
	    	    	if(serviceArray.getJSONObject(i).get("darat").equals(true))
    		    	{
    	    			if(rateArray.size() > 0)
    	    			{
    	    	    		for(int k = 0; k < rateArray.size(); k++)
    	    	    		{
//    	    	    			List<Object[]> rates = mRateService.checkinsert(rateArray.getJSONObject(k).getInt("kotaFrom"),
//    	    	    					rateArray.getJSONObject(k).getInt("kotaTo"), 
//    	    	    					serviceArray.getJSONObject(i).getInt("service_id"), 
//    	    	    					"darat", 
//    	    	    					serviceArray.getJSONObject(i).getString("start_darat").equals("") ? "" :serviceArray.getJSONObject(i).getString("start_darat"), 
//    	    	    					serviceArray.getJSONObject(i).getString("end_darat").equals("") ? "" :serviceArray.getJSONObject(i).getString("end_darat"), 
//    	    	    					dataRequest.getString("rateCompanyId"));
//    	    	    			response.put("responsetest", rates.get(0)[0]);
//    	    	    			if(rates.get(0)[0].equals(0))
//    	    	    			{
	    	    	    			id2++;
	    	    	    	    	MRate data = new MRate();
	    	    	    	    	data.setRateId1(id);
	    	    	    	    	data.setRateId2(id2);
	    	    	    	    	data.setmKotaFromId(rateArray.getJSONObject(k).getInt("kotaFrom"));
	    	    	    	    	data.setmProvinceFromId(rateArray.getJSONObject(k).getInt("provinceFrom"));
	    	    	    	    	data.setmKotaToId(rateArray.getJSONObject(k).getInt("kotaTo"));
	    	    	    	    	data.setmProvinceToId(rateArray.getJSONObject(k).getInt("provinceTo"));
	    	    	    	    	data.setmServiceId(serviceArray.getJSONObject(i).getInt("service_id"));
	    	    		    		data.setRateVehicle("darat");
	    	    		    		data.setRateStartdate(serviceArray.getJSONObject(i).get("start_darat").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("start_darat")));
	    	    		    		data.setRateEnddate(serviceArray.getJSONObject(i).get("end_darat").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("end_darat")));
	    	    		    		data.setRateDraftstatus("draft");
	    	    	    	    	data.setRateStatus(dataRequest.getBoolean("rateStatus"));
	    	    	    	    	data.setRateCompanyId(dataRequest.getString("rateCompanyId"));
	    	    	    	    	datasRates.add(data);	
//    	    	    			}
    	    	    		}
    	    			}	
    		    	}
	    	    	if(serviceArray.getJSONObject(i).get("laut").equals(true))
    		    	{
    	    			if(rateArray.size() > 0)
    	    			{
    	    	    		for(int k = 0; k < rateArray.size(); k++)
    	    	    		{
//    	    	    			List<Object[]> rates = mRateService.checkinsert(rateArray.getJSONObject(k).getInt("kotaFrom"),
//    	    	    					rateArray.getJSONObject(k).getInt("kotaTo"), 
//    	    	    					serviceArray.getJSONObject(i).getInt("service_id"), 
//    	    	    					"laut", 
//    	    	    					serviceArray.getJSONObject(i).getString("start_laut").equals("") ? "" :serviceArray.getJSONObject(i).getString("start_laut"), 
//		    	    	    			serviceArray.getJSONObject(i).getString("end_laut").equals("") ? "" :serviceArray.getJSONObject(i).getString("end_laut"), 
//    	    	    					dataRequest.getString("rateCompanyId"));
//    	    	    			response.put("responsetest", rates.get(0)[0]);
//    	    	    			if(rates.get(0)[0].equals(0))
//    	    	    			{
    	    	    				id2++;
	    	    	    	    	MRate data = new MRate();
	    	    	    	    	data.setRateId1(id);
	    	    	    	    	data.setRateId2(id2);
	    	    	    	    	data.setmKotaFromId(rateArray.getJSONObject(k).getInt("kotaFrom"));
	    	    	    	    	data.setmProvinceFromId(rateArray.getJSONObject(k).getInt("provinceFrom"));
	    	    	    	    	data.setmKotaToId(rateArray.getJSONObject(k).getInt("kotaTo"));
	    	    	    	    	data.setmProvinceToId(rateArray.getJSONObject(k).getInt("provinceTo"));
	    	    	    	    	data.setmServiceId(serviceArray.getJSONObject(i).getInt("service_id"));
	    	    		    		data.setRateVehicle("laut");
	    	    		    		data.setRateStartdate(serviceArray.getJSONObject(i).get("start_laut").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("start_laut")));
	    	    		    		data.setRateEnddate(serviceArray.getJSONObject(i).get("end_laut").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("end_laut")));
	    	    		    		data.setRateDraftstatus("draft");
	    	    	    	    	data.setRateStatus(dataRequest.getBoolean("rateStatus"));
	    	    	    	    	data.setRateCompanyId(dataRequest.getString("rateCompanyId"));
	    	    	    	    	datasRates.add(data);	
//    	    	    			}
    	    	    		}
    	    			}
    		    	}
	    	    	if(serviceArray.getJSONObject(i).get("udara").equals(true))
    		    	{
    	    			if(rateArray.size() > 0)
    	    			{
    	    	    		for(int k = 0; k < rateArray.size(); k++)
    	    	    		{
//    	    	    			List<Object[]> rates = mRateService.checkinsert(rateArray.getJSONObject(k).getInt("kotaFrom"),
//    	    	    					rateArray.getJSONObject(k).getInt("kotaTo"), 
//    	    	    					serviceArray.getJSONObject(i).getInt("service_id"), 
//    	    	    					"udara", 
//    	    	    					serviceArray.getJSONObject(i).getString("start_udara").equals("") ? "" :serviceArray.getJSONObject(i).getString("start_udara"), 
//    	    	    					serviceArray.getJSONObject(i).getString("end_udara").equals("") ? "" :serviceArray.getJSONObject(i).getString("end_udara"), 
//    	    	    					dataRequest.getString("rateCompanyId"));
//    	    	    			response.put("responsetest", rates.get(0)[0]);
//    	    	    			if(rates.get(0)[0].equals(0))
//    	    	    			{
	      	    	    			id2++;
	    	    	    	    	MRate data = new MRate();
	    	    	    	    	data.setRateId1(id);
	    	    	    	    	data.setRateId2(id2);
	    	    	    	    	data.setmKotaFromId(rateArray.getJSONObject(k).getInt("kotaFrom"));
	    	    	    	    	data.setmProvinceFromId(rateArray.getJSONObject(k).getInt("provinceFrom"));
	    	    	    	    	data.setmKotaToId(rateArray.getJSONObject(k).getInt("kotaTo"));
	    	    	    	    	data.setmProvinceToId(rateArray.getJSONObject(k).getInt("provinceTo"));
	    	    	    	    	data.setmServiceId(serviceArray.getJSONObject(i).getInt("service_id"));
	    	    		    		data.setRateVehicle("udara");
	    	    		    		data.setRateStartdate(serviceArray.getJSONObject(i).get("start_udara").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("start_udara")));
	    	    		    		data.setRateEnddate(serviceArray.getJSONObject(i).get("end_udara").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("end_udara")));
	    	    		    		data.setRateDraftstatus("draft");
	    	    	    	    	data.setRateStatus(dataRequest.getBoolean("rateStatus"));
	    	    	    	    	data.setRateCompanyId(dataRequest.getString("rateCompanyId"));
	    	    	    	    	datasRates.add(data);	
//    	    	    			}
    	    	    		}
    	    			}
    		    	}
	    		    }
	    		}
	    	mRateService.saveallRate(datasRates);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Rate success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Rate failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/copyRate")
	public String copyRate(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	JSONArray rateArray = dataRequest.getJSONArray("rateArray");
			if(rateArray.size() > 0)
			{
	    		for(int i = 0; i < rateArray.size(); i++)
	    		{
	    	    	ArrayList <MRate> datasRates = new ArrayList <MRate>();
	    			List<Object[]> lastind = mRateService.getRatelastindex("a", rateArray.getJSONObject(i).getInt("rateId1"), rateArray.getJSONObject(i).getString("rateCompanyId"));
	    			List<Object[]> rates = mRateService.getRateDetail("a", rateArray.getJSONObject(i).getInt("rateId1"), rateArray.getJSONObject(i).getInt("rateId2"), rateArray.getJSONObject(i).getString("rateCompanyId"));
						int id2 = Integer.parseInt(lastind.get(0)[1].toString());
						id2++;
						MRate data = new MRate();
    	    	    	data.setRateId1(Integer.parseInt(rates.get(0)[0].toString()));
    	    	    	data.setRateId2(id2);
    	    	    	data.setmKotaFromId(Integer.parseInt(rates.get(0)[2].toString()));
    	    	    	data.setmProvinceFromId(Integer.parseInt(rates.get(0)[3].toString()));
    	    	    	data.setmKotaToId(Integer.parseInt(rates.get(0)[4].toString()));
    	    	    	data.setmProvinceToId(Integer.parseInt(rates.get(0)[5].toString()));
    	    	    	data.setmServiceId(Integer.parseInt(rates.get(0)[14].toString()));
    	    	    	data.setRateVehicle(rates.get(0)[7].toString());
    		    		data.setRateDraftstatus("draft");
    	    	    	data.setRateStatus(Boolean.parseBoolean(rates.get(0)[12].toString()));
    	    	    	data.setRateCompanyId(rates.get(0)[13].toString());
    		    		datasRates.add(data);
    		    		mRateService.saveallRate(datasRates);
	    		}
			}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Rate success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Rate failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateRate")
	public String updateRate(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
			JSONObject response = new JSONObject();
		    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
			ArrayList <MRate> datasRates = new ArrayList <MRate>();
		    try
		    {
    				JSONArray rateArray = dataRequest.getJSONArray("rateArray");
	    			if(rateArray.size() > 0)
	    			{
	    				
	    	    		for(int i = 0; i < rateArray.size(); i++)
	    	    		{
	    	    			List<Object[]> rates = mRateService.checkupdate(rateArray.getJSONObject(i).getInt("kotaFrom"),
	    	    					rateArray.getJSONObject(i).getInt("kotaTo"), 
	    	    					rateArray.getJSONObject(i).getInt("service_id"), 
	    	    					rateArray.getJSONObject(i).getString("vehicle"), 
	    	    					rateArray.getJSONObject(i).get("startdate").equals("") ? "" : rateArray.getJSONObject(i).getString("startdate"), 
	    	    					rateArray.getJSONObject(i).get("enddate").equals("") ? "" : rateArray.getJSONObject(i).getString("enddate"), 
	    	    					dataRequest.getString("rateCompanyId"), 
	    	    					rateArray.getJSONObject(i).getInt("rateId2"));
	    	    			response.put("responsetest", rates.get(0)[0]);
	    	    			logger.info("cek ini juga = " + response);
	    	    			if(rates.get(0)[0].equals(0))
	    	    			{
	    	    				Optional <MRate> dataupdateRate = mRateService.getdetail(rateArray.getJSONObject(i).getInt("rateId1"), rateArray.getJSONObject(i).getInt("rateId2"), dataRequest.getString("rateCompanyId"));
		    	    			if(dataupdateRate.isPresent())
		    	    			{
		    	    		    	if(rateArray.getJSONObject(i).has("kotaFrom"))
		    	    		    	{
			    	    				((MRate)dataupdateRate.get()).setmKotaFromId(rateArray.getJSONObject(i).getInt("kotaFrom"));		
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).has("provinceFrom"))
		    	    		    	{
			    	    				((MRate)dataupdateRate.get()).setmProvinceFromId(rateArray.getJSONObject(i).getInt("provinceFrom"));	
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).has("kotaTo"))
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setmKotaToId(rateArray.getJSONObject(i).getInt("kotaTo"));
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).has("provinceTo"))
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setmProvinceToId(rateArray.getJSONObject(i).getInt("provinceTo"));	
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).has("service"))
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setmServiceId(rateArray.getJSONObject(i).getInt("service"));
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).has("vehicle"))
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setRateVehicle(rateArray.getJSONObject(i).getString("vehicle"));
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).has("price"))
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setRatePrice(Float.parseFloat(rateArray.getJSONObject(i).getString("price")));	
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).get("startdate").equals(""))
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setRateStartdate(null);	
		    	    		    	}
		    	    		    	else
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setRateStartdate(LocalDate.parse(rateArray.getJSONObject(i).getString("startdate")));
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).get("enddate").equals(""))
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setRateEnddate(null);		
		    	    		    	}
		    	    		    	else
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setRateEnddate(LocalDate.parse(rateArray.getJSONObject(i).getString("enddate")));	
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).has("draftstatus"))
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setRateDraftstatus(rateArray.getJSONObject(i).getString("draftstatus"));
		    	    		    	}
		    	    		    	if(rateArray.getJSONObject(i).has("rateStatus"))
		    	    		    	{
		    	    		    		((MRate)dataupdateRate.get()).setRateStatus(rateArray.getJSONObject(i).getBoolean("rateStatus"));
		    	    		    	}
		    	    		    	datasRates.add(dataupdateRate.get());
		    	    			}	
	    	    			}
	    	    		}
	    			}
	    		mRateService.saveallRate(datasRates);
		    	response.put("responseCode", "00");
			    response.put("responseDesc", "Update Rate success");
		    }
		    catch(Exception e)
		    {
		    	 response.put("responseCode", "99");
			     response.put("responseDesc", "Update Rate failed");
			     response.put("responseError", e.getMessage());
			     logger.info("ERROR #### " + e.getMessage());
		    }
		    return response.toString();
	}
	@PostMapping("/updateRateperrow")
	public String updateRateperrow(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	ArrayList <MRate> datasRates = new ArrayList <MRate>();
	    	Optional <MRate> dataupdateRate = mRateService.getdetail(dataRequest.getInt("rateId1"), dataRequest.getInt("rateId2"), dataRequest.getString("rateCompanyId"));
	    	if(dataupdateRate.isPresent())
	    	{
    			List<Object[]> rates = mRateService.checkupdate(dataRequest.getInt("kotaFrom"),
    					dataRequest.getInt("kotaTo"), 
    					dataRequest.getInt("service_id"), 
    					dataRequest.getString("vehicle"), 
    					dataRequest.get("startdate")  == "" ? "" : dataRequest.getString("startdate"), 
    					dataRequest.get("enddate")  == "" ? "" : dataRequest.getString("enddate"), 
    					dataRequest.getString("rateCompanyId"), 
    					dataRequest.getInt("rateId2"));
    			response.put("responsetest", rates.get(0)[0]);
    			if(rates.get(0)[0].equals(0))
    			{
    		    	if(dataRequest.get("price").equals(""))
    		    	{
    		    		((MRate)dataupdateRate.get()).setRatePrice(null);	
    		    	}
    		    	else
    		    	{
    		    		((MRate)dataupdateRate.get()).setRatePrice(Float.parseFloat(dataRequest.getString("price")));
    		    	}
    		    	if(dataRequest.get("startdate").equals(""))
    		    	{
    		    		((MRate)dataupdateRate.get()).setRateStartdate(null);	
    		    	}
    		    	else
    		    	{
    		    		((MRate)dataupdateRate.get()).setRateStartdate(LocalDate.parse(dataRequest.getString("startdate")));
    		    	}
    		    	if(dataRequest.get("enddate").equals(""))
    		    	{
    		    		((MRate)dataupdateRate.get()).setRateEnddate(null);		
    		    	}
    		    	else
    		    	{
    		    		((MRate)dataupdateRate.get()).setRateEnddate(LocalDate.parse(dataRequest.getString("enddate")));	
    		    	}
    		    	if(dataRequest.has("status"))
    		    	{
    		    		((MRate)dataupdateRate.get()).setRateStatus(dataRequest.getBoolean("status"));	
    		    	}
    		    datasRates.add(dataupdateRate.get());
    	    	mRateService.saveallRate(datasRates);
    	    	response.put("responseCode", "00");	
    		    response.put("responseDesc", "Update Rate success");
    			}
    			else if (rates.get(0)[0].equals(1))
    			{
        	    	response.put("responseCode", "01");
        		    response.put("responseDesc", "the data already exists");
    			}
	    	}
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Update Rate failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateRatepricestatus")
	public String updateRatepricestatus(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	ArrayList <MRate> datasRates = new ArrayList <MRate>();
	    	Optional <MRate> dataupdateRate = mRateService.getdetail(dataRequest.getInt("rateId1"), dataRequest.getInt("rateId2"), dataRequest.getString("rateCompanyId"));
	    	if(dataupdateRate.isPresent())
	    	{
		    	if(dataRequest.get("price").equals(""))
		    	{
		    		((MRate)dataupdateRate.get()).setRatePrice(null);	
		    	}
		    	else
		    	{
		    		((MRate)dataupdateRate.get()).setRatePrice(Float.parseFloat(dataRequest.getString("price")));
		    	}
		    	if(dataRequest.has("status"))
		    	{
		    		((MRate)dataupdateRate.get()).setRateStatus(dataRequest.getBoolean("status"));	
		    	}
    		    datasRates.add(dataupdateRate.get());
    	    	mRateService.saveallRate(datasRates);
    	    	response.put("responseCode", "00");	
    		    response.put("responseDesc", "Update Rate success");
	    	}
	    	response.put("responseCode", "00");	
		    response.put("responseDesc", "Update Rate success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Update Rate failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/deleteRateperrow")
	public String deleteRateperrow(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			mRateService.deleteRate(dataRequest.getInt("rateId1"), dataRequest.getInt("rateId2"), dataRequest.getString("rateCompanyId"));
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Delete Rate success");
		}
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "delete Rate failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/addRate")
	public String addRate(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	ArrayList <MRate> datasRates = new ArrayList <MRate>();
	    	List<Object[]> cekid = mRateService.getRatelastindex("a", dataRequest.getInt("rateId1"), dataRequest.getString("rateCompanyId"));
	    	int id2 = Integer.parseInt(cekid.get(0)[1].toString());
			JSONArray rateArray = dataRequest.getJSONArray("rateArray");
	    	JSONArray serviceArray = dataRequest.getJSONArray("serviceArray");
	    	if(serviceArray.size() > 0)
	    	{
	    		for(int i = 0; i < serviceArray.size(); i++)
	    		{
	    	    	    	    	if(serviceArray.getJSONObject(i).get("darat").equals(true))
		    	    		    	{
	    		    	    			if(rateArray.size() > 0)
	    		    	    			{
	    		    	    	    		for(int k = 0; k < rateArray.size(); k++)
	    		    	    	    		{
	    		    	    	    			List<Object[]> rates = mRateService.checkinsert(rateArray.getJSONObject(k).getInt("kotaFrom"),
	    		    	    	    					rateArray.getJSONObject(k).getInt("kotaTo"), 
	    		    	    	    					serviceArray.getJSONObject(i).getInt("service_id"), 
	    		    	    	    					"darat", 
	    		    	    	    					serviceArray.getJSONObject(i).getString("start_darat").equals("") ? "" :serviceArray.getJSONObject(i).getString("start_darat"), 
	    	    		    	    	    			serviceArray.getJSONObject(i).getString("end_darat").equals("") ? "" :serviceArray.getJSONObject(i).getString("end_darat"), 
	    		    	    	    					dataRequest.getString("rateCompanyId"));
	    		    	    	    			response.put("responsetest", rates.get(0)[0]);
	    		    	    	    			if(rates.get(0)[0].equals(0))
	    		    	    	    			{
	    		    	    	    				id2++;
		    		    	    	    	    	MRate data = new MRate();
		    		    	    	    	    	data.setRateId1(dataRequest.getInt("rateId1"));
		    		    	    	    	    	data.setRateId2(id2);
		    		    	    	    	    	data.setmKotaFromId(rateArray.getJSONObject(k).getInt("kotaFrom"));
		    		    	    	    	    	data.setmProvinceFromId(rateArray.getJSONObject(k).getInt("provinceFrom"));
		    		    	    	    	    	data.setmKotaToId(rateArray.getJSONObject(k).getInt("kotaTo"));
		    		    	    	    	    	data.setmProvinceToId(rateArray.getJSONObject(k).getInt("provinceTo"));
		    		    	    	    	    	data.setmServiceId(serviceArray.getJSONObject(i).getInt("service_id"));
		    		    	    		    		data.setRateVehicle("darat");
		    		    	    		    		data.setRateStartdate(serviceArray.getJSONObject(i).get("start_darat").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("start_darat")));
		    		    	    		    		data.setRateEnddate(serviceArray.getJSONObject(i).get("end_darat").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("end_darat")));
		    		    	    		    		data.setRateDraftstatus("draft");
		    		    	    	    	    	data.setRateStatus(dataRequest.getBoolean("rateStatus"));
		    		    	    	    	    	data.setRateCompanyId(dataRequest.getString("rateCompanyId"));
		    		    	    	    	    	datasRates.add(data);	
	    		    	    	    			}
	    		    	    	    		}
	    		    	    				
		    	    		    	}
	    	    	    	    	if(serviceArray.getJSONObject(i).get("laut").equals(true))
		    	    		    	{
	    		    	    			if(rateArray.size() > 0)
	    		    	    			{
	    		    	    	    		for(int k = 0; k < rateArray.size(); k++)
	    		    	    	    		{
	    		    	    	    			List<Object[]> rates = mRateService.checkinsert(rateArray.getJSONObject(k).getInt("kotaFrom"),
	    		    	    	    					rateArray.getJSONObject(k).getInt("kotaTo"), 
	    		    	    	    					serviceArray.getJSONObject(i).getInt("service_id"), 
	    		    	    	    					"laut", 
	    		    	    	    					serviceArray.getJSONObject(i).getString("start_laut").equals("") ? "" :serviceArray.getJSONObject(i).getString("start_laut"), 
	    	    		    	    	    			serviceArray.getJSONObject(i).getString("end_laut").equals("") ? "" :serviceArray.getJSONObject(i).getString("end_laut"), 
	    		    	    	    					dataRequest.getString("rateCompanyId"));
	    		    	    	    			response.put("responsetest", rates.get(0)[0]);
	    		    	    	    			if(rates.get(0)[0].equals(0))
	    		    	    	    			{
	    		    	    	    				id2++;
		    		    	    	    	    	MRate data = new MRate();
		    		    	    	    	    	data.setRateId1(dataRequest.getInt("rateId1"));
		    		    	    	    	    	data.setRateId2(id2);
		    		    	    	    	    	data.setmKotaFromId(rateArray.getJSONObject(k).getInt("kotaFrom"));
		    		    	    	    	    	data.setmProvinceFromId(rateArray.getJSONObject(k).getInt("provinceFrom"));
		    		    	    	    	    	data.setmKotaToId(rateArray.getJSONObject(k).getInt("kotaTo"));
		    		    	    	    	    	data.setmProvinceToId(rateArray.getJSONObject(k).getInt("provinceTo"));
		    		    	    	    	    	data.setmServiceId(serviceArray.getJSONObject(i).getInt("service_id"));
		    		    	    		    		data.setRateVehicle("laut");
		    		    	    		    		data.setRateStartdate(serviceArray.getJSONObject(i).get("start_laut").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("start_laut")));
		    		    	    		    		data.setRateEnddate(serviceArray.getJSONObject(i).get("end_laut").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("end_laut")));
		    		    	    		    		data.setRateDraftstatus("draft");
		    		    	    	    	    	data.setRateStatus(dataRequest.getBoolean("rateStatus"));
		    		    	    	    	    	data.setRateCompanyId(dataRequest.getString("rateCompanyId"));
		    		    	    	    	    	datasRates.add(data);	
	    		    	    	    			}
	    		    	    	    		}
	    		    	    			}
		    	    		    	}
	    	    	    	    	if(serviceArray.getJSONObject(i).get("udara").equals(true))
		    	    		    	{
	    		    	    			if(rateArray.size() > 0)
	    		    	    			{
	    		    	    	    		for(int k = 0; k < rateArray.size(); k++)
	    		    	    	    		{
	    		    	    	    			List<Object[]> rates = mRateService.checkinsert(rateArray.getJSONObject(k).getInt("kotaFrom"),
	    		    	    	    					rateArray.getJSONObject(k).getInt("kotaTo"), 
	    		    	    	    					serviceArray.getJSONObject(i).getInt("service_id"), 
	    		    	    	    					"udara", 
	    		    	    	    					serviceArray.getJSONObject(i).getString("start_udara").equals("") ? "" :serviceArray.getJSONObject(i).getString("start_udara"), 
	    	    		    	    	    					serviceArray.getJSONObject(i).getString("end_udara").equals("") ? "" :serviceArray.getJSONObject(i).getString("end_udara"), 
	    		    	    	    					dataRequest.getString("rateCompanyId"));
	    		    	    	    			response.put("responsetest", rates.get(0)[0]);
	    		    	    	    			if(rates.get(0)[0].equals(0))
	    		    	    	    			{
	    		    	    	    				id2++;
		    		    	    	    	    	MRate data = new MRate();
		    		    	    	    	    	data.setRateId1(dataRequest.getInt("rateId1"));
		    		    	    	    	    	data.setRateId2(id2);
		    		    	    	    	    	data.setmKotaFromId(rateArray.getJSONObject(k).getInt("kotaFrom"));
		    		    	    	    	    	data.setmProvinceFromId(rateArray.getJSONObject(k).getInt("provinceFrom"));
		    		    	    	    	    	data.setmKotaToId(rateArray.getJSONObject(k).getInt("kotaTo"));
		    		    	    	    	    	data.setmProvinceToId(rateArray.getJSONObject(k).getInt("provinceTo"));
		    		    	    	    	    	data.setmServiceId(serviceArray.getJSONObject(i).getInt("service_id"));
		    		    	    		    		data.setRateVehicle("udara");
		    		    	    		    		data.setRateStartdate(serviceArray.getJSONObject(i).get("start_udara").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("start_udara")));
		    		    	    		    		data.setRateEnddate(serviceArray.getJSONObject(i).get("end_udara").equals("") ? null : LocalDate.parse(serviceArray.getJSONObject(i).getString("end_udara")));
		    		    	    		    		data.setRateDraftstatus("draft");
		    		    	    	    	    	data.setRateStatus(dataRequest.getBoolean("rateStatus"));
		    		    	    	    	    	data.setRateCompanyId(dataRequest.getString("rateCompanyId"));
		    		    	    	    	    	datasRates.add(data);	
	    		    	    	    			}
	    		    	    	    		}
	    		    	    			}
		    	    		    	}
		    	    		    }
	    					}
	    	}
	    	mRateService.saveallRate(datasRates);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Rate success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Rate failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
