package com.proacc.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.entity.MPacking;
import com.proacc.helper.Helper;
import com.proacc.service.MPackingService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MPackingController {
	private static final Logger logger = LoggerFactory.getLogger(MPackingController.class);
	
	@Autowired
	MPackingService mPackingService;
	@PostMapping("/getAllPacking/{paramAktif}")
	public String getAllPacking(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> packings = mPackingService.getallpacking(paramAktif.toString().toLowerCase(), dataRequest.getString("packingCompanyId"));
			packings.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				data.put("packingId", column[0]);
				data.put("packingName", column[1] == null ? "" : column[1].toString());
				data.put("packingDescription", column[2] == null ? "" : column[2].toString());
				data.put("packingCreatedBy", column[3].toString());
				data.put("packingCreatedAt", column[4].toString());
				data.put("packingUpdatedBy", column[5] == null ? "" : column[5].toString());
				data.put("packingUpdatedAt", column[6] == null ? "" : column[6].toString());
				data.put("packingStatus", column[7].toString());
				data.put("packingCompanyId", column[8]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data packing berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data packing gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getPackingDetail/{paramAktif}")
	public String getPackingDetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> packings = mPackingService.getpacking(paramAktif.toString().toLowerCase(), dataRequest.getInt("packingId"), dataRequest.getString("packingCompanyId"));
			packings.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				data.put("packingId", column[0]);
				data.put("packingName", column[1] == null ? "" : column[1].toString());
				data.put("packingDescription", column[2] == null ? "" : column[2].toString());
				data.put("packingCreatedBy", column[3].toString());
				data.put("packingCreatedAt", column[4].toString());
				data.put("packingUpdatedBy", column[5] == null ? "" : column[5].toString());
				data.put("packingUpdatedAt", column[6] == null ? "" : column[6].toString());
				data.put("packingStatus", column[7].toString());
				data.put("packingCompanyId", column[8]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data packing berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data packing gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/savePacking")
	public String savePacking(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	int id = 0;
	    	List<Object[]> dataNext = mPackingService.nextvalMPacking();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	MPacking data = new MPacking();
	    	data.setPackingId(id);
	    	data.setPackingName(dataRequest.getString("packingName"));
	    	if(dataRequest.has("packingDescription"))
	    	{
	    		data.setPackingDescription(dataRequest.getString("packingDescription"));	
	    	}
	    	data.setPackingCreatedBy(user_uuid);
	    	data.setPackingCreatedAt(localDateTime2);
	    	data.setPackingStatus(dataRequest.getBoolean("packingStatus"));
	    	data.setPackingCompanyId(dataRequest.getString("packingCompanyId"));
	    	mPackingService.savepacking(data);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save packing success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save packing failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updatePacking")
	public String updatePacking(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	Optional<MPacking> dataupdate = mPackingService.getdetail(dataRequest.getInt("packingId"), dataRequest.getString("packingCompanyId"));
	    	if(dataupdate.isPresent())
	    	{
	    		if(dataRequest.containsValue(dataRequest.getString("packingName")))
	    		{
	    			((MPacking)dataupdate.get()).setPackingName(dataRequest.getString("packingName"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getString("packingDescription")))
	    		{
	    			((MPacking)dataupdate.get()).setPackingDescription(dataRequest.getString("packingDescription"));
	    		}
	    		((MPacking)dataupdate.get()).setPackingUpdatedBy(user_uuid);
	    		((MPacking)dataupdate.get()).setPackingUpdatedAt(localDateTime2);
	    		((MPacking)dataupdate.get()).setPackingStatus(dataRequest.getBoolean("packingStatus"));
	    		mPackingService.savepacking(dataupdate.get());
	    	}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Service success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Update Position failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
