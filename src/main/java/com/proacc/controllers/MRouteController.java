package com.proacc.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MAgen;
import com.proacc.entity.MAgendetail;
import com.proacc.entity.MRate;
import com.proacc.entity.MRateCustomer;
import com.proacc.entity.MRateHarga;
import com.proacc.entity.MRoute;
import com.proacc.entity.MRouteJalur;
import com.proacc.entity.MRouteJalurDetail;
import com.proacc.entity.MagenCoverArea;
import com.proacc.helper.Helper;
import com.proacc.service.AllRateCustomerService;
import com.proacc.service.AllRouteService;
import com.proacc.service.MRateCustomerService;
import com.proacc.service.MRateHargaService;
import com.proacc.service.MRouteJalurDetailService;
import com.proacc.service.MRouteJalurService;
import com.proacc.service.MRouteService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MRouteController {

	private static final Logger logger = LoggerFactory.getLogger(MRouteController.class);
	
	@Autowired
	MRouteService MRouteService;
	
	@Autowired
	AllRouteService AllRouteService;
	
	@Autowired
	MRouteJalurService MRouteJalurService;
	
	@Autowired
	MRouteJalurDetailService MRouteJalurDetailService;
	


	@PostMapping("/listRoute/{paramAktif}")
	public String listRoute(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
//		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
//		RestTemplate restTemplate = new RestTemplate();
//	    HttpHeaders headers = new HttpHeaders();
//	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//	    headers.set("x-access-code", header);
//	    HttpEntity<String> entity = new HttpEntity<String>(headers);
//	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
//	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT #### " + dataRequest.toString());
		logger.info("API #### listRoute ()");
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> listRoute = MRouteService.listRouteByStatusAndCreatedBy(paramAktif.toLowerCase(), user_uuid.toString(), dataRequest.getString("companyId"));
			List<Object[]> listRouteJalur = MRouteJalurService.listRouteJalur(paramAktif.toLowerCase(), dataRequest.getString("companyId"));
			listRoute.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("routeId", col[0]);
				data.put("routeName", col[1]);
				data.put("routeFromDate", col[2] == null ? "" : col[2].toString());
				data.put("routeToDate", col[3] == null ? "" : col[3].toString());
				data.put("routeStatus", col[4]);
				data.put("routeCompanyId", col[5]);
				List<JSONObject> dataJalur = new ArrayList<>();
				listRouteJalur.stream().forEach(col2->{
					JSONObject data2 = new JSONObject();
					data2.put("routeId", col2[0]);
					data2.put("routeCompanyId", col2[1]);
					data2.put("cabangId", col2[2] == null ? "" : col2[2]);
					data2.put("cabangName", col2[3] == null ? "" : col2[3]);
					data2.put("cabangProvinsiId", col2[4] == null ? "" : col2[4]);
					data2.put("cabangKotaId", col2[5] == null ? "" : col2[5]);
					data2.put("agenId", col2[6] == null ? "" : col2[6]);
					data2.put("agenDetailId", col2[7] == null ? "" : col2[7]);
					data2.put("agenProvinsiId", col2[8] == null ? "" : col2[8]);
					data2.put("agenKotaId", col2[9] == null ? "" : col2[9]);
					
					if(col2[0] == col[0])
					{
						dataJalur.add(data2);
					}
				});
				data.put("detail", dataJalur);
				datas.add(data);
			});
			
			response.put("responseCode", "00");
		    response.put("responseDesc", "List Route Success");
		    response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "List Route Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/detailRoute/{paramAktif}")
	public String detailRoute(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
//		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
//		RestTemplate restTemplate = new RestTemplate();
//	    HttpHeaders headers = new HttpHeaders();
//	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//	    headers.set("x-access-code", header);
//	    HttpEntity<String> entity = new HttpEntity<String>(headers);
//	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
//	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT #### " + dataRequest.toString());
		logger.info("API #### detailRoute ()");
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			Optional<MRoute> detailRoute = MRouteService.detailRoute(dataRequest.getInt("routeId"), dataRequest.getString("companyId"));
			JSONObject dataHeader = new JSONObject();
			dataHeader.put("routeId", detailRoute.get().getRouteId());
			dataHeader.put("routeName", detailRoute.get().getRouteName());
			dataHeader.put("routeStartDate", detailRoute.get().getRouteFromDate().toString());
			dataHeader.put("routeEndDate", detailRoute.get().getRouteToDate().toString());
			dataHeader.put("routeStatus", detailRoute.get().getRouteStatus());
			
			
			
			List<JSONObject> dataJalur = new ArrayList<>();
			List<Object[]> listObject = MRouteService.detailJalur(dataRequest.getInt("routeId"), dataRequest.getString("companyId"), paramAktif.toLowerCase());
			listObject.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("routeId", col[0]);
				data.put("routeJalurId", col[1]);
				data.put("name", col[2] == null ? "" : col[2]);
				data.put("mKotaId", col[3]);
				data.put("routeJalurStatus", col[4]);
				data.put("routeJalurCompanyId", col[5]);
				data.put("mCabangId", col[6] == null ? "" : col[6]);
				data.put("mAgenId", col[7] == null ? "" : col[7]);
				data.put("mAgenDetailId", col[8] == null ? "" : col[8]);
				
				List<JSONObject> dataJalurDetail = new ArrayList<>();
				List<Object[]> listJalurDetail = MRouteService.detailJalurDetail(dataRequest.getInt("routeId"), dataRequest.getString("companyId"), paramAktif.toLowerCase(), Integer.parseInt(col[1].toString()));
				listJalurDetail.stream().forEach(col2->{
					JSONObject data2 = new JSONObject();
					data2.put("routeId", col2[0]);
					data2.put("routeJalurId", col2[1]);
					data2.put("routeJalurDetailId", col2[2]);
					data2.put("name", col2[3] == null ? "" : col2[3]);
					data2.put("mKotaId", col2[4]);
					data2.put("routeJalurDetailStatus", col2[5]);
					data2.put("routeJalurCompanyId", col2[6]);
					data2.put("mCabangId", col2[7] == null ? "" : col2[7]);
					data2.put("mCabangDetailId", col2[8] == null ? "" : col2[8]);
					data2.put("mAgenId", col2[9] == null ? "" : col2[9]);
					data2.put("mAgenDetailId", col2[10] == null ? "" : col2[10]);
					dataJalurDetail.add(data2);
				});
				data.put("detailJalur", dataJalurDetail);
				dataJalur.add(data);
			});
			
//			for (JSONObject objects : dataJalur) {
//				List<JSONObject> dataJalurJalur = new ArrayList<>();
//				for (JSONObject objectsDetail : dataJalurDetail) {
//					if(objects.getInt("routeId") == objectsDetail.getInt("routeId") && objects.getInt("routeJalurId") == objectsDetail.getInt("routeJalurId") )
//					{
//						dataJalurJalur.add(objectsDetail);
//						
//					}
//				}
//				objects.put("detailJalur", dataJalurJalur);
//			}
			
			dataHeader.put("jalur", dataJalur);
			datas.add(dataHeader);
			
			
			
			
			response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Route Success");
		    response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Route Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/saveRoute")
	public String saveRoute(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT #### " + dataRequest.toString());
		logger.info("API #### saveRoute()");
		try
		{
			ArrayList<MRouteJalur> dataRoute = new ArrayList<>();
	    	ArrayList<MRouteJalurDetail> dataRouteDetail = new ArrayList<>();
	    	
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	
	    	int id = 0;
	    	List<Object[]> dataNext = MRouteService.nextvalRoute();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	
	    	MRoute data = new MRoute();
	    	data.setRouteId(id);
	    	data.setRouteName(dataRequest.getString("routeName"));
	    	data.setRouteFromDate(LocalDate.parse(dataRequest.getString("routeFrom")));
	    	data.setRouteToDate(LocalDate.parse(dataRequest.getString("routeTo")));
	    	data.setRouteStatus(dataRequest.getBoolean("routeStatus"));
	    	data.setRouteCompanyId(dataRequest.getString("routeCompanyId"));
	    	data.setRouteCreatedBy(user_uuid);
	    	data.setRouteCreatedAt(localDateTime2);
	    	JSONArray routeJalur = dataRequest.getJSONArray("jalur");
	    	if(routeJalur.size() > 0)
	    	{
	    		for (int i = 0; i < routeJalur.size(); i++) {
					MRouteJalur dataJalur = new MRouteJalur();
					dataJalur.setMRouteId(id);
					dataJalur.setRouteJalurId(routeJalur.getJSONObject(i).getInt("routeJalurId"));
					dataJalur.setmCabangId(routeJalur.getJSONObject(i).has("mCabangId") == true ? routeJalur.getJSONObject(i).getInt("mCabangId") : null);
					dataJalur.setmAgenId(routeJalur.getJSONObject(i).has("mAgenId") == true ? routeJalur.getJSONObject(i).getInt("mAgenId") : null);
					dataJalur.setmAgendetailId(routeJalur.getJSONObject(i).has("mAgenDetailId") == true ? routeJalur.getJSONObject(i).getInt("mAgenDetailId") : null);
					dataJalur.setRouteJalurStatus(routeJalur.getJSONObject(i).getBoolean("routeJalurStatus"));
					dataJalur.setRouteJalurCompanyId(dataRequest.getString("routeCompanyId"));
					JSONArray routeJalurDetail = routeJalur.getJSONObject(i).getJSONArray("detailJalur");
			    	if(routeJalurDetail.size() > 0)
			    	{
			    		for (int j = 0; j < routeJalurDetail.size(); j++) {
							MRouteJalurDetail dataDetail = new MRouteJalurDetail();
							dataDetail.setMRouteId(id);
							dataDetail.setMRouteJalurId(routeJalur.getJSONObject(i).getInt("routeJalurId"));
							dataDetail.setRouteJalurDetailId(routeJalurDetail.getJSONObject(j).getInt("routeJalurDetailId"));
							dataDetail.setmCabangId(routeJalurDetail.getJSONObject(j).has("mCabangId") == true ? routeJalurDetail.getJSONObject(j).getInt("mCabangId") : null);
							dataDetail.setmAgenId(routeJalurDetail.getJSONObject(j).has("mAgenId") == true ?routeJalurDetail.getJSONObject(j).getInt("mAgenId") : null );
							dataDetail.setmAgendetailId(routeJalurDetail.getJSONObject(j).has("mAgenDetailId") == true ? routeJalurDetail.getJSONObject(j).getInt("mAgenDetailId") : null);
							dataDetail.setRouteJalurStatus(routeJalurDetail.getJSONObject(j).getBoolean("routeJalurDetailStatus"));
							dataDetail.setmCabangDetaild(routeJalurDetail.getJSONObject(j).has("mCabangDetailId") == true ? routeJalurDetail.getJSONObject(j).getInt("mCabangDetailId") : null);
							dataDetail.setRouteJalurDetailCompanyId(dataRequest.getString("routeCompanyId"));
							dataRouteDetail.add(dataDetail);
						}
			    	}
			    	dataRoute.add(dataJalur);
				}
	    	}
	    	
	    	AllRouteService.saveallRoute(data, dataRoute, dataRouteDetail);
			response.put("responseCode", "00");
		    response.put("responseDesc", "Save Route Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Save Route Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/updateRoute")
	public String updateRoute(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	logger.info("INPUT #### " + dataRequest.toString());
			logger.info("API #### updateRoute()");
	    	ArrayList<MRouteJalur> dataRouteJalur = new ArrayList<>();
	    	ArrayList<MRouteJalurDetail> dataRouteJalurDetail = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	Optional <MRoute> dataRouteHeader = MRouteService.getDetail(dataRequest.getInt("routeId"), dataRequest.getString("routeCompanyId"));
	    	if(dataRouteHeader.isPresent())
	    	{
	    		((MRoute)dataRouteHeader.get()).setRouteName(dataRequest.getString("routeName"));
	    		((MRoute)dataRouteHeader.get()).setRouteFromDate(LocalDate.parse(dataRequest.getString("routeFrom")));
	    		((MRoute)dataRouteHeader.get()).setRouteToDate(LocalDate.parse(dataRequest.getString("routeTo")));
	    		((MRoute)dataRouteHeader.get()).setRouteUpdatedBy(user_uuid);
	    		((MRoute)dataRouteHeader.get()).setRouteUpdatedAt(localDateTime2);
	    		((MRoute)dataRouteHeader.get()).setRouteStatus(dataRequest.getBoolean("routeStatus"));
	    		((MRoute)dataRouteHeader.get()).setRouteCompanyId(dataRequest.getString("routeCompanyId"));
	    		JSONArray routeJalur = dataRequest.getJSONArray("jalur");
		    	if(routeJalur.size() > 0)
		    	{
	    			List<Object[]> detail = MRouteJalurService.getdetailall(dataRequest.getInt("routeId"), dataRequest.getString("routeCompanyId"));
	    			detail.stream().forEach(column -> {
		    			Optional<MRouteJalur> dataJalurUpdate = MRouteJalurService.getdetailById(Integer.parseInt(column[0].toString()), Integer.parseInt(column[1].toString()), dataRequest.getString("routeCompanyId"));
		    			if(dataJalurUpdate.isPresent())
		    			{
		    				((MRouteJalur)dataJalurUpdate.get()).setRouteJalurStatus(false);
		    			}
		    			
	    			});
	    			
	    			
	    			List<Object[]> detailCover = MRouteJalurDetailService.getAll(dataRequest.getInt("routeId"), dataRequest.getString("routeCompanyId"));
	    			
	    			detailCover.stream().forEach(col->{
	    				Optional<MRouteJalurDetail> dataAgen = MRouteJalurDetailService.getDetail(Integer.parseInt(col[0].toString()), Integer.parseInt(col[1].toString()), Integer.parseInt(col[2].toString()), dataRequest.getString("routeCompanyId"));
	    				if(dataAgen.isPresent())
	    				{
	    					((MRouteJalurDetail)dataAgen.get()).setRouteJalurStatus(false);
	    				}
	    			});
	    			for (int i = 0; i < routeJalur.size(); i++) {
						MRouteJalur dataJalur = new MRouteJalur();
						dataJalur.setMRouteId(dataRequest.getInt("routeId"));
						dataJalur.setRouteJalurId(routeJalur.getJSONObject(i).getInt("routeJalurId"));
						dataJalur.setmCabangId(routeJalur.getJSONObject(i).has("mCabangId") == true ? routeJalur.getJSONObject(i).getInt("mCabangId") : null);
						dataJalur.setmAgenId(routeJalur.getJSONObject(i).has("mAgenId") == true ? routeJalur.getJSONObject(i).getInt("mAgenId") : null);
						dataJalur.setmAgendetailId(routeJalur.getJSONObject(i).has("mAgenDetailId") == true ? routeJalur.getJSONObject(i).getInt("mAgenDetailId") : null);
						dataJalur.setRouteJalurStatus(routeJalur.getJSONObject(i).getBoolean("routeJalurStatus"));
						dataJalur.setRouteJalurCompanyId(dataRequest.getString("routeCompanyId"));
						JSONArray routeJalurDetail = routeJalur.getJSONObject(i).getJSONArray("detailJalur");
				    	if(routeJalurDetail.size() > 0)
				    	{
				    		for (int j = 0; j < routeJalurDetail.size(); j++) {
								MRouteJalurDetail dataDetail = new MRouteJalurDetail();
								dataDetail.setMRouteId(dataRequest.getInt("routeId"));
								dataDetail.setMRouteJalurId(routeJalur.getJSONObject(i).getInt("routeJalurId"));
								dataDetail.setRouteJalurDetailId(routeJalurDetail.getJSONObject(j).getInt("routeJalurDetailId"));
								dataDetail.setmCabangId(routeJalurDetail.getJSONObject(j).has("mCabangId") == true ? routeJalurDetail.getJSONObject(j).getInt("mCabangId") : null);
								dataDetail.setmAgenId(routeJalurDetail.getJSONObject(j).has("mAgenId") == true ?routeJalurDetail.getJSONObject(j).getInt("mAgenId") : null );
								dataDetail.setmAgendetailId(routeJalurDetail.getJSONObject(j).has("mAgenDetailId") == true ? routeJalurDetail.getJSONObject(j).getInt("mAgenDetailId") : null);
								dataDetail.setRouteJalurStatus(routeJalurDetail.getJSONObject(j).getBoolean("routeJalurDetailStatus"));
								dataDetail.setmCabangDetaild(routeJalurDetail.getJSONObject(j).has("mCabangDetailId") == true ? routeJalurDetail.getJSONObject(j).getInt("mCabangDetailId") : null);
								dataDetail.setRouteJalurDetailCompanyId(dataRequest.getString("routeCompanyId"));
								dataRouteJalurDetail.add(dataDetail);
							}
				    	}
				    	dataRouteJalur.add(dataJalur);
					}
	    			
		    	}
	    	}
	    	AllRouteService.saveallRoute(dataRouteHeader.get(), dataRouteJalur, dataRouteJalurDetail);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Route Success");
	    }
	    catch(Exception e) {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "Update Route Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}

}
