package com.proacc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.service.MVolumeService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MVolumeController {
	private static final Logger logger = LoggerFactory.getLogger(MVolumeController.class);
	
	@Autowired
	MVolumeService mVolumeService;
	
	@PostMapping("/getAllVolume/{paramAktif}")
	public String getAllVolume(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> volumes = mVolumeService.getallVolume(paramAktif.toString().toLowerCase(), dataRequest.getString("volumeCompanyId"));
			volumes.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("volumeId", column[0]);
				data.put("volumeName", column[1] == null ? "" : column[1].toString());
				data.put("volumeTom3", column[2] == null ? "" : column[2].toString());
				data.put("volumeDescription", column[3] == null ? "" : column[3].toString());
				data.put("volumeCreatedBy", column[4].toString());
				data.put("volumeCreatedAt", column[5].toString());
				data.put("volumeUpdatedBy", column[6] == null ? "" : column[6].toString());
				data.put("volumeUpdatedAt", column[7] == null ? "" : column[7].toString());
				data.put("volumeStatus", column[8].toString());
				data.put("volumeCompanyId", column[9]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data volume berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data volume gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getVolume/{paramAktif}")
	public String getVolume(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> volumes = mVolumeService.getVolume(paramAktif.toString().toLowerCase(), dataRequest.getInt("volumeId"), dataRequest.getString("volumeCompanyId"));
			volumes.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("volumeId", column[0]);
				data.put("volumeName", column[1] == null ? "" : column[1].toString());
				data.put("volumeTom3", column[2] == null ? "" : column[2].toString());
				data.put("volumeDescription", column[3] == null ? "" : column[3].toString());
				data.put("volumeCreatedBy", column[4].toString());
				data.put("volumeCreatedAt", column[5].toString());
				data.put("volumeUpdatedBy", column[6] == null ? "" : column[6].toString());
				data.put("volumeUpdatedAt", column[7] == null ? "" : column[7].toString());
				data.put("volumeStatus", column[8].toString());
				data.put("volumeCompanyId", column[9]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data volume berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data volume gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
