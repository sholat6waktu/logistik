package com.proacc.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MCabang;
import com.proacc.entity.MCabangdetail;
import com.proacc.helper.Helper;
import com.proacc.service.AllCabangService;
import com.proacc.service.MCabangService;
import com.proacc.service.MCabangdetailService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MCabangController {

	private static final Logger logger = LoggerFactory.getLogger(MCabangController.class);
	
	@Autowired
	MCabangService mCabangService;
	
	@Autowired
	MCabangdetailService mCabangdetailService;
	
	@Autowired
	AllCabangService allCabangService;
	@PostMapping("/getAllCabang/{paramAktif}")
	public String getAllCabang(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> cabangs = mCabangService.getallCabang(paramAktif.toString().toLowerCase(), dataRequest.getString("cabangCompanyId"));
			cabangs.stream().forEach(column -> {
				List<JSONObject> datascabangdetails = new ArrayList<>();
				JSONObject data = new JSONObject();
				data.put("cabangId", column[0]);
				data.put("cabang", column[8].toString());
				if(column[1] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[1].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaName", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaId", column[1]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaName", "");
							data.put("mKotaId", "");
						}
					}
				}
				else
				{
					data.put("kotaName", "");
					data.put("mKotaId", "");
				}
				data.put("cabangCreatedBy", column[2].toString());
				data.put("cabangCreatedAt", column[3].toString());
				data.put("cabangUpdatedBy", column[4] == null ? "" : column[4].toString());
				data.put("cabangUpdatedAt", column[5] == null ? "" : column[5].toString());
				data.put("cabangStatus", column[6].toString());
				data.put("cabangCompanyId", column[7]);
				List<Object[]> cabangDetail = mCabangdetailService.getallCabangdetail(paramAktif.toString().toLowerCase(), Integer.parseInt(column[0].toString()), dataRequest.getString("cabangCompanyId"));
				cabangDetail.stream().forEach(columndetail->{
					JSONObject datadetail = new JSONObject();
					datadetail.put("mCabangId", columndetail[0]);
					datadetail.put("mCabangDetailId", columndetail[1]);
					if(columndetail[2] != null)
					{
						for(int i = 0; i < dataprovinces.size(); i++)
						{
							if(columndetail[2].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
							{
								datadetail.put("provinceName", dataprovinces.getJSONObject(i).get("prov_name"));
								datadetail.put("mProvinceId", columndetail[2]);
								break;
							}
							else if(dataprovinces.size() == i + 1)
							{
								datadetail.put("provinceName", "");
								datadetail.put("mProvinceId", "");
							}
						}
					}
					else
					{
						datadetail.put("provinceName", "");
						datadetail.put("mProvinceId", "");
					}
					if(columndetail[3] != null)
					{
						for(int i = 0; i < datacities.size(); i++)
						{
							if(columndetail[3].equals(datacities.getJSONObject(i).getInt("city_id")))
							{
								datadetail.put("kotaName", datacities.getJSONObject(i).get("city_name"));
								datadetail.put("mKotaId", columndetail[3]);
								break;
							}
							else if(datacities.size() == i + 1)
							{
								datadetail.put("kotaName", "");
								datadetail.put("mKotaId", "");
							}
						}
					}
					else
					{
						datadetail.put("kotaName", "");
						datadetail.put("mKotaId", "");
					}
					datadetail.put("agenId", columndetail[4] == null ? "" : columndetail[4]);
					datadetail.put("agenName", columndetail[5] == null ? "" : columndetail[5].toString());
					datadetail.put("mParent", columndetail[6] == null ? "" : columndetail[6]);
					datadetail.put("agentype", columndetail[7].toString());
					datadetail.put("cabangdetailCreatedBy", columndetail[8].toString());
					datadetail.put("cabangdetailCreatedAt", columndetail[9].toString());
					datadetail.put("cabangdetailUpdatedBy", columndetail[10] == null ? "" : columndetail[10].toString());
					datadetail.put("cabangdetailUpdatedAt", columndetail[11] == null ? "" : columndetail[11].toString());
					datadetail.put("cabangdetailStatus", columndetail[12].toString());
					datadetail.put("cabangdetailCompanyId", columndetail[13]);
					datascabangdetails.add(datadetail);
				});
				data.put("cabangdetailArray", datascabangdetails);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getCabangdetailByCabang/{paramAktif}")
	public String getCabangdetailByCabang(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> cabangs = mCabangService.getCabang(paramAktif.toString().toLowerCase(), dataRequest.getInt("cabangId"), dataRequest.getString("cabangCompanyId"));
			cabangs.stream().forEach(column -> {
				List<JSONObject> datascabangdetails = new ArrayList<>();
				JSONObject data = new JSONObject();
				data.put("cabangId", column[0]);
				data.put("cabang", column[8].toString());
				if(column[1] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(column[1].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							data.put("kotaName", datacities.getJSONObject(i).get("city_name"));
							data.put("mKotaId", column[1]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							data.put("kotaName", "");
							data.put("mKotaId", "");
						}
					}
				}
				else
				{
					data.put("kotaName", "");
					data.put("mKotaId", "");
				}
				data.put("cabangCreatedBy", column[2].toString());
				data.put("cabangCreatedAt", column[3].toString());
				data.put("cabangUpdatedBy", column[4] == null ? "" : column[4].toString());
				data.put("cabangUpdatedAt", column[5] == null ? "" : column[5].toString());
				data.put("cabangStatus", column[6].toString());
				data.put("cabangCompanyId", column[7]);
				List<Object[]> cabangDetail = mCabangdetailService.getallCabangdetail(paramAktif.toString().toLowerCase(), Integer.parseInt(column[0].toString()), dataRequest.getString("cabangCompanyId"));
				cabangDetail.stream().forEach(columndetail->{
					JSONObject datadetail = new JSONObject();
					datadetail.put("mCabangId", columndetail[0]);
					datadetail.put("mCabangDetailId", columndetail[1]);
					if(columndetail[2] != null)
					{
						for(int i = 0; i < dataprovinces.size(); i++)
						{
							if(columndetail[2].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
							{
								datadetail.put("provinceName", dataprovinces.getJSONObject(i).get("prov_name"));
								datadetail.put("mProvinceId", columndetail[2]);
								break;
							}
							else if(dataprovinces.size() == i + 1)
							{
								datadetail.put("provinceName", "");
								datadetail.put("mProvinceId", "");
							}
						}
					}
					else
					{
						datadetail.put("provinceName", "");
						datadetail.put("mProvinceId", "");
					}
					if(columndetail[3] != null)
					{
						for(int i = 0; i < datacities.size(); i++)
						{
							if(columndetail[3].equals(datacities.getJSONObject(i).getInt("city_id")))
							{
								datadetail.put("kotaName", datacities.getJSONObject(i).get("city_name"));
								datadetail.put("mKotaId", columndetail[3]);
								break;
							}
							else if(datacities.size() == i + 1)
							{
								datadetail.put("kotaName", "");
								datadetail.put("mKotaId", "");
							}
						}
					}
					else
					{
						datadetail.put("kotaName", "");
						datadetail.put("mKotaId", "");
					}
					datadetail.put("agenId", columndetail[4] == null ? "" : columndetail[4]);
					datadetail.put("agenName", columndetail[5] == null ? "" : columndetail[5].toString());
					datadetail.put("mParent", columndetail[6] == null ? "" : columndetail[6]);
					datadetail.put("agentype", columndetail[7].toString());
					datadetail.put("cabangdetailCreatedBy", columndetail[8].toString());
					datadetail.put("cabangdetailCreatedAt", columndetail[9].toString());
					datadetail.put("cabangdetailUpdatedBy", columndetail[10] == null ? "" : columndetail[10].toString());
					datadetail.put("cabangdetailUpdatedAt", columndetail[11] == null ? "" : columndetail[11].toString());
					datadetail.put("cabangdetailStatus", columndetail[12].toString());
					datadetail.put("cabangdetailCompanyId", columndetail[13]);
					datascabangdetails.add(datadetail);
				});
				data.put("cabangdetailArray", datascabangdetails);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getallCabangdetail/{paramAktif}")
	public String getAllCabangdetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<JSONObject> datascabangdetails = new ArrayList<>();
			List<Object[]> cabangDetails = mCabangdetailService.getalldetail(paramAktif.toString().toLowerCase(), dataRequest.getString("cabangCompanyId"));
			cabangDetails.stream().forEach(columndetail -> {
				JSONObject datadetail = new JSONObject();
				datadetail.put("cabangId", columndetail[0]);
				if(columndetail[1] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(columndetail[1].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							datadetail.put("cabang", datacities.getJSONObject(i).get("city_name"));
							break;
						}
						else if(datacities.size() == i + 1)
						{
							datadetail.put("cabang", "");
						}
					}
				}
				else
				{
					datadetail.put("cabang", "");
				}
				if(columndetail[2] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(columndetail[2].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							datadetail.put("provinsi", dataprovinces.getJSONObject(i).get("prov_name"));
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							datadetail.put("provinsi", "");
						}
					}
				}
				else
				{
					datadetail.put("provinsi", "");
				}
				String hasilyangreal = "";
				if(columndetail[3] != null)
				{
					String listkota = columndetail[3].toString();
					String[] kotatype = listkota.split(",");
					for(String hasil: kotatype)
					{
						String kota = hasil.split("-")[0];
						String type = hasil.split("-")[1];
						for(int i = 0; i < datacities.size(); i++)
						{	
							if(kota.equals(datacities.getJSONObject(i).getString("city_id")))
							{
								hasilyangreal = hasilyangreal + datacities.getJSONObject(i).get("city_name") + "(" + type + ") ";
							}	
						}
					}
				}
				datadetail.put("kota", hasilyangreal);
				datadetail.put("listkota", columndetail[3]);
				datadetail.put("status", columndetail[4]);
				datadetail.put("cabangdetailstatus", columndetail[5]);
				datadetail.put("cabangdetailCompanyId", columndetail[6]);
				datascabangdetails.add(datadetail);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datascabangdetails);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/getcabangforupdate/{paramAktif}")
	public String getcabangforupdate(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> cabangs = mCabangService.getCabang("a",dataRequest.getInt("id"), dataRequest.getString("cabangCompanyId"));
			cabangs.stream().forEach(column -> {
				List<JSONObject> datasprovinsi = new ArrayList<>();
				JSONObject data = new JSONObject();
				data.put("id", column[0]);
				data.put("mKotaId", column[1]);
				data.put("cabang", column[8].toString());
				data.put("status", column[6]);
				data.put("agenCompanyId", column[7]);
				data.put("kode", column[9] == null ? "" : column[9]);
				data.put("typeKode", column[10] == null ? "" : column[10]);
				data.put("mCabangIdKode", column[11] == null ? "" : column[11]);
				data.put("mAgenIdKode", column[12] == null ? "" : column[12]);
				data.put("agendetailIdKode", column[13] == null ? "" : column[13]);
				List<Object[]> cabangprovince = mCabangdetailService.getdetailprovinsiforupdate(paramAktif.toString().toLowerCase(), dataRequest.getInt("id"), dataRequest.getString("cabangCompanyId"));	
				cabangprovince.stream().forEach(column1 -> {
					List<JSONObject> dataskota = new ArrayList<>();
					JSONObject dataprovinsi = new JSONObject();
					if(!column1[0].equals(null))
					{
						
						for(int i = 0; i < dataprovinces.size(); i++)
						{
							if(column1[0].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
							{
								dataprovinsi.put("provinsi_name", dataprovinces.getJSONObject(i).get("prov_name"));
								dataprovinsi.put("provinsi_name_id", column1[0]);
								break;
							}
							else if(dataprovinces.size() == i + 1)
							{
								dataprovinsi.put("provinsi_name", "");
								dataprovinsi.put("provinsi_name_id", column1[0]);
							}
						}
					}
					else
					{
						dataprovinsi.put("provinsi_name", "");
						dataprovinsi.put("provinsi_name_id", column1[0]);
					}
					logger.info("column1[0] = " + column1[0]);
					List<Object[]> cabangkota = mCabangdetailService.getdetailkotaforupdate(paramAktif.toString().toLowerCase(), dataRequest.getInt("id"), Integer.parseInt(column1[0].toString()), dataRequest.getString("cabangCompanyId"));
					cabangkota.stream().forEach(column2 -> {
//						if(!column1[0].equals(null)) {
//							if(column1[0].equals(obj))
//						}
						JSONObject datakota = new JSONObject();
						datakota.put("provinsiId", column2[0]);
						if(!column2[1].equals(null))
						{
							for(int i = 0; i < datacities.size(); i++)
							{
								if(column2[1].equals(datacities.getJSONObject(i).getInt("city_id")))
								{
									datakota.put("city", datacities.getJSONObject(i).get("city_name"));
									datakota.put("city_id", column2[1]);
									break;
								}
								else if(datacities.size() == i + 1)
								{
									datakota.put("city", "");
									datakota.put("city_id", "");
								} 
							}
						}
						else
						{
							datakota.put("city", "");
							datakota.put("city_id", "");
						}
						datakota.put("priority", column2[2] == null ? "" : column2[2].toString());
						datakota.put("cabang_detail_id", column2[4]);
						datakota.put("agent_id", column2[5] == null ? "" : column2[5].toString());
						datakota.put("agen_detail_id", column2[11] == null ? "" : column2[11].toString());
						datakota.put("parent_id", column2[7] == null ? "" : column2[7].toString());
						datakota.put("type", column2[3]);
						datakota.put("agent", column2[6]  == null ? "internal" : column2[6].toString());
						datakota.put("status_detail", column2[9]);
						datakota.put("cabangdetailCompanyId", column2[10]);
						
//						datakota.put("kode", column2[12] == null ? "" : column2[12]);
//						datakota.put("typeKode", column2[13] == null ? "" : column2[13]);
//						datakota.put("mCabangIdKode", column2[14] == null ? "" : column2[14]);
//						datakota.put("mAgenIdKode", column2[15] == null ? "" : column2[15]);
//						datakota.put("agendetailIdKode", column2[16] == null ? "" : column2[16]);
						dataskota.add(datakota);
					});
					dataprovinsi.put("transfer_point_array", dataskota);
					datasprovinsi.add(dataprovinsi);
				});
				data.put("cover_area", datasprovinsi);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Agen berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Agen gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/getallCabangbyprovince/{paramAktif}")
	public String getallCabangbyprovince(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<JSONObject> datascabangdetails = new ArrayList<>();
			List<Object[]> cabangDetails = mCabangService.getcabangbyprovince(paramAktif.toString().toLowerCase(), dataRequest.getInt("provinsiid"), dataRequest.getString("cabangCompanyId"));
			cabangDetails.stream().forEach(columndetail -> {
				JSONObject datadetail = new JSONObject();
				datadetail.put("id", columndetail[2]);
				datadetail.put("cabang", columndetail[3].toString());
				if(columndetail[1] != null)
				{
					for(int i = 0; i < datacities.size(); i++)
					{
						if(columndetail[1].equals(datacities.getJSONObject(i).getInt("city_id")))
						{
							datadetail.put("kota_name", datacities.getJSONObject(i).get("city_name"));
							datadetail.put("kota_id", columndetail[1]);
							break;
						}
						else if(datacities.size() == i + 1)
						{
							datadetail.put("kota_name", "");
							datadetail.put("kota_id", "");
						}
					}
				}
				else
				{
					datadetail.put("kota_name", "");
					datadetail.put("kota_id", "");
				}
				if(columndetail[0] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(columndetail[0].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							datadetail.put("provinsi_name", dataprovinces.getJSONObject(i).get("prov_name"));
							datadetail.put("provinsi_id", columndetail[0]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							datadetail.put("provinsi_name", "");
							datadetail.put("provinsi_id", "");
						}
					}
				}
				else
				{
					datadetail.put("provinsi_name", "");
					datadetail.put("provinsi_id", "");
				}
				datascabangdetails.add(datadetail);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Cabang berhasil didapat");
			response.put("responseData", datascabangdetails);
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Cabang gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/saveCabang")
	public String saveCabang(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	ArrayList<MCabangdetail> datascabangdetails = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	int id = 0;
	    	List<Object[]> dataNext = mCabangService.nextvalMCabang();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	MCabang data = new MCabang();
	    	data.setCabangId(id);
	    	data.setCabangName(dataRequest.getString("cabang_nama"));
	    	data.setmProvinceId(dataRequest.getInt("cabang_province"));
	    	data.setmKotaId(dataRequest.getInt("cabang_id"));
	    	data.setCabangCreatedBy(user_uuid);
	    	data.setCabangCreatedAt(localDateTime2);
	    	data.setCabangStatus(dataRequest.getBoolean("status"));
	    	data.setCabangCompanyId(dataRequest.getString("cabangCompanyId"));
	    	data.setKode(dataRequest.getString("kode"));
	    	data.setTypeKode(dataRequest.getInt("typeKode"));
			if(dataRequest.has("mCabangIdKode"))
			{
				data.setmCabangIdKode(dataRequest.getInt("mCabangIdKode"));
			}
			
			if(dataRequest.has("mAgenIdKode"))
			{
				data.setmAgenIdKode(dataRequest.getInt("mAgenIdKode"));
			}
			
			if(dataRequest.has("agendetailIdKode"))
			{
				data.setAgendetailIdKode(dataRequest.getInt("agendetailIdKode"));
			}
	    	JSONArray coverareaArray = dataRequest.getJSONArray("cover_area");
	    	if(coverareaArray.size() > 0)
	    	{
	    		for(int i = 0; i < coverareaArray.size(); i++)
	    		{
	    			JSONArray cityarray = coverareaArray.getJSONObject(i).getJSONArray("transfer_point_array");
	    	    	if(cityarray.size() > 0)
	    	    	{
	    	    		for(int j = 0; j < cityarray.size(); j++)
	    	    		{
	    	    			MCabangdetail datadetail = new MCabangdetail();
	    	    			datadetail.setmCabangId(id);
	    	    			datadetail.setCabangdetailId(cityarray.getJSONObject(j).getInt("cabang_detail_id"));
	    	    			datadetail.setmProvinceId(coverareaArray.getJSONObject(i).getInt("provinsi_name_id"));
	    	    			if(cityarray.getJSONObject(j).has("priority"))
	    	    			{
		    	    			datadetail.setCabangdetailPriority(cityarray.getJSONObject(j).get("priority")  == "" ? null : cityarray.getJSONObject(j).getInt("priority"));	
	    	    			}
	    	    			datadetail.setmKotaId(cityarray.getJSONObject(j).getInt("city_id"));
	    	    			if(cityarray.getJSONObject(j).has("agent_id"))
	    	    			{
	    	    				datadetail.setmAgenId(cityarray.getJSONObject(j).get("agent_id") == "" ? null : cityarray.getJSONObject(j).getInt("agent_id"));
	    	    				datadetail.setmAgenDetailId(cityarray.getJSONObject(j).get("agentDetailId") == "" ? null : cityarray.getJSONObject(j).getInt("agentDetailId"));
	    	    			}
	    	    			if(cityarray.getJSONObject(j).has("parent_id"))
	    	    			{
		    	    			datadetail.setmParentId(cityarray.getJSONObject(j).get("parent_id") == "" ? null : cityarray.getJSONObject(j).getInt("parent_id"));
	    	    			}
	    	    			datadetail.setCabangdetailAgenttype(cityarray.getJSONObject(j).getString("type"));
	    	    			datadetail.setCabangdetailCreatedBy(user_uuid);
	    	    			datadetail.setCabangdetailCreatedAt(localDateTime2);
	    	    			datadetail.setCabangdetailStatus(cityarray.getJSONObject(j).getBoolean("status_detail"));
	    	    			datadetail.setCabangdetailCompanyId(cityarray.getJSONObject(j).getString("cabangdetailCompanyId"));
	    	    			datascabangdetails.add(datadetail);
	    	    		}
	    	    	}
	    		}
	    	}
	    	allCabangService.saveallCabang(data, datascabangdetails);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Cabang success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Cabang failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/updateCabang")
	public String updateCabang(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	ArrayList<MCabangdetail> datascabangdetails = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	Optional <MCabang> dataUpdateCabang = mCabangService.getDetail(dataRequest.getInt("id"), dataRequest.getString("cabangCompanyId"));
	    	if(dataUpdateCabang.isPresent())
	    	{
	    		((MCabang)dataUpdateCabang.get()).setCabangName(dataRequest.getString("cabang_nama"));
	    		((MCabang)dataUpdateCabang.get()).setmProvinceId(dataRequest.getInt("cabang_province"));
	    		((MCabang)dataUpdateCabang.get()).setmKotaId(dataRequest.getInt("cabang_id"));
	    		((MCabang)dataUpdateCabang.get()).setCabangUpdatedBy(user_uuid);
	    		((MCabang)dataUpdateCabang.get()).setCabangUpdatedAt(localDateTime2);
	    		((MCabang)dataUpdateCabang.get()).setCabangStatus(dataRequest.getBoolean("status"));
	    		((MCabang)dataUpdateCabang.get()).setKode(dataRequest.getString("kode"));
	    		((MCabang)dataUpdateCabang.get()).setTypeKode(dataRequest.getInt("typeKode"));
				if(dataRequest.has("mCabangIdKode"))
				{
					((MCabang)dataUpdateCabang.get()).setmCabangIdKode(dataRequest.getInt("mCabangIdKode"));
				}
				
				if(dataRequest.has("mAgenIdKode"))
				{
					((MCabang)dataUpdateCabang.get()).setmAgenIdKode(dataRequest.getInt("mAgenIdKode"));
				}
				
				if(dataRequest.has("agendetailIdKode"))
				{
					((MCabang)dataUpdateCabang.get()).setAgendetailIdKode(dataRequest.getInt("agendetailIdKode"));
				}
		    	JSONArray coverareaArray = dataRequest.getJSONArray("cover_area");
		    	if(coverareaArray.size() > 0)
		    	{
	    			List<Object[]> detail = mCabangdetailService.getdetailall(dataRequest.getInt("id"), dataRequest.getString("cabangCompanyId"));
	    			detail.stream().forEach(column -> {	    
	    				Optional <MCabangdetail> dataUpdatecabangdetail = mCabangdetailService.getdetail(Integer.parseInt(column[0].toString()), Integer.parseInt(column[1].toString()), dataRequest.getString("cabangCompanyId"));
		    			if(dataUpdatecabangdetail.isPresent())
		    			{
		    				((MCabangdetail)dataUpdatecabangdetail.get()).setCabangdetailStatus(false);
		    			}
	    			});
		    		for(int i = 0; i < coverareaArray.size(); i++)
		    		{
		    			JSONArray cityarray = coverareaArray.getJSONObject(i).getJSONArray("transfer_point_array");
		    	    	if(cityarray.size() > 0)
		    	    	{
		    	    		for(int j = 0; j < cityarray.size(); j++)
		    	    		{
		    	    			MCabangdetail datadetail = new MCabangdetail();
		    	    			datadetail.setmCabangId(dataRequest.getInt("id"));
		    	    			datadetail.setCabangdetailId(cityarray.getJSONObject(j).getInt("cabang_detail_id"));
		    	    			datadetail.setmProvinceId(coverareaArray.getJSONObject(i).getInt("provinsi_name_id"));
		    	    			datadetail.setCabangdetailPriority(cityarray.getJSONObject(j).get("priority")  == "" ? null : cityarray.getJSONObject(j).getInt("priority"));
		    	    			datadetail.setmKotaId(cityarray.getJSONObject(j).getInt("city_id"));
		    	    			datadetail.setmAgenId(cityarray.getJSONObject(j).get("agent_id").equals("") ? null : cityarray.getJSONObject(j).getInt("agent_id"));
		    	    			datadetail.setmAgenDetailId(cityarray.getJSONObject(j).get("agentDetailId").equals("") ? null : cityarray.getJSONObject(j).getInt("agentDetailId"));
		    	    			datadetail.setmParentId(cityarray.getJSONObject(j).get("parent_id").equals("") ? null : cityarray.getJSONObject(j).getInt("parent_id"));
		    	    			datadetail.setCabangdetailAgenttype(cityarray.getJSONObject(j).getString("type"));
		    	    			datadetail.setCabangdetailUpdatedBy(user_uuid);
		    	    			datadetail.setCabangdetailUpdatedAt(localDateTime2);
		    	    			datadetail.setCabangdetailStatus(cityarray.getJSONObject(j).getBoolean("status_detail"));
		    	    			datadetail.setCabangdetailCompanyId(cityarray.getJSONObject(j).getString("cabangdetailCompanyId"));
		    	    			datascabangdetails.add(datadetail);
		    	    		}
		    	    	}
		    		}
		    	}
	    	}
	    	allCabangService.saveallCabang(dataUpdateCabang.get(), datascabangdetails);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Cabang success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Update Cabang failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/listCabangKantor/{paramAktif}")
	public String listCabangKantor(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
//		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
//		RestTemplate restTemplate = new RestTemplate();
//	    HttpHeaders headers = new HttpHeaders();
//	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//	    headers.set("x-access-code", header);
//	    HttpEntity<String> entity = new HttpEntity<String>(headers);
//	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
//	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
//	    
//		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
//	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
//	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
//	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> cabangs = mCabangService.getallCabang(paramAktif.toString().toLowerCase(), dataRequest.getString("cabangCompanyId"));
			cabangs.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("cabangId", col[0]);
				data.put("cabangName", col[8]);
				data.put("mKotaId", col[1]);
				data.put("mProvinceId", col[9]);
				data.put("cabangStatus", col[6]);
				List<Object[]> getDetail = mCabangdetailService.getCoverArea(Integer.parseInt(col[0].toString()), dataRequest.getString("cabangCompanyId"));
				datas.add(data);
			});
			response.put("responseCode", "00");
		    response.put("responseDesc", "List Cabang Office success");
		    response.put("responseData", datas);
		}
		catch(Exception e)
		{
			 response.put("responseCode", "99");
		     response.put("responseDesc", "List Cabang Office Error");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/detailCabangKantor/{paramAktif}")
	public String detailCabangKantor(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("#### API = detailCabangKantor()");
		logger.info("#### INPUT = " + request);
		try
		{
			List<Object[]> getDetail = mCabangdetailService.getDetailKhususAgen(paramAktif.toLowerCase(), dataRequest.getInt("cabangId"), dataRequest.getString("companyId"));
			getDetail.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("cabangId", col[0]);
				data.put("cabangDetailId", col[1]);
				data.put("mAgenId", col[2] == null ? "" : col[2]);
				data.put("mAgenDetailId", col[3] == null ? "" : col[3]);
				data.put("companyId", col[4] == null ? "" : col[4]);
				data.put("mProvinceId", col[5] == null ? "" : col[5]);
				data.put("mKotaId", col[6] == null ? "" : col[6]);
				data.put("agenName", col[7] == null ? "" : col[7]);
				datas.add(data);
			});
			response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Cabang Office success");
		    response.put("responseData", datas);
		}
		catch(Exception e)
		{
			 response.put("responseCode", "99");
		     response.put("responseDesc", "Detail Cabang Office Error");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();	
		
	}
	
}