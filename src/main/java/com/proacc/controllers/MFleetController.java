package com.proacc.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.entity.MFleet;
import com.proacc.helper.Helper;
import com.proacc.service.MFleetService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MFleetController {
	private static final Logger logger = LoggerFactory.getLogger(MFleetController.class);
	@Autowired
	MFleetService mFleetService;
	
	@PostMapping("/getAllFleet/{paramAktif}")
	public String getAllFleet(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> fleets = mFleetService.getallFleet(paramAktif.toString().toLowerCase(), dataRequest.getString("fleetCompanyId"), user_uuid);
			fleets.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("No", column[0]);
				data.put("Name", column[1] == null ? "" : column[1].toString());
				data.put("Vehicle", column[2] == null ? "" : column[2].toString());
				data.put("Type", column[3].toString());
				data.put("Capacity", column[4].toString());
				data.put("Height", column[5] == null ? "" : column[5].toString());
				data.put("Width", column[6] == null ? "" : column[6].toString());
				data.put("Length", column[7].toString());
				data.put("Volume", column[8]);
				data.put("status", column[9].toString());
				data.put("fleetcompanyId", column[10].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data fleet berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data fleet gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getFleet/{paramAktif}")
	public String getFleet(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> fleets = mFleetService.getFleet(paramAktif.toString().toLowerCase(), dataRequest.getInt("fleetId"), dataRequest.getString("fleetCompanyId"));
			fleets.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("No", column[0]);
				data.put("Name", column[1] == null ? "" : column[1].toString());
				data.put("Vehicle", column[2] == null ? "" : column[2].toString());
				data.put("Type", column[3]);
				data.put("Capacity", column[4]);
				data.put("WeightType", column[5]);
				data.put("Height", column[6]);
				data.put("HeightType", column[7]);
				data.put("Width",  column[8]);
				data.put("WidthType", column[9]);
				data.put("Length", column[10]);
				data.put("LengthType", column[11]);
				data.put("Volume", column[12]);
				data.put("VolumeType", column[13]);
				data.put("status", column[14]);
				data.put("fleetcompanyId", column[15]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data fleet berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data fleet gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/saveFleet")
	public String saveFleet(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	int id = 0;
	    	List<Object[]> dataNext = mFleetService.nextvalMFleet();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	MFleet data = new MFleet();
	    	data.setFleetId(id);
	    	data.setFleetName(dataRequest.getString("Name"));
	    	data.setFleetVehicle(dataRequest.getString("Vehicle"));
	    	data.setmVehicletypeId(dataRequest.getInt("Type"));
	    	data.setFleetWeight(Float.parseFloat(dataRequest.getString("Capacity")));
	    	data.setmWeightId(dataRequest.getInt("WeightType"));
	    	data.setFleetHeight(Float.parseFloat(dataRequest.getString("Height")));
	    	data.setmHeightId(dataRequest.getInt("HeightType"));
	    	data.setFleetWidth(Float.parseFloat(dataRequest.getString("Width")));
	    	data.setmWidthId(dataRequest.getInt("WidthType"));
	    	data.setFleetLength(Float.parseFloat(dataRequest.getString("Length")));
	    	data.setmLengthId(dataRequest.getInt("LengthType"));
	    	data.setFleetVolume(Float.parseFloat(dataRequest.getString("Volume")));
	    	data.setmVolumeId(dataRequest.getInt("VolumeType"));
	    	data.setFleetCreatedBy(user_uuid);
	    	data.setFleetCreatedAt(localDateTime2);
	    	data.setFleetStatus(dataRequest.getBoolean("Status"));
	    	data.setFleetCompanyId(dataRequest.getString("fleetCompanyId"));
	    	mFleetService.saveFleet(data);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save fleet success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save fleet failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateFleet")
	public String updateFleet(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	Optional <MFleet> dataupdate = mFleetService.getdetail(dataRequest.getInt("No"), dataRequest.getString("fleetCompanyId"));
	    	if(dataupdate.isPresent())
	    	{
	    		if(dataRequest.containsValue(dataRequest.getString("Name")))
	    		{
	    			((MFleet)dataupdate.get()).setFleetName(dataRequest.getString("Name"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getString("Vehicle")))
	    		{
	    			((MFleet)dataupdate.get()).setFleetVehicle(dataRequest.getString("Vehicle"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getInt("Type")))
	    		{
	    			((MFleet)dataupdate.get()).setmVehicletypeId(dataRequest.getInt("Type"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.get("Capacity")))
	    		{
	    			((MFleet)dataupdate.get()).setFleetWeight(Float.parseFloat(dataRequest.getString("Capacity")));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getInt("WeightType")))
	    		{
	    			((MFleet)dataupdate.get()).setmWeightId(dataRequest.getInt("WeightType"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.get("Height")))
	    		{
	    			((MFleet)dataupdate.get()).setFleetHeight(Float.parseFloat(dataRequest.getString("Height")));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getInt("HeightType")))
	    		{
	    			((MFleet)dataupdate.get()).setmHeightId(dataRequest.getInt("HeightType"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.get("Width")))
	    		{
	    			((MFleet)dataupdate.get()).setFleetWidth(Float.parseFloat(dataRequest.getString("Width")));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getInt("WidthType")))
	    		{
	    			((MFleet)dataupdate.get()).setmWidthId(dataRequest.getInt("WidthType"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.get("Length")))
	    		{
	    			((MFleet)dataupdate.get()).setFleetLength(Float.parseFloat(dataRequest.getString("Length")));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getInt("LengthType")))
	    		{
	    			((MFleet)dataupdate.get()).setmLengthId(dataRequest.getInt("LengthType"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.get("Volume")))
	    		{
	    			((MFleet)dataupdate.get()).setFleetVolume(Float.parseFloat(dataRequest.getString("Volume")));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getInt("VolumeType")))
	    		{
	    			((MFleet)dataupdate.get()).setmVolumeId(dataRequest.getInt("VolumeType"));
	    		}
	    		((MFleet)dataupdate.get()).setFleetUpdatedBy(user_uuid);
	    		((MFleet)dataupdate.get()).setFleetUpdatedAt(localDateTime2);
	    		((MFleet)dataupdate.get()).setFleetStatus(dataRequest.getBoolean("Status"));
	    		((MFleet)dataupdate.get()).setFleetCompanyId(dataRequest.getString("fleetCompanyId"));
	    		mFleetService.saveFleet(dataupdate.get());
	    	}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update fleet success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Update fleet failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
