package com.proacc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.service.MWeightService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MWeightController {
	private static final Logger logger = LoggerFactory.getLogger(MWeightController.class);
	
	@Autowired
	MWeightService mWeightService;
	
	@PostMapping("/getAllWeight/{paramAktif}")
	public String getAllWeight(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> weights = mWeightService.getallWeight(paramAktif.toString().toLowerCase(), dataRequest.getString("weightCompanyId"));
			weights.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("weightId", column[0]);
				data.put("weightName", column[1] == null ? "" : column[1].toString());
				data.put("weightTokg", column[2] == null ? "" : column[2].toString());
				data.put("weightDescription", column[3] == null ? "" : column[3].toString());
				data.put("weightCreatedBy", column[4].toString());
				data.put("weightCreatedAt", column[5].toString());
				data.put("weightUpdatedBy", column[6] == null ? "" : column[6].toString());
				data.put("weightUpdatedAt", column[7] == null ? "" : column[7].toString());
				data.put("weightStatus", column[8].toString());
				data.put("weightCompanyId", column[9]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data weight berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data weight gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getWeight/{paramAktif}")
	public String getWeight(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> weights = mWeightService.getWeight(paramAktif.toString().toLowerCase(), dataRequest.getInt("weightId"), dataRequest.getString("weightCompanyId"));
			weights.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("weightId", column[0]);
				data.put("weightName", column[1] == null ? "" : column[1].toString());
				data.put("weightTokg", column[2] == null ? "" : column[2].toString());
				data.put("weightDescription", column[3] == null ? "" : column[3].toString());
				data.put("weightCreatedBy", column[4].toString());
				data.put("weightCreatedAt", column[5].toString());
				data.put("weightUpdatedBy", column[6] == null ? "" : column[6].toString());
				data.put("weightUpdatedAt", column[7] == null ? "" : column[7].toString());
				data.put("weightStatus", column[8].toString());
				data.put("weightCompanyId", column[9]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data weight berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data weight gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
