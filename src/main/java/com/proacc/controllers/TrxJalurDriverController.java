package com.proacc.controllers;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MDriver;
import com.proacc.entity.MRouteJalurDetail;
import com.proacc.entity.TrxDriverschedule;
import com.proacc.entity.TrxJalurDriver;
import com.proacc.entity.TrxJalurDriverHeader;
import com.proacc.entity.TrxOrder;
import com.proacc.entity.TrxTracking;
import com.proacc.helper.Helper;
import com.proacc.service.MDriverService;
import com.proacc.service.MVolumeService;
import com.proacc.service.MWeightService;
import com.proacc.service.TrxDriverscheduleService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxTrackingService;
import com.proacc.service.TrxJalurDriverHeaderService;
import com.proacc.service.AllJalurService;
import com.proacc.service.MAgendetailService;
import com.proacc.service.MCabangService;
import com.proacc.service.MDriverService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TrxJalurDriverController {
	private static final Logger logger = LoggerFactory.getLogger(TrxJalurDriverController.class);
	
	@Autowired
	TrxJalurDriverHeaderService TrxJalurDriverHeaderService;
	
	@Autowired
	AllJalurService AllJalurService;
	
	@Autowired
	MDriverService MDriverService;
	
	@Autowired
	MAgendetailService mAgendetailService;
	
	@Autowired
	MCabangService mCabangdetailService;
	
	Integer getIdFull = null;
	int a = 1;
	@PostMapping("/detailJalurDriver/{paramAktif}")
	public String detailJalurDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> dataList = TrxJalurDriverHeaderService.getDetail(paramAktif.toString().toLowerCase(), dataRequest.getString("companyId"), dataRequest.getInt("trxJalurdriverheaderId"));
			if(dataList.size()>0)
			{
				getIdFull = null;
				dataList.stream().forEach(col->{
					JSONObject data = new JSONObject();
					data.put("trxJalurdriverheaderId", dataRequest.getInt("trxJalurdriverheaderId"));
					data.put("trxJalurDriverDetailId", col[0] == null ? "" : col[0]);
					data.put("mRouteId", col[1] == null ? "" : col[1]);
					data.put("mRouteName", col[2] == null ? "" : col[2]);
					data.put("mRouteJalurId", col[3] == null ? "" : col[3]);
					data.put("mAgenId", col[4] == null ? "" : col[4]);
					data.put("mAgenDetailId", col[5] == null ? "" : col[5]);
					data.put("mCabangId", col[6] == null ? "" : col[6]);
					data.put("mCabangName", col[7] == null ? "" : col[7]);
					data.put("kotaCabangId", col[8] == null ? "" : col[8]);
					data.put("kotaAgenId", col[9] == null ? "" : col[9]);
					data.put("AgenName", col[10] == null ? "" : col[10]);
					data.put("status", col[11] == null ? "" : col[11]);
					data.put("statusProgress", col[12] == null ? "" : col[12]);
					data.put("positionCabangId", col[13] == null ? "" : col[13]);
					data.put("positionAgenId", col[14] == null ? "" : col[14]);
					data.put("positionAgenDetailId", col[15] == null ? "" : col[15]);
					List<JSONObject> detail = new ArrayList<>();
					if(col[4] != null)
					{
						//agen
						
						List<Object[]> getCoverArea = mAgendetailService.getCoverArea(Integer.parseInt(col[4].toString()), Integer.parseInt(col[5].toString()), dataRequest.getString("companyId"));
						if(getCoverArea.size()>0)
						{
							getCoverArea.stream().forEach(col2->{
								JSONObject data2 = new JSONObject();
								data2.put("agenId", col2[0] );
								data2.put("agenDetailId", col2[1] );
								data2.put("agenDetailCoverAreaId", col2[2] );
								data2.put("provinsiId", col2[3] );
								data2.put("kotaId", col2[4] );
								data2.put("status", col2[5] );
								data2.put("companyId", col2[6] );
								detail.add(data2);
							});
							
						}
					}
					else
					{
						List<Object[]> getDetail = mCabangdetailService.getCoverArea(Integer.parseInt(col[6].toString()), dataRequest.getString("companyId"));
						if(getDetail.size()>0)
						{
							getDetail.stream().forEach(col2->{
								JSONObject data2 = new JSONObject();
								data2.put("cabangId", col2[0] );
								data2.put("cabangDetailId", col2[1] );
								data2.put("provinsiId", col2[2] );
								data2.put("kotaId", col2[3] );
								data2.put("status", col2[4] );
								data2.put("companyId", col2[5] );
								detail.add(data2);
							});
						}
						//cabang
					}
					data.put("detail", detail);
					datas.add(data);
				});
				 a = 1;
				 
				 for (int i = 0; i < datas.size(); i++) {
					 if(!datas.get(i).getString("mAgenId").isEmpty() == true)
					 {
						 if(datas.get(i).get("mAgenId").equals(datas.get(i).get("positionAgenId")) && datas.get(i).get("mAgenDetailId").equals(datas.get(i).get("positionAgenDetailId")) )
						 {
							 getIdFull = datas.get(i).getInt("trxJalurDriverDetailId");
							 break;
						 }
					 }
					 else
					 {
						 
						 if( datas.get(i).get("mCabangId").equals(datas.get(i).get("positionCabangId") )   )
						 {
								getIdFull =  datas.get(i).getInt("trxJalurDriverDetailId");
								break;
						 }
					 }
					
					 
					
				}
				 if(getIdFull!=null)
				 {
					 datas.forEach(col->{
							if(col.getInt("trxJalurDriverDetailId") >= getIdFull)
							{
								col.put("active", true);
							}
							else
							{
								col.put("active", false);
							}
						}); 
				 }
				
			}
			response.put("responseCode", "00");
			response.put("responseDesc", "Detail Track Driver Success");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Track Driver Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	

	@PostMapping("/saveJalurDriver")
	public String saveJalurDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<TrxJalurDriver> dataDetailTrx = new ArrayList<>();
	    	int id = 0;
	    	List<Object[]> dataNext = TrxJalurDriverHeaderService.nextval();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	TrxJalurDriverHeader data = new TrxJalurDriverHeader();
	    	data.setTrxDriverjalurHeaderId(id);
	    	data.setmRouteId(dataRequest.getInt("mRouteId"));
	    	data.setCompanyId(dataRequest.getString("companyId"));
	    	JSONArray routeJalurDetail = dataRequest.getJSONArray("detailRoute");
	    	if(routeJalurDetail.size() > 0)
	    	{
	    		for (int j = 0; j < routeJalurDetail.size(); j++) {
					TrxJalurDriver dataDetail = new TrxJalurDriver();
					dataDetail.setTrxDriverjalurHeaderId(id);
					dataDetail.setTrxDriverjalurId(routeJalurDetail.getJSONObject(j).getInt("trxJalurDriverDetailId"));
					dataDetail.setmRouteId(dataRequest.getInt("mRouteId"));
					dataDetail.setmRoutejalurId(routeJalurDetail.getJSONObject(j).getInt("mRouteJalurId"));
					dataDetail.setStatus(routeJalurDetail.getJSONObject(j).getBoolean("status"));
					dataDetail.setStatusProgress(1);
					dataDetail.setCompanyId(dataRequest.getString("companyId"));
					dataDetailTrx.add(dataDetail);
				}
	    	
	    	}
	    	
	    	Optional<MDriver> getDetail = MDriverService.getdetail(dataRequest.getInt("mDriverId"), dataRequest.getString("companyId"));
	    	if(getDetail.isPresent())
	    	{
	    		((MDriver)getDetail.get()).setTrxJalurdriverheaderId(id);
	    	}
	    	
	    	AllJalurService.saveAll(data, dataDetailTrx);
	    	response.put("responseCode", "00");
			response.put("responseDesc", "Save Track Driver Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Save Track Driver Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/updateJalurDriver")
	public String updateJalurDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<TrxJalurDriver> dataDetailTrx = new ArrayList<>();
	    	int id = 0;
	    	List<Object[]> dataNext = TrxJalurDriverHeaderService.nextval();
	    	id = dataRequest.getInt("trxJalurDriverHeader");
	    	TrxJalurDriverHeader data = new TrxJalurDriverHeader();
	    	data.setTrxDriverjalurHeaderId(id);
	    	data.setmRouteId(dataRequest.getInt("mRouteId"));
	    	data.setCompanyId(dataRequest.getString("companyId"));
	    	JSONArray routeJalurDetail = dataRequest.getJSONArray("detailRoute");
	    	if(routeJalurDetail.size() > 0)
	    	{
	    		for (int j = 0; j < routeJalurDetail.size(); j++) {
					TrxJalurDriver dataDetail = new TrxJalurDriver();
					dataDetail.setTrxDriverjalurHeaderId(id);
					dataDetail.setTrxDriverjalurId(routeJalurDetail.getJSONObject(j).getInt("trxJalurDriverDetailId"));
					dataDetail.setmRouteId(dataRequest.getInt("mRouteId"));
					dataDetail.setmRoutejalurId(routeJalurDetail.getJSONObject(j).getInt("mRouteJalurId"));
					dataDetail.setStatus(routeJalurDetail.getJSONObject(j).getBoolean("status"));
					dataDetail.setStatusProgress(1);
					dataDetail.setCompanyId(dataRequest.getString("companyId"));
					dataDetailTrx.add(dataDetail);
				}
	    	
	    	}
	    	
	    	Optional<MDriver> getDetail = MDriverService.getdetail(dataRequest.getInt("mDriverId"), dataRequest.getString("companyId"));
	    	if(getDetail.isPresent())
	    	{
	    		((MDriver)getDetail.get()).setTrxJalurdriverheaderId(id);
	    	}
	    	
	    	AllJalurService.saveAll(data, dataDetailTrx);
	    	response.put("responseCode", "00");
			response.put("responseDesc", "Update Track Driver Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Update Track Driver Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}

}
