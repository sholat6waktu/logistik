package com.proacc.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MAgen;
import com.proacc.entity.MAgendetail;
import com.proacc.entity.MagenCoverArea;
import com.proacc.helper.Helper;
import com.proacc.service.AllAgenService;
import com.proacc.service.MAgenCoverAreaService;
import com.proacc.service.MAgenService;
import com.proacc.service.MAgendetailService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MAgenController {

	private static final Logger logger = LoggerFactory.getLogger(MAgenController.class);
	
	@Autowired
	MAgenService mAgenService;
	
	@Autowired
	MAgendetailService mAgendetailService;
	
	@Autowired
	MAgenCoverAreaService MAgenCoverAreaService;
	
	@Autowired
	AllAgenService allAgenService;
	
	@PostMapping("/listKode/{paramAktif}")
	public String listKode(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
	   
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		
		logger.info("listKode API CALL ===");
		logger.info("listKode JSON === " + dataRequest);
		    
		List<JSONObject> datasagendetails = new ArrayList<>();
		try
		{
			List<Object[]> listkode = mAgendetailService.listkode(paramAktif, dataRequest.getString("companyId"));
			listkode.stream().forEach(col->{
				JSONObject data= new JSONObject();
				data.put("name", col[0] == null ? "" : col[0]);
				data.put("mCabangId", col[1] == null ? "" : col[1]);
				data.put("mAgenId", col[2] == null ? "" : col[2]);
				data.put("mAgenDetailId", col[3] == null ? "" : col[3]);
				data.put("mKotaId", col[4] == null ? "" : col[4]);
				data.put("companyId", col[5] == null ? "" : col[5]);
				data.put("kode", col[6] == null ? "" : col[6]);
				data.put("status", col[8] == null ? "" : col[8]);
				datasagendetails.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "List Code Success");
			response.put("responseData", datasagendetails);	
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "data Agen gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	

	
	@PostMapping("/getAllAgen/{paramAktif}")
	public String getAllAgen(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		List<JSONObject> datasagendetails = new ArrayList<>();
		try
		{
			List<Object[]> agens = mAgenService.getallAgen(paramAktif.toString().toLowerCase(), dataRequest.getString("agenCompanyId"));
			agens.stream().forEach(column -> {
				List<Object[]> agenDetails = mAgendetailService.getallAgendetail(paramAktif.toString().toLowerCase(), Integer.parseInt(column[0].toString()), dataRequest.getString("agenCompanyId"));
				agenDetails.stream().forEach(columndetail-> {
					JSONObject datadetail = new JSONObject();
					datadetail.put("id", columndetail[0]);
					datadetail.put("agent_id", columndetail[1]);
					datadetail.put("agent", "iki gawe jeneng");
					if(columndetail[2] != null)
					{
						for(int i = 0; i < dataprovinces.size(); i++)
						{
							if(columndetail[2].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
							{
								datadetail.put("provinsi", dataprovinces.getJSONObject(i).get("prov_name"));
								break;
							}
							else if(dataprovinces.size() == i + 1)
							{
								datadetail.put("provinsi", "");
							}
						}
					}
					else
					{
						datadetail.put("provinsi", "");
					}
					if(columndetail[3] != null)
					{
						for(int i = 0; i < datacities.size(); i++)
						{
							if(columndetail[3].equals(datacities.getJSONObject(i).getInt("city_id")))
							{
								datadetail.put("transfer_point", datacities.getJSONObject(i).get("city_name"));
								break;
							}
							else if(datacities.size() == i + 1)
							{
								datadetail.put("transfer_point", "");
							}
						}
					}
					else
					{
						datadetail.put("transfer_point", "");
					}
					if(columndetail[4] != null)
					{
						String listkota = columndetail[4].toString();
						String[] kotatype = listkota.split(",");
						String hasilyangreal = "";
						for(String hasil: kotatype)
						{
							String kota = hasil.split("-")[0];
							String type = hasil.split("-")[1];
							for(int i = 0; i < datacities.size(); i++)
							{	
								if(kota.equals(datacities.getJSONObject(i).getString("city_id")))
								{
									hasilyangreal = hasilyangreal + datacities.getJSONObject(i).get("city_name") + "(" + type + ") ";
								}	
							}
						}
						datadetail.put("kota", hasilyangreal);
					}
					else
					{
						datadetail.put("kotaTo", "");
					}
					datadetail.put("listkota", columndetail[4].toString());
					datadetail.put("status", column[6].toString());
					datadetail.put("agendetailStatus", columndetail[9].toString());
					datadetail.put("agendetailCompanyId", columndetail[10]);
					datasagendetails.add(datadetail);
				});
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Agen berhasil didapat");
			response.put("responseData", datasagendetails);	
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Agen gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	String hasilyangreal = "";
	@PostMapping("/getallAgendetail/{paramAktif}")
	public String getAllAgendetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
				List<JSONObject> datasagendetails = new ArrayList<>();
				List<JSONObject> datasagendetailsHeader = new ArrayList<>();
				List<Object[]> agenDetails = mAgendetailService.getalldetail(paramAktif.toString().toLowerCase(), dataRequest.getString("agenCompanyId"));
//				if(agenDetails.size()>0)
//				{
					agenDetails.stream().forEach(columndetail-> {
						JSONObject datadetail = new JSONObject();
						datadetail.put("id", columndetail[0]);
						datadetail.put("agen_id", columndetail[1]);
						datadetail.put("agen", "iki gawe jeneng");
						datadetail.put("mProvinsiId", columndetail[2] );
						if(columndetail[2] != null)
						{
							for(int i = 0; i < dataprovinces.size(); i++)
							{
								if(columndetail[2].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
								{
									datadetail.put("provinsi", dataprovinces.getJSONObject(i).get("prov_name"));
									break;
								}
								else if(dataprovinces.size() == i + 1)
								{
									datadetail.put("provinsi", "");
								}
							}
						}
						else
						{
							datadetail.put("provinsi", "");
						}
						
						if(columndetail[3] != null)
						{
							for(int i = 0; i < datacities.size(); i++)
							{
								if(columndetail[3].equals(datacities.getJSONObject(i).getInt("city_id")))
								{
									datadetail.put("transfer_point", datacities.getJSONObject(i).get("city_name"));
									break;
								}
								else if(datacities.size() == i + 1)
								{
									datadetail.put("transfer_point", "");
								}
							}
						}
						else
						{
							datadetail.put("transfer_point", "");
						}
						if(columndetail[4] != null)
						{
//							for(int i = 0; i < datacities.size(); i++)
//							{
//								if(columndetail[4].equals(datacities.getJSONObject(i).getInt("city_id")))
//								{
//									datadetail.put("kota", datacities.getJSONObject(i).get("city_name"));
//									break;
//								}
//								else if(datacities.size() == i + 1)
//								{
//									datadetail.put("kota", "");
//								}
//							}
							String listkota = columndetail[4].toString().replace("{", "");
							System.out.print("ini list kota nya  = " + listkota);
							String[] kotatype = listkota.split(",");
							hasilyangreal = "";
							for(String hasil: kotatype)
							{
								System.out.print("ini apa = " + hasil.toString());
								String kota =hasil;
//								String kota = hasil.split("-")[0];
//								String type = hasil.split("-")[1];
								for(int i = 0; i < datacities.size(); i++)
								{	
									if(kota.equals(datacities.getJSONObject(i).getString("city_id")))
									{
										hasilyangreal = hasilyangreal + datacities.getJSONObject(i).getString("city_name") + " ";
									}	
								}
							}
//							datadetail.put("kota", hasilyangreal);
							datadetail.put("kota", hasilyangreal);
						}
						else
						{
							datadetail.put("kota", "");
						}
						
						datadetail.put("listkota", columndetail[4] == null ? "" : columndetail[4].toString());
						datadetail.put("agendetailStatus", columndetail[5].toString());
						datadetail.put("status", columndetail[6]);
						datadetail.put("agendetailCompanyId", columndetail[7]);
						datasagendetails.add(datadetail);
						datasagendetailsHeader.add(datadetail);
						
				});
//				}
				
//				List<JSONObject> listDataDetail = new ArrayList<>();
//				List<Object[]> listDetail = mAgendetailService.listDetail(dataRequest.getString("agenCompanyId"), paramAktif.toLowerCase());
//				listDetail.stream().forEach(col->{
//					JSONObject data = new JSONObject();
//					data.put("agenId", col[0] == null ? "" :  Integer.parseInt(col[0].toString()));
//					data.put("agenDetailId", col[1] == null ? "" :  Integer.parseInt(col[1].toString()));
//					data.put("agenDetailCoverAreaId", col[2] == null ? "" :  Integer.parseInt(col[2].toString()));
//					data.put("mProvinceAsalId", col[3] == null ? "" :  col[3]);
//					data.put("mKotaAsalId", col[4] == null ? "" :  col[4]);
//					data.put("mProvinceId", col[5] == null ? "" :  col[5]);
//					data.put("mKotaId", col[6] == null ? "" :  col[6]);
//					listDataDetail.add(data);
//				});
//				HashSet<Object> seen1= new HashSet<>();
//				datasagendetailsHeader.removeIf(e -> !seen1.add(Arrays.asList(e.getInt("id"), e.getString("agendetailCompanyId"))));
//				
//				if(datasagendetails.size()>0 && listDataDetail.size() > 0)
//				{
//					for (JSONObject detail : datasagendetails) {
//						List<JSONObject> dataBaru = new ArrayList<>();
//						List<JSONObject> dataBaruHeader = new ArrayList<>();
//						List<JSONObject> result = new ArrayList<>();
//						List<JSONObject> resultHeader = new ArrayList<>();
//						if(listDataDetail.size()>0)
//						{
//							for (JSONObject detailJSON : listDataDetail) {
//								if(detailJSON.get("agenId") != "")
//								{
//									if(detail.getInt("id") == detailJSON.getInt("agenId") && detail.getInt("mProvinsiId") == detailJSON.getInt("mProvinceAsalId"))
//										
//									{
//										dataBaru.add(detailJSON);
//										dataBaruHeader.add(detailJSON);
//									}
//								}
//								
//							}
//						}
//						
//						 HashSet<Object> seen= new HashSet<>();
//						 dataBaruHeader.removeIf(e -> !seen.add(Arrays.asList(e.getInt("agenId"), e.getInt("mProvinceId"))));
//
//						 if(dataBaruHeader.size() > 0)
//						 {
//							 for (JSONObject header1 : dataBaruHeader) {
//								 
//								 if(header1.getInt("agenId") == detail.getInt("id"))
//								 {
//									
//									resultHeader.add(header1);
//								 }
//								
//							}
//							 
//							 if(resultHeader.size() > 0)
//							 {
//								 for(JSONObject detail1 : resultHeader)
//								 {
//									 List<JSONObject> detailRe = new ArrayList<>();
//									 for(JSONObject data2 : dataBaru)
//									 {
//										 if(data2.getInt("agenId") == detail1.getInt("agenId") && data2.getInt("mProvinceId") == detail1.getInt("mProvinceId"))
//										 {
//											 detailRe.add(data2);
//											
//										 }
//									 }
//									 detail1.put("coverArea", detailRe);
//								 }
//							 }
//							 
//						 }
//						 
//						 
//						 detail.put("detailBaru", resultHeader);
//					}
//				}
				
				
//		   		 pairs2.removeIf(e -> !seen.add(Arrays.asList(e.getCompany_id(), e.getBoardId(), e.getBoardProjectDetailId() )));
//		    	
//		   		 pairs2.stream().forEach(col->{
//		   			JSONObject dataHeaderFull = new JSONObject();
//		   			ArrayList<ChildComment> dataHeader = new ArrayList<ChildComment>();
//		   			
//		   			Map<Integer, ParentComment> hmTry = new HashMap<>();
//		   			pairs.stream().forEach(col2->{
//		   				
//		   				if(col2.getCompany_id().equals(col.getCompany_id())
//		   				   && col2.getBoardId().equals(col.getBoardId())
//		   				   &&  col2.getBoardProjectDetailId().equals(col.getBoardProjectDetailId())
//		   						)
//		   				{
//		   					ChildComment data = new ChildComment();
//				    		data.setChildId(col2.getChildId());
//				    		data.setParentId(col2.getParentId());
//				    		data.setBoardId(col2.getBoardId());
//				    		data.setBoardProjectDetailId(col2.getBoardProjectDetailId());
//				    		data.setBoardProjectDetailDisId(col2.getBoardProjectDetailDisId());
//				    		data.setCompany_id(col2.getCompany_id());
//				    		data.setMProjectId(col2.getMProjectId());
//				    		data.setProjectDetailId(col2.getProjectDetailId());
//				    		data.setParentId(col2.getParentId());
//				    		data.setMProjectId(col2.getMProjectId());
//				    		data.setProjectDetailId(col2.getProjectDetailId());
//				    		data.setNamaTask(col2.getNamaTask());
//				    		
//				    		data.setContactId(col2.getContactId());
//				    		data.setCommentDiscussion(col2.getCommentDiscussion());
//				    		data.setDateComment(col2.getDateComment());
//				    		dataHeader.add(data);
//		   				}
//		   			});
//		   			dataHeaderFull.put("header", "Task > " + col.getNamaTask());
//		   			dataHeaderFull.put("boardid", col.getBoardId());
//		   			dataHeaderFull.put("boardprojectdetailid", col.getBoardProjectDetailId());
//		   			dataHeaderFull.put("detail", dataHeader);
//		   			finalFull.add(dataHeaderFull);
//		   		});
				
			response.put("responseCode", "00");
			response.put("responseDesc", "data Agen berhasil didapat");
			response.put("responseData", datasagendetails);	
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Agen gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}

	@PostMapping("/getdetailforupdate/{paramAktif}")
	public String getdetailforupdate(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<JSONObject> listDataDetail = new ArrayList<>();
			List<Object[]> listDetail = mAgendetailService.listDetail(dataRequest.getString("agenCompanyId"), paramAktif.toLowerCase());
			listDetail.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("agenId", col[0] == null ? "" :  Integer.parseInt(col[0].toString()));
				data.put("agenDetailId", col[1] == null ? "" :  Integer.parseInt(col[1].toString()));
				data.put("agenDetailCoverAreaId", col[2] == null ? "" :  Integer.parseInt(col[2].toString()));
				data.put("mProvinceAsalId", col[3] == null ? "" :  col[3]);
				data.put("mKotaAsalId", col[4] == null ? "" :  col[4]);
				data.put("mProvinceId", col[5] == null ? "" :  col[5]);
				data.put("mKotaId", col[6] == null ? "" :  col[6]);
				listDataDetail.add(data);
				
				
			});
			
			
			List<Object[]> agens = mAgenService.getAgen("a",dataRequest.getInt("agenId"), dataRequest.getString("agenCompanyId"));
			agens.stream().forEach(column -> {
				List<JSONObject> datasprovinsi = new ArrayList<>();
				JSONObject data = new JSONObject();
				data.put("id", column[0]);
				data.put("agent_id", column[1].toString());
				data.put("agent", "iki kolom gawe jeneng");
				data.put("status", column[6]);
				data.put("agenCompanyId", column[7]);
				data.put("agen_type", column[8]);
			List<Object[]> agenprovince = mAgendetailService.getdetailprovinsiforupdate(paramAktif.toString().toLowerCase(), dataRequest.getInt("agenId"), dataRequest.getString("agenCompanyId"));
			agenprovince.stream().forEach(column1 -> {
				List<JSONObject> datastransferpoint = new ArrayList<>();
				JSONObject dataprovinsi = new JSONObject();
				if(column1[0] != null)
				{
					for(int i = 0; i < dataprovinces.size(); i++)
					{
						if(column1[0].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
						{
							dataprovinsi.put("provinsi_name", dataprovinces.getJSONObject(i).get("prov_name"));
							dataprovinsi.put("provinsi_name_id", column1[0]);
							break;
						}
						else if(dataprovinces.size() == i + 1)
						{
							dataprovinsi.put("provinsi_name", "");
							dataprovinsi.put("provinsi_name_id", column1[0]);
						}
					}
				}
				else
				{
					dataprovinsi.put("provinsi_name", "");
					dataprovinsi.put("provinsi_name_id", column1[0]);
				}
				List<Object[]> agentransferpoint = mAgendetailService.getdetailtransferpointforupdate(paramAktif.toString().toLowerCase(), dataRequest.getInt("agenId"), Integer.parseInt(column1[0].toString()), dataRequest.getString("agenCompanyId"));
				agentransferpoint.stream().forEach(column2 -> {
					List<JSONObject> dataskota = new ArrayList<>();
					JSONObject datatransferpoint = new JSONObject();
					//datatransferpoint.put("provinsi_name_id", column2[0]);
					if(column2[1] != null)
					{
						for(int i = 0; i < datacities.size(); i++)
						{
							if(column2[1].equals(datacities.getJSONObject(i).getInt("city_id")))
							{
								datatransferpoint.put("transer_point", datacities.getJSONObject(i).get("city_name"));
								datatransferpoint.put("transer_point_id", column2[1]);
								break;
							}
							else if(datacities.size() == i + 1)
							{
								datatransferpoint.put("transer_point", "");
								datatransferpoint.put("transer_point_id", "");
							} 
						}
					}
					else
					{
						datatransferpoint.put("transer_point", "");
						datatransferpoint.put("transer_point_id", "");
					}
					List<Object[]> agenkota = mAgendetailService.getdetailkotaforupdate(paramAktif.toString().toLowerCase(), dataRequest.getInt("agenId"), Integer.parseInt(column1[0].toString()), Integer.parseInt(column2[1].toString()), dataRequest.getString("agenCompanyId"));
					agenkota.stream().forEach(column3 -> {
						JSONObject datakota = new JSONObject();
						datakota.put("provinsiId", column3[0]);
						datakota.put("TransferpointId", column3[1]);
						if(column3[2] != null)
						{
							for(int i = 0; i < datacities.size(); i++)
							{
								if(column3[2].equals(datacities.getJSONObject(i).getInt("city_id")))
								{
									datakota.put("city", datacities.getJSONObject(i).get("city_name"));
									datakota.put("city_id", column3[2]);
									break;
								}
								else if(datacities.size() == i + 1)
								{
									datakota.put("city", "");
									datakota.put("city_id", "");
								} 
							}
						}
						else
						{
							datakota.put("city", "");
							datakota.put("city_id", "");
						}
						List<JSONObject> dataBaru = new ArrayList<>();
						List<JSONObject> dataBaruHeader = new ArrayList<>();
						List<JSONObject> result = new ArrayList<>();
						List<JSONObject> resultHeader = new ArrayList<>();
						for (JSONObject detailJSON : listDataDetail) {
							if(detailJSON.get("agenId") != "")
							{
								if(Integer.parseInt(column[0].toString()) == detailJSON.getInt("agenId") && Integer.parseInt(column3[4].toString()) == detailJSON.getInt("agenDetailId"))
								{
									dataBaru.add(detailJSON);
									dataBaruHeader.add(detailJSON);
								}
							}
							
						}
						HashSet<Object> seen= new HashSet<>();
						dataBaruHeader.removeIf(e -> !seen.add(Arrays.asList(e.getInt("agenId"),e.getInt("agenDetailId"), e.getInt("mProvinceId"))));
						if(dataBaruHeader.size() > 0)
						{
							 for (JSONObject header1 : dataBaruHeader) {
								 
								 if(header1.getInt("agenId") == Integer.parseInt(column[0].toString()) && Integer.parseInt(column3[4].toString()) == header1.getInt("agenDetailId"))
								 {
									
									resultHeader.add(header1);
								 }
								
							}
								 
							 if(resultHeader.size() > 0)
							 {
								 for(JSONObject detail1 : resultHeader)
								 {
									 List<JSONObject> detailRe = new ArrayList<>();
									 for(JSONObject data2 : dataBaru)
									 {
										if(detail1.get("agenId") != "")
										{
											if(data2.getInt("agenId") == detail1.getInt("agenId") && data2.getInt("mProvinceId") == detail1.getInt("mProvinceId") && detail1.getInt("agenDetailId") == data2.getInt("agenDetailId"))
											 {
												 detailRe.add(data2);
												
											 }
										}
										 
									 }
									 detail1.put("coverArea", detailRe);
								 }
							 }
							 
						}
						datakota.put("detailBaru", resultHeader);
						datakota.put("type", column3[3]);
						datakota.put("agen_detail_id", column3[4]);
						datakota.put("status_detail", column3[5]);
						datakota.put("agendetailCompanyId", column3[6]);
						datakota.put("kode", column3[7] == null ? "" : column3[7]);
						datakota.put("typeKode", column3[8] == null ? "" : column3[8]);
						datakota.put("mCabangIdKode", column3[9] == null ? "" : column3[9]);
						datakota.put("mAgenIdKode", column3[10] == null ? "" : column3[10]);
						datakota.put("agendetailIdKode", column3[11] == null ? "" : column3[11]);
						
						dataskota.add(datakota);
					});
					datatransferpoint.put("city_array", dataskota);
					datastransferpoint.add(datatransferpoint);
				});
				dataprovinsi.put("transfer_point_array", datastransferpoint);
				datasprovinsi.add(dataprovinsi);
			});
			data.put("cover_area", datasprovinsi);
			datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Agen berhasil didapat");
			response.put("responseData", datas);	
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Agen gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/listAgenKantor/{paramAktif}")
	public String listAgenKantor(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
//		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
//	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
//	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
//	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		
		try
		{
			List<Object[]> listAgenKantor = mAgendetailService.listAgenKantor(paramAktif.toLowerCase(), dataRequest.getString("compId"));
			listAgenKantor.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("agenId", col[0]);
				data.put("agenDetailId", col[1]);
				data.put("agenName", col[2]);
				data.put("mProvinceId", col[3]);
				data.put("mKotaId", col[5]);
				data.put("agenStatus", col[10]);
				data.put("agenCompanyId", col[11]);
				
				List<JSONObject> detail = new ArrayList<>();
				List<Object[]> getCoverArea = mAgendetailService.getCoverArea(Integer.parseInt(col[0].toString()), Integer.parseInt(col[1].toString()), dataRequest.getString("compId"));
				if(getCoverArea.size()>0)
				{
					getCoverArea.stream().forEach(col2->{
						JSONObject data2 = new JSONObject();
						data2.put("agenId", col2[0] );
						data2.put("agenDetailId", col2[1] );
						data2.put("agenDetailCoverAreaId", col2[2] );
						data2.put("provinsiId", col2[3] );
						data2.put("kotaId", col2[4] );
						data2.put("status", col2[5] );
						data2.put("companyId", col2[6] );
						detail.add(data2);
					});
					
				}
				data.put("detail", detail);
				datas.add(data);
			});
			
			response.put("responseCode", "00");
			response.put("responseDesc", "List Agen Office Success");
			response.put("responseData", datas);	
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "List Agen Office Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/listAgenDetail/{paramAktif}")
	public String listAgenDetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		
		try
		{
			List<Object[]> listAgen = mAgendetailService.listAgen(paramAktif.toLowerCase(), dataRequest.getString("compId"));
			listAgen.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("agenId", col[0] == null ? "" : col[0]);
				data.put("agenDetailId", col[1] == null ? "" : col[1]);
				data.put("agenDetailCompanyId", col[2] == null ? "" : col[2]);
				data.put("mKotaId", col[3] == null ? "" : col[3]);
				data.put("agenName", col[4] == null ? "" : col[4]);
				data.put("agenTask", col[5] == null ? "" : col[5]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "List Agen Success");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "List Agen Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/getAgendetailByAgen/{paramAktif}")
	public String getAgendetailByAgen(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		
		try
		{
			List<Object[]> agens = mAgenService.getAgen(paramAktif.toString().toLowerCase(),dataRequest.getInt("agenId"), dataRequest.getString("agenCompanyId"));
			agens.stream().forEach(column -> {
				List<JSONObject> datasagendetails = new ArrayList<>();
				JSONObject data = new JSONObject();
				data.put("agenId", column[0]);
				data.put("agenName", column[1].toString());
				data.put("agenCreatedBy", column[2].toString());
				data.put("agenCreatedAt", column[3].toString());
				data.put("agenUpdatedBy", column[4] == null ? "" : column[4].toString());
				data.put("agenUpdatedAt", column[5] == null ? "" : column[5].toString());
				data.put("agenStatus", column[6].toString());
				data.put("agenCompanyId", column[7].toString());
				List<Object[]> agenDetails = mAgendetailService.getallAgendetail(paramAktif.toString().toLowerCase(), Integer.parseInt(column[0].toString()), dataRequest.getString("agenCompanyId"));
				agenDetails.stream().forEach(columndetail-> {
					JSONObject datadetail = new JSONObject();
					if(columndetail[0] != null)
					{
						for(int i = 0; i < dataprovinces.size(); i++)
						{
							if(columndetail[0].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
							{
								datadetail.put("provinceName", dataprovinces.getJSONObject(i).get("prov_name"));
								datadetail.put("mProvinceId", columndetail[0]);
								break;
							}
							else if(dataprovinces.size() == i + 1)
							{
								datadetail.put("provinceName", "");
								datadetail.put("mProvinceId", "");
							}
						}
					}
					else
					{
						datadetail.put("provinceName", "");
						datadetail.put("mProvinceId", "");
					}
					if(columndetail[1] != null)
					{
						for(int i = 0; i < datacities.size(); i++)
						{
							if(columndetail[1].equals(datacities.getJSONObject(i).getInt("city_id")))
							{
								datadetail.put("kotaTransferpoint", datacities.getJSONObject(i).get("city_name"));
								datadetail.put("mTransferpointId", columndetail[1]);
								break;
							}
							else if(datacities.size() == i + 1)
							{
								datadetail.put("kotaTransferpoint", "");
								datadetail.put("mTransferpointId", "");
							} 
						}
					}
					else
					{
						datadetail.put("kotaTransferpoint", "");
						datadetail.put("mTransferpointId", "");
					}
					if(columndetail[2] != null)
					{
						String listkota = columndetail[2].toString();
						String[] kotatype = listkota.split(",");
						String hasilyangreal = "";
						for(String hasil: kotatype)
						{
							String kota = hasil.split("-")[0];
							String type = hasil.split("-")[1];
							for(int i = 0; i < datacities.size(); i++)
							{	
								if(kota.equals(datacities.getJSONObject(i).getString("city_id")))
								{
									hasilyangreal = hasilyangreal + datacities.getJSONObject(i).get("city_name") + "(" + type + ") ";
								}	
							}
						}
						datadetail.put("kotaTo", hasilyangreal);
					}
					else
					{
						datadetail.put("kotaTo", "");
					}
					datadetail.put("listkota", columndetail[2].toString());
					datadetail.put("agendetailCreatedBy", columndetail[3].toString());
					datadetail.put("agendetailCreatedAt", columndetail[4].toString());
					datadetail.put("agendetailUpdatedBy", columndetail[5] == null ? "" : columndetail[5].toString());
					datadetail.put("agendetailUpdatedAt", columndetail[6] == null ? "" : columndetail[6].toString());
					datadetail.put("agendetailStatus", columndetail[7].toString());
					datadetail.put("agendetailCompanyId", columndetail[8]);
					datasagendetails.add(datadetail);
				});
				data.put("agendetailArray", datasagendetails);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Agen berhasil didapat");
			response.put("responseData", datas);	
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Agen gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	
	@PostMapping("/getAgendetailByprovince/{paramAktif}")
	public String getAgendetailByprovince(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/cities?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecities = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacities = (JSONObject)JSONSerializer.toJSON(responsecities.getBody());
	    net.sf.json.JSONArray datacities = objdatacities.getJSONArray("data");
	    
		final String url2 = "http://54.169.109.123:3004/api/v1/provinces?limit=all";
	    ResponseEntity<String> responseprovinces = restTemplate.exchange(url2, HttpMethod.GET, entity, String.class);
	    JSONObject objdataprovinces = (JSONObject)JSONSerializer.toJSON(responseprovinces.getBody());
	    net.sf.json.JSONArray dataprovinces = objdataprovinces.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
				List<JSONObject> datasagendetails = new ArrayList<>();
				List<Object[]> agenDetails = mAgendetailService.getAgendetailbyprovince(paramAktif.toString().toLowerCase(), dataRequest.getInt("provinceId"), dataRequest.getString("agenCompanyId"));
				agenDetails.stream().forEach(columndetail-> {
					JSONObject datadetail = new JSONObject();
					datadetail.put("agenId", columndetail[0]);
					datadetail.put("agenDetailId", columndetail[4]);
					datadetail.put("agenname", columndetail[3]);
					if(columndetail[1] != null)
					{
						for(int i = 0; i < dataprovinces.size(); i++)
						{
							if(columndetail[1].equals(dataprovinces.getJSONObject(i).getInt("prov_id")))
							{
								datadetail.put("provinceName", dataprovinces.getJSONObject(i).get("prov_name"));
								datadetail.put("mProvinceId", columndetail[1]);
								break;
							}
							else if(dataprovinces.size() == i + 1)
							{
								datadetail.put("provinceName", "");
								datadetail.put("mProvinceId", "");
							}
						}
					}
					else
					{
						datadetail.put("provinceName", "");
						datadetail.put("mProvinceId", "");
					}
					if(columndetail[2] != null)
					{
						for(int i = 0; i < datacities.size(); i++)
						{
							if(columndetail[2].equals(datacities.getJSONObject(i).getInt("city_id")))
							{
								datadetail.put("kotaname", datacities.getJSONObject(i).get("city_name"));
								datadetail.put("kotaId", columndetail[2]);
								break;
							}
							else if(datacities.size() == i + 1)
							{
								datadetail.put("kotaname", "");
								datadetail.put("kotaId", "");
							} 
						}
					}
					else
					{
						datadetail.put("kotaname", "");
						datadetail.put("kotaId", "");
					}
					datasagendetails.add(datadetail);
				});
			response.put("responseCode", "00");
			response.put("responseDesc", "data Agen berhasil didapat");
			response.put("responseData", datasagendetails);	
		}
		catch(Exception e)
		{
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data Agen gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
	    return response.toString();
	}
	@PostMapping("/saveAgen")
	public String saveAgen(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	ArrayList<MAgendetail> datasagendetails = new ArrayList<>();
	    	ArrayList<MagenCoverArea> dataCoverAreaHeader = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	int id = 0;
	    	List<Object[]> dataNext = mAgenService.nextvalMAgen();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	MAgen data = new MAgen();
	    	data.setAgenId(id);
	    	data.setAgenName(dataRequest.getString("agent_id"));
	    	data.setAgenCreatedBy(user_uuid);
	    	data.setAgenCreatedAt(localDateTime2);
	    	data.setAgenStatus(dataRequest.getBoolean("status"));
	    	data.setAgenCompanyId(dataRequest.getString("agenCompanyId"));
	    	data.setAgenType(dataRequest.getInt("agen_type"));
	    	JSONArray coverareaArray = dataRequest.getJSONArray("cover_area");
	    	if(coverareaArray.size() > 0)
	    	{
	    		for(int i = 0; i < coverareaArray.size(); i++)
	    		{
	    			JSONArray transferpointarray = coverareaArray.getJSONObject(i).getJSONArray("transfer_point_array");
	    	    	if(transferpointarray.size() > 0)
	    	    	{
	    	    		for(int j = 0; j < transferpointarray.size(); j++)
	    	    		{
	    	    			JSONArray cityarray = transferpointarray.getJSONObject(j).getJSONArray("city_array");
	    	    	    	if(cityarray.size() > 0)
	    	    	    	{
	    	    	    		for(int k = 0; k < cityarray.size(); k++)
	    	    	    		{	
	    	    	    			MAgendetail datadetail = new MAgendetail();
	    	    	    			datadetail.setmAgenId(id);
	    	    	    			datadetail.setAgendetailId(cityarray.getJSONObject(k).getInt("agen_detail_id"));
	    	    	    			datadetail.setmProvinceId(coverareaArray.getJSONObject(i).getInt("provinsi_name_id"));
	    	    	    			datadetail.setmTransferpointId(transferpointarray.getJSONObject(j).getInt("transer_point_id"));
	    	    	    			datadetail.setmKotaId(cityarray.getJSONObject(k).getInt("city_id"));
	    	    	    			datadetail.setAgendetailTask(cityarray.getJSONObject(k).getString("type"));
	    	    	    			datadetail.setAgendetailCreatedBy(user_uuid);
	    	    	    			datadetail.setAgendetailCreatedAt(localDateTime2);
	    	    	    			datadetail.setAgendetailStatus(cityarray.getJSONObject(k).getBoolean("status_detail"));
	    	    	    			datadetail.setAgendetailCompanyId(cityarray.getJSONObject(k).getString("agendetailCompanyId"));
	    	    	    			datadetail.setKode(cityarray.getJSONObject(k).getString("kode"));
	    	    	    			datadetail.setTypeKode(cityarray.getJSONObject(k).getInt("typeKode"));
	    	    	    			if(cityarray.getJSONObject(k).has("mCabangIdKode"))
	    	    	    			{
	    	    	    				datadetail.setmCabangIdKode(cityarray.getJSONObject(k).getInt("mCabangIdKode"));
	    	    	    			}
	    	    	    			
	    	    	    			if(cityarray.getJSONObject(k).has("mAgenIdKode"))
	    	    	    			{
	    	    	    				datadetail.setmAgenIdKode(cityarray.getJSONObject(k).getInt("mAgenIdKode"));
	    	    	    			}
	    	    	    			
	    	    	    			if(cityarray.getJSONObject(k).has("agendetailIdKode"))
	    	    	    			{
	    	    	    				datadetail.setAgendetailIdKode(cityarray.getJSONObject(k).getInt("agendetailIdKode"));
	    	    	    			}
	    	    	    			
	    	    	    			datasagendetails.add(datadetail);
	    	    	    			JSONArray detailBaru = cityarray.getJSONObject(k).getJSONArray("detailBaru");
	    	    	    			if(detailBaru.size()>0)
	    	    	    			{
	    	    	    				for (int l1 = 0; l1 < detailBaru.size(); l1++) {
	    	    	    					JSONArray coverAreaArray = detailBaru.getJSONObject(l1).getJSONArray("coverArea");
			    	    	    			if(coverAreaArray.size()>0)
			    	    	    			{
			    	    	    				for (int l = 0; l < coverAreaArray.size(); l++) {
			    	    	    					MagenCoverArea dataCoverArea = new MagenCoverArea();
				    	    	    				dataCoverArea.setmAgenId(id);
				    	    	    				dataCoverArea.setMAagendetailId(cityarray.getJSONObject(k).getInt("agen_detail_id"));
				    	    	    				dataCoverArea.setAgenCoverAreaId(coverAreaArray.getJSONObject(l).getInt("agenDetailCoverAreaId"));
				    	    	    				dataCoverArea.setmProvinceId(coverAreaArray.getJSONObject(l).getInt("mProvinceId"));
				    	    	    				dataCoverArea.setmKotaId(coverAreaArray.getJSONObject(l).getInt("mKotaId"));
				    	    	    				dataCoverArea.setAgenCoverAreaStatus(coverAreaArray.getJSONObject(l).getBoolean("statusCoverArea"));
				    	    	    				dataCoverArea.setAgenCoverAreaCompany_id(dataRequest.getString("agenCompanyId"));
				    	    	    				dataCoverAreaHeader.add(dataCoverArea);
												}
			    	    	    				
			    	    	    			}
	    	    	    				}
	    	    	    			}
	    	    	    		}
	    	    	    	}
	    	    		}
	    	    	}
	    		}
	    	}
	    	allAgenService.saveallAgen(data, datasagendetails, dataCoverAreaHeader);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Agen success");
		    response.put("responseData", datasagendetails);
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Agen failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateAgen")
	public String updateAgen(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	ArrayList<MAgendetail> datasagendetails = new ArrayList<>();
	    	ArrayList<MagenCoverArea> dataCoverAreaHeader = new ArrayList<>();
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	Optional <MAgen> dataUpdateAgen = mAgenService.getDetail(dataRequest.getInt("id"), dataRequest.getString("agenCompanyId"));
	    	if(dataUpdateAgen.isPresent())
	    	{
	    		((MAgen)dataUpdateAgen.get()).setAgenName(dataRequest.getString("agent_id"));
	    		((MAgen)dataUpdateAgen.get()).setAgenUpdatedBy(user_uuid);
	    		((MAgen)dataUpdateAgen.get()).setAgenUpdatedAt(localDateTime2);
	    		((MAgen)dataUpdateAgen.get()).setAgenStatus(dataRequest.getBoolean("status"));
	    		((MAgen)dataUpdateAgen.get()).setAgenType(dataRequest.getInt("agen_type"));
		    	JSONArray coverareaArray = dataRequest.getJSONArray("cover_area");
		    	if(coverareaArray.size() > 0)
		    	{
	    			List<Object[]> detail = mAgendetailService.getdetailall(dataRequest.getInt("id"), dataRequest.getString("agenCompanyId"));
	    			detail.stream().forEach(column -> {
		    			Optional<MAgendetail> dataUpdateagendetail = mAgendetailService.getdetail(Integer.parseInt(column[0].toString()), Integer.parseInt(column[1].toString()), dataRequest.getString("agenCompanyId"));
		    			if(dataUpdateagendetail.isPresent())
		    			{
		    				((MAgendetail)dataUpdateagendetail.get()).setAgendetailStatus(false);
		    			}
	    			});
	    			
	    			List<Object[]> detailCover = MAgenCoverAreaService.getAll(dataRequest.getInt("id"), dataRequest.getString("agenCompanyId"));
	    			
	    			detailCover.stream().forEach(col->{
	    				Optional<MagenCoverArea> dataAgen = MAgenCoverAreaService.getDetail(Integer.parseInt(col[0].toString()), Integer.parseInt(col[1].toString()), Integer.parseInt(col[2].toString()), col[4].toString());
	    				if(dataAgen.isPresent())
	    				{
	    					((MagenCoverArea)dataAgen.get()).setAgenCoverAreaStatus(false);
	    				}
	    			});
	    			
	    			
		    		for(int i = 0; i < coverareaArray.size(); i++)
		    		{
		    			JSONArray transferpointarray = coverareaArray.getJSONObject(i).getJSONArray("transfer_point_array");
		    	    	if(transferpointarray.size() > 0)
		    	    	{
		    	    		for(int j = 0; j < transferpointarray.size(); j++)
		    	    		{
		    	    			JSONArray cityarray = transferpointarray.getJSONObject(j).getJSONArray("city_array");
		    	    	    	if(cityarray.size() > 0)
		    	    	    	{
		    	    	    		for(int k = 0; k < cityarray.size(); k++)
		    	    	    		{	
		    			    			MAgendetail datadetail = new MAgendetail();
		    	    	    			datadetail.setmAgenId(dataRequest.getInt("id"));
		    	    	    			datadetail.setAgendetailId(cityarray.getJSONObject(k).getInt("agen_detail_id"));
		    	    	    			datadetail.setmProvinceId(coverareaArray.getJSONObject(i).getInt("provinsi_name_id"));
		    	    	    			datadetail.setmTransferpointId(transferpointarray.getJSONObject(j).getInt("transer_point_id"));
		    	    	    			datadetail.setmKotaId(cityarray.getJSONObject(k).getInt("city_id"));
		    	    	    			datadetail.setAgendetailTask(cityarray.getJSONObject(k).getString("type"));
		    	    	    			datadetail.setAgendetailCreatedBy(user_uuid);
		    	    	    			datadetail.setAgendetailCreatedAt(localDateTime2);
		    	    	    			datadetail.setAgendetailStatus(cityarray.getJSONObject(k).getBoolean("status_detail"));
		    	    	    			datadetail.setAgendetailCompanyId(cityarray.getJSONObject(k).getString("agendetailCompanyId"));
		    	    	    			datadetail.setKode(cityarray.getJSONObject(k).getString("kode"));
		    	    	    			datadetail.setTypeKode(cityarray.getJSONObject(k).getInt("typeKode"));
		    	    	    			if(cityarray.getJSONObject(k).has("mCabangIdKode"))
		    	    	    			{
		    	    	    				datadetail.setmCabangIdKode(cityarray.getJSONObject(k).getInt("mCabangIdKode"));
		    	    	    			}
		    	    	    			
		    	    	    			if(cityarray.getJSONObject(k).has("mAgenIdKode"))
		    	    	    			{
		    	    	    				datadetail.setmAgenIdKode(cityarray.getJSONObject(k).getInt("mAgenIdKode"));
		    	    	    			}
		    	    	    			
		    	    	    			if(cityarray.getJSONObject(k).has("agendetailIdKode"))
		    	    	    			{
		    	    	    				datadetail.setAgendetailIdKode(cityarray.getJSONObject(k).getInt("agendetailIdKode"));
		    	    	    			}
		    	    	    			datasagendetails.add(datadetail);
		    	    	    			JSONArray detailBaru = cityarray.getJSONObject(k).getJSONArray("detailBaru");
		    	    	    			if(detailBaru.size()>0)
		    	    	    			{
		    	    	    				for (int l1 = 0; l1 < detailBaru.size(); l1++) {
		    	    	    					JSONArray coverAreaArray = detailBaru.getJSONObject(l1).getJSONArray("coverArea");
				    	    	    			if(coverAreaArray.size()>0)
				    	    	    			{
				    	    	    				for (int l = 0; l < coverAreaArray.size(); l++) {
				    	    	    					MagenCoverArea dataCoverArea = new MagenCoverArea();
					    	    	    				dataCoverArea.setmAgenId(dataRequest.getInt("id"));
					    	    	    				dataCoverArea.setMAagendetailId(cityarray.getJSONObject(k).getInt("agen_detail_id"));
					    	    	    				dataCoverArea.setAgenCoverAreaId(coverAreaArray.getJSONObject(l).getInt("agenDetailCoverAreaId"));
					    	    	    				dataCoverArea.setmProvinceId(coverAreaArray.getJSONObject(l).getInt("mProvinceId"));
					    	    	    				dataCoverArea.setmKotaId(coverAreaArray.getJSONObject(l).getInt("mKotaId"));
					    	    	    				dataCoverArea.setAgenCoverAreaStatus(coverAreaArray.getJSONObject(l).getBoolean("statusCoverArea"));
					    	    	    				dataCoverArea.setAgenCoverAreaCompany_id(dataRequest.getString("agenCompanyId"));
					    	    	    				dataCoverAreaHeader.add(dataCoverArea);
													}
				    	    	    				
				    	    	    			}
		    	    	    				}
		    	    	    			}
		    	    	    			
		    	    	    		}
		    	    	    	}
		    	    		}
		    	    	}
		    		}
		    	}
	    	}
	    	allAgenService.saveallAgen(dataUpdateAgen.get(), datasagendetails, dataCoverAreaHeader);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Agen success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Update Agen failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
