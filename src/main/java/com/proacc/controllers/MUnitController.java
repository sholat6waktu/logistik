package com.proacc.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.service.MUnitService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MUnitController {
	private static final Logger logger = LoggerFactory.getLogger(MUnitController.class);
	
	@Autowired
	MUnitService mUnitService;
	
	@PostMapping("/getAllUnit/{paramAktif}")
	public String getAllUnit(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> Units = mUnitService.getallUnit(paramAktif.toString().toLowerCase(), dataRequest.getString("unitCompanyId"));
			Units.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("unitId", column[0]);
				data.put("unitName", column[1] == null ? "" : column[1].toString());
				data.put("unitDescription", column[2] == null ? "" : column[2].toString());
				data.put("unitCreatedBy", column[3].toString());
				data.put("unitCreatedAt", column[4].toString());
				data.put("unitUpdatedBy", column[5] == null ? "" : column[5].toString());
				data.put("unitUpdatedAt", column[6] == null ? "" : column[6].toString());
				data.put("unitStatus", column[7].toString());
				data.put("unitCompanyId", column[8]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data vehicletype berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data vehicletype gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString(); 
	}
	@PostMapping("/getUnitDetail/{paramAktif}")
	public String getUnitDetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> Units = mUnitService.getUnit(paramAktif.toString().toLowerCase(), dataRequest.getInt("unitId"), dataRequest.getString("unitCompanyId"));
			Units.stream().forEach(column -> {
				JSONObject data = new JSONObject();
				data.put("unitId", column[0]);
				data.put("unitName", column[1] == null ? "" : column[1].toString());
				data.put("unitDescription", column[2] == null ? "" : column[2].toString());
				data.put("unitCreatedBy", column[3].toString());
				data.put("unitCreatedAt", column[4].toString());
				data.put("unitUpdatedBy", column[5] == null ? "" : column[5].toString());
				data.put("unitUpdatedAt", column[6] == null ? "" : column[6].toString());
				data.put("unitStatus", column[7].toString());
				data.put("unitCompanyId", column[8]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data unit berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data unit gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
