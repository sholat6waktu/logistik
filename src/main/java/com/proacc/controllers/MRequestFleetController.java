package com.proacc.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.entity.MRequestFleet;
import com.proacc.entity.MRequestFleetDetail;
import com.proacc.entity.MRouteJalur;
import com.proacc.entity.MRouteJalurDetail;
import com.proacc.entity.TrxNumber;
import com.proacc.helper.Helper;
import com.proacc.service.AllRequestFleetService;
import com.proacc.service.AllRouteService;
import com.proacc.service.MRequestFleetDetailService;
import com.proacc.service.MRequestFleetService;
import com.proacc.service.MRouteJalurDetailService;
import com.proacc.service.MRouteJalurService;
import com.proacc.service.MRouteService;
import com.proacc.service.TrxNumberService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MRequestFleetController {
	
	@Autowired
	MRequestFleetService MRequestFleetService;
	@Autowired
	TrxNumberService trxNumberService;
	@Autowired
	AllRequestFleetService AllRequestFleetService;
	@Autowired
	MRequestFleetDetailService MRequestFleetDetailService;

	private static final Logger logger = LoggerFactory.getLogger(MRequestFleetController.class);
	
	@PostMapping("/detailRequestFleet/{paramAktif}")
	public String detailRequestFleet(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
//		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
//		RestTemplate restTemplate = new RestTemplate();
//	    HttpHeaders headers = new HttpHeaders();
//	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//	    headers.set("x-access-code", header);
//	    HttpEntity<String> entity = new HttpEntity<String>(headers);
//	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
//	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT #### " + dataRequest.toString());
		logger.info("API #### listRequestFleet ()");
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> detailRequest = MRequestFleetService.detailRequest(paramAktif.toLowerCase(), dataRequest.getString("statusDetail").toLowerCase(), user_uuid, dataRequest.getString("companyId"), dataRequest.getInt("id"));
			detailRequest.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("id", col[0]);
				data.put("detailId", col[1]);
				data.put("no", col[2] == null ? "" : col[2].toString());
				data.put("dateRequest", col[3] == null ? "" : col[3].toString());
				data.put("cabangIdFrom", col[4] == null ? "" : col[4]);
				data.put("cabangNameFrom", col[5] == null ? "" : col[5]);
				data.put("cabangProvinsiIdFrom", col[6] == null ? "" : col[6]);
				data.put("cabangKotaIdFrom", col[7] == null ? "" : col[7]);
				data.put("agenIdFrom", col[8] == null ? "" : col[8]);
				data.put("agenDetailIdFrom", col[9] == null ? "" : col[9]);
				data.put("agenNameFrom", col[10] == null ? "" : col[10]);
				data.put("agenProvinsiId", col[11] == null ? "" : col[11]);
				data.put("agenKotaId", col[12] == null ? "" : col[12]);
				data.put("cabangIdTo", col[13] == null ? "" : col[13]);
				data.put("cabangNameTo", col[14] == null ? "" : col[14]);
				data.put("cabangProvinsiIdTo", col[15] == null ? "" : col[15]);
				data.put("cabangKotaIdTo", col[16] == null ? "" : col[16]);
				data.put("agenIdTo", col[17] == null ? "" : col[17]);
				data.put("agenDetailIdTo", col[18] == null ? "" : col[18]);
				data.put("agenNameFrom", col[19] == null ? "" : col[19]);
				data.put("agenProvinsiId", col[20] == null ? "" : col[20]);
				data.put("agenKotaId", col[21] == null ? "" : col[21]);
				data.put("statusHeader", col[22] == null ? "" : col[22]);
				data.put("statusDetail", col[23] == null ? "" : col[23]);
				data.put("actor", col[24] == null ? "" : col[24]);
				data.put("orderId", col[25] == null ? "" : col[25]);
				data.put("orderDate", col[26] == null ? "" : col[26].toString());
				data.put("customerIdFrom", col[27] == null ? "" : col[27]);
				data.put("customerIdTo", col[28] == null ? "" : col[28]);
				data.put("koli", col[29] == null ? "" : col[29]);
				data.put("mWeightId", col[30] == null ? "" : col[30]);
				data.put("weightTotal", col[31] == null ? "" : col[31]);
				data.put("weightName", col[32] == null ? "" : col[32]);
				data.put("mVolumeId", col[33] == null ? "" : col[33]);
				data.put("volumeTotal", col[34] == null ? "" : col[34]);
				data.put("volumeName", col[35] == null ? "" : col[35]);
				datas.add(data);
			});
			
			
			
			response.put("responseCode", "00");
		    response.put("responseDesc", "Detail Request Fleet Success");
		    response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Detail Request Fleet Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/listRequestFleet/{paramAktif}")
	public String listRequestFleet(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
//		final String url3 = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
//		RestTemplate restTemplate = new RestTemplate();
//	    HttpHeaders headers = new HttpHeaders();
//	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//	    headers.set("x-access-code", header);
//	    HttpEntity<String> entity = new HttpEntity<String>(headers);
//	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url3, HttpMethod.GET, entity, String.class);
//	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
//	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
	    
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT #### " + dataRequest.toString());
		logger.info("API #### listRequestFleet ()");
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> listRequest = MRequestFleetService.listRequest(paramAktif.toLowerCase(), user_uuid, dataRequest.getString("companyId"));
			listRequest.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("id", col[0]);
				data.put("no", col[1] == null ? "" : col[1].toString());
				data.put("dateRequest", col[2] == null ? "" : col[2].toString());
				data.put("cabangIdFrom", col[3] == null ? "" : col[3]);
				data.put("cabangNameFrom", col[4] == null ? "" : col[4]);
				data.put("cabangProvinsiIdFrom", col[5] == null ? "" : col[5]);
				data.put("cabangKotaIdFrom", col[6] == null ? "" : col[6]);
				data.put("agenIdFrom", col[7] == null ? "" : col[7]);
				data.put("agenDetailIdFrom", col[8] == null ? "" : col[8]);
				data.put("agenNameFrom", col[9] == null ? "" : col[9]);
				data.put("agenProvinsiId", col[10] == null ? "" : col[10]);
				data.put("agenKotaId", col[11] == null ? "" : col[11]);
				data.put("cabangIdTo", col[12] == null ? "" : col[12]);
				data.put("cabangNameTo", col[13] == null ? "" : col[13]);
				data.put("cabangProvinsiIdTo", col[14] == null ? "" : col[14]);
				data.put("cabangKotaIdTo", col[15] == null ? "" : col[15]);
				data.put("agenIdTo", col[16] == null ? "" : col[16]);
				data.put("agenDetailIdTo", col[17] == null ? "" : col[17]);
				data.put("agenNameFrom", col[18] == null ? "" : col[18]);
				data.put("agenProvinsiId", col[19] == null ? "" : col[19]);
				data.put("agenKotaId", col[20] == null ? "" : col[20]);
				data.put("statusHeader", col[21] == null ? "" : col[21]);
				data.put("actor", col[22] == null ? "" : col[22]);
				data.put("totalManifest", col[23] == null ? "" : col[23]);
				
				datas.add(data);
			});
			
			response.put("responseCode", "00");
		    response.put("responseDesc", "List Request Fleet Success");
		    response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "List Request Fleet Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/saveRequestFleet")
	public String saveRequestFleet(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT #### " + dataRequest.toString());
		logger.info("API #### saveRequestFleet()");
		try
		{
			ArrayList<MRequestFleetDetail> dataDetailRequest = new ArrayList<>();
	    	
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	
	    	int id = 0;
	    	List<Object[]> dataNext = MRequestFleetService.nextvalRequestFleet();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	
	    	Optional <TrxNumber> dataUpdateNumber = trxNumberService.getdetail(3, dataRequest.getString("companyId"));
	    	
	    	MRequestFleet data = new MRequestFleet();
	    	data.setId(id);
	    	List<Object[]> numbers = trxNumberService.getNumber("a", 3,  dataRequest.getString("companyId"));
	    	((TrxNumber)dataUpdateNumber.get()).setTrxNumberRunningnumber(dataUpdateNumber.get().getTrxNumberRunningnumber() + 1);
	    	
	    	data.setNo(numbers.get(0)[6].toString());
	    	if(dataRequest.has("cabangIdFrom"))
	    	{
	    		data.setmCabangId(dataRequest.getInt("cabangIdFrom"));
	    	}
	    	if(dataRequest.has("agenIdFrom"))
	    	{
	    		data.setmAgenId(dataRequest.getInt("agenIdFrom"));
	    	}
	    	if(dataRequest.has("agenDetailIdFrom"))
	    	{
	    		data.setmAgendetailId(dataRequest.getInt("agenDetailIdFrom"));
	    	}
	    	
	    	if(dataRequest.has("cabangIdTo"))
	    	{
	    		data.setmCabangIdTo(dataRequest.getInt("cabangIdTo"));
	    	}
	    	if(dataRequest.has("agenIdTo"))
	    	{
	    		data.setmAgenIdTo(dataRequest.getInt("agenIdTo"));
	    	}
	    	if(dataRequest.has("agenDetailIdTo"))
	    	{
	    		data.setmAgendetailIdTo(dataRequest.getInt("agenDetailIdTo"));
	    	}
	    	data.setDateRequest(LocalDate.parse(dataRequest.getString("dateRequest")));
	    	data.setCompanyId(dataRequest.getString("companyId"));
	    	data.setStatus(dataRequest.getBoolean("status"));
	    	
	    	JSONArray requestDetail = dataRequest.getJSONArray("detail");
	    	if(requestDetail.size() > 0)
	    	{
	    		for (int j = 0; j < requestDetail.size(); j++) {
					MRequestFleetDetail dataDetail = new MRequestFleetDetail();
					dataDetail.setId(id);
					dataDetail.setDetailId(requestDetail.getJSONObject(j).getInt("detailId"));
					dataDetail.setOrderId(requestDetail.getJSONObject(j).getString("orderId"));
					dataDetail.setCompanyId(dataRequest.getString("companyId"));
					dataDetail.setStatus(requestDetail.getJSONObject(j).getBoolean("status"));
					dataDetailRequest.add(dataDetail);
				}
	    	}
	    	AllRequestFleetService.saveAll(data, dataDetailRequest);
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Request Fleet Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Save Request Fleet Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/updateRequestFleet")
	public String updateRequestFleet(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		logger.info("INPUT #### " + dataRequest.toString());
		logger.info("API #### updateRequestFleet()");
		try
		{
			ArrayList<MRequestFleetDetail> dataDetailRequest = new ArrayList<>();
	    	
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	
	    	List<Object[]> getAll = MRequestFleetDetailService.getAll(dataRequest.getInt("id"), dataRequest.getString("companyId"));
	    	getAll.stream().forEach(column -> {
    			Optional<MRequestFleetDetail> dataJalurUpdate = MRequestFleetDetailService.getdetailById(Integer.parseInt(column[0].toString()), Integer.parseInt(column[1].toString()), dataRequest.getString("companyId"));
    			if(dataJalurUpdate.isPresent())
    			{
    				((MRequestFleetDetail)dataJalurUpdate.get()).setStatus(false);
    			}
			});
	    	
	    	Optional<MRequestFleet> data = MRequestFleetService.getDetail(dataRequest.getInt("id"), dataRequest.getString("companyId"));
	    	
	    	if(dataRequest.has("cabangIdFrom"))
	    	{
	    		((MRequestFleet)data.get()).setmCabangId(dataRequest.getInt("cabangIdFrom"));
	    	}
	    	if(dataRequest.has("agenIdFrom"))
	    	{
	    		((MRequestFleet)data.get()).setmAgenId(dataRequest.getInt("agenIdFrom"));
	    	}
	    	if(dataRequest.has("agenDetailIdFrom"))
	    	{
	    		((MRequestFleet)data.get()).setmAgendetailId(dataRequest.getInt("agenDetailIdFrom"));
	    	}
	    	
	    	if(dataRequest.has("cabangIdTo"))
	    	{
	    		((MRequestFleet)data.get()).setmCabangIdTo(dataRequest.getInt("cabangIdTo"));
	    	}
	    	if(dataRequest.has("agenIdTo"))
	    	{
	    		((MRequestFleet)data.get()).setmAgenIdTo(dataRequest.getInt("agenIdTo"));
	    	}
	    	if(dataRequest.has("agenDetailIdTo"))
	    	{
	    		((MRequestFleet)data.get()).setmAgendetailIdTo(dataRequest.getInt("agenDetailIdTo"));
	    	}
	    	((MRequestFleet)data.get()).setDateRequest(LocalDate.parse(dataRequest.getString("dateRequest")));
	    	((MRequestFleet)data.get()).setStatus(dataRequest.getBoolean("status"));
	    	JSONArray requestDetail = dataRequest.getJSONArray("detail");
	    	if(requestDetail.size() > 0)
	    	{
	    		for (int j = 0; j < requestDetail.size(); j++) {
					MRequestFleetDetail dataDetail = new MRequestFleetDetail();
					dataDetail.setId(dataRequest.getInt("id"));
					dataDetail.setDetailId(requestDetail.getJSONObject(j).getInt("detailId"));
					dataDetail.setOrderId(requestDetail.getJSONObject(j).getString("orderId"));
					dataDetail.setCompanyId(dataRequest.getString("companyId"));
					dataDetail.setStatus(requestDetail.getJSONObject(j).getBoolean("status"));
					dataDetailRequest.add(dataDetail);
				}
	    	}
	    	
	    	AllRequestFleetService.saveAll(data.get(), dataDetailRequest);
	    	
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Request Fleet Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Update Request Fleet Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
}
