package com.proacc.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.entity.MDriver;
import com.proacc.helper.Helper;
import com.proacc.service.MDriverService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MDriverController {
	private static final Logger logger = LoggerFactory.getLogger(MDriverController.class);
	
	@Autowired
	MDriverService mDriverService;
	@PostMapping("/getAllDriver/{paramAktif}")
	public String getAllDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			logger.info("user_uuid = " + user_uuid.toString());
			List<Object[]> drivers = mDriverService.getallDriver(paramAktif.toString().toLowerCase(), LocalDate.parse(dataRequest.getString("driverDate")), dataRequest.getString("driverCompanyId"), user_uuid);
			drivers.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				data.put("driverId", column[0] == null ? "" : column[0]);
				data.put("fleetId", column[1] == null ? "" : column[1]);
				data.put("fleetName", column[2] == null ? "" : column[2].toString());
				data.put("fleetVehicle", column[3] == null ? "" : column[3].toString());
				data.put("driverDate", column[4]);
				data.put("time", column[5] == null ? "" : column[5].toString());
				data.put("timeFrom", column[6] == null ? "" : column[6]);
				data.put("timeTo", column[7] == null ? "" : column[7]);
				data.put("driver", column[8] == null ? "" : column[8].toString());
				data.put("helper", column[9] == null ? "" : column[9].toString());
				data.put("symbol", column[10] == null ? "" : column[10]);
				data.put("cabangId", column[11] == null ? "" : column[11]);
				data.put("cabangName", column[12] == null ? "" : column[12]);
				data.put("mProvinceIdCabang", column[13] == null ? "" : column[13]);
				data.put("mKotaIdCabang", column[14] == null ? "" : column[14]);
				data.put("agenId", column[15] == null ? "" : column[15]);
				data.put("agenDetailId", column[16] == null ? "" : column[16]);
				data.put("mProvinceIdAgen", column[17] == null ? "" : column[17]);
				data.put("mKotaIdAgen", column[18] == null ? "" : column[18]);
				data.put("agenName", column[19] == null ? "" : column[19]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data driver berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data driver gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getDriver/{paramAktif}")
	public String getDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> drivers = mDriverService.getDriver(paramAktif.toString().toLowerCase(), LocalDate.parse(dataRequest.getString("driverDate")), dataRequest.getInt("driverId"), dataRequest.getString("driverCompanyId"));
			drivers.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				data.put("driverId", column[0]);
				data.put("fleetName", column[1] == null ? "" : column[1].toString());
				data.put("fleetVehicle", column[2] == null ? "" : column[2].toString());
				data.put("driverDate", column[3]);
				data.put("timeFrom", column[4] == null ? "" : column[4]);
				data.put("timeTo", column[5] == null ? "" : column[5]);
				data.put("driver", column[6] == null ? "" : column[6].toString());
				data.put("helper", column[7] == null ? "" : column[7].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data driver berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data driver gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/saveDriver")
	public String saveDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	int id = 0;
	    	List<Object[]> dataNext = mDriverService.nextvalMDriver();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	MDriver data = new MDriver();
	    	data.setDriverId(id);
	    	data.setmFleetId(dataRequest.getInt("mFleetId"));
	    	data.setDriverDate(LocalDate.parse(dataRequest.getString("driverDate")));
	    	data.setDriverCreatedBy(user_uuid);
	    	data.setDriverCreatedAt(localDateTime2);
	    	data.setDriverStatus(dataRequest.getBoolean("driverStatus"));
	    	data.setDriverCompanyId(dataRequest.getString("driverCompanyId"));
	    	if(dataRequest.has("cabangId"))
	    	{
	    		data.setPositionCabangId(dataRequest.getInt("cabangId"));
	    	}
	    	
	    	if(dataRequest.has("agenId"))
	    	{
	    		data.setPositionAgenId(dataRequest.getInt("agenId"));
	    	}
	    	
	    	if(dataRequest.has("agenDetailId"))
	    	{
	    		data.setPositionAgenDetailId(dataRequest.getInt("agenDetailId"));
	    	}
	    	
	    	mDriverService.saveDriver(data);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save driver success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save driver failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateDriver")
	public String updateDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	MDriver data = new MDriver();
	    	data.setDriverId(dataRequest.getInt("driverId"));
	    	if(dataRequest.has("mFleetId"))
	    	{
		    	data.setmFleetId(dataRequest.getInt("mFleetId"));	
	    	}
	    	if(dataRequest.has("mContactDriver"))
	    	{
		    	data.setmContactDriver(UUID.fromString(dataRequest.getString("mContactDriver")));
		    	data.setDriverName(dataRequest.getString("mContactDriver"));
	    	}
	    	if(dataRequest.has("mContactHelper"))
	    	{
		    	data.setmContactHelper(UUID.fromString(dataRequest.getString("mContactHelper")));
		    	data.setDriverHelper(dataRequest.getString("mContactHelper"));
	    	}
	    	if(dataRequest.has("driverDate"))
	    	{
		    	data.setDriverDate(LocalDate.parse(dataRequest.getString("driverDate")));	
	    	}
	    	if(dataRequest.has("timeFrom"))
	    	{
		    	data.setDriverTimeFrom(LocalTime.parse(dataRequest.getString("timeFrom")));	
	    	}
	    	if(dataRequest.has("timeTo"))
	    	{
		    	data.setDriverTimeTo(LocalTime.parse(dataRequest.getString("timeTo")));	
	    	}
	    	data.setDriverUpdatedBy(user_uuid);
	    	data.setDriverUpdatedAt(localDateTime2);
	    	data.setDriverCreatedBy(user_uuid);
	    	data.setDriverCreatedAt(localDateTime2);
	    	if(dataRequest.has("driverStatus"))
	    	{
		    	data.setDriverStatus(dataRequest.getBoolean("driverStatus"));
	    	}
	    	if(dataRequest.has("driverCompanyId"))
	    	{
		    	data.setDriverCompanyId(dataRequest.getString("driverCompanyId"));
	    	}
	    	
	    	if(dataRequest.has("cabangId"))
	    	{
	    		data.setPositionCabangId(dataRequest.getInt("cabangId"));
	    	}
	    	
	    	if(dataRequest.has("agenId"))
	    	{
	    		data.setPositionAgenId(dataRequest.getInt("agenId"));
	    	}
	    	
	    	if(dataRequest.has("agenDetailId"))
	    	{
	    		data.setPositionAgenDetailId(dataRequest.getInt("agenDetailId"));
	    	}
	    	
	    	mDriverService.saveDriver(data);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save driver success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save driver failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/savingDriver")
	public String savingDriver(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	int id = 0;
	    	List<Object[]> dataNext = mDriverService.nextvalMDriver();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	MDriver data = new MDriver();
	    	data.setDriverId(id);
	    	if(dataRequest.has("mFleetId"))
	    	{
		    	data.setmFleetId(dataRequest.getInt("mFleetId"));	
	    	}
	    	if(dataRequest.has("mContactDriver"))
	    	{
		    	data.setmContactDriver(UUID.fromString(dataRequest.getString("mContactDriver")));	
	    	}
	    	if(dataRequest.has("mContactHelper"))
	    	{
		    	data.setmContactHelper(UUID.fromString(dataRequest.getString("mContactHelper")));	
	    	}
	    	if(dataRequest.has("driverDate"))
	    	{
		    	data.setDriverDate(LocalDate.parse(dataRequest.getString("driverDate")));	
	    	}
	    	if(dataRequest.has("timeFrom"))
	    	{
		    	data.setDriverTimeFrom(LocalTime.parse(dataRequest.getString("timeFrom")));	
	    	}
	    	if(dataRequest.has("timeTo"))
	    	{
		    	data.setDriverTimeTo(LocalTime.parse(dataRequest.getString("timeTo")));	
	    	}
//	    	data.setDriverUpdatedBy(user_uuid);
//	    	data.setDriverUpdatedAt(localDateTime2);
	    	data.setDriverCreatedBy(user_uuid);
	    	data.setDriverCreatedAt(localDateTime2);
	    	if(dataRequest.has("driverStatus"))
	    	{
		    	data.setDriverStatus(dataRequest.getBoolean("driverStatus"));
	    	}
	    	if(dataRequest.has("driverCompanyId"))
	    	{
		    	data.setDriverCompanyId(dataRequest.getString("driverCompanyId"));
	    	}
	    	
	    	if(dataRequest.has("cabangId"))
	    	{
	    		data.setPositionCabangId(dataRequest.getInt("cabangId"));
	    	}
	    	
	    	if(dataRequest.has("agenId"))
	    	{
	    		data.setPositionAgenId(dataRequest.getInt("agenId"));
	    	}
	    	
	    	if(dataRequest.has("agenDetailId"))
	    	{
	    		data.setPositionAgenDetailId(dataRequest.getInt("agenDetailId"));
	    	}
	    	
	    	data.setStatusCondition(0);
	    	
	    	
	    	
	    	mDriverService.saveDriver(data);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save driver success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save driver failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
