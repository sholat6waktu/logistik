package com.proacc.controllers;

import java.math.BigInteger;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.MCabang;
import com.proacc.entity.TrxDriverTracking;
import com.proacc.entity.TrxDriverschedule;
import com.proacc.entity.TrxManifest;
import com.proacc.entity.TrxNumber;
import com.proacc.entity.TrxOrder;
import com.proacc.entity.TrxOutbound;
import com.proacc.entity.TrxTracking;
import com.proacc.helper.Helper;
import com.proacc.service.MDriverService;
import com.proacc.service.MVolumeService;
import com.proacc.service.MWeightService;
import com.proacc.service.TrxDriverTrackingService;
import com.proacc.service.TrxDriverscheduleService;
import com.proacc.service.TrxJalurDriverService;
import com.proacc.service.TrxManifestService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxTrackingService;
import com.proacc.service.TrxNumberService;
import com.proacc.service.TrxOutboundService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TrxDriverscheduleController {
	private static final Logger logger = LoggerFactory.getLogger(TrxDriverscheduleController.class);
	
	@Autowired
	TrxOrderService trxOrderService;
	@Autowired
	TrxTrackingService trxTrackingService;
	@Autowired
	MDriverService mDriverService;
	@Autowired
	TrxDriverscheduleService trxDriverscheduleService;
	@Autowired
	MWeightService mWeightService;
	@Autowired
	MVolumeService mVolumeService;
	@Autowired
	TrxDriverTrackingService TrxDriverTrackingService;
	@Autowired
	TrxJalurDriverService TrxJalurDriverService;
	@Autowired
	TrxManifestService TrxManifestService;
	@Autowired
	TrxOrderService TrxOrderService;
	
	@Autowired
	TrxNumberService TrxNumberService;
	
	@Autowired
	TrxOutboundService TrxOutboundService;
	
	
	@PostMapping("/listTracking/{paramAktif}")
	public String listTracking(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> drivers = trxDriverscheduleService.getalldriverTracking(paramAktif.toString().toLowerCase(), LocalDate.parse(dataRequest.getString("driverDate")), dataRequest.getString("driverCompanyId"), user_uuid);
			drivers.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				List<JSONObject> datasdriverschedule = new ArrayList<>();
				data.put("driverId", column[0] == null ? "" : column[0]);
				data.put("fleetId", column[1] == null ? "" : column[1]);
				data.put("fleetName", column[2] == null ? "" : column[2].toString());
				data.put("fleetVehicle", column[3] == null ? "" : column[3].toString());
				data.put("driverDate", column[4]);
				data.put("time", column[5] == null ? "" : column[5].toString());
				data.put("timeFrom", column[6] == null ? "" : column[6]);
				data.put("timeTo", column[7] == null ? "" : column[7]);
				data.put("driver", column[8] == null ? "" : column[8].toString());
				data.put("helper", column[9] == null ? "" : column[9].toString());
				data.put("capacityBerat", column[10] == null || column[11] == null ? "" : column[10].toString() + " " + column[11].toString());
				data.put("capacityVolume", column[12] == null || column[13] == null ? "" : column[12].toString() + " " + column[13].toString());
				List<Object[]> availableberat = trxDriverscheduleService.availablecapacityberat(Integer.parseInt(column[0].toString()), "0", dataRequest.getString("driverCompanyId"));
				data.put("availableberat", availableberat.get(0)[0] == null ? "" : availableberat.get(0)[0].toString() + " kg");
				List<Object[]> availablevolume = trxDriverscheduleService.availablecapacityvolume(Integer.parseInt(column[0].toString()), "0", dataRequest.getString("driverCompanyId"));
				data.put("availablevolume", availablevolume.get(0)[0] == null ? "" : availablevolume.get(0)[0].toString() + " m3");
				data.put("trxJalurDriverHeaderId", column[14] == null ? "" : column[14]);
				data.put("mRouteId", column[15]  == null ? "" : column[15]);
				data.put("mRouteName", column[16] == null ? "" : column[16]);
				data.put("actor", column[20] == null ? "" : column[20]);

				data.put("type", column[17] == null ? "" : column[17]);
				
				if(column[15] == null)
				{
					List<Object[]> detailRoute = TrxDriverTrackingService.findByDriver(Integer.parseInt(column[0].toString()), dataRequest.getString("driverCompanyId"));
					if(detailRoute.size()>0)
					{
						detailRoute.stream().forEach(col->{
							JSONObject data2 = new JSONObject();
							data2.put("customerFromId", col[0] == null ? "" : col[0]);
							data2.put("cabangName", col[1] == null ? "" : col[1]);
							data2.put("cabangId", col[2] == null ? "" : col[2]);
							data2.put("cabangProvinceId", col[3] == null ? "" : col[3]);
							data2.put("cabangKotaId", col[4] == null ? "" : col[4]);
							data2.put("agenName", col[5] == null ? "" : col[5]);
							data2.put("agenId", col[6] == null ? "" : col[6]);
							data2.put("agenDetailId", col[7] == null ? "" : col[7]);
							data2.put("agenProvinceId", col[8] == null ? "" : col[8]);
							data2.put("agenKotaId", col[9] == null ? "" : col[9]);
							data2.put("companyId", col[10] == null ? "" : col[10]);
							data2.put("status", col[11] == null ? "" : col[11]);
							data2.put("dateTracking", col[12] == null ? "" : col[12].toString());
							data2.put("dateKantor", col[13] == null ? "" : col[13].toString());
							data2.put("orderId", col[14] == null ? "" : col[14].toString());
							data2.put("id", col[15] == null ? "" : col[15]);
							datasdriverschedule.add(data2);
						});
					}
				}
				else
				{
					List<Object[]> detailRoute = TrxJalurDriverService.findByJalurHeaderId(Integer.parseInt(column[14].toString()), dataRequest.getString("driverCompanyId"));
					if(detailRoute.size()>0)
					{
						detailRoute.stream().forEach(col->{
							JSONObject data2 = new JSONObject();
							data2.put("cabangName", col[0] == null ? "" : col[0]);
							data2.put("cabangProvinceId", col[1] == null ? "" : col[1]);
							data2.put("cabangKotaId", col[2] == null ? "" : col[2]);
							data2.put("agenName", col[3] == null ? "" : col[3]);
							data2.put("agenProvinceId", col[4] == null ? "" : col[4]);
							data2.put("agenKotaId", col[5] == null ? "" : col[5]);
							data2.put("status", col[6] == null ? "" : col[6]);
							data2.put("dateTracking", col[7] == null ? "" : col[7].toString());
							data2.put("dateKantor", col[8] == null ? "" : col[8].toString());
							datasdriverschedule.add(data2);
						});
					}
				}
				

				data.put("detail", datasdriverschedule);
				datas.add(data);
			});
			
			HashSet<Object> seen= new HashSet<>();
			datas.removeIf(e -> !seen.add(Arrays.asList(e.get("driverId"), e.get("companyId"))));
			response.put("responseCode", "00");
			response.put("responseDesc", "data schedule berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "data schedule gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/getallDriverschedule/{paramAktif}")
	public String getallDriverschedule(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			List<Object[]> drivers = trxDriverscheduleService.getalldriver(paramAktif.toString().toLowerCase(), LocalDate.parse(dataRequest.getString("driverDate")), dataRequest.getString("driverCompanyId"), user_uuid);
			drivers.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				List<JSONObject> datasdriverschedule = new ArrayList<>();
				data.put("driverId", column[0] == null ? "" : column[0]);
				data.put("fleetId", column[1] == null ? "" : column[1]);
				data.put("fleetName", column[2] == null ? "" : column[2].toString());
				data.put("fleetVehicle", column[3] == null ? "" : column[3].toString());
				data.put("driverDate", column[4]);
				data.put("time", column[5] == null ? "" : column[5].toString());
				data.put("timeFrom", column[6] == null ? "" : column[6]);
				data.put("timeTo", column[7] == null ? "" : column[7]);
				data.put("driver", column[8] == null ? "" : column[8].toString());
				data.put("helper", column[9] == null ? "" : column[9].toString());
				data.put("capacityBerat", column[10] == null || column[11] == null ? "" : column[10].toString() + " " + column[11].toString());
				data.put("capacityVolume", column[12] == null || column[13] == null ? "" : column[12].toString() + " " + column[13].toString());
				List<Object[]> availableberat = trxDriverscheduleService.availablecapacityberat(Integer.parseInt(column[0].toString()), "0", dataRequest.getString("driverCompanyId"));
				data.put("availableberat", availableberat.get(0)[0] == null ? "" : availableberat.get(0)[0].toString() + " kg");
				List<Object[]> availablevolume = trxDriverscheduleService.availablecapacityvolume(Integer.parseInt(column[0].toString()), "0", dataRequest.getString("driverCompanyId"));
				data.put("availablevolume", availablevolume.get(0)[0] == null ? "" : availablevolume.get(0)[0].toString() + " m3");
				List<Object[]> schedules = trxDriverscheduleService.getallDriverschedulebydriverbystatus(paramAktif.toString().toLowerCase(), dataRequest.getString("driverCompanyId"), Integer.parseInt(column[0].toString()));
				schedules.stream().forEach(columndetail-> {
					JSONObject datadetail = new JSONObject();
					datadetail.put("orderId", columndetail[0] == null ? "" : columndetail[0]);
					datadetail.put("driverId", columndetail[1] == null ? "" : columndetail[1]);
					datadetail.put("driverscheduleId", columndetail[2] == null ? "" : columndetail[2]);
					datadetail.put("trackingfrom", columndetail[3] == null ? "" : columndetail[3]);
					datadetail.put("trackingto", columndetail[4] == null ? "" : columndetail[4]);
					datadetail.put("schedule", columndetail[5] == null ? "" : columndetail[5]);
					datadetail.put("weight", columndetail[6] == null || columndetail[7] == null ? "" : columndetail[6].toString() + " " + columndetail[7].toString());
					datadetail.put("volume", columndetail[8] == null || columndetail[9] == null ? "" : columndetail[8].toString() + " " + columndetail[9].toString());
					datadetail.put("statusInbound", columndetail[10] == null ? "" : columndetail[10]);
					datadetail.put("companyId", columndetail[11] == null ? "" : columndetail[11]);
					if(columndetail[12] != null)
					{
						for(int i = 0; i < datacustomer.size(); i++)
						{
							if(columndetail[12].equals(datacustomer.getJSONObject(i).getInt("contact_id")))
							{
								datadetail.put("customer", datacustomer.getJSONObject(i).getString("contact_name"));
								datadetail.put("customerid", columndetail[12]);
								break;
							}
							else if(datacustomer.size() == i + 1)
							{
								datadetail.put("customer", "");
								datadetail.put("customerid", "");
							}
						}
					}
					else
					{
						datadetail.put("customer", "");
						datadetail.put("customerid", "");
					}
					datadetail.put("customerIdFromFrom", columndetail[39] == null ? "" : columndetail[39]);
					datadetail.put("customerIdToFrom", columndetail[40] == null ? "" : columndetail[40]);
					datadetail.put("receipantAddress", columndetail[38] == null ? "" : columndetail[38]);
					datadetail.put("mCabangIdFrom", columndetail[13] == null ? "" : columndetail[13]);
					datadetail.put("mCabangNameFrom", columndetail[14] == null ? "" : columndetail[14]);
					datadetail.put("mCabangProvinceIdFrom", columndetail[15] == null ? "" : columndetail[15]);
					datadetail.put("mCabangKotaIdFrom", columndetail[16] == null ? "" : columndetail[16]);
					datadetail.put("mAgenIdFrom", columndetail[17] == null ? "" : columndetail[17]);
					datadetail.put("mAgenNameFrom", columndetail[18] == null ? "" : columndetail[18]);
					datadetail.put("mAgenProvinceIdFrom", columndetail[19] == null ? "" : columndetail[19]);
					datadetail.put("mAgenKotaIdFrom", columndetail[20] == null ? "" : columndetail[20]);
					datadetail.put("mAgenDetailIdFrom", columndetail[21] == null ? "" : columndetail[21]);
//					datadetail.put("customerIdToFrom", columndetail[22] == null ? "" : columndetail[22]);
					datadetail.put("trxHeaderJalurIdFrom", columndetail[34] == null ? "" : columndetail[34]);
					datadetail.put("trxDetailJalurIdFrom", columndetail[35] == null ? "" : columndetail[35]);
					

					datadetail.put("customerIdFromTo", columndetail[23] == null ? "" : columndetail[23]);
					datadetail.put("pickUpAddress", columndetail[12] == null ? "" : columndetail[12]);
					datadetail.put("mCabangIdTo", columndetail[24] == null ? "" : columndetail[24]);
					datadetail.put("mCabangNameTo", columndetail[25] == null ? "" : columndetail[25]);
					datadetail.put("mCabangProvinceIdTo", columndetail[26] == null ? "" : columndetail[26]);
					datadetail.put("mCabangKotaIdTo", columndetail[27] == null ? "" : columndetail[27]);
					datadetail.put("mAgenIdTo", columndetail[28] == null ? "" : columndetail[28]);
					datadetail.put("mAgenNameTo", columndetail[29] == null ? "" : columndetail[29]);
					datadetail.put("mAgenProvinceIdTo", columndetail[30] == null ? "" : columndetail[30]);
					datadetail.put("mAgenKotaIdTo", columndetail[31] == null ? "" : columndetail[31]);
					datadetail.put("mAgenDetailIdTo", columndetail[32] == null ? "" : columndetail[32]);
					datadetail.put("customerIdToTo", columndetail[33] == null ? "" : columndetail[33]);
					datadetail.put("trxHeaderJalurIdTo", columndetail[36] == null ? "" : columndetail[36]);
					datadetail.put("trxDetailJalurIdTo", columndetail[37] == null ? "" : columndetail[37]);
					datadetail.put("jmlhKoli", columndetail[41] == null ? "" : columndetail[41]);
					datadetail.put("serviceName", columndetail[42] == null ? "" : columndetail[42]);
					datasdriverschedule.add(datadetail);
				});
				data.put("trxJalurDriverHeaderId", column[14] == null ? "" : column[14]);
				data.put("mRouteId", column[15]  == null ? "" : column[15]);
				data.put("mRouteName", column[16] == null ? "" : column[16]);
				data.put("vehicleTypeName", column[17] == null ? "" : column[17]);
				data.put("companyId", column[18] == null ? "" : column[18]);
				data.put("actor", column[20] == null ? "" : column[20]);
				data.put("driverscheduleArray", datasdriverschedule);
				datas.add(data);
			});
			HashSet<Object> seen= new HashSet<>();
			datas.removeIf(e -> !seen.add(Arrays.asList(e.get("driverId"), e.get("companyId"))));

			logger.info("drivers = " + drivers.size());
			response.put("responseCode", "00");
			response.put("responseDesc", "data schedule berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data schedule gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	
	@PostMapping("/getDriverScheduleByOrder/{paramAktif}")
	public String getDriverByOrder(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> getDetail = trxDriverscheduleService.getDriverschedulebyorder("y", dataRequest.getString("orderId"), dataRequest.getString("companyId"));
			getDetail.stream().forEach(col->{
				JSONObject data = new JSONObject();
				data.put("orderId", col[0].toString());
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data schedule by order berhasil didapat");
			response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "data schedule by order gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/saveDriverscheduleV2")
	public String saveDriverscheduleV2(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			//1. cek kota tujuannya itu sama dengan kota tujuan yang di truk 
			//		jika iya maka gak usah nambah data di trx_tracking
			// 		jika tidak maka tambah trx_tracking
			ArrayList<TrxTracking> dataTracking = new ArrayList<>();
			Integer McabangId = dataRequest.has("mCabangId") == false ? null : dataRequest.getInt("mCabangId");
			Integer mAgenId = dataRequest.has("mAgenId") == false ? null : dataRequest.getInt("mAgenId");
			Integer mAgenDetailId = dataRequest.has("mAgenDetailId") == false ? null : dataRequest.getInt("mAgenDetailId");
			
			List<Object[]> getLastIDDriverSchedule = trxDriverscheduleService.getLastDriverSchedule(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
			
			
			
			List<Object[]> cekKotaTujuan = trxDriverscheduleService.cekKotaTujuan(McabangId, mAgenId, mAgenDetailId, dataRequest.getString("driverschedulecompanyId"), dataRequest.getString("OrderId"));
			
//			List<Object[]> cekJalurDriverUntukManifest = TrxManifestService.searchId
			
			if(cekKotaTujuan.size() == 0)
			{
				List<Object[]> getDetailTracking = trxTrackingService.getTrackingByOrder("a", dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
				getDetailTracking.stream().forEach(col->{
					LocalDateTime startTime=null;
					if(col[20] != null) {

						String replace = col[20].toString().replaceAll(" ", "T");
						startTime = LocalDateTime.parse(replace);	
					}
					TrxTracking data = new TrxTracking ();
					data.setTmTrxOrderId(col[0].toString());
					data.setmContactCustomerFromId(col[2] == null ? null : Integer.parseInt(col[2].toString()));
					data.setmCabangId(col[3] == null ? null : Integer.parseInt(col[3].toString()));
					data.setmCabangDetailId(col[4] == null ? null : Integer.parseInt(col[4].toString()));
					data.setmAgenId(col[6] == null ? null : Integer.parseInt(col[6].toString()));
					data.setmParentId(col[7] == null ? null : Integer.parseInt(col[7].toString()));
					data.setmContactCustomerToId(col[9] == null ? null : Integer.parseInt(col[9].toString()));
					data.setTrxTrackingStatus(col[10] == null ? null : col[10].toString());
					data.setTrxTrackingEstimetedTime(col[20] == null ? null :startTime);
					data.setTrxTrackingCompanyId(dataRequest.getString("driverschedulecompanyId"));
					data.setTrxTrackingStatusinbound(col[18] == null ? null : col[18].toString());
					data.setTrxTrackingStatusoutbound(col[19] == null ? null : col[19].toString());
					data.setmAgenDetailId(col[21] == null ? null : Integer.parseInt(col[21].toString()));
//					data.setTrxTrackingId(col[1] == null ? null : Integer.parseInt(col[1].toString()));
//					
//					if(dataRequest.getInt("trackingId") == Integer.parseInt(col[1].toString()))
//					{
//						data.setTrxTrackingId(Integer.parseInt(col[1].toString()));
//						data.setmAgenId(dataRequest.getInt("mAgenId"));
//						data.setmAgenDetailId(dataRequest.getInt("mAgenDetailId"));
//						data.setTmTrxOrderId(col[0].toString());
//						data.setTrxTrackingCompanyId(dataRequest.getString("compId"));
//						data.setmCabangId(null);
//						data.setmCabangDetailId(null);
//						data.setmParentId(null);
//					}
					if(dataRequest.getInt("trackingto") <= Integer.parseInt(col[1].toString()))
					{
						data.setTrxTrackingId(Integer.parseInt(col[1].toString())+1 );
					}
					else
					{
						data.setTrxTrackingId(Integer.parseInt(col[1].toString()) );
					}
					
					
					
					
					dataTracking.add(data);
				});
//				cekKotaTujuan.stream().forEach(col->{
//					LocalDateTime startTime=null;
//					if(col[9] != null) {
//
//						String replace = col[9].toString().replaceAll(" ", "T");
//						startTime = LocalDateTime.parse(replace);	
//					}
//					TrxTracking data = new TrxTracking ();
//					data.setTmTrxOrderId(col[0].toString());
//					data.setmContactCustomerFromId(col[2] == null ? null : Integer.parseInt(col[2].toString()));
//					data.setmCabangId(col[3] == null ? null : Integer.parseInt(col[3].toString()));
//					data.setmCabangDetailId(col[4] == null ? null : Integer.parseInt(col[4].toString()));
//					data.setmAgenId(col[5] == null ? null : Integer.parseInt(col[5].toString()));
//					data.setmParentId(col[6] == null ? null : Integer.parseInt(col[6].toString()));
//					data.setmContactCustomerToId(col[7] == null ? null : Integer.parseInt(col[7].toString()));
//					data.setTrxTrackingStatus(col[8] == null ? null : col[8].toString());
//					data.setTrxTrackingEstimetedTime(startTime);
//					data.setTrxTrackingCompanyId(col[10] == null ? null : col[10].toString());
//					data.setTrxTrackingStatusinbound(col[11] == null ? null : col[11].toString());
//					data.setTrxTrackingStatusoutbound(col[12] == null ? null : col[12].toString());
//					data.setmAgenDetailId(col[13] == null ? null : Integer.parseInt(col[13].toString()));
//					data.setTrxDriverjalurId(col[14] == null ? null : Integer.parseInt(col[14].toString()));
//					data.setTrxDriverjalurheaderId(col[15] == null ? null : Integer.parseInt(col[15].toString()));
//					
//					if(dataRequest.getInt("trackingto") == Integer.parseInt(col[1].toString()))
//					{
//						data.setTrxTrackingId(Integer.parseInt(col[1].toString())+1 );
//					}
//					else
//					{
//						data.setTrxTrackingId(Integer.parseInt(col[1].toString()) );
//					}
//					dataTracking.add(data);
//				});
				
				
				TrxTracking data2 = new TrxTracking ();
				data2.setTrxTrackingId(dataRequest.getInt("trackingto"));
				data2.setTmTrxOrderId(dataRequest.getString("OrderId"));
				data2.setTrxTrackingCompanyId(dataRequest.getString("driverschedulecompanyId"));
				data2.setTrxTrackingStatusinbound("waiting driver");
				data2.setTrxTrackingStatusoutbound(null);
				data2.setmCabangId(McabangId);
				data2.setmCabangDetailId(null);
				data2.setmAgenId(mAgenId);
				data2.setmAgenDetailId(mAgenDetailId);
				data2.setmParentId(McabangId);
				data2.setTrxDriverjalurheaderId(dataRequest.getInt("trxJalurDriverHeaderToId"));
				data2.setTrxDriverjalurId(dataRequest.getInt("trxJalurDriverDetailId"));
				data2.setTypeTambahan("1");
				dataTracking.add((dataRequest.getInt("trackingto")-1), data2);
				Optional <TrxOrder> dataUpdateorder = trxOrderService.getdetail(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
		    	if(dataUpdateorder.isPresent())
		    	{
		    		((TrxOrder)dataUpdateorder.get()).setTrxOrderStatus("on Proggress");
		    		trxOrderService.saveOrder(dataUpdateorder.get());
		    	}
				for (TrxTracking objects : dataTracking) {
					if(objects.getTrxTrackingId() == dataRequest.getInt("trackingfrom") )
					{
						objects.setTrxDriverjalurheaderId(dataRequest.getInt("trxJalurDriverHeaderFromId"));
						objects.setTrxDriverjalurId(dataRequest.getInt("trxJalurDriverDetailIdFrom"));
						objects.setTrxTrackingStatusoutbound("on Proggress");
						objects.setTrxTrackingStatusinbound(null);
						objects.setTrxTrackingStatus(null);	
					}
					else if(objects.getTrxTrackingId() == dataRequest.getInt("trackingto"))
		    		{
						objects.setTrxTrackingStatusoutbound(null);
						objects.setTrxTrackingStatusinbound("waiting driver");
						objects.setTrxTrackingStatus(null);	
		    		}
					else
		    		{
						objects.setTrxTrackingStatusoutbound(null);
						objects.setTrxTrackingStatusinbound(null);
						objects.setTrxTrackingStatus("on Proggress");	
		    		}
				}
				trxTrackingService.saveAllTracking(dataTracking);
				TrxDriverschedule data = new TrxDriverschedule();
				data.setTmTrxOrderId(dataRequest.getString("OrderId"));
//				data.setTrxDriverscheduleId(dataRequest.getInt("DriverscheduleId"));
				data.setTrxDriverscheduleId(getLastIDDriverSchedule.size() > 0 ? Integer.parseInt(getLastIDDriverSchedule.get(0)[0].toString()) + 1 : 0);
				data.setmDriverId(dataRequest.getInt("DriverId"));
				data.setTmTrxTrackingIdFrom(dataRequest.getInt("trackingfrom"));
				data.setTmTrxTrackingIdTo(dataRequest.getInt("trackingto"));
				data.setTrx_driverschedule_time_from(LocalTime.parse(dataRequest.getString("timeFrom")));
				data.setTrx_driverschedule_time_to(LocalTime.parse(dataRequest.getString("timeTo")));
				data.setTrx_driverschedule_weight(Float.parseFloat(dataRequest.getString("weight")));
				data.setmWeightId(dataRequest.getInt("weightId"));
				data.setTrx_driverschedule_volume(Float.parseFloat(dataRequest.getString("volume")));
				data.setmVolumeId(dataRequest.getInt("volumeId"));
				data.setTrxDriverscheduleStatus("pick Up");
				data.setTrxDriverscheduleCompanyId(dataRequest.getString("driverschedulecompanyId"));
				trxDriverscheduleService.savedriverschedule(data);
				
				Optional<TrxTracking> getDetailFrom = trxTrackingService.getdetail(dataRequest.getString("OrderId"), dataRequest.getInt("trackingfrom"), dataRequest.getString("driverschedulecompanyId"));
				Optional<TrxTracking> getDetailTo = trxTrackingService.getdetail(dataRequest.getString("OrderId"), dataRequest.getInt("trackingto"), dataRequest.getString("driverschedulecompanyId"));

				
//				List<Object[]> getLast = TrxManifestService.getManifest(dataRequest.getInt("DriverId"),  getDetailFrom.get().getmCabangId(),  getDetailFrom.get().getmAgenId(), getDetailFrom.get().getmAgenDetailId(),
//						getDetailTo.get().getmCabangId(),getDetailTo.get().getmAgenId(),getDetailTo.get().getmAgenDetailId()
//						,dataRequest.getString("OrderId"),dataRequest.getString("driverschedulecompanyId")
//						);
//				logger.info("getLast manifest = " + getLast.size());
				
				//cara create manifest otomatis
//				if(getLast.size()>0)
//				{
//					logger.info("ada isinya");
//					Optional<TrxOrder> getDetail = TrxOrderService.getdetail(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
//					if(getDetail.isPresent())
//					{
//						((TrxOrder)getDetail.get()).setManifestNo(getLast.get(0)[1].toString());
//					}
//				}
//				else
//				{
//					logger.info("kosong lur");
//					List<Object[]> numbers = TrxNumberService.getNumber("a", 5,  dataRequest.getString("driverschedulecompanyId"));
//					logger.info("numbers = " + numbers.size());
//			    	numbers.stream().forEach(column-> {
//			    		
//				    	Optional <TrxNumber> dataUpdateNumber = TrxNumberService.getdetail(Integer.parseInt(column[0].toString()), dataRequest.getString("driverschedulecompanyId"));
//				    	if(dataUpdateNumber.isPresent())
//				    	{
//				    		((TrxNumber)dataUpdateNumber.get()).setTrxNumberRunningnumber(Integer.parseInt(column[3].toString()) + 1);
//				    		TrxNumberService.saveNumber(dataUpdateNumber.get());
//				    		Optional<TrxOrder> getDetail = TrxOrderService.getdetail(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
//							if(getDetail.isPresent())
//							{
//								((TrxOrder)getDetail.get()).setManifestNo(column[6].toString());
//								TrxOrderService.saveOrder(getDetail.get());
//								
//
//								Optional<TrxTracking> gettracking = trxTrackingService.getdetail(dataRequest.getString("OrderId"), dataRequest.getInt("trackingfrom"),  dataRequest.getString("driverschedulecompanyId"));
//								
//								Optional<TrxTracking> gettrackingTo = trxTrackingService.getdetail(dataRequest.getString("OrderId"), dataRequest.getInt("trackingto"),  dataRequest.getString("driverschedulecompanyId"));
//								
//								int id = 0;
//								List<Object[]> getId = TrxManifestService.getId();
//						    	id = Integer.parseInt(String.valueOf(getId.get(0)));
//								TrxManifest dataManifest = new TrxManifest();
//								
//								dataManifest.setId(id);
//								
//								dataManifest.setManifestNo(column[6].toString());
//								dataManifest.setDriverId(dataRequest.getInt("DriverId"));
//								
//								if(gettracking.isPresent())
//								{
//									if(String.valueOf(gettracking.get().getmCabangId()) == "null" || String.valueOf(gettracking.get().getmCabangId()) == null || !String.valueOf(gettracking.get().getmCabangId()).equals(null) || !String.valueOf(gettracking.get().getmCabangId()).equals("null"))
//									{
//										dataManifest.setCabangIdFrom(Integer.parseInt(gettracking.get().getmCabangId().toString()));
//									}
//									if(String.valueOf(gettracking.get().getmAgenId()) == "null" || String.valueOf(gettracking.get().getmAgenId()) == null || !String.valueOf(gettracking.get().getmAgenId()).equals(null) || !String.valueOf(gettracking.get().getmAgenId()).equals("null"))
//									{
//										dataManifest.setAgenIdFrom(gettracking.get().getmAgenId());
//										dataManifest.setAgenDetailIdFrom(gettracking.get().getmAgenDetailId());
//									}
//								}
//								if(gettrackingTo.isPresent())
//								{
//									if(String.valueOf(gettrackingTo.get().getmCabangId()) == "null" || String.valueOf(gettrackingTo.get().getmCabangId()) == null || !String.valueOf(gettrackingTo.get().getmCabangId()).equals(null) || !String.valueOf(gettrackingTo.get().getmCabangId()).equals("null"))
//									{
//										dataManifest.setCabangIdTo(gettrackingTo.get().getmCabangId());
//									}
//									
//									if(String.valueOf(gettrackingTo.get().getmAgenId()) == "null" || String.valueOf(gettrackingTo.get().getmAgenId()) == null || !String.valueOf(gettrackingTo.get().getmAgenId()).equals(null) || !String.valueOf(gettrackingTo.get().getmAgenId()).equals("null"))
//									{
//										dataManifest.setAgenIdTo(gettrackingTo.get().getmAgenId());
//										dataManifest.setAgenDetailIdTo(gettrackingTo.get().getmAgenDetailId());
//									}
//								}
//								dataManifest.setCompanyId(dataRequest.getString("driverschedulecompanyId"));
//								TrxManifestService.save(dataManifest);
//								
//							}
//				    	}
//			    	});
//			    	
//			    	
//				}
				
				response.put("responseCode", "00");
			    response.put("responseDesc", "Save schedule success");
			}
			else
			{
				logger.info("disini semua");
				List<Object[]> schedules = trxDriverscheduleService.schedulecheck(dataRequest.getInt("DriverId"), dataRequest.getString("timeFrom"), dataRequest.getString("timeTo"), dataRequest.getString("driverschedulecompanyId"));
				List<Object[]> weights = mWeightService.getWeight("a", dataRequest.getInt("weightId"), dataRequest.getString("driverschedulecompanyId"));
				List<Object[]> volumes = mVolumeService.getVolume("a", dataRequest.getInt("volumeId"), dataRequest.getString("driverschedulecompanyId"));
				Float berat = Float.parseFloat(weights.get(0)[2].toString()) * Float.parseFloat(dataRequest.getString("weight"));
				logger.info(berat.toString());
				Float volume = Float.parseFloat(volumes.get(0)[2].toString()) * Float.parseFloat(dataRequest.getString("volume"));
				List<Object[]> availableberat = trxDriverscheduleService.availablecapacityberat(dataRequest.getInt("DriverId"), "0", dataRequest.getString("driverschedulecompanyId"));
				List<Object[]> availablevolume = trxDriverscheduleService.availablecapacityvolume(dataRequest.getInt("DriverId"), "0", dataRequest.getString("driverschedulecompanyId"));
				Float sisaberat = Float.parseFloat(availableberat.get(0)[0].toString());
				logger.info(sisaberat.toString());
				Float sisavolume = Float.parseFloat(availablevolume.get(0)[0].toString());
				if(schedules.get(0)[0].equals(1))
				{
					response.put("responseCode", "99");
				    response.put("responseDesc", "Please change Time Schedule, Overlap with other schedule");
				}
				else if(sisaberat < berat)
				{
					response.put("responseCode", "99");
				    response.put("responseDesc", "can't save available capacity for mass");
				}
				else if(sisavolume < volume)
				{
					response.put("responseCode", "99");
				    response.put("responseDesc", "can't save available capacity for volume");
				}
				else
				{
					logger.info("masuk sini lur");
					TrxDriverschedule data = new TrxDriverschedule();
					data.setTmTrxOrderId(dataRequest.getString("OrderId"));
					data.setTrxDriverscheduleId(getLastIDDriverSchedule.size() > 0 ? Integer.parseInt(getLastIDDriverSchedule.get(0)[0].toString()) + 1 : 0);
//					data.setTrxDriverscheduleId(dataRequest.getInt("DriverscheduleId"));
					data.setmDriverId(dataRequest.getInt("DriverId"));
					data.setTmTrxTrackingIdFrom(dataRequest.getInt("trackingfrom"));
					data.setTmTrxTrackingIdTo(dataRequest.getInt("trackingto"));
					data.setTrx_driverschedule_time_from(LocalTime.parse(dataRequest.getString("timeFrom")));
					data.setTrx_driverschedule_time_to(LocalTime.parse(dataRequest.getString("timeTo")));
					data.setTrx_driverschedule_weight(Float.parseFloat(dataRequest.getString("weight")));
					data.setmWeightId(dataRequest.getInt("weightId"));
					data.setTrx_driverschedule_volume(Float.parseFloat(dataRequest.getString("volume")));
					data.setmVolumeId(dataRequest.getInt("volumeId"));
					data.setTrxDriverscheduleStatus("pick Up");
					data.setTrxDriverscheduleCompanyId(dataRequest.getString("driverschedulecompanyId"));
					trxDriverscheduleService.savedriverschedule(data);
					Optional <TrxOrder> dataUpdateorder = trxOrderService.getdetail(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
			    	if(dataUpdateorder.isPresent())
			    	{
			    		((TrxOrder)dataUpdateorder.get()).setTrxOrderStatus("on Proggress");
			    		trxOrderService.saveOrder(dataUpdateorder.get());
			    	}
			    	List<Object[]> tracking = trxTrackingService.getTrackingByOrder("a", dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
			    	ArrayList <TrxTracking> datastracking = new ArrayList<>();
			    	tracking.stream().forEach(column-> {
						Optional <TrxTracking> dataUpdatetracking = trxTrackingService.getdetail(dataRequest.getString("OrderId"),Integer.parseInt(column[1].toString()), dataRequest.getString("driverschedulecompanyId"));
				    	if(dataUpdatetracking.isPresent())
				    	{
				    		if(Integer.parseInt(column[1].toString()) == dataRequest.getInt("trackingfrom"))
				    		{
				    			((TrxTracking)dataUpdatetracking.get()).setTrxDriverjalurheaderId(dataRequest.getInt("trxJalurDriverHeaderFromId"));
				    			((TrxTracking)dataUpdatetracking.get()).setTrxDriverjalurId(dataRequest.getInt("trxJalurDriverDetailIdFrom"));
				    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusoutbound("on Proggress");
				    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusinbound(null);
				    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatus(null);	
				    		}
				    		else if(Integer.parseInt(column[1].toString()) == dataRequest.getInt("trackingto"))
				    		{
				    			((TrxTracking)dataUpdatetracking.get()).setTrxDriverjalurheaderId(dataRequest.getInt("trxJalurDriverHeaderToId"));
				    			((TrxTracking)dataUpdatetracking.get()).setTrxDriverjalurId(dataRequest.getInt("trxJalurDriverDetailId"));
				    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusoutbound(null);
				    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusinbound("waiting driver");
				    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatus(null);
				    			
				    		}
				    		else
				    		{
				    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusoutbound(null);
				    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusinbound(null);
				    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatus("on Proggress");	
				    		}
				    		datastracking.add(dataUpdatetracking.get());
				    	}
				    	trxTrackingService.saveAllTracking(datastracking);
			    	});
			    	
			    	Optional<TrxTracking> getDetailFrom = trxTrackingService.getdetail(dataRequest.getString("OrderId"), dataRequest.getInt("trackingfrom"), dataRequest.getString("driverschedulecompanyId"));
					Optional<TrxTracking> getDetailTo = trxTrackingService.getdetail(dataRequest.getString("OrderId"), dataRequest.getInt("trackingto"), dataRequest.getString("driverschedulecompanyId"));
			    	
//					cara create manifest otomatis
//			    	List<Object[]> getLast = TrxManifestService.getManifest(dataRequest.getInt("DriverId"),  getDetailFrom.get().getmCabangId(),  getDetailFrom.get().getmAgenId(), getDetailFrom.get().getmAgenDetailId(),
//							getDetailTo.get().getmCabangId(),getDetailTo.get().getmAgenId(),getDetailTo.get().getmAgenDetailId()
//							,dataRequest.getString("OrderId"),dataRequest.getString("driverschedulecompanyId")
//							);
//					logger.info("getLast manifest = " + getLast.size());
//					
//					if(getLast.size()>0)
//					{
//						logger.info("ada isinya");
//						Optional<TrxOrder> getDetail = TrxOrderService.getdetail(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
//						if(getDetail.isPresent())
//						{
//							((TrxOrder)getDetail.get()).setManifestNo(getLast.get(0)[1].toString());
//							TrxOrderService.saveOrder(getDetail.get());
//						}
//					}
//					else
//					{
//						logger.info("kosong lur kedua");
//						List<Object[]> numbers = TrxNumberService.getNumber("a", 5,  dataRequest.getString("driverschedulecompanyId"));
//						
//				    	numbers.stream().forEach(column-> {
//				    		logger.info("numbers = " + column[6].toString());
//					    	Optional <TrxNumber> dataUpdateNumber = TrxNumberService.getdetail(Integer.parseInt(column[0].toString()), dataRequest.getString("driverschedulecompanyId"));
//					    	
//					    	if(dataUpdateNumber.isPresent())
//					    	{
//					    		((TrxNumber)dataUpdateNumber.get()).setTrxNumberRunningnumber(Integer.parseInt(column[3].toString()) + 1);
//					    		TrxNumberService.saveNumber(dataUpdateNumber.get());
//					    		Optional<TrxOrder> getDetail = TrxOrderService.getdetail(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
//								if(getDetail.isPresent())
//								{
//									((TrxOrder)getDetail.get()).setManifestNo(column[6].toString());
//									TrxOrderService.saveOrder(getDetail.get());
//									Optional<TrxOrder> getDetail1 = TrxOrderService.getdetail(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
//									if(getDetail1.isPresent())
//									{
//										((TrxOrder)getDetail1.get()).setManifestNo(column[6].toString());
//										TrxOrderService.saveOrder(getDetail1.get());
//										
//
//										Optional<TrxTracking> gettracking = trxTrackingService.getdetail(dataRequest.getString("OrderId"), dataRequest.getInt("trackingfrom"),  dataRequest.getString("driverschedulecompanyId"));
//										
//										Optional<TrxTracking> gettrackingTo = trxTrackingService.getdetail(dataRequest.getString("OrderId"), dataRequest.getInt("trackingto"),  dataRequest.getString("driverschedulecompanyId"));
//										
//										int id = 0;
//										List<Object[]> getId = TrxManifestService.getId();
//								    	id = Integer.parseInt(String.valueOf(getId.get(0)));
//								    	
//										
//										TrxManifest dataManifest = new TrxManifest();
//										dataManifest.setId(id);
//										dataManifest.setManifestNo(column[6].toString());
//										dataManifest.setDriverId(dataRequest.getInt("DriverId"));
//										if(gettracking.isPresent())
//										{
//											if(String.valueOf(gettracking.get().getmCabangId()) == "null" || String.valueOf(gettracking.get().getmCabangId()) == null || !String.valueOf(gettracking.get().getmCabangId()).equals(null) || !String.valueOf(gettracking.get().getmCabangId()).equals("null"))
//											{
//												dataManifest.setCabangIdFrom(Integer.parseInt(gettracking.get().getmCabangId().toString()));
//											}
//											if(String.valueOf(gettracking.get().getmAgenId()) == "null" || String.valueOf(gettracking.get().getmAgenId()) == null || !String.valueOf(gettracking.get().getmAgenId()).equals(null) || !String.valueOf(gettracking.get().getmAgenId()).equals("null"))
//											{
//												dataManifest.setAgenIdFrom(gettracking.get().getmAgenId());
//												dataManifest.setAgenDetailIdFrom(gettracking.get().getmAgenDetailId());
//											}
//										}
//										if(gettrackingTo.isPresent())
//										{
//											if(String.valueOf(gettrackingTo.get().getmCabangId()) == "null" || String.valueOf(gettrackingTo.get().getmCabangId()) == null || !String.valueOf(gettrackingTo.get().getmCabangId()).equals(null) || !String.valueOf(gettrackingTo.get().getmCabangId()).equals("null"))
//											{
//												dataManifest.setCabangIdTo(gettrackingTo.get().getmCabangId());
//											}
//											
//											if(String.valueOf(gettrackingTo.get().getmAgenId()) == "null" || String.valueOf(gettrackingTo.get().getmAgenId()) == null || !String.valueOf(gettrackingTo.get().getmAgenId()).equals(null) || !String.valueOf(gettrackingTo.get().getmAgenId()).equals("null"))
//											{
//												dataManifest.setAgenIdTo(gettrackingTo.get().getmAgenId());
//												dataManifest.setAgenDetailIdTo(gettrackingTo.get().getmAgenDetailId());
//											}
//										}
//										dataManifest.setCompanyId(dataRequest.getString("driverschedulecompanyId"));
//										TrxManifestService.save(dataManifest);
//										
//									}
//						    	
//									
//								}
//					    	}
//				    	});
//				    	
//				    	
//					}
			    	
			    	response.put("responseCode", "00");
				    response.put("responseDesc", "Save schedule success" );
				}
			}
			
			
			
//		    response.put("responseData",dataTracking);
		}
		catch(Exception e)
		{
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save schedule failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/updateDriverschedule")
	public String updateDriverschedule(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			SimpleDateFormat formatter11 = new SimpleDateFormat("yyyy-MM-dd");
			Optional<TrxDriverschedule> getDetail =  trxDriverscheduleService.getDetail(dataRequest.getString("orderId"), dataRequest.getInt("driverScheduleId"),dataRequest.getString("companyId") );
			if(getDetail.isPresent())
	    	{
	    		((TrxDriverschedule)getDetail.get()).setTrx_driverschedule_time_from(LocalTime.parse(dataRequest.getString("fromTime")));
	    		((TrxDriverschedule)getDetail.get()).setTrx_driverschedule_time_to(LocalTime.parse(dataRequest.getString("toTime")));
	    		trxDriverscheduleService.savedriverschedule(getDetail.get());
	    	}
			response.put("responseCode", "00");
			response.put("responseDesc", "Update schedule Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		     response.put("responseDesc", "Update schedule failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/saveDriverschedule")
	public String saveDriverschedule(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		
		LocalDateTime localDateTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
        String formatDateTime = localDateTime.format(formatter);
        LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
		
		try
		{
			
			List<Object[]> schedules = trxDriverscheduleService.schedulecheck(dataRequest.getInt("DriverId"), dataRequest.getString("timeFrom"), dataRequest.getString("timeTo"), dataRequest.getString("driverschedulecompanyId"));
			List<Object[]> weights = mWeightService.getWeight("a", dataRequest.getInt("weightId"), dataRequest.getString("driverschedulecompanyId"));
			List<Object[]> volumes = mVolumeService.getVolume("a", dataRequest.getInt("volumeId"), dataRequest.getString("driverschedulecompanyId"));
			
			List<Object[]> getLastIDDriverSchedule = trxDriverscheduleService.getLastDriverSchedule(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
			
			Float berat = Float.parseFloat(weights.get(0)[2].toString()) * Float.parseFloat(dataRequest.getString("weight"));
			logger.info(berat.toString());
			Float volume = Float.parseFloat(volumes.get(0)[2].toString()) * Float.parseFloat(dataRequest.getString("volume"));
			List<Object[]> availableberat = trxDriverscheduleService.availablecapacityberat(dataRequest.getInt("DriverId"), "0", dataRequest.getString("driverschedulecompanyId"));
			List<Object[]> availablevolume = trxDriverscheduleService.availablecapacityvolume(dataRequest.getInt("DriverId"), "0", dataRequest.getString("driverschedulecompanyId"));
			Float sisaberat = Float.parseFloat(availableberat.get(0)[0].toString());
			logger.info(sisaberat.toString());
			Float sisavolume = Float.parseFloat(availablevolume.get(0)[0].toString());
			if(schedules.get(0)[0].equals(1))
			{
				response.put("responseCode", "99");
			    response.put("responseDesc", "Please change Time Schedule, Overlap with other schedule");
			}
			else if(sisaberat < berat)
			{
				response.put("responseCode", "99");
			    response.put("responseDesc", "can't save available capacity for mass");
			}
			else if(sisavolume < volume)
			{
				response.put("responseCode", "99");
			    response.put("responseDesc", "can't save available capacity for volume");
			}
			else
			{
				TrxDriverschedule data = new TrxDriverschedule();
				data.setTmTrxOrderId(dataRequest.getString("OrderId"));
				data.setTrxDriverscheduleId(getLastIDDriverSchedule.size() > 0 ? Integer.parseInt(getLastIDDriverSchedule.get(0)[0].toString()) + 1 : 0);
				data.setmDriverId(dataRequest.getInt("DriverId"));
				data.setTmTrxTrackingIdFrom(dataRequest.getInt("trackingfrom"));
				data.setTmTrxTrackingIdTo(dataRequest.getInt("trackingto"));
				data.setTrx_driverschedule_time_from(LocalTime.parse(dataRequest.getString("timeFrom")));
				data.setTrx_driverschedule_time_to(LocalTime.parse(dataRequest.getString("timeTo")));
				data.setTrx_driverschedule_weight(Float.parseFloat(dataRequest.getString("weight")));
				data.setmWeightId(dataRequest.getInt("weightId"));
				data.setTrx_driverschedule_volume(Float.parseFloat(dataRequest.getString("volume")));
				data.setmVolumeId(dataRequest.getInt("volumeId"));
				data.setTrxDriverscheduleStatus("pick Up");
				data.setTrxDriverscheduleCompanyId(dataRequest.getString("driverschedulecompanyId"));
				trxDriverscheduleService.savedriverschedule(data);
				Optional <TrxOrder> dataUpdateorder = trxOrderService.getdetail(dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
		    	if(dataUpdateorder.isPresent())
		    	{
		    		((TrxOrder)dataUpdateorder.get()).setTrxOrderStatus("on Proggress");
		    		trxOrderService.saveOrder(dataUpdateorder.get());
		    	}
		    	List<Object[]> tracking = trxTrackingService.getTrackingByOrder("a", dataRequest.getString("OrderId"), dataRequest.getString("driverschedulecompanyId"));
		    	ArrayList <TrxTracking> datastracking = new ArrayList<>();
		    	tracking.stream().forEach(column-> {
					Optional <TrxTracking> dataUpdatetracking = trxTrackingService.getdetail(dataRequest.getString("OrderId"),Integer.parseInt(column[1].toString()), dataRequest.getString("driverschedulecompanyId"));
			    	if(dataUpdatetracking.isPresent())
			    	{
			    		if(Integer.parseInt(column[1].toString()) == dataRequest.getInt("trackingfrom"))
			    		{
			    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusoutbound("on Proggress");
			    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusinbound(null);
			    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatus(null);	
			    		}
			    		else if(Integer.parseInt(column[1].toString()) == dataRequest.getInt("trackingto"))
			    		{
			    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusoutbound(null);
			    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusinbound("waiting driver");
			    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatus(null);	
			    		}
			    		else
			    		{
			    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusoutbound(null);
			    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatusinbound(null);
			    			((TrxTracking)dataUpdatetracking.get()).setTrxTrackingStatus("on Proggress");	
			    		}
			    		datastracking.add(dataUpdatetracking.get());
			    	}
			    	trxTrackingService.saveAllTracking(datastracking);
		    	});
		    	
		    	TrxDriverTracking data1 = new TrxDriverTracking();
		    	data1.setId(1);
		    	data1.setTmTrxOrderId(dataRequest.getString("OrderId"));
		    	data1.setTrackingId(1);
		    	data1.setStatus(1);
		    	data1.setmDriverId(dataRequest.getInt("DriverId"));
		    	data1.setCompanyId(dataRequest.getString("driverschedulecompanyId")); 
		    	TrxDriverTrackingService.save(data1);
		    	
		    	TrxDriverTracking data2 = new TrxDriverTracking();
		    	data2.setId(2);
		    	data2.setTmTrxOrderId(dataRequest.getString("OrderId"));
		    	data2.setTrackingId(2);
		    	data2.setStatus(1);
		    	data2.setmDriverId(dataRequest.getInt("DriverId"));
		    	data2.setCompanyId(dataRequest.getString("driverschedulecompanyId")); 
		    	
		    	
		    	
		    	TrxDriverTrackingService.save(data2);
		    	response.put("responseCode", "00");
			    response.put("responseDesc", "Save schedule success");
			}
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save schedule failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
