package com.proacc.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.entity.MService;
import com.proacc.helper.Helper;
import com.proacc.service.MServiceService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MServiceController {
	private static final Logger logger = LoggerFactory.getLogger(MServiceController.class);
	
	@Autowired
	MServiceService mServiceService;
	@PostMapping("/getAllService/{paramAktif}")
	public String getAllService(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> services = mServiceService.getallservice(paramAktif.toString().toLowerCase(), dataRequest.getString("serviceCompanyId"));
			services.stream().forEach(column->{
				JSONObject data = new JSONObject();
				String waktu = "";
				data.put("serviceId", column[0]);
				data.put("serviceName", column[1] == null ? "" : column[1].toString());
				waktu = column[2].toString() + " hari";
				data.put("servicedeliverytime", column[3] == null ? waktu : waktu + " - " + column[3].toString() + " hari");
				data.put("serviceDescription", column[4] == null ? "" : column[4].toString());
				data.put("serviceCreatedBy", column[5].toString());
				data.put("serviceCreatedAt", column[6].toString());
				data.put("serviceUpdatedBy", column[7] == null ? "" : column[5].toString());
				data.put("serviceUpdatedAt", column[8] == null ? "" : column[6].toString());
				data.put("serviceStatus", column[9].toString());
				data.put("serviceCompanyId", column[10]);
				data.put("serviceDeliveryTime1", column[2] == null ? "" : column[2]);
				data.put("serviceDeliveryTime2", column[3] == null ? "" : column[3]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data service berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data service gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getServiceDetail/{paramAktif}")
	public String getServiceDetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> services = mServiceService.getservice(paramAktif.toString().toLowerCase(), dataRequest.getInt("serviceId"), dataRequest.getString("serviceCompanyId"));
			services.stream().forEach(column->{
				JSONObject data = new JSONObject();
				String waktu = "";
				data.put("serviceId", column[0]);
				data.put("serviceName", column[1] == null ? "" : column[1].toString());
				waktu = column[2].toString() + " hari";
				data.put("servicedeliverytime", column[3] == null ? waktu : waktu + " - " + column[3].toString() + " hari");
				data.put("serviceDescription", column[4] == null ? "" : column[4].toString());
				data.put("serviceCreatedBy", column[5].toString());
				data.put("serviceCreatedAt", column[6].toString());
				data.put("serviceUpdatedBy", column[7] == null ? "" : column[5].toString());
				data.put("serviceUpdatedAt", column[8] == null ? "" : column[6].toString());
				data.put("serviceStatus", column[9].toString());
				data.put("serviceCompanyId", column[10]);
				data.put("serviceDeliveryTime1", column[2] == null ? "" : column[2]);
				data.put("serviceDeliveryTime2", column[3] == null ? "" : column[3]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data service berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data service gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/saveService")
	public String saveService(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	int id = 0;
	    	List<Object[]> dataNext = mServiceService.nextvalMService();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	MService data = new MService();
	    	data.setServiceId(id);
	    	data.setServiceName(dataRequest.get("serviceName") == "" ? null : dataRequest.getString("serviceName"));
	    	data.setServiceDeliverytime1(dataRequest.getInt("serviceDeliverytime1"));
	    	data.setServiceDeliverytime2(dataRequest.get("serviceDeliverytime2").equals("") ? null : dataRequest.getInt("serviceDeliverytime2"));
	    	data.setServiceDescription(dataRequest.get("serviceDescription").equals("") ? null : dataRequest.getString("serviceDescription"));
	    	data.setServiceCreatedBy(user_uuid);
	    	data.setServiceCreatedAt(localDateTime2);
	    	data.setServiceStatus(dataRequest.getBoolean("serviceStatus"));
	    	data.setServiceCompanyId(dataRequest.getString("serviceCompanyId"));
	    	mServiceService.saveservice(data);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Service success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save Service failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateService")
	public String updateService(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	MService data = new MService();
	    	data.setServiceId(dataRequest.getInt("serviceId"));
	    	data.setServiceName(dataRequest.get("serviceName") == "" ? null : dataRequest.getString("serviceName"));
	    	data.setServiceDeliverytime1(dataRequest.getInt("serviceDeliverytime1"));
	    	data.setServiceDeliverytime2(dataRequest.get("serviceDeliverytime2").equals("") ? null : dataRequest.getInt("serviceDeliverytime2"));
	    	data.setServiceDescription(dataRequest.get("serviceDescription").equals("") ? null : dataRequest.getString("serviceDescription"));
	    	data.setServiceCreatedBy(user_uuid);
	    	data.setServiceCreatedAt(localDateTime2);
	    	data.setServiceStatus(dataRequest.get("serviceStatus").equals("") ? null : dataRequest.getBoolean("serviceStatus"));
	    	data.setServiceCompanyId(dataRequest.get("serviceCompanyId").equals("") ? null : dataRequest.getString("serviceCompanyId"));
	    	mServiceService.saveservice(data);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update Service success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Update Position failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
