package com.proacc.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.proacc.entity.TrxJalurDriver;
import com.proacc.entity.TrxNumber;
import com.proacc.helper.Helper;
import com.proacc.entity.TrxManifest;
import com.proacc.entity.TrxManifestDetail;
import com.proacc.service.MDriverService;
import com.proacc.service.MVolumeService;
import com.proacc.service.MWeightService;
import com.proacc.service.TrxDriverTrackingService;
import com.proacc.service.TrxDriverscheduleService;
import com.proacc.service.TrxHistroyTrackingService;
import com.proacc.service.TrxJalurDriverService;
import com.proacc.service.TrxManifestDetailService;
import com.proacc.service.TrxManifestService;
import com.proacc.service.TrxOrderService;
import com.proacc.service.TrxTrackingService;
import com.proacc.service.TrxNumberService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class TrxManifestController {
	private static final Logger logger = LoggerFactory.getLogger(TrxManifestController.class);
	
	@Autowired
	TrxOrderService trxOrderService;
	@Autowired
	TrxTrackingService trxTrackingService;
	@Autowired
	MDriverService mDriverService;
	@Autowired
	TrxDriverscheduleService trxDriverscheduleService;
	@Autowired
	MWeightService mWeightService;
	@Autowired
	MVolumeService mVolumeService;
	@Autowired
	TrxDriverTrackingService TrxDriverTrackingService;
	@Autowired
	TrxJalurDriverService TrxJalurDriverService;
	
	@Autowired
	TrxNumberService TrxNumberService;
	
	@Autowired
	TrxManifestService TrxManifestService;
	
	@Autowired
	TrxManifestDetailService TrxManifestDetailService;
	
	int id = 0;
	@PostMapping("/saveManifest")
	public String saveManifet(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		
		logger.info("API == saveManifet");
		logger.info("INPUT == " + dataRequest);
		try
		{
			List<TrxManifestDetail> dataDetail = new ArrayList<>();
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
			JSONArray routeJalurDetail = dataRequest.getJSONArray("manifestDetail");
			TrxManifest dataManifest = new TrxManifest();
			id = 0;
	    	
	    		
	    		List<Object[]> numbers = TrxNumberService.getNumber("a", 5,  dataRequest.getString("companyId"));
		    	numbers.stream().forEach(column-> {
		    		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
			    	Optional <TrxNumber> dataUpdateNumber = TrxNumberService.getdetail(Integer.parseInt(column[0].toString()), dataRequest.getString("companyId"));
			    	if(dataUpdateNumber.isPresent())
			    	{
			    		
			    		

						
						if(dataRequest.has("id"))
						{
							id  = dataRequest.getInt("id");
							((TrxNumber)dataUpdateNumber.get()).setTrxNumberRunningnumber(Integer.parseInt(column[3].toString()) + 1);
				    		TrxNumberService.saveNumber(dataUpdateNumber.get());
						}
						else
						{
							List<Object[]> getId = TrxManifestService.getId();
					    	id = Integer.parseInt(String.valueOf(getId.get(0)));
						}
						
						
						
						dataManifest.setId(id);
						dataManifest.setManifestNo(column[6].toString());
						dataManifest.setDriverId(dataRequest.getInt("driverId"));
						
						if(dataRequest.has("mCabangIdFrom"))
						{
							dataManifest.setCabangIdFrom(dataRequest.getInt("mCabangIdFrom"));
						}
						
						if(dataRequest.has("mAgenIdFrom"))
						{
							dataManifest.setAgenIdFrom(dataRequest.getInt("mAgenIdFrom"));
							dataManifest.setAgenDetailIdFrom(dataRequest.getInt("mAgenIdDetailFrom"));
						}
						
						if(dataRequest.has("mCabangIdTo"))
						{
							dataManifest.setCabangIdTo(dataRequest.getInt("mCabangIdTo"));
						}
						
						if(dataRequest.has("mAgenIdTo"))
						{
							dataManifest.setAgenIdTo(dataRequest.getInt("mAgenIdTo"));
							dataManifest.setAgenDetailIdTo(dataRequest.getInt("mAgenIdDetailTo"));
						}
						dataManifest.setCompanyId(dataRequest.getString("companyId"));
						dataManifest.setCreatedAt(localDateTime2);
						dataManifest.setCreatedBy(user_uuid);
						try {
							dataManifest.setTanggal(dataRequest.get("tanggal") == "" ? null : formatter1.parse(dataRequest.getString("tanggal")));
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						if(routeJalurDetail.size() > 0)
				    	{
						for (int j = 0; j < routeJalurDetail.size(); j++) {
							TrxManifestDetail data = new TrxManifestDetail();
							data.setId(id);
							data.setDetailId(routeJalurDetail.getJSONObject(j).getInt("detailId"));
							data.setOrderId(routeJalurDetail.getJSONObject(j).getString("orderId"));
							data.setCabangIdFrom(routeJalurDetail.getJSONObject(j).has("mCabangIdFrom") == false ? null : routeJalurDetail.getJSONObject(j).getInt("mCabangIdFrom"));
							data.setAgenIdFrom(routeJalurDetail.getJSONObject(j).has("mAgenIdFrom") == false? null : routeJalurDetail.getJSONObject(j).getInt("mAgenIdFrom"));
							data.setAgenDetailIdFrom(routeJalurDetail.getJSONObject(j).has("mAgenDetailIdFrom")== false ? null : routeJalurDetail.getJSONObject(j).getInt("mAgenDetailIdFrom"));
							
							try {
								data.setTanggal(formatter1.parse(routeJalurDetail.getJSONObject(j).getString("tanggal")));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							data.setCabangIdTo(routeJalurDetail.getJSONObject(j).has("mCabangIdTo") == false ? null : routeJalurDetail.getJSONObject(j).getInt("mCabangIdTo"));
							data.setAgenIdTo(routeJalurDetail.getJSONObject(j).has("mAgenIdTo") == false ? null : routeJalurDetail.getJSONObject(j).getInt("mAgenIdTo"));
							data.setAgenDetailIdTo(routeJalurDetail.getJSONObject(j).has("mAgenDetailIdTo") == false? null : routeJalurDetail.getJSONObject(j).getInt("mAgenDetailIdTo"));
							data.setCompanyId(dataRequest.getString("companyId"));
							dataDetail.add(data);
						}

				    	}
			    	}
		    	});
	    	TrxManifestService.save(dataManifest);
			TrxManifestDetailService.deleteAll(id, dataRequest.getString("companyId"));
			if(dataDetail.size()>0)
			{
				TrxManifestDetailService.saveAll(dataDetail, id, dataRequest.getString("companyId"));
			}
			
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save Manifest Success");
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "Save Manifest Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}
	
	@PostMapping("/listManifest")
	public String listManifest(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		
		
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		
		final String url = "http://54.169.109.123:3004/api/v1/contacts?limit=all";
		RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("x-access-code", header);
	    HttpEntity<String> entity = new HttpEntity<String>(headers);
	    ResponseEntity<String> responsecustomer = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
	    JSONObject objdatacustomer = (JSONObject)JSONSerializer.toJSON(responsecustomer.getBody());
	    net.sf.json.JSONArray datacustomer = objdatacustomer.getJSONArray("data");
		
		logger.info("API == listManifest");
		logger.info("INPUT == " + dataRequest);
		try
		{
			UUID user_uuid = UUID.fromString(Helper.getUserId(header));
			
			List<Object[]> listManifest = TrxManifestService.listManifest(dataRequest.getString("companyId"), dataRequest.getString("tanggal"), user_uuid);
			if(listManifest.size()>0)
			{
				listManifest.stream().forEach(col->{
					JSONObject data = new JSONObject();
					data.put("id", col[0]);
					data.put("driverId", col[1]);
					data.put("fleetId", col[2]);
					data.put("manifestNo", col[3]);
					data.put("fleetName", col[4] == null ? "" : col[4]);
					data.put("cabangIdFrom", col[5] == null ? "" : col[5]);
					data.put("agenIdFrom", col[6] == null ? "" : col[6]);
					data.put("agenDetailIdFrom", col[7] == null ? "" : col[7]);
					data.put("cabangIdTo", col[8] == null ? "" : col[8]);
					data.put("agenIdTo", col[9] == null ? "" : col[9]);
					data.put("agenDetailIdTo", col[10] == null ? "" : col[10]);
					data.put("fleetWeight", col[11] == null ? "" : col[11]);
					data.put("mWeightId", col[12] == null ? "" : col[12]);
					data.put("mWeightName", col[13] == null ? "" : col[13]);
					data.put("fleetVolume", col[14] == null ? "" : col[14]);
					data.put("mVolumeId", col[15] == null ? "" : col[15]);
					data.put("mVolumeName", col[16] == null ? "" : col[16]);
					data.put("tanggal", col[17] == null ? "" : col[17].toString());
					data.put("trxJalurHeaderId", col[18] == null ? "" : col[18]);
					data.put("cabangProvinsiIdFrom", col[19] == null ? "" : col[19]);
					data.put("cabangKotaIdFrom", col[20] == null ? "" : col[20]);
					data.put("agenProvinsiIdFrom", col[21] == null ? "" : col[21]);
					data.put("agenKotaIdFrom", col[22] == null ? "" : col[22]);
					data.put("cabangProvinsiIdTo", col[23] == null ? "" : col[23]);
					data.put("cabangKotaIdTo", col[24] == null ? "" : col[24]);
					data.put("agenProvinsiIdTo", col[25] == null ? "" : col[25]);
					data.put("agenKotaIdTo", col[26] == null ? "" : col[26]);
					List<JSONObject> datasdriverschedule = new ArrayList<>();
					List<Object[]> schedules = TrxManifestService.detailManifest(dataRequest.getString("companyId"), Integer.parseInt(col[0].toString()));
					schedules.stream().forEach(columndetail-> {
						JSONObject datadetail = new JSONObject();
						datadetail.put("orderId", columndetail[0] == null ? "" : columndetail[0]);
						datadetail.put("tanggal", columndetail[1] == null ? "" : columndetail[1].toString());
						datadetail.put("customer", columndetail[2] == null ? "" : columndetail[2]);
						datadetail.put("cabangId", columndetail[3] == null ? "" : columndetail[3]);
						datadetail.put("agenId", columndetail[4] == null ? "" : columndetail[4]);
						datadetail.put("agenDetailId", columndetail[5] == null ? "" : columndetail[5]);
						datadetail.put("serviceName", columndetail[6] == null ? "" : columndetail[6].toString() );
						datadetail.put("jumlahKoli", columndetail[7] == null ? "" : columndetail[7]);
						datadetail.put("weightJumlah", columndetail[8] == null ? "" : columndetail[8]);
						datadetail.put("weightName", columndetail[9] == null ? "" : columndetail[9]);
						datadetail.put("volumeJumlah", columndetail[10] == null ? "" : columndetail[10]);
						datadetail.put("volumeName", columndetail[11] == null ? "" : columndetail[11]);
						
						
						datasdriverschedule.add(datadetail);
					});
					data.put("detail", datasdriverschedule);
					datas.add(data);
				});
			}
			response.put("responseCode", "00");
		    response.put("responseDesc", "List Manifest Success");
		    response.put("responseData", datas);
		}
		catch(Exception e)
		{
			response.put("responseCode", "99");
		    response.put("responseDesc", "List Manifest Error");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
		}
		return response.toString();
	}

}
