package com.proacc.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proacc.entity.MJenispaket;
import com.proacc.helper.Helper;
import com.proacc.service.MJenispaketService;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

@RestController
@RequestMapping("/api")
public class MJenispaketController {
	private static final Logger logger = LoggerFactory.getLogger(MJenispaketController.class);
	
	@Autowired
	MJenispaketService mJenispaketService;
	@PostMapping("/getAllJenispaket/{paramAktif}")
	public String getAllJenispaket(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> jenispakets = mJenispaketService.getalljenispaket(paramAktif.toString().toLowerCase(), dataRequest.getString("jenispaketCompanyId"));
			jenispakets.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				data.put("jenispaketId", column[0]);
				data.put("jenispaketName", column[1] == null ? "" : column[1].toString());
				data.put("jenispaketDescription", column[2] == null ? "" : column[2].toString());
				data.put("jenispaketCreatedBy", column[3].toString());
				data.put("jenispaketCreatedAt", column[4].toString());
				data.put("jenispaketUpdatedBy", column[5] == null ? "" : column[5].toString());
				data.put("jenispaketUpdatedAt", column[6] == null ? "" : column[6].toString());
				data.put("jenispaketStatus", column[7].toString());
				data.put("jenispaketCompanyId", column[8]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data packing berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data packing gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/getJenispaketDetail/{paramAktif}")
	public String getJenispaketDetail(@RequestBody String request, @RequestHeader(value = "User-Access") String header,@PathVariable("paramAktif") String paramAktif)
	{
		JSONObject response = new JSONObject();
		List<JSONObject> datas = new ArrayList<>();
		JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
		try
		{
			List<Object[]> jenispakets = mJenispaketService.getjenispaket(paramAktif.toString().toLowerCase(), dataRequest.getInt("jenispaketId"), dataRequest.getString("jenispaketCompanyId"));
			jenispakets.stream().forEach(column-> {
				JSONObject data = new JSONObject();
				data.put("jenispaketId", column[0]);
				data.put("jenispaketName", column[1] == null ? "" : column[1].toString());
				data.put("jenispaketDescription", column[2] == null ? "" : column[2].toString());
				data.put("jenispaketCreatedBy", column[3].toString());
				data.put("jenispaketCreatedAt", column[4].toString());
				data.put("jenispaketUpdatedBy", column[5] == null ? "" : column[5].toString());
				data.put("jenispaketUpdatedAt", column[6] == null ? "" : column[6].toString());
				data.put("jenispaketStatus", column[7].toString());
				data.put("jenispaketCompanyId", column[8]);
				datas.add(data);
			});
			response.put("responseCode", "00");
			response.put("responseDesc", "data packing berhasil didapat");
			response.put("responseData", datas);
		}
	    catch(Exception e)
	    {
	    	response.put("responseCode", "99");
		    response.put("responseDesc", "data packing gagal didapat");
		    response.put("responseError", e.getMessage());
		    logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/saveJenispaket")
	public String saveJenispaket(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	int id = 0;
	    	List<Object[]> dataNext = mJenispaketService.nextvalMJenispaket();
	    	id = Integer.parseInt(String.valueOf(dataNext.get(0)));
	    	MJenispaket data = new MJenispaket();
	    	data.setJenispaketId(id);
	    	data.setJenispaketName(dataRequest.get("jenispaketName") == "" ? null : dataRequest.getString("jenispaketName"));
	    	data.setJenispaketDescription(dataRequest.get("jenispaketDescription") == "" ? null : dataRequest.getString("jenispaketDescription"));
	    	data.setJenispaketCreatedBy(user_uuid);
	    	data.setJenispaketCreatedAt(localDateTime2);
	    	data.setJenispaketStatus(dataRequest.get("jenispaketStatus") == "" ? null : dataRequest.getBoolean("jenispaketStatus"));
	    	data.setJenispaketCompanyId(dataRequest.get("jenispaketCompanyId") == "" ? null : dataRequest.getString("jenispaketCompanyId"));
	    	mJenispaketService.savejenispaket(data);
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Save jenis paket success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Save jenis paket failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
	@PostMapping("/updateJenispaket")
	public String updateJenispaket(@RequestBody String request, @RequestHeader(value = "User-Access") String header)
	{
		JSONObject response = new JSONObject();
	    JSONObject dataRequest = (JSONObject)JSONSerializer.toJSON(request);
	    try
	    {
	    	UUID user_uuid = UUID.fromString(Helper.getUserId(header));
	    	LocalDateTime localDateTime = LocalDateTime.now();
	    	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a");
	    	String formatDateTime = localDateTime.format(formatter);
	    	LocalDateTime localDateTime2 = LocalDateTime.parse(formatDateTime, formatter);
	    	Optional<MJenispaket> dataupdate = mJenispaketService.getdetail(dataRequest.getInt("jenispaketId"), dataRequest.getString("jenispaketCompanyId"));
	    	if(dataupdate.isPresent())
	    	{
	    		if(dataRequest.containsValue(dataRequest.getString("jenispaketName")))
	    		{
	    			((MJenispaket)dataupdate.get()).setJenispaketName(dataRequest.getString("jenispaketName"));
	    		}
	    		if(dataRequest.containsValue(dataRequest.getString("jenispaketDescription")))
	    		{
	    			((MJenispaket)dataupdate.get()).setJenispaketDescription(dataRequest.getString("jenispaketDescription"));
	    		}
	    		((MJenispaket)dataupdate.get()).setJenispaketUpdatedBy(user_uuid);
	    		((MJenispaket)dataupdate.get()).setJenispaketUpdatedAt(localDateTime2);
	    		((MJenispaket)dataupdate.get()).setJenispaketStatus(dataRequest.getBoolean("jenispaketStatus"));
	    		mJenispaketService.savejenispaket(dataupdate.get());
	    	}
	    	response.put("responseCode", "00");
		    response.put("responseDesc", "Update jenis paket success");
	    }
	    catch(Exception e)
	    {
	    	 response.put("responseCode", "99");
		     response.put("responseDesc", "Update jenis paket failed");
		     response.put("responseError", e.getMessage());
		     logger.info("ERROR #### " + e.getMessage());
	    }
	    return response.toString();
	}
}
