package com.proacc.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxHistroyTracking;
import com.proacc.entity.TrxInbound;
import com.proacc.entity.TrxInvoiceHeader;

@Component
public interface TrxInvoiceHeaderService {

	List<Object[]> getId();

	List<Object[]> listOpenInvoice(UUID user_uuid, String paramAktif, int i, String string);

	void save(TrxInvoiceHeader data);

	Optional<TrxInvoiceHeader> getDetail(int int1, String string);

	List<Object[]> listPaidInvoice(UUID user_uuid, String paramAktif, String string);


}
