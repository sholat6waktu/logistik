package com.proacc.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxHistroyTracking;
import com.proacc.entity.TrxInbound;

@Component
public interface TrxInboundService {

	List<Object[]> getId();

	void save(TrxInbound dataInboundHistory);

	List<Object[]> getList(String date, String order, String koli, String weight, String header);


}
