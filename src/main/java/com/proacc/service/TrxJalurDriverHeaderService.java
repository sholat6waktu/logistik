package com.proacc.service;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface TrxJalurDriverHeaderService {

	List<Object[]> getDetail(String lowerCase, String string, int int1);

	List<Object[]> nextval();

}
