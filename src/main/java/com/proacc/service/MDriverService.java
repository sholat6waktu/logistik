package com.proacc.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.proacc.entity.MDriver;

public interface MDriverService {
	List<Object[]> nextvalMDriver();
	List<Object[]> getallDriver(String lowerCase, LocalDate driverDate, String compId, UUID user_uuid);
	List<Object[]> getDriver(String lowerCase, LocalDate driverDate, Integer driverId, String compId);
	List<Object[]> getDriverAlltime(String lowerCase, UUID customer, String compId);
	public void saveDriver(MDriver mDriver);
	Optional <MDriver> getdetail(Integer Id, String Company);
}
