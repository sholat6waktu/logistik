package com.proacc.service;

import java.util.List;

public interface MWeightService {
	List<Object[]> getallWeight(String lowerCase, String compId);
	List<Object[]> getWeight(String lowerCase, Integer WeightId, String compId);
}
