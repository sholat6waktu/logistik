package com.proacc.service;

import java.util.List;
import java.util.Optional;

import com.proacc.entity.TrxNumber;

public interface TrxNumberService {
	List<Object[]> getallNumber(String lowerCase, String compId);
	List<Object[]> getNumber(String lowerCase, Integer NumberId, String compId);
	public void saveNumber(TrxNumber trxNumber);
	Optional <TrxNumber> getdetail(Integer Id, String Company);
}
