package com.proacc.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.proacc.entity.TrxDriverschedule;

public interface TrxDriverscheduleService {

	List<Object[]> getalldriver(String lowerCase, LocalDate driverDate, String compId, UUID user_uuid);
	List<Object[]> getallDriverschedule(String lowerCase, String compId);
	List<Object[]> getallDriverschedulebydriver(String lowerCase, String compId, Integer driverId);
	List<Object[]> getallDriverschedulebydriverbystatus(String lowerCase, String compId, Integer driverId);
	List<Object[]> getDriverschedulebyorder(String lowerCase, String OrderId, String compId);
	List<Object[]> availablecapacityberat(Integer driverid, String status, String compId);
	List<Object[]> availablecapacityvolume(Integer driverid, String status, String compId);
	List<Object[]> schedulecheck(Integer driverId, String timefrom, String timeto, String compId);
	List<Object[]> getDriverschedulebytracking(String lowerCase, String OrderId, Integer trackingfrom, Integer trackingto, String compId);
	List<Object[]> getDriverschedulebytrackingtocheckstat(String lowerCase, String OrderId, Integer trackingto, String compId);
	List<Object[]> getmyinbound(String lowerCase, UUID customer,  String compId, String kotato, String kurirname, String orderdate, String conno);
	List<Object[]> getmyoutbound(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno);
	List<Object[]> getmynotif(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno);
	public void savedriverschedule(TrxDriverschedule trxDriverschedule);
	public void savealldriverschedule(ArrayList <TrxDriverschedule> trxDriverscheduleArray);
	Optional <TrxDriverschedule> getdetail(String OrderId, Integer Id, String Company);
	public void updatedriver(String OrderId, Integer trackto, String Company, String statsout, String statsin);
	public void updatedriverstatusbyorder(String OrderId, String compid);
	void delete(int int1, String string, String string2);
	List<Object[]> cekKotaTujuan(Integer mcabangId, Integer mAgenId, Integer mAgenDetailId, String string, String string2);
	Optional<TrxDriverschedule> getDetail(String string, int int1, String string2);
	List<Object[]> getLastDriverSchedule(String string, String string2);
	List<Object[]> getalldriverTracking(String lowerCase, LocalDate parse, String string, UUID user_uuid);
}
