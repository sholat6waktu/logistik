package com.proacc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.MRequestFleetDetail;

@Component
public interface MRequestFleetDetailService {

	List<Object[]> getAll(int int1, String string);

	Optional<MRequestFleetDetail> getdetailById(int parseInt, int parseInt2, String string);

}
