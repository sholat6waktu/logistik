package com.proacc.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.proacc.entity.MFleet;

public interface MFleetService {
	List<Object[]> nextvalMFleet();
	List<Object[]> getallFleet(String lowerCase, String compId, UUID user_uuid);
	List<Object[]> getFleet(String lowerCase, Integer fleetId, String compId);
	public void saveFleet(MFleet mFleet); 
	Optional<MFleet> getdetail(Integer Id, String Company);
}
