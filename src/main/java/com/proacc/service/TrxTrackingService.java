package com.proacc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.proacc.entity.TrxTracking;

public interface TrxTrackingService {
	List<Object[]> getallTracking(String lowerCase, String compId);
	List<Object[]> getTracking(String lowerCase, String orderId, Integer trackingId, String compId);
	List<Object[]> getTrackingViewOngoing(String lowerCase, String compId);
	List<Object[]> getTrackingViewDone(String lowerCase, String compId);
	List<Object[]> getTrackingSettingOngoing(String lowerCase, String compId, String useruuid);
	List<Object[]> getTrackingSettingDone(String lowerCase, String compId);
	List<Object[]> getRoute(Integer kotaFrom, Integer KotaTo, String CompanyId);
	List<Object[]> getTrackingByOrder(String lowerCase, String orderId, String compId);
	public void saveAllTracking(ArrayList <TrxTracking> trxTrackingArray);
	Optional <TrxTracking> getdetail(String orderid, Integer trackingId, String compId);
	Optional <TrxTracking> getupdate(String orderid, String compId);
	public void updatestrackingtatusbyorder(String OrderId, String compId);
	List<Object[]> getRouteKhususGetFromCoverAreaAgent(Integer getmKotaId, Integer tokota, String string);
	void saveTracking(TrxTracking trxTracking);
	void updatestrackingtatusbyorderCancell(String string, String string2);
	List<Object[]> getAgen(Integer fromkota, Integer tokota, String string);
	List<Object[]> getCabang(Integer fromkota, Integer tokota, String string);
	List<Object[]> getLastTrackingId(String string, String string2);
	void deleteTracking(String string, Integer typeTambahan, String string2);
	void updateId(Integer trxTracking, Integer sebelum, String company, String orderId);
}
