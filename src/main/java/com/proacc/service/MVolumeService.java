package com.proacc.service;

import java.util.List;


public interface MVolumeService {

	List<Object[]> getallVolume(String lowerCase, String compId);
	List<Object[]> getVolume(String lowerCase, Integer volumeId, String compId);
}
