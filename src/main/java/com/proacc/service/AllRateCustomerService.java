package com.proacc.service;

import java.util.ArrayList;

import com.proacc.entity.MRateCustomer;
import com.proacc.entity.MRateHarga;

public interface AllRateCustomerService {

	void saveallRateCustomer(MRateCustomer mRateCustomer, ArrayList <MRateHarga> mRatehargaArray);
}
