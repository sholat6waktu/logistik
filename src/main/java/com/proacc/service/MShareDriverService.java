package com.proacc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.proacc.entity.MShareDriver;

@Component
public interface MShareDriverService {

	List<Object[]> detailShareDriver(int int1, String string, String paramAktif);

	void saveAll(ArrayList<MShareDriver> dataDetailRequest);

	void updateStatusFalse(int id, int driverId, String string);

}
