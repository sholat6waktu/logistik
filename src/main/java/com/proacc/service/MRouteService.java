package com.proacc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.MRoute;

@Component
public interface MRouteService {

	List<Object[]> listRouteByStatusAndCreatedBy(String lowerCase, String string, String string2);

	Optional<MRoute> detailRoute(int int1, String string);

	List<Object[]> detailJalur(int int1, String string, String lowerCase);

	List<Object[]> detailJalurDetail(int int1, String string, String lowerCase, Integer routeJalurId);

	List<Object[]> nextvalRoute();

	void save(MRoute route);

	Optional<MRoute> getDetail(int int1, String string);


}
