package com.proacc.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxManifestDetail;

@Component
public interface TrxManifestDetailService {

	void saveAll(List<TrxManifestDetail> dataDetail, int id, String string);

	void deleteAll(int id, String string);



}
