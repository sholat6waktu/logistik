package com.proacc.service;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.proacc.entity.MRateCustomer;
import com.proacc.entity.MRateHarga;
import com.proacc.entity.MRoute;
import com.proacc.entity.MRouteJalur;
import com.proacc.entity.MRouteJalurDetail;

@Component
public interface AllRouteService {

	void saveallRoute(MRoute route, ArrayList <MRouteJalur> routeJalur, ArrayList<MRouteJalurDetail> routeJalurDetail);
}
