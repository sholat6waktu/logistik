package com.proacc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxHistroyTracking;
import com.proacc.entity.TrxInbound;
import com.proacc.entity.TrxInvoiceDetail;

@Component
public interface TrxInvoiceDetailService {

	List<Object[]> detailInvoice(String paramAktif, String string, int invoice);

	void saveDetail(ArrayList<TrxInvoiceDetail> dataDetail);

	Optional<TrxInvoiceDetail> getDetail(int parseInt, int parseInt2, String string);

	void save(TrxInvoiceDetail trxInvoiceDetail);



}
