package com.proacc.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.proacc.entity.TrxOrder;

public interface TrxOrderService {
	List<Object[]> getallOrder(String lowerCase, String compId, String status, UUID customer);
	List<Object[]> getdeliveryOrder(String lowerCase, String compId);
	List<Object[]> getOrder(String lowerCase, String OrderId, String compId);
	List<Object[]> getIncomingOrder(String lowerCase, String OrderId, String compId);
	List<Object[]> getmyOrder(String lowerCase, UUID customerFrom, String compId);
	List<Object[]> getmyinbound(String lowerCase, UUID customer,  String compId, String kotato, String kurirname, String orderdate, String conno);
	List<Object[]> getmyoutbound(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno);
	List<Object[]> getmynotif(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno);
	List<Object[]> estcost(Integer customerId, Integer cityFrom, Integer cityTo, Integer service, String vehicle, Float weight, String company);
	public void saveOrder(TrxOrder trxOrder);
	Optional <TrxOrder> getdetail(String Id, String Company);
	List<Object[]> getComplete(String string2, UUID user_uuid);
}
