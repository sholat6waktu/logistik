package com.proacc.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.proacc.entity.MRequestFleet;

@Component
public interface MRequestFleetService {

	List<Object[]> detailRequest(String lowerCase, String lowerCase2, UUID user_uuid, String string, Integer id);

	List<Object[]> listRequest(String lowerCase, UUID user_uuid, String string);

	List<Object[]> nextvalRequestFleet();

	Optional<MRequestFleet> getDetail(int int1, String string);


}
