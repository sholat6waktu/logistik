package com.proacc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxJalurDriver;

@Component
public interface TrxJalurDriverService {

	Optional<TrxJalurDriver> getDetail(Integer trxDriverjalurheaderId, Integer trxDriverjalurId,
			String trxTrackingCompanyId);

	List<Object[]> findByJalurHeaderId(int parseInt, String string);

}
