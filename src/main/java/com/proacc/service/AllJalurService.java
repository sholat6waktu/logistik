package com.proacc.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxJalurDriver;
import com.proacc.entity.TrxJalurDriverHeader;

@Component
public interface AllJalurService {

	void saveAll(TrxJalurDriverHeader data, List<TrxJalurDriver> dataDetailTrx);

}
