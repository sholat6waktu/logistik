package com.proacc.service;

import java.util.List;

public interface MLwhService {

	List<Object[]> getallLwh(String lowerCase, String compId);
	List<Object[]> getLwh(String lowerCase, Integer lwhId, String compId);
}
