package com.proacc.service;

import java.util.List;
import java.util.Optional;

import com.proacc.entity.MCabang;

public interface MCabangService {
	List<Object[]> nextvalMCabang();
	List<Object[]> getallCabang(String lowerCase, String compId);
	List<Object[]> getCabang(String lowerCase, Integer cabangId, String compId);
	List<Object[]> getdetailbyprovince(String lowerCase, Integer provinsiId, String compId);
	List<Object[]> getcabangbyprovince(String lowerCase, Integer provinsiId, String compId);
	public void saveCabang(MCabang mCabang);
	Optional<MCabang> getDetail(Integer Id, String Company);
	List<Object[]> getCoverArea(int parseInt, String string);
}
