package com.proacc.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxHistroyTracking;
import com.proacc.entity.TrxOutbound;

@Component
public interface TrxOutboundService {

	List<Object[]> getId();

	void save(TrxOutbound dataOutboundHistory);

	List<Object[]> getList(String date, String order, String koli, String weight, String header);

	void delete(String order, int from, int to, String company);


}
