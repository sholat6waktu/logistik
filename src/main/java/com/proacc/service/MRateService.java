package com.proacc.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.proacc.entity.MRate;

public interface MRateService {
	List<Object[]> nextvalMRate();
	List<Object[]> getallRate(String lowerCase, String compId);
	List<Object[]> getallRatebystats(String lowerCase, String compId, String stats);
	List<Object[]> getRate(String lowerCase, Integer RateId1, String compId);
	List<Object[]> getRatelastindex(String lowerCase, Integer RateId1, String compId);
	List<Object[]> getRateDetail(String lowerCase, Integer RateId1, Integer RateId2, String compId);
	List<Object[]> checkinsert(Integer cityfrom, Integer cityto, Integer service, String vehicle, String startdate, String enddate, String company);
	List<Object[]> checkupdate(Integer cityfrom, Integer cityto, Integer service, String vehicle, String startdate, String enddate, String company, Integer rateId2);
	public void saveallRate(ArrayList <MRate> mRateArray);
	Optional <MRate> getdetail(Integer Id1, Integer Id2, String Company);
	public void deleteRate(Integer Id1, Integer Id2, String Company);
}
