package com.proacc.service;

import java.util.ArrayList;

import com.proacc.entity.TrxDeliveryorder;
import com.proacc.entity.TrxKolidetail;
import com.proacc.entity.TrxNumber;
import com.proacc.entity.TrxNumbergen;
import com.proacc.entity.TrxOrder;
import com.proacc.entity.TrxPartdetail;
import com.proacc.entity.TrxTracking;

public interface AllOrderService {
	void saveallOrder(TrxNumber trxNumber, TrxNumbergen trxNumbergen, TrxOrder trxOrder, ArrayList<TrxKolidetail> trxKolidetailArray, ArrayList<TrxPartdetail> trxPartdetailArray, ArrayList <TrxTracking> trxTrackingArray);
	void savealldeliveryorder(TrxDeliveryorder deliveryorder, TrxOrder trxOrder);
}
