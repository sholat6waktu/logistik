package com.proacc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.proacc.entity.MAgendetail;

public interface MAgendetailService {
	List<Object[]> getallAgendetail(String lowerCase, Integer agenId, String compId);
	List<Object[]> getdetailprovinsiforupdate(String lowerCase, Integer agenId, String compId);
	List<Object[]> getdetailtransferpointforupdate(String lowerCase, Integer agenId, Integer provinsiId, String compId);
	List<Object[]> getdetailkotaforupdate(String lowerCase, Integer agenId, Integer provinsiId, Integer transferpointId, String compId);
	List<Object[]> getalldetail(String lowerCase, String compId);
	List<Object[]> getAgendetail(String lowerCase, Integer agendetailId, String compId);
	List<Object[]> getAgendetailbyprovince(String lowerCase, Integer provinceId, String compId);
	List<Object[]> getdetailall(Integer agenId, String compId);
	public void saveallAgendetail(ArrayList<MAgendetail> MAgendetailArray);
	Optional<MAgendetail> getdetail(Integer agenId, Integer agendetailId, String agendetailCompanyId);
	List<Object[]> getAgenByName(String header);
	List<Object[]> getAgenDetailByIdAndMKotaId(int parseInt, Integer fromkota);
	List<Object[]> getCustomerCityByAgenTransferPoint(String string, Integer fromkota);
	List<Object[]> getAgenByCityId(Integer fromkota, String string);
	List<Object[]> listAgen(String lowerCase, String string);
	List<Object[]> listDetail(String string, String string2);
	List<Object[]> listAgenKantor(String lowerCase, String string);
	List<Object[]> getCoverArea(int parseInt, int parseInt2, String string);
	List<Object[]> listkode(String paramAktif, String company);

}
