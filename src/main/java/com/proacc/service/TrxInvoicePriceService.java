package com.proacc.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxHistroyTracking;
import com.proacc.entity.TrxInbound;
import com.proacc.entity.TrxInvoicePrice;

@Component
public interface TrxInvoicePriceService {



	void saveDetail(ArrayList<TrxInvoicePrice> dataDetail);

	Optional<TrxInvoicePrice> getDetail(int parseInt, int parseInt2, String string);

	void save(TrxInvoicePrice trxInvoiceDetail);

	List<Object[]> detailPrice(String paramAktif, String company, int id);


}
