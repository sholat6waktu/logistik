package com.proacc.service;

import java.util.ArrayList;
import java.util.List;

import com.proacc.entity.TrxPartdetail;

public interface TrxPartdetailService {
	List<Object[]> getPart(String lowerCase, String orderId, Integer KoliId, String compId);
	public void saveallPart(ArrayList <TrxPartdetail> trxPartdetailArray);
}
