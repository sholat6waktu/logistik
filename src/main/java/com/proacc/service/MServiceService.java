package com.proacc.service;

import java.util.List;
import java.util.Optional;

import com.proacc.entity.MService;

public interface MServiceService {
	List<Object[]> nextvalMService();
	List<Object[]> getallservice(String lowerCase, String compId);
	List<Object[]> getservice(String lowerCase, Integer serviceId, String compId);
	public void saveservice(MService mService);
	Optional<MService> getdetail(Integer Id, String Company);
}
