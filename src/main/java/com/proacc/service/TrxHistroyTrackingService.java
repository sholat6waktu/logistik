package com.proacc.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxHistroyTracking;

@Component
public interface TrxHistroyTrackingService {

	List<Object[]> getLatestId(String string, String string2);

	void save(TrxHistroyTracking dataHistory);

	List<Object[]> getByOrderId(String string, String string2);

}
