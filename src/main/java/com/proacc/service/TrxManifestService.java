package com.proacc.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxManifest;

@Component
public interface TrxManifestService {

	List<Object[]> getManifest(int int1, Integer getmCabangId, Integer getmAgenId, Integer getmAgenDetailId,
			Integer getmCabangId2, Integer getmAgenId2, Integer getmAgenDetailId2, String orderId, String companyId);

	List<Object[]> getId();

	void save(TrxManifest dataManifest);

	List<Object[]> listManifest(String string, String string2, UUID user_uuid);

	List<Object[]> detailManifest(String string, int parseInt);

}
