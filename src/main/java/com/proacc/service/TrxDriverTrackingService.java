package com.proacc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.TrxDriverTracking;

@Component
public interface TrxDriverTrackingService {

	void save(TrxDriverTracking data1);

	Optional<TrxDriverTracking> getDetail(String string, Integer tmTrxTrackingIdFrom,
			String trxDriverscheduleCompanyId);

	List<Object[]> findByDriver(int parseInt, String string);

}
