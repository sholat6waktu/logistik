package com.proacc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.proacc.entity.MCabang;
import com.proacc.entity.MCabangdetail;

public interface MCabangdetailService {
	List<Object[]> getallCabangdetail(String lowerCase, Integer cabangId, String compId);
	List<Object[]> getdetailprovinsiforupdate(String lowerCase, Integer cabangId, String compId);
	List<Object[]> getdetailkotaforupdate(String lowerCase, Integer cabangId, Integer provinsiId, String compId);
	List<Object[]> getalldetail(String lowerCase, String compId);
	List<Object[]> getdetailall(Integer cabangId, String compId);
	List<Object[]> getCabangdetail(String lowerCase, Integer cabangdetailId, String compId);
	public void saveallCabangdetail(ArrayList<MCabangdetail> MCabangdetailArray);
	Optional<MCabangdetail> getdetail(Integer cabangId, Integer cabangdetailId, String cabangdetailCompanyId);
	List<Object[]> getCabangIdWithCityAndAgenId(int agenId, Integer fromkota);
	Optional<MCabang> getDetailCabang(int parseInt, String string);
	List<Object[]> getCabangIdByCityId(Integer fromkota, String string);
	List<Object[]> getCabangHeaderIdByCityId(Integer fromkota, String string);
	List<Object[]> checkActor(String string, UUID fromString);
	List<Object[]> getDetailKhususAgen(String lowerCase, int int1, String string);
	List<Object[]> getCoverArea(int parseInt, String string);
}
