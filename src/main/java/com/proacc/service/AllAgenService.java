package com.proacc.service;

import java.util.ArrayList;

import com.proacc.entity.MAgen;
import com.proacc.entity.MAgendetail;
import com.proacc.entity.MagenCoverArea;

public interface AllAgenService {

	void saveallAgen(MAgen mAgen, ArrayList<MAgendetail> MAgendetailArray, ArrayList<MagenCoverArea> dataCoverAreaHeader);
}
