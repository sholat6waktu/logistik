package com.proacc.service;

import java.util.List;
import java.util.Optional;

import com.proacc.entity.MJenispaket;

public interface MJenispaketService {
	List<Object[]> nextvalMJenispaket();
	List<Object[]> getalljenispaket(String lowerCase, String compId);
	List<Object[]> getjenispaket(String lowerCase, Integer jenispaketId, String compId);
	public void savejenispaket(MJenispaket mJenispaket);
	Optional<MJenispaket> getdetail(Integer Id, String Company);
}
