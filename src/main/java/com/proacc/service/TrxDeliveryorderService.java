package com.proacc.service;

import java.util.List;

import com.proacc.entity.TrxDeliveryorder;

public interface TrxDeliveryorderService {
	List<Object[]> getallDeliveryorder(String lowerCase, String compId);
	public void savedeliveryorder(TrxDeliveryorder Trxdeliveryorder);
	List<Object[]> searchDoneOrder(String string, String string2);
}
