package com.proacc.service;

import java.util.List;
import java.util.Optional;

import com.proacc.entity.MVehicletype;

public interface MVehicletypeService {
	List<Object[]> nextvalMVehicletype();
	List<Object[]> getallVehicletype(String lowerCase, String compId);
	List<Object[]> getVehicletype(String lowerCase, Integer vehicletypeId, String compId);
	public void saveVehicletype(MVehicletype mVehicletype);
	Optional <MVehicletype> getdetail(Integer Id, String Company);
}
