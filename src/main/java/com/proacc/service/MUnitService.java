package com.proacc.service;

import java.util.List;

public interface MUnitService {
	List<Object[]> getallUnit(String lowerCase, String compId);
	List<Object[]> getUnit(String lowerCase, Integer unitId, String compId);
}
