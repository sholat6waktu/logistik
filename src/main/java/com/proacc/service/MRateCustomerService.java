package com.proacc.service;

import java.util.List;
import java.util.Optional;

import com.proacc.entity.MRateCustomer;

public interface MRateCustomerService {
	List<Object[]> nextvalMRateCustomer();
	List<Object[]> getallRateCustomer(String lowerCase, String compId);
	List<Object[]> getRateCustomer(String lowerCase, Integer RateCustomerId, String compId);
	public void saveallrateharga(MRateCustomer mRateCustomer);
	Optional <MRateCustomer> getDetail(Integer RateCustomerId, String compId);;
}
