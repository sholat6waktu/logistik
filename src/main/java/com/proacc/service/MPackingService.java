package com.proacc.service;

import java.util.List;
import java.util.Optional;

import com.proacc.entity.MPacking;

public interface MPackingService {
	List<Object[]> nextvalMPacking();
	List<Object[]> getallpacking(String lowerCase, String compId);
	List<Object[]> getpacking(String lowerCase, Integer packingId, String compId);
	public void savepacking(MPacking mPacking);
	Optional<MPacking> getdetail(Integer Id, String Company);
}
