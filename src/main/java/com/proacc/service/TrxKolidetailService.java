package com.proacc.service;

import java.util.ArrayList;
import java.util.List;

import com.proacc.entity.TrxKolidetail;

public interface TrxKolidetailService {
	List<Object[]> getKoli(String lowerCase, String OrderId, String compId);
	public void saveallKoli(ArrayList<TrxKolidetail> trxKolidetailArray);
}
