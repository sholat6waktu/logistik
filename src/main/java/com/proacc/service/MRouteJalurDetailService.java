package com.proacc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.MRoute;
import com.proacc.entity.MRouteJalurDetail;
import com.proacc.entity.MagenCoverArea;

@Component
public interface MRouteJalurDetailService {

	void saveAll(ArrayList<MRouteJalurDetail> routeJalurDetail);

	List<Object[]> getAll(int int1, String string);

	Optional<MRouteJalurDetail> getDetail(int parseInt, int parseInt2, int parseInt3, String string);


}
