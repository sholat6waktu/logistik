package com.proacc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.proacc.entity.MRateHarga;

public interface MRateHargaService {
	List<Object[]> nextvalMRate();
	List<Object[]> getallRateHarga(String lowerCase, String compId);
	List<Object[]> getbyCustomerRateHarga(String lowerCase, Integer mRateCustomerId, String compId);
	List<Object[]> getRateHarga(String lowerCase, Integer RateHargaId1, String compId);
	List<Object[]> getRateDetail(String lowerCase, Integer RateHargaId1, Integer RateHargaId2, String compId);
	List<Object[]> nettdisc(Float price, String diskon, String nett);
	public void saverateharga(MRateHarga mRateharga);
	public void saveallrateharga(ArrayList <MRateHarga> mRatehargaArray);
	Optional <MRateHarga> getDetail(Integer mRateId1, Integer mRateId2, Integer ratehargaId1, Integer ratehargaId2, Integer mRatecustomerId, String ratehargaCompanyId);
}
