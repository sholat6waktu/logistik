package com.proacc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Component;
import com.proacc.entity.MagenCoverArea;

@Component
public interface MAgenCoverAreaService {
	List<Object[]> getAll(int int1, String string);
	Optional<MagenCoverArea> getDetail(int parseInt, int parseInt2, int parseInt3, String string);

} 
