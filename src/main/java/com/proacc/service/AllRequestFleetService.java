package com.proacc.service;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.proacc.entity.MRequestFleet;
import com.proacc.entity.MRequestFleetDetail;

@Component
public interface AllRequestFleetService {

	void saveAll(MRequestFleet data, ArrayList<MRequestFleetDetail> dataDetailRequest);

}
