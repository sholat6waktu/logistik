package com.proacc.service;

import java.util.List;
import java.util.Optional;

import com.proacc.entity.MAgen;

public interface MAgenService {
	List<Object[]> nextvalMAgen();
	List<Object[]> getallAgen(String lowerCase, String compId);
	List<Object[]> getAgen(String lowerCase, Integer agenId, String compId);
	public void saveAgen(MAgen mAgen);
	Optional<MAgen> getDetail(Integer Id, String Company);
}
