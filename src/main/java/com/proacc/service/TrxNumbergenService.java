package com.proacc.service;

import java.util.List;
import java.util.Optional;

import com.proacc.entity.TrxNumbergen;

public interface TrxNumbergenService {
	List<Object[]> getallNumbergen(String lowerCase, String compId);
	public void savenumbergen(TrxNumbergen trxNumbergen);
	Optional <TrxNumbergen> getdetail(Integer numberId, String numbergenId, String Company);
}
