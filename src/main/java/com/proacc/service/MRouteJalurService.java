package com.proacc.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.proacc.entity.MRoute;
import com.proacc.entity.MRouteJalur;

@Component
public interface MRouteJalurService {

	void saveAll(ArrayList<MRouteJalur> routeJalur);

	List<Object[]> getdetailall(int int1, String string);

	Optional<MRouteJalur> getdetailById(int parseInt, int parseInt2, String string);

	List<Object[]> listRouteJalur(String lowerCase, String string);


}
