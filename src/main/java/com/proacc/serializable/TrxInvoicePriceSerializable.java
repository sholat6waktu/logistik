package com.proacc.serializable;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class TrxInvoicePriceSerializable implements Serializable{

	@Column(name = "invoice_header_id")
	private Integer invoiceHeaderId;
	
	@Column(name = "invoice_detail_transaksi_id")
	private Integer invoiceDetailTransaksiId;
	
	@Column(name = "company_id")
	private String companyId;
	
	public TrxInvoicePriceSerializable() {}
	
	public TrxInvoicePriceSerializable(Integer invoiceHeaderId, Integer invoiceDetailTransaksiId, String companyId) 
	{
		this.invoiceHeaderId = invoiceHeaderId;
		this.invoiceDetailTransaksiId = invoiceDetailTransaksiId;
		this.companyId = companyId;
	}

	public Integer getInvoiceHeaderId() {
		return invoiceHeaderId;
	}

	public void setInvoiceHeaderId(Integer invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}

	public Integer getInvoiceDetailTransaksiId() {
		return invoiceDetailTransaksiId;
	}

	public void setInvoiceDetailTransaksiId(Integer invoiceDetailTransaksiId) {
		this.invoiceDetailTransaksiId = invoiceDetailTransaksiId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	
}
