package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxJalurDriverHeaderSerializable implements Serializable{

	@Column(name = "trx_driverjalurheader_id")
	private Integer trxDriverjalurHeaderId;
	

	@Column(name = "company_id")
	private String companyId;
	
	
	public TrxJalurDriverHeaderSerializable() {}
	
	public TrxJalurDriverHeaderSerializable(Integer trxDriverjalurHeaderId,String companyId) {
		this.trxDriverjalurHeaderId = trxDriverjalurHeaderId;
		this.companyId = companyId;
	}
	
	

	public Integer getTrxDriverjalurHeaderId() {
		return trxDriverjalurHeaderId;
	}

	public void setTrxDriverjalurHeaderId(Integer trxDriverjalurHeaderId) {
		this.trxDriverjalurHeaderId = trxDriverjalurHeaderId;
	}
	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
