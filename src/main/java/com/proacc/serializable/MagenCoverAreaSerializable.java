package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class MagenCoverAreaSerializable implements Serializable{

	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	@Column(name = "m_agendetail_id")
	private Integer MAagendetailId;
	
	@Column(name = "agen_cover_area_id")
	private Integer agenCoverAreaId;
	
	@Column(name = "agen_cover_area_company_id")
	private String agenCoverAreaCompany_id;
	
	public MagenCoverAreaSerializable() {}
	
	public MagenCoverAreaSerializable(Integer mAgenId, Integer MAagendetailId, Integer agenCoverAreaId, String agenCoverAreaCompany_id) {
		this.mAgenId = mAgenId;
		this.MAagendetailId = MAagendetailId;
		this.agenCoverAreaId = agenCoverAreaId;
		this.agenCoverAreaCompany_id = agenCoverAreaCompany_id;
	}

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getMAagendetailId() {
		return MAagendetailId;
	}

	public void setMAagendetailId(Integer mAagendetailId) {
		MAagendetailId = mAagendetailId;
	}

	public Integer getAgenCoverAreaId() {
		return agenCoverAreaId;
	}

	public void setAgenCoverAreaId(Integer agenCoverAreaId) {
		this.agenCoverAreaId = agenCoverAreaId;
	}

	public String getAgenCoverAreaCompany_id() {
		return agenCoverAreaCompany_id;
	}

	public void setAgenCoverAreaCompany_id(String agenCoverAreaCompany_id) {
		this.agenCoverAreaCompany_id = agenCoverAreaCompany_id;
	}
	
	
}
