package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxDriverscheduleSerializable implements Serializable{

	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Column(name = "trx_driverschedule_id")
	private Integer trxDriverscheduleId;
	
	@Column(name = "trx_driverschedule_company_id")
	private String trxDriverscheduleCompanyId;
	
	public TrxDriverscheduleSerializable() {}
	
	public TrxDriverscheduleSerializable(String tmTrxOrderId, Integer trxDriverscheduleId, String trxDriverscheduleCompanyId) {
		this.tmTrxOrderId = tmTrxOrderId;
		this.trxDriverscheduleId = trxDriverscheduleId;
		this.trxDriverscheduleCompanyId = trxDriverscheduleCompanyId;
	}

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getTrxDriverscheduleId() {
		return trxDriverscheduleId;
	}

	public void setTrxDriverscheduleId(Integer trxDriverscheduleId) {
		this.trxDriverscheduleId = trxDriverscheduleId;
	}

	public String getTrxDriverscheduleCompanyId() {
		return trxDriverscheduleCompanyId;
	}

	public void setTrxDriverscheduleCompanyId(String trxDriverscheduleCompanyId) {
		this.trxDriverscheduleCompanyId = trxDriverscheduleCompanyId;
	}
	


}
