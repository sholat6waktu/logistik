package com.proacc.serializable;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class TrxTrackingSerializable implements Serializable{

	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Column(name = "trx_tracking_id")
	private Integer trxTrackingId;
	
	@Column(name = "trx_tracking_company_id")
	private String trxTrackingCompanyId;
	
	public TrxTrackingSerializable() {}
	
	public TrxTrackingSerializable(String tmTrxOrderId, Integer trxTrackingId, String trxTrackingCompanyId) 
	{
		this.tmTrxOrderId = tmTrxOrderId;
		this.trxTrackingId = trxTrackingId;
		this.trxTrackingCompanyId = trxTrackingCompanyId;
	}
	
	public TrxTrackingSerializable(String tmTrxOrderId, String trxTrackingCompanyId) 
	{
		this.tmTrxOrderId = tmTrxOrderId;
		this.trxTrackingCompanyId = trxTrackingCompanyId;
	}

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getTrxTrackingId() {
		return trxTrackingId;
	}

	public void setTrxTrackingId(Integer trxTrackingId) {
		this.trxTrackingId = trxTrackingId;
	}

	public String getTrxTrackingCompanyId() {
		return trxTrackingCompanyId;
	}

	public void setTrxTrackingCompanyId(String trxTrackingCompanyId) {
		this.trxTrackingCompanyId = trxTrackingCompanyId;
	}
}
