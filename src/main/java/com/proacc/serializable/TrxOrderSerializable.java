package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxOrderSerializable implements Serializable{

	@Column(name = "trx_order_id")
	private String trxOrderId;
	
	@Column(name = "trx_order_company_id")
	private String trxOrderCompanyId;
	
	public TrxOrderSerializable() {}
	
	public TrxOrderSerializable(String trxOrderId, String trxOrderCompanyId) 
	{
		this.trxOrderId = trxOrderId;
		this.trxOrderCompanyId = trxOrderCompanyId;
	}

	public String getTrxOrderId() {
		return trxOrderId;
	}

	public void setTrxOrderId(String trxOrderId) {
		this.trxOrderId = trxOrderId;
	}

	public String getTrxOrderCompanyId() {
		return trxOrderCompanyId;
	}

	public void setTrxOrderCompanyId(String trxOrderCompanyId) {
		this.trxOrderCompanyId = trxOrderCompanyId;
	}
}
