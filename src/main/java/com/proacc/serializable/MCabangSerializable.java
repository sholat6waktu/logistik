package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MCabangSerializable implements Serializable{
	@Column(name = "cabang_id")
	private Integer cabangId;
	
	@Column(name = "cabang_company_id")
	private String cabangCompanyId;
	
	public MCabangSerializable() {}

	public MCabangSerializable(Integer cabangId, String cabangCompanyId) {
		this.cabangId = cabangId;
		this.cabangCompanyId = cabangCompanyId;
	}
	
	public Integer getCabangId() {
		return cabangId;
	}

	public void setCabangId(Integer cabangId) {
		this.cabangId = cabangId;
	}

	public String getCabangCompanyId() {
		return cabangCompanyId;
	}

	public void setCabangCompanyId(String cabangCompanyId) {
		this.cabangCompanyId = cabangCompanyId;
	}
}
