package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class MCabangdetailSerializable implements Serializable{
	@Column(name = "m_cabang_id")
	private Integer mCabangId;
	
	@Column(name = "cabangdetail_id")
	private Integer cabangdetailId;
	
	@Column(name = "cabangdetail_company_id")
	private String cabangdetailCompanyId;
	
	public MCabangdetailSerializable() {}
	
	public MCabangdetailSerializable(Integer mCabangId, Integer cabangdetailId, String cabangdetailCompanyId) {
		this.mCabangId = mCabangId;
		this.cabangdetailId = cabangdetailId;
		this.cabangdetailCompanyId = cabangdetailCompanyId;
	}

	public Integer getmCabangId() {
		return mCabangId;
	}

	public void setmCabangId(Integer mCabangId) {
		this.mCabangId = mCabangId;
	}

	public Integer getCabangdetailId() {
		return cabangdetailId;
	}

	public void setCabangdetailId(Integer cabangdetailId) {
		this.cabangdetailId = cabangdetailId;
	}

	public String getCabangdetailCompanyId() {
		return cabangdetailCompanyId;
	}

	public void setCabangdetailCompanyId(String cabangdetailCompanyId) {
		this.cabangdetailCompanyId = cabangdetailCompanyId;
	}
}
