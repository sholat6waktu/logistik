package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MJenispaketSerializable implements Serializable{
	@Column(name = "jenispaket_id")
	private Integer jenispaketId;
	
	@Column(name = "jenispaket_company_id")
	private String jenispaketCompanyId;

	public MJenispaketSerializable() {}
	
	public MJenispaketSerializable(Integer jenispaketId, String jenispaketCompanyId) {
		this.jenispaketId = jenispaketId;
		this.jenispaketCompanyId = jenispaketCompanyId;
	}
	
	public Integer getJenispaketId() {
		return jenispaketId;
	}

	public void setJenispaketId(Integer jenispaketId) {
		this.jenispaketId = jenispaketId;
	}

	public String getJenispaketCompanyId() {
		return jenispaketCompanyId;
	}

	public void setJenispaketCompanyId(String jenispaketCompanyId) {
		this.jenispaketCompanyId = jenispaketCompanyId;
	}
}
