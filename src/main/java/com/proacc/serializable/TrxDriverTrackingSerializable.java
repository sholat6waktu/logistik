package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxDriverTrackingSerializable implements Serializable{	
	
	@Column(name = "id")
	private Integer id;

	@Column(name = "trx_order_id")
	private String tmTrxOrderId;

	
	@Column(name = "company_id")
	private String companyId;
	
	public TrxDriverTrackingSerializable() {}
	
	public TrxDriverTrackingSerializable(Integer id, String tmTrxOrderId,  String companyId) {
		this.id = id;
		this.tmTrxOrderId = tmTrxOrderId;
		this.companyId = companyId;
	}

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	

}
