package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MRequestFleetSerializable implements Serializable{

	@Column(name = "id")
	private Integer id;
	
	@Column(name = "company_id")
	private String companyId;
	
	public MRequestFleetSerializable() {}
	
	public MRequestFleetSerializable(Integer id, String companyId) 
	{
		this.id = id;
		this.companyId = companyId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
