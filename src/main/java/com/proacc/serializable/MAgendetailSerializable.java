package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MAgendetailSerializable implements Serializable{

	@Column(name = "m_agen_id")
	private Integer mAgenId;
	
	@Column(name = "agendetail_id")
	private Integer agendetailId;
	
	@Column(name = "agendetail_company_id")
	private String agendetailCompanyId;
	
	public MAgendetailSerializable() {}
	
	public MAgendetailSerializable(Integer agenId, Integer agendetailId, String agendetailCompanyId) {
		this.mAgenId = agenId;
		this.agendetailId = agendetailId;
		this.agendetailCompanyId = agendetailCompanyId;
	}

	public Integer getmAgenId() {
		return mAgenId;
	}

	public void setmAgenId(Integer mAgenId) {
		this.mAgenId = mAgenId;
	}

	public Integer getAgendetailId() {
		return agendetailId;
	}

	public void setAgendetailId(Integer agendetailId) {
		this.agendetailId = agendetailId;
	}

	public String getAgendetailCompanyId() {
		return agendetailCompanyId;
	}

	public void setAgendetailCompanyId(String agendetailCompanyId) {
		this.agendetailCompanyId = agendetailCompanyId;
	}
}
