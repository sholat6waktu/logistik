package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxInvoiceDetailSerailzable implements Serializable{

	@Column(name = "invoice_header_id")
	private Integer invoiceHeaderId;
	
	@Column(name = "company_id")
	private String companyId;
	
	@Column(name = "invoice_detail_id")
	private Integer invoiceDetailId;
	
	public TrxInvoiceDetailSerailzable() {}
	
	public TrxInvoiceDetailSerailzable(Integer invoiceHeaderId, Integer invoiceDetailId, String companyId) 
	{
		this.invoiceHeaderId = invoiceHeaderId;
		this.invoiceDetailId = invoiceDetailId;
		this.companyId = companyId;
	}

	
	
	public Integer getInvoiceDetailId() {
		return invoiceDetailId;
	}

	public void setInvoiceDetailId(Integer invoiceDetailId) {
		this.invoiceDetailId = invoiceDetailId;
	}

	public Integer getInvoiceHeaderId() {
		return invoiceHeaderId;
	}

	public void setInvoiceHeaderId(Integer invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
