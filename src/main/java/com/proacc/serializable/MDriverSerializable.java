package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MDriverSerializable implements Serializable{

	@Column(name = "driver_id")
	private Integer driverId;
	
	@Column(name = "driver_company_id")
	private String driverCompanyId;
	
	public MDriverSerializable() {}
	
	public MDriverSerializable(Integer driverId, String driverCompanyId) 
	{
		this.driverId = driverId;
		this.driverCompanyId = driverCompanyId;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public String getDriverCompanyId() {
		return driverCompanyId;
	}

	public void setDriverCompanyId(String driverCompanyId) {
		this.driverCompanyId = driverCompanyId;
	}
}
