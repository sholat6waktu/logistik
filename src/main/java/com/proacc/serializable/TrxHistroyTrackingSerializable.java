package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxHistroyTrackingSerializable implements Serializable{

	@Column(name = "order_id")
	private String orderId;
	
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "company_id")
	private String companyId;
	
	public TrxHistroyTrackingSerializable() {}
	
	public TrxHistroyTrackingSerializable(String orderId, Integer id, String companyId) 
	{
		this.orderId = orderId;
		this.id = id;
		this.companyId = companyId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
