package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MAgenSerializable implements Serializable{

	@Column(name = "agen_id")
	private Integer agenId;
	
	@Column(name = "agen_company_id")
	private String agenCompanyId;
	
	public MAgenSerializable() {}
	
	public MAgenSerializable(Integer agenId, String agenCompanyId) 
	{
		this.agenId = agenId;
		this.agenCompanyId = agenCompanyId;
	}

	public Integer getAgenId() {
		return agenId;
	}

	public void setAgenId(Integer agenId) {
		this.agenId = agenId;
	}

	public String getAgenCompanyId() {
		return agenCompanyId;
	}

	public void setAgenCompanyId(String agenCompanyId) {
		this.agenCompanyId = agenCompanyId;
	}
}
