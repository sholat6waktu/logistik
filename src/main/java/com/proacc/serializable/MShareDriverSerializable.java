package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MShareDriverSerializable implements Serializable{

	@Column(name = "id")
	private Integer id;
	
	@Column(name = "m_driver_id")
	private Integer mDriverId;
	
	@Column(name = "company_id")
	private String companyId;
	
	public MShareDriverSerializable() {}
	
	public MShareDriverSerializable(Integer id, Integer mDriverId, String companyId)
	{
		this.id = id;
		this.mDriverId = mDriverId;
		this.companyId = companyId;
	}
	

	
	

	public Integer getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(Integer mDriverId) {
		this.mDriverId = mDriverId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
