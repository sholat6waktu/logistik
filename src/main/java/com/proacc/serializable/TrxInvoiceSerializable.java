package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxInvoiceSerializable implements Serializable{

	@Column(name = "invoice_header_id")
	private Integer invoiceHeaderId;
	
	@Column(name = "company_id")
	private String companyId;
	
	public TrxInvoiceSerializable() {}
	
	public TrxInvoiceSerializable(Integer invoiceHeaderId, String companyId) 
	{
		this.invoiceHeaderId = invoiceHeaderId;
		this.companyId = companyId;
	}

	
	
	public Integer getInvoiceHeaderId() {
		return invoiceHeaderId;
	}

	public void setInvoiceHeaderId(Integer invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
