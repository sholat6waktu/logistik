package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxNumberSerializable implements Serializable{

	@Column(name = "trx_number_id")
	private Integer trxNumberId;
	
	@Column(name = "trx_number_company_id")
	private String trxNumberCompanyId;
	
	public TrxNumberSerializable() {}
	
	public TrxNumberSerializable(Integer trxNumberId, String trxNumberCompanyId) 
	{
		this.trxNumberId = trxNumberId;
		this.trxNumberCompanyId = trxNumberCompanyId;
	}

	public Integer getTrxNumberId() {
		return trxNumberId;
	}

	public void setTrxNumberId(Integer trxNumberId) {
		this.trxNumberId = trxNumberId;
	}

	public String getTrxNumberCompanyId() {
		return trxNumberCompanyId;
	}

	public void setTrxNumberCompanyId(String trxNumberCompanyId) {
		this.trxNumberCompanyId = trxNumberCompanyId;
	}
}