package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MPackingSerializable implements Serializable{

	@Column(name = "packing_id")
	private Integer packingId;
	
	@Column(name = "packing_company_id")
	private String packingCompanyId;
	
	public MPackingSerializable() {}
	
	public MPackingSerializable(Integer packingId, String packingCompanyId) {
		this.packingId = packingId;
		this.packingCompanyId =packingCompanyId;
	}

	public Integer getPackingId() {
		return packingId;
	}

	public void setPackingId(Integer packingId) {
		this.packingId = packingId;
	}

	public String getPackingCompanyId() {
		return packingCompanyId;
	}

	public void setPackingCompanyId(String packingCompanyId) {
		this.packingCompanyId = packingCompanyId;
	}
}
