package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MRateSerializable implements Serializable{

	@Column(name = "rate_id1")
	private Integer rateId1;
	
	@Column(name = "rate_id2")
	private Integer rateId2;
	
	@Column(name = "rate_company_id")
	private String rateCompanyId;
	
	public MRateSerializable() {}
	
	public MRateSerializable(Integer rateId1, Integer rateId2, String rateCompanyId) 
	{
		this.rateId1 = rateId1;
		this.rateId2 = rateId2;
		this.rateCompanyId = rateCompanyId;
	}

	public Integer getRateId1() {
		return rateId1;
	}

	public void setRateId1(Integer rateId1) {
		this.rateId1 = rateId1;
	}

	public Integer getRateId2() {
		return rateId2;
	}

	public void setRateId2(Integer rateId2) {
		this.rateId2 = rateId2;
	}

	public String getRateCompanyId() {
		return rateCompanyId;
	}

	public void setRateCompanyId(String rateCompanyId) {
		this.rateCompanyId = rateCompanyId;
	}
}
