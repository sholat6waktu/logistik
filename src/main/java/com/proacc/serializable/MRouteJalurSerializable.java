package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class MRouteJalurSerializable implements Serializable{

	@Column(name = "m_route_id")
	private Integer MRouteId;
	

	@Column(name = "route_jalur_id")
	private Integer routeJalurId;
	
	@Column(name = "route_jalur_company_id")
	private String routeJalurCompanyId;
	
	public MRouteJalurSerializable() {}
	
	public MRouteJalurSerializable(Integer MRouteId, Integer routeJalurId, String routeJalurCompanyId) 
	{
		this.MRouteId = MRouteId;
		this.routeJalurId = routeJalurId;
		this.routeJalurCompanyId = routeJalurCompanyId;
	}

	public Integer getMRouteId() {
		return MRouteId;
	}

	public void setMRouteId(Integer mRouteId) {
		MRouteId = mRouteId;
	}

	public Integer getRouteJalurId() {
		return routeJalurId;
	}

	public void setRouteJalurId(Integer routeJalurId) {
		this.routeJalurId = routeJalurId;
	}

	public String getRouteJalurCompanyId() {
		return routeJalurCompanyId;
	}

	public void setRouteJalurCompanyId(String routeJalurCompanyId) {
		this.routeJalurCompanyId = routeJalurCompanyId;
	}


	
}
