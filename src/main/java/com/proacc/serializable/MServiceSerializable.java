package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MServiceSerializable implements Serializable{

	@Column(name = "service_id")
	private Integer serviceId;
	
	@Column(name = "service_company_id")
	private String serviceCompanyId;
	
	public MServiceSerializable() {}
	
	public MServiceSerializable(Integer serviceId, String serviceCompanyId)
	{
		this.serviceId = serviceId;
		this.serviceCompanyId = serviceCompanyId;
	}

	public Integer getServiceId() {
		return serviceId;
	}

	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceCompanyId() {
		return serviceCompanyId;
	}

	public void setServiceCompanyId(String serviceCompanyId) {
		this.serviceCompanyId = serviceCompanyId;
	}
}
