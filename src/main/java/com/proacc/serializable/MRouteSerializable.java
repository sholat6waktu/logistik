package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MRouteSerializable implements Serializable{

	@Column(name = "route_id")
	private Integer routeId;
	
	@Column(name = "route_company_id")
	private String routeCompanyId;
	
	public MRouteSerializable() {}
	
	public MRouteSerializable(Integer routeId, String routeCompanyId) 
	{
		this.routeId = routeId;
		this.routeCompanyId = routeCompanyId;
	}

	public Integer getRouteId() {
		return routeId;
	}

	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}

	public String getRouteCompanyId() {
		return routeCompanyId;
	}

	public void setRouteCompanyId(String routeCompanyId) {
		this.routeCompanyId = routeCompanyId;
	}

	
}
