package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxKolidetailSerializable implements Serializable{

	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Column(name = "trx_kolidetail_id")
	private Integer trxKolidetailId;
	
	@Column(name = "trx_kolidetail_company_id")
	private String trxKolidetailCompanyId;
	
	public TrxKolidetailSerializable() {}
	
	public TrxKolidetailSerializable(String tmTrxOrderId, Integer trxKolidetailId, String trxKolidetailCompanyId) 
	{
		this.tmTrxOrderId = tmTrxOrderId;
		this.trxKolidetailId = trxKolidetailId;
		this.trxKolidetailCompanyId = trxKolidetailCompanyId;
	}

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getTrxKolidetailId() {
		return trxKolidetailId;
	}

	public void setTrxKolidetailId(Integer trxKolidetailId) {
		this.trxKolidetailId = trxKolidetailId;
	}

	public String getTrxKolidetailCompanyId() {
		return trxKolidetailCompanyId;
	}

	public void setTrxKolidetailCompanyId(String trxKolidetailCompanyId) {
		this.trxKolidetailCompanyId = trxKolidetailCompanyId;
	}
}
