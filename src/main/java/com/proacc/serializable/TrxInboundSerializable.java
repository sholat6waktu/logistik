package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxInboundSerializable implements Serializable{

	@Column(name = "report_inbound_id")
	private Integer reportInboundId;
	
	@Column(name = "company_id")
	private String companyId;
	
	public TrxInboundSerializable() {}
	
	public TrxInboundSerializable(Integer reportInboundId, String companyId) 
	{
		this.reportInboundId = reportInboundId;
		this.companyId = companyId;
	}

	public Integer getReportInboundId() {
		return reportInboundId;
	}

	public void setReportInboundId(Integer reportInboundId) {
		this.reportInboundId = reportInboundId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
