package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxNumbergenSerializable implements Serializable{

	@Column(name = "tm_trx_number_id")
	private Integer tmTrxNumberId;
	
	@Column(name = "trx_numbergen_id")
	private String trxNumbergenId;
	
	@Column(name = "trx_numbergen_Company_id")
	private String trxNumbergenCompanyId;
	
	public TrxNumbergenSerializable() {}
	
	public TrxNumbergenSerializable(Integer tmTrxNumberId, String trxNumbergenId, String trxNumbergenCompanyId) 
	{
		this.tmTrxNumberId = tmTrxNumberId;
		this.trxNumbergenId = trxNumbergenId;
		this.trxNumbergenCompanyId = trxNumbergenCompanyId;
	}

	public Integer getTmTrxNumberId() {
		return tmTrxNumberId;
	}

	public void setTmTrxNumberId(Integer tmTrxNumberId) {
		this.tmTrxNumberId = tmTrxNumberId;
	}

	public String getTrxNumbergenId() {
		return trxNumbergenId;
	}

	public void setTrxNumbergenId(String trxNumbergenId) {
		this.trxNumbergenId = trxNumbergenId;
	}

	public String getTrxNumbergenCompanyId() {
		return trxNumbergenCompanyId;
	}

	public void setTrxNumbergenCompanyId(String trxNumbergenCompanyId) {
		this.trxNumbergenCompanyId = trxNumbergenCompanyId;
	}
}
