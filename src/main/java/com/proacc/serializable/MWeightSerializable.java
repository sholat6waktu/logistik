package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MWeightSerializable implements Serializable{

	@Column(name = "weight_id")
	private Integer weightId;
	
	@Column(name = "weight_company_id")
	private String weightCompanyId;
	
	public MWeightSerializable() {}
	
	public MWeightSerializable(Integer weightId, String weightCompanyId)
	{
		this.weightId = weightId;
		this.weightCompanyId = weightCompanyId;
	}

	public Integer getWeightId() {
		return weightId;
	}

	public void setWeightId(Integer weightId) {
		this.weightId = weightId;
	}

	public String getWeightCompanyId() {
		return weightCompanyId;
	}

	public void setWeightCompanyId(String weightCompanyId) {
		this.weightCompanyId = weightCompanyId;
	}
}
