package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxJalurDriverSerializable implements Serializable{

	@Column(name = "trx_driverjalurheader_id")
	private Integer trxDriverjalurHeaderId;
	
	@Column(name = "trx_driverjalur_id")
	private Integer trxDriverjalurId;
	

	@Column(name = "company_id")
	private String companyId;
	
	
	public TrxJalurDriverSerializable() {}
	
	public TrxJalurDriverSerializable(Integer trxDriverjalurHeaderId, Integer trxDriverjalurId, String companyId) {
		this.trxDriverjalurHeaderId = trxDriverjalurHeaderId;
		this.trxDriverjalurId = trxDriverjalurId;
		this.companyId = companyId;
	}
	
	

	public Integer getTrxDriverjalurHeaderId() {
		return trxDriverjalurHeaderId;
	}

	public void setTrxDriverjalurHeaderId(Integer trxDriverjalurHeaderId) {
		this.trxDriverjalurHeaderId = trxDriverjalurHeaderId;
	}

	public Integer getTrxDriverjalurId() {
		return trxDriverjalurId;
	}

	public void setTrxDriverjalurId(Integer trxDriverjalurId) {
		this.trxDriverjalurId = trxDriverjalurId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
