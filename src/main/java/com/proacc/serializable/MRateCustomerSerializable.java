package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MRateCustomerSerializable implements Serializable{

	@Column(name = "ratecustomer_id")
	private Integer ratecustomerId;
	
	@Column(name = "ratecustomer_company_id")
	private String ratecustomerCompanyId;
	
	public MRateCustomerSerializable() {}
	
	public MRateCustomerSerializable(Integer ratecustomerId, String ratecustomerCompanyId) 
	{
		this.ratecustomerId = ratecustomerId;
		this.ratecustomerCompanyId = ratecustomerCompanyId;
	}

	public Integer getRatecustomerId() {
		return ratecustomerId;
	}

	public void setRatecustomerId(Integer ratecustomerId) {
		this.ratecustomerId = ratecustomerId;
	}

	public String getRatecustomerCompanyId() {
		return ratecustomerCompanyId;
	}

	public void setRatecustomerCompanyId(String ratecustomerCompanyId) {
		this.ratecustomerCompanyId = ratecustomerCompanyId;
	}
}
