package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MVehicletypeSerializable implements Serializable{

	@Column(name = "vehicletype_id")
	private Integer vehicletypeId;
	
	@Column(name = "vehicletype_company_id")
	private String vehicletypeCompanyId;
	
	public MVehicletypeSerializable() {}
	
	public MVehicletypeSerializable(Integer vehicletypeId, String vehicletypeCompanyId) {
		this.vehicletypeId = vehicletypeId;
		this.vehicletypeCompanyId = vehicletypeCompanyId;
	}

	public Integer getVehicletypeId() {
		return vehicletypeId;
	}

	public void setVehicletypeId(Integer vehicletypeId) {
		this.vehicletypeId = vehicletypeId;
	}

	public String getVehicletypeCompanyId() {
		return vehicletypeCompanyId;
	}

	public void setVehicletypeCompanyId(String vehicletypeCompanyId) {
		this.vehicletypeCompanyId = vehicletypeCompanyId;
	}
	
}
