package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class MRateHargaSerializable implements Serializable{

	@Column(name = "m_rate_id1")
	private Integer mRateId1;
	
	@Column(name = "m_rate_id2")
	private Integer mRateId2;
	
	@Column(name = "rateharga_id1")
	private Integer ratehargaId1;
	
	@Column(name = "rateharga_id2")
	private Integer ratehargaId2;
	
	@Column(name = "m_ratecustomer_id")
	private Integer mRatecustomerId;
	
	@Column(name = "rateharga_company_id")
	private String ratehargaCompanyId;
	
	public MRateHargaSerializable() {}
	
	public MRateHargaSerializable(Integer mRateId1, Integer mRateId2, Integer ratehargaId1, Integer ratehargaId2, Integer mRatecustomerId, String ratehargaCompanyId) 
	{
		this.mRateId1 = mRateId1;
		this.mRateId2 = mRateId2;
		this.ratehargaId1 = ratehargaId1;
		this.ratehargaId2 = ratehargaId2;
		this.mRatecustomerId = mRatecustomerId;
		this.ratehargaCompanyId = ratehargaCompanyId;
	}

	public Integer getmRateId1() {
		return mRateId1;
	}

	public void setmRateId1(Integer mRateId1) {
		this.mRateId1 = mRateId1;
	}

	public Integer getmRateId2() {
		return mRateId2;
	}

	public void setmRateId2(Integer mRateId2) {
		this.mRateId2 = mRateId2;
	}

	public Integer getRatehargaId1() {
		return ratehargaId1;
	}

	public void setRatehargaId1(Integer ratehargaId1) {
		this.ratehargaId1 = ratehargaId1;
	}

	public Integer getRatehargaId2() {
		return ratehargaId2;
	}

	public void setRatehargaId2(Integer ratehargaId2) {
		this.ratehargaId2 = ratehargaId2;
	}

	public Integer getmRatecustomerId() {
		return mRatecustomerId;
	}

	public void setmRatecustomerId(Integer mRatecustomerId) {
		this.mRatecustomerId = mRatecustomerId;
	}

	public String getRatehargaCompanyId() {
		return ratehargaCompanyId;
	}

	public void setRatehargaCompanyId(String ratehargaCompanyId) {
		this.ratehargaCompanyId = ratehargaCompanyId;
	}
}