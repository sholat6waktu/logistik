package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxDeliveryorderSerializable implements Serializable{

	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Column(name = "trx_deliveryorder_company_id")
	private String trxDeliveryorderCompanyId;
	
	public TrxDeliveryorderSerializable() {}
	
	public TrxDeliveryorderSerializable(String tmTrxOrderId, String trxDeliveryorderCompanyId) 
	{
		this.tmTrxOrderId = tmTrxOrderId;
		this.trxDeliveryorderCompanyId = trxDeliveryorderCompanyId;
	}

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public String getTrxDeliveryorderCompanyId() {
		return trxDeliveryorderCompanyId;
	}

	public void setTrxDeliveryorderCompanyId(String trxDeliveryorderCompanyId) {
		this.trxDeliveryorderCompanyId = trxDeliveryorderCompanyId;
	}
}
