package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;

@Embeddable
public class MRouteJalurDetailSerializable implements Serializable{

	@Column(name = "m_route_id")
	private Integer MRouteId;
	

	@Column(name = "m_route_jalur_id")
	private Integer MRouteJalurId;
	
	@Column(name = "route_jalur_detail_id")
	private Integer routeJalurDetailId;
	
	@Column(name = "route_jalur_detail_company_id")
	private String routeJalurDetailCompanyId;
	
	public MRouteJalurDetailSerializable() {}
	
	public MRouteJalurDetailSerializable(Integer MRouteId, Integer MRouteJalurId, Integer routeJalurDetailId, String routeJalurDetailCompanyId) 
	{
		this.MRouteId = MRouteId;
		this.MRouteJalurId = MRouteJalurId;
		this.routeJalurDetailId = routeJalurDetailId;
		this.routeJalurDetailCompanyId = routeJalurDetailCompanyId;
	}

	public Integer getMRouteId() {
		return MRouteId;
	}

	public void setMRouteId(Integer mRouteId) {
		MRouteId = mRouteId;
	}

	public Integer getMRouteJalurId() {
		return MRouteJalurId;
	}

	public void setMRouteJalurId(Integer mRouteJalurId) {
		MRouteJalurId = mRouteJalurId;
	}

	public Integer getRouteJalurDetailId() {
		return routeJalurDetailId;
	}

	public void setRouteJalurDetailId(Integer routeJalurDetailId) {
		this.routeJalurDetailId = routeJalurDetailId;
	}

	public String getRouteJalurDetailCompanyId() {
		return routeJalurDetailCompanyId;
	}

	public void setRouteJalurDetailCompanyId(String routeJalurDetailCompanyId) {
		this.routeJalurDetailCompanyId = routeJalurDetailCompanyId;
	}

	

	
}
