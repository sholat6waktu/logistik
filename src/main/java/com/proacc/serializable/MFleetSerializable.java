package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MFleetSerializable implements Serializable{

	@Column(name = "fleet_id")
	private Integer fleetId;
	
	@Column(name = "fleet_company_id")
	private String fleetCompanyId;
	
	public MFleetSerializable() {}
	
	public MFleetSerializable(Integer fleetId, String fleetCompanyId) 
	{
		this.fleetId = fleetId;
		this.fleetCompanyId = fleetCompanyId;
	}

	public Integer getFleetId() {
		return fleetId;
	}

	public void setFleetId(Integer fleetId) {
		this.fleetId = fleetId;
	}

	public String getFleetCompanyId() {
		return fleetCompanyId;
	}

	public void setFleetCompanyId(String fleetCompanyId) {
		this.fleetCompanyId = fleetCompanyId;
	}
	
}
