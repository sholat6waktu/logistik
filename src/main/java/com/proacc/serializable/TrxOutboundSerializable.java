package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxOutboundSerializable implements Serializable{

	@Column(name = "report_outbound_id")
	private Integer reportOutboundId;
	
	@Column(name = "company_id")
	private String companyId;
	
	public TrxOutboundSerializable() {}
	
	public TrxOutboundSerializable(Integer reportOutboundId, String companyId) 
	{
		this.reportOutboundId = reportOutboundId;
		this.companyId = companyId;
	}


	

	public Integer getReportOutboundId() {
		return reportOutboundId;
	}

	public void setReportOutboundId(Integer reportOutboundId) {
		this.reportOutboundId = reportOutboundId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
