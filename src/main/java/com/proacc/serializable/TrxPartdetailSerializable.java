package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrxPartdetailSerializable implements Serializable{

	@Column(name = "tm_trx_order_id")
	private String tmTrxOrderId;
	
	@Column(name = "tm_trx_kolidetail_id")
	private Integer tmTrxKolidetailId;
	
	@Column(name = "trx_partdetail_id")
	private Integer trxpartdetailId;
	
	@Column(name = "trx_partdetail_company_id")
	private String trxPartdetailCompanyId;
	
	public TrxPartdetailSerializable() {}
	
	public TrxPartdetailSerializable(String tmTrxOrderId, Integer tmTrxKolidetailId, Integer trxpartdetailId, String trxPartdetailCompanyId) 
	{
		this.tmTrxOrderId = tmTrxOrderId;
		this.tmTrxKolidetailId = tmTrxKolidetailId;
		this.trxpartdetailId = trxpartdetailId;
		this.trxPartdetailCompanyId = trxPartdetailCompanyId;
	}

	public String getTmTrxOrderId() {
		return tmTrxOrderId;
	}

	public void setTmTrxOrderId(String tmTrxOrderId) {
		this.tmTrxOrderId = tmTrxOrderId;
	}

	public Integer getTmTrxKolidetailId() {
		return tmTrxKolidetailId;
	}

	public void setTmTrxKolidetailId(Integer tmTrxKolidetailId) {
		this.tmTrxKolidetailId = tmTrxKolidetailId;
	}

	public Integer getTrxpartdetailId() {
		return trxpartdetailId;
	}

	public void setTrxpartdetailId(Integer trxpartdetailId) {
		this.trxpartdetailId = trxpartdetailId;
	}

	public String getTrxPartdetailCompanyId() {
		return trxPartdetailCompanyId;
	}

	public void setTrxPartdetailCompanyId(String trxPartdetailCompanyId) {
		this.trxPartdetailCompanyId = trxPartdetailCompanyId;
	}
}
