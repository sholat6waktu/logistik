package com.proacc.serializable;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class MRequestFleetDetailSerializable implements Serializable{

	@Column(name = "id")
	private Integer id;
	
	@Column(name = "detail_id")
	private Integer detailId;
	
	@Column(name = "company_id")
	private String companyId;
	
	public MRequestFleetDetailSerializable() {}
	
	public MRequestFleetDetailSerializable(Integer id, Integer detailId, String companyId) 
	{
		this.id = id;
		this.detailId = detailId;
		this.companyId = companyId;
	}
	
	

	public Integer getDetailId() {
		return detailId;
	}

	public void setDetailId(Integer detailId) {
		this.detailId = detailId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	
}
