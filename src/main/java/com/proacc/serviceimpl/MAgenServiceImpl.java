package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MAgen;
import com.proacc.repository.MAgenRepository;
import com.proacc.serializable.MAgenSerializable;
import com.proacc.service.MAgenService;

@Service
public class MAgenServiceImpl implements MAgenService{

	private static final Logger logger = LoggerFactory.getLogger(MAgenServiceImpl.class);
	
	@Autowired
	EntityManager em;

	@Autowired
	MAgenRepository mAgenRepository;

	@Override
	public List<Object[]> nextvalMAgen() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('agen_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getallAgen(String lowerCase, String compId) {
		String status = "";
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	agen_id,\r\n"
				+ "	agen_name,\r\n"
				+ "	CAST ( agen_created_by AS VARCHAR ),\r\n"
				+ "	agen_created_at,\r\n"
				+ "	CAST ( agen_updated_by AS VARCHAR ),\r\n"
				+ "	agen_updated_at,\r\n"
				+ "	agen_status,\r\n"
				+ "	agen_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_agen\r\n"
				+ "WHERE\r\n"
				+ "agen_company_id = :compid "
				+ status
				+ " ORDER BY agen_id ASC").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getAgen(String lowerCase, Integer agenId, String compId) {
		String status = "";
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	agen_id,\r\n"
				+ "	agen_name,\r\n"
				+ "	CAST ( agen_created_by AS VARCHAR ),\r\n"
				+ "	agen_created_at,\r\n"
				+ "	CAST ( agen_updated_by AS VARCHAR ),\r\n"
				+ "	agen_updated_at,\r\n"
				+ "	agen_status,\r\n"
				+ "	agen_company_id,"
				+ " agen_type \r\n"
				+ "FROM\r\n"
				+ "	m_agen\r\n"
				+ "WHERE\r\n"
				+ "agen_company_id = :compid\r\n"
				+ "AND agen_id = :agenid\r\n"
				+ status
				+ " ORDER BY agen_id ASC").setParameter("compid", compId).setParameter("agenid", agenId).getResultList();
	}

	@Override
	public void saveAgen(MAgen mAgen) {
		// TODO Auto-generated method stub
		mAgenRepository.save(mAgen);
	}

	@Override
	public Optional<MAgen> getDetail(Integer Id, String Company) {
		// TODO Auto-generated method stub
		return mAgenRepository.findById(new MAgenSerializable(Id, Company));
	}
}
