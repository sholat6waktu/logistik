package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MShareDriver;
import com.proacc.repository.MShareDriverRepository;
import com.proacc.service.MShareDriverService;

@Service
public class MShareDriverServiceImpl implements MShareDriverService {
	private static final Logger logger = LoggerFactory.getLogger(MShareDriverServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	MShareDriverRepository MShareDriverRepository;

	@Override
	public List<Object[]> detailShareDriver(int int1, String string, String lowerCase) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and a.status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and a.status = false";
		}
		else
		{
			status = "";
		}
		
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	a.no,\r\n" + 
				"	a.id,\r\n" + 
				"	a.m_driver_id,\r\n" + 
				"	d.cabang_id,\r\n" + 
				"	d.cabang_name,\r\n" + 
				"	d.m_province_id,\r\n" + 
				"	d.m_kota_id,\r\n" + 
				"	h.agen_id,\r\n" + 
				"	e.agendetail_id,\r\n" + 
				"	h.agen_name,\r\n" + 
				"	e.m_province_id as d,\r\n" + 
				"	e.m_kota_id as df,\r\n" + 
				"	a.start_date,\r\n" + 
				"	a.end_date ,\r\n" + 
				"	a.status\r\n" + 
				"FROM\r\n" + 
				"	m_share_driver as a \r\n" + 
				"	LEFT JOIN m_cabang AS d ON A.m_cabang_id = d.cabang_id \r\n" + 
				"	AND A.company_id = d.cabang_company_id\r\n" + 
				"	LEFT JOIN m_agendetail AS e ON A.m_agen_id = e.m_agen_id \r\n" + 
				"	AND A.m_agendetail_id = e.agendetail_id \r\n" + 
				"	AND A.company_id = e.agendetail_company_id\r\n" + 
				"	LEFT JOIN m_agen AS h ON A.m_agen_id = h.agen_id \r\n" + 
				"	AND A.company_id = h.agen_company_id\r\n" + 
				"	where\r\n" + 
				"	a.m_driver_id = :id \r\n" + 
				"	and a.company_id = :company " + status)
				.setParameter("company", string)
				.setParameter("id", int1)
				.getResultList();
	}

	@Override
	public void saveAll(ArrayList<MShareDriver> dataDetailRequest) {
		// TODO Auto-generated method stub
		MShareDriverRepository.saveAll(dataDetailRequest);
	}

	@Override
	@Transactional
	public void updateStatusFalse(int id, int driverId, String string) {
		// TODO Auto-generated method stub
		this.em.createNativeQuery("UPDATE m_share_driver SET status=false WHERE id = :id and m_driver_id = :driverId and company_id = :company")
		.setParameter("company", string).setParameter("id", id).setParameter("driverId", driverId).executeUpdate();
	}
}
