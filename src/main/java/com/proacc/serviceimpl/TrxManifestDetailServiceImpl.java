package com.proacc.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxManifestDetail;
import com.proacc.repository.TrxManifestDetailRepository;
import com.proacc.service.TrxManifestDetailService;

@Service
public class TrxManifestDetailServiceImpl implements TrxManifestDetailService{

	private static final Logger logger = LoggerFactory.getLogger(TrxManifestDetailServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxManifestDetailRepository TrxManifestDetailRepository;

	@Override
	public void saveAll(List<TrxManifestDetail> dataDetail, int id, String company) {
		// TODO Auto-generated method stub

		
		TrxManifestDetailRepository.saveAll(dataDetail);
	}

	@Override
	@Transactional
	public void deleteAll(int id, String string) {
		// TODO Auto-generated method stub
		em.createNativeQuery("delete from trx_manifest_detail where id = :id and company_id = :company")
		.setParameter("id", id).setParameter("company", string).executeUpdate();
		
	}

}
