package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MVehicletype;
import com.proacc.repository.MVehicletypeRepository;
import com.proacc.serializable.MVehicletypeSerializable;
import com.proacc.service.MVehicletypeService;

@Service
public class MVehicletypeServiceImpl implements MVehicletypeService{

	private static final Logger logger = LoggerFactory.getLogger(MVehicletypeServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	MVehicletypeRepository mVehicletypeRepository;

	@Override
	public List<Object[]> nextvalMVehicletype() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('vehicletype_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getallVehicletype(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and vehicletype_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and vehicletype_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	vehicletype_id,\r\n"
				+ "	vehicletype_name,\r\n"
				+ "	vehicletype_type,\r\n"
				+ "	vehicletype_description,\r\n"
				+ "	CAST ( vehicletype_created_by AS VARCHAR ),\r\n"
				+ "	vehicletype_created_at,\r\n"
				+ "	CAST ( vehicletype_updated_by AS VARCHAR ),\r\n"
				+ "	vehicletype_updated_at,\r\n"
				+ "	vehicletype_status,\r\n"
				+ "	vehicletype_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_vehicletype \r\n"
				+ "WHERE\r\n"
				+ "	vehicletype_company_id = :compid" + status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getVehicletype(String lowerCase, Integer vehicletypeId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and vehicletype_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and vehicletype_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	vehicletype_id,\r\n"
				+ "	vehicletype_name,\r\n"
				+ "	vehicletype_type,\r\n"
				+ "	vehicletype_description,\r\n"
				+ "	CAST ( vehicletype_created_by AS VARCHAR ),\r\n"
				+ "	vehicletype_created_at,\r\n"
				+ "	CAST ( vehicletype_updated_by AS VARCHAR ),\r\n"
				+ "	vehicletype_updated_at,\r\n"
				+ "	vehicletype_status,\r\n"
				+ "	vehicletype_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_vehicletype \r\n"
				+ "WHERE\r\n"
				+ "	vehicletype_company_id = :compid\r\n" 
				+ "	and vehicletype_id = :vehicletypeid" + status).setParameter("compid", compId).setParameter("vehicletypeid", vehicletypeId).getResultList();
	}

	@Override
	public void saveVehicletype(MVehicletype mVehicletype) {
		// TODO Auto-generated method stub
		mVehicletypeRepository.save(mVehicletype);
	}

	@Override
	public Optional<MVehicletype> getdetail(Integer Id, String Company) {
		// TODO Auto-generated method stub
		return mVehicletypeRepository.findById(new MVehicletypeSerializable(Id, Company));
	}
}
