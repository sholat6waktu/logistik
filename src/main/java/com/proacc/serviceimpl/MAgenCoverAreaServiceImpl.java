package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MagenCoverArea;
import com.proacc.repository.MagenDetailCoverAreaRepository;
import com.proacc.service.MAgenCoverAreaService;
import com.proacc.serializable.MagenCoverAreaSerializable;

@Service
public class MAgenCoverAreaServiceImpl implements MAgenCoverAreaService {

	private static final Logger logger = LoggerFactory.getLogger(MAgenCoverAreaServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	MagenDetailCoverAreaRepository MagenDetailCoverAreaRepository;

	@Override
	public List<Object[]> getAll(int int1, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select m_agen_id, m_agendetail_id, agen_cover_area_id, agen_cover_area_status, agen_cover_area_company_id from m_agen_cover_area\r\n" + 
				"where\r\n" + 
				"m_agen_id = :agenId \r\n" + 
				"and agen_cover_area_company_id = :company ").setParameter("agenId", int1).setParameter("company", string).getResultList();
	}

	@Override
	public Optional<MagenCoverArea> getDetail(int parseInt, int parseInt2, int parseInt3, String string) {
		// TODO Auto-generated method stub
		
		return MagenDetailCoverAreaRepository.findById(new MagenCoverAreaSerializable(parseInt, parseInt2, parseInt3, string));
	}
	
	
}
