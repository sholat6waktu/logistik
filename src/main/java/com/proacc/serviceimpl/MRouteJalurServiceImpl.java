package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRoute;
import com.proacc.entity.MRouteJalur;
import com.proacc.repository.MRouteJalurRepository;
import com.proacc.serializable.MRouteJalurSerializable;
import com.proacc.serializable.MRouteSerializable;
import com.proacc.service.MRouteJalurService;

@Service
public class MRouteJalurServiceImpl implements MRouteJalurService {
	
	private static final Logger logger = LoggerFactory.getLogger(MRouteJalurServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MRouteJalurRepository MRouteJalurRepository;

	@Override
	public void saveAll(ArrayList<MRouteJalur> routeJalur) {
		// TODO Auto-generated method stub
		MRouteJalurRepository.saveAll(routeJalur);
	}

	@Override
	public List<Object[]> getdetailall(int int1, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select m_route_id, route_jalur_id, route_jalur_company_id from m_route_jalur where m_route_id = :routeId and route_jalur_company_id = :company")
				.setParameter("routeId", int1)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public Optional<MRouteJalur> getdetailById(int parseInt, int parseInt2, String string) {
		// TODO Auto-generated method stub
		return MRouteJalurRepository.findById(new MRouteJalurSerializable(parseInt, parseInt2, string));
	}

	@Override
	public List<Object[]> listRouteJalur(String lowerCase, String string) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and a.route_jalur_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and a.route_jalur_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT A\r\n" + 
				"	.m_route_id,\r\n" + 
				"	A.route_jalur_company_id,\r\n" + 
				"	b.cabang_id,\r\n" + 
				"	b.cabang_name,\r\n" + 
				"	b.m_province_id as af,\r\n" + 
				"	b.m_kota_id s,\r\n" + 
				"	C.agen_id,\r\n" + 
				"	d.agendetail_id,\r\n" + 
				"	C.agen_name,\r\n" + 
				"	d.m_province_id,\r\n" + 
				"	d.m_kota_id \r\n" + 
				"FROM\r\n" + 
				"	\"m_route_jalur\"\r\n" + 
				"	AS A LEFT JOIN m_cabang AS b ON A.m_cabang_id = b.cabang_id \r\n" + 
				"	AND A.route_jalur_company_id = b.cabang_company_id\r\n" + 
				"	LEFT JOIN m_agen AS C ON C.agen_id = A.m_agen_id \r\n" + 
				"	AND C.agen_company_id = A.route_jalur_company_id\r\n" + 
				"	LEFT JOIN m_agendetail d ON d.agendetail_id = A.m_agendetail_id \r\n" + 
				"	AND d.m_agen_id = A.m_agen_id \r\n" + 
				"	AND d.agendetail_company_id = A.route_jalur_company_id \r\n" + 
				"WHERE\r\n" + 
				"	A.route_jalur_company_id = :company \r\n" + 
				"	 \r\n" + status+
				" ORDER BY\r\n" + 
				"	A.m_route_id")
				.setParameter("company", string)
				.getResultList();
	}

}
