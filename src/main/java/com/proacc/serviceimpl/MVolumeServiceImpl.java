package com.proacc.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.service.MVolumeService;

@Service
public class MVolumeServiceImpl implements MVolumeService{

	private static final Logger logger = LoggerFactory.getLogger(MVolumeServiceImpl.class);
	
	@Autowired
	EntityManager em;

	@Override
	public List<Object[]> getallVolume(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and volume_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and volume_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	volume_id,\r\n"
				+ "	volume_name,\r\n"
				+ "	volume_tom3,\r\n"
				+ "	volume_description,\r\n"
				+ "	CAST ( volume_created_by AS VARCHAR ),\r\n"
				+ "	volume_created_at,\r\n"
				+ "	CAST ( volume_updated_by AS VARCHAR ),\r\n"
				+ "	volume_updated_at,\r\n"
				+ "	volume_status,\r\n"
				+ "	volume_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_volume \r\n"
				+ "WHERE\r\n"
				+ "	volume_company_id = :compid" + status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getVolume(String lowerCase, Integer volumeId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and volume_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and volume_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	volume_id,\r\n"
				+ "	volume_name,\r\n"
				+ "	volume_tom3,\r\n"
				+ "	volume_description,\r\n"
				+ "	CAST ( volume_created_by AS VARCHAR ),\r\n"
				+ "	volume_created_at,\r\n"
				+ "	CAST ( volume_updated_by AS VARCHAR ),\r\n"
				+ "	volume_updated_at,\r\n"
				+ "	volume_status,\r\n"
				+ "	volume_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_volume \r\n"
				+ "WHERE\r\n"
				+ "	volume_company_id = :compid"
				+ "	and volume_id = :volumeid" + status).setParameter("compid", compId).setParameter("volumeid", volumeId).getResultList();
	}
}
