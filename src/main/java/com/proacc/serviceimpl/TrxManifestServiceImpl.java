package com.proacc.serviceimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxManifest;
import com.proacc.repository.TrxManifestRepository;
import com.proacc.service.TrxManifestService;

@Service
public class TrxManifestServiceImpl implements TrxManifestService{

	private static final Logger logger = LoggerFactory.getLogger(TrxManifestServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxManifestRepository TrxManifestRepository;

	@Override
	public List<Object[]> getManifest(int int1, Integer getmCabangId, Integer getmAgenId, Integer getmAgenDetailId,
			Integer getmCabangId2, Integer getmAgenId2, Integer getmAgenDetailId2, String orderId, String companyId) {
		// TODO Auto-generated method stub
		String status = "";
		String statusTo = "";
		if(getmCabangId == null && getmAgenId != null)
		{
			status = "	and agen_id_from = " +getmAgenId+  
					"	and agen_detail_id_from = "+getmAgenDetailId;
		}
		else if(getmCabangId != null && getmAgenId != null)
		{
			status = "	and cabang_id_from = " +getmCabangId;
		}
		else if(getmCabangId != null && getmAgenId == null)
		{
			status = "	and cabang_id_from = " +getmCabangId;
		}
		else
		{
			status = "";
		}
		
		if(getmCabangId2 == null && getmAgenId2 != null)
		{
			statusTo = "	and agen_id_to = " +getmAgenId2+  
					"	and agen_detail_id_to = "+getmAgenDetailId2;
		}
		else if(getmCabangId2 != null && getmAgenId2 != null)
		{
			statusTo = "	and cabang_id_to = " +getmCabangId2;
		}
		else if(getmCabangId2 != null && getmAgenId2 == null)
		{
			statusTo = "	and cabang_id_to = " +getmCabangId2;
		}
		else
		{
			statusTo = "";
		}
		
		
		
		return this.em.createNativeQuery("SELECT * FROM trx_manifest\r\n" + 
				"where\r\n" + 
				"driver_id = :driverId\r\n" + 
				"\r\n" + 
				"and company_id = :companyId \r\n" + 
				"" + status + statusTo)
				.setParameter("driverId", int1)
				
				.setParameter("companyId", companyId)
				.getResultList();
	}

	@Override
	public List<Object[]> getId() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('manifest_sequence')").getResultList();
	}

	@Override
	public void save(TrxManifest dataManifest) {
		// TODO Auto-generated method stub
		TrxManifestRepository.save(dataManifest);
	}

	@Override
	public List<Object[]> listManifest(String string, String string2, UUID user_uuid) {
		// TODO Auto-generated method stub
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = formatter1.parse(string2);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.em.createNativeQuery("SELECT A\r\n" + 
				"	.ID,\r\n" + 
				"	A.driver_id,\r\n" + 
				"	C.fleet_id,\r\n" + 
				"	A.manifest_no,\r\n" + 
				"	C.fleet_name,\r\n" + 
				"	A.cabang_id_from,\r\n" + 
				"	A.agen_id_from,\r\n" + 
				"	A.agen_detail_id_from,\r\n" + 
				"	A.cabang_id_to,\r\n" + 
				"	A.agen_id_to,\r\n" + 
				"	A.agen_detail_id_to,\r\n" + 
				"	C.fleet_weight,\r\n" + 
				"	C.m_weight_id,\r\n" + 
				"	j.weight_name,\r\n" + 
				"	C.fleet_volume,\r\n" + 
				"	C.m_volume_id,\r\n" + 
				"	K.volume_name,\r\n" + 
				"	A.tanggal,\r\n" + 
				"	b.trx_jalurdriverheader_id,\r\n" + 
				"	d.m_province_id cabangprovinsifrom,\r\n" + 
				"	d.m_kota_id cabangkotafrom,\r\n" + 
				"	f.m_province_id agenprovinsifrom,\r\n" + 
				"	f.m_kota_id agenkotafrom,\r\n" + 
				"	G.m_province_id cabangprovinsito,\r\n" + 
				"	G.m_kota_id cabangkotato,\r\n" + 
				"	i.m_province_id agenprovinsito,\r\n" + 
				"	i.m_kota_id agenkotato \r\n" + 
				"FROM\r\n" + 
				"	\"trx_manifest\"\r\n" + 
				"	A LEFT JOIN m_driver b ON b.driver_id = A.driver_id \r\n" + 
				"	AND b.driver_company_id = A.company_id\r\n" + 
				"	LEFT JOIN m_fleet C ON C.fleet_id = b.m_fleet_id \r\n" + 
				"	AND C.fleet_company_id = b.driver_company_id\r\n" + 
				"	LEFT JOIN m_cabang d ON d.cabang_id = A.cabang_id_from \r\n" + 
				"	AND d.cabang_company_id = A.company_id\r\n" + 
				"	LEFT JOIN m_agen e ON e.agen_id = A.agen_id_from \r\n" + 
				"	AND e.agen_company_id = A.company_id\r\n" + 
				"	LEFT JOIN m_agendetail f ON f.m_agen_id = A.agen_id_from \r\n" + 
				"	AND f.agendetail_id = A.agen_detail_id_from \r\n" + 
				"	AND f.agendetail_company_id = A.company_id\r\n" + 
				"	LEFT JOIN m_cabang G ON G.cabang_id = A.cabang_id_to \r\n" + 
				"	AND G.cabang_company_id = A.company_id\r\n" + 
				"	LEFT JOIN m_agen h ON h.agen_id = A.agen_id_to \r\n" + 
				"	AND h.agen_company_id = A.company_id\r\n" + 
				"	LEFT JOIN m_agendetail i ON i.m_agen_id = A.agen_id_to \r\n" + 
				"	AND i.agendetail_id = A.agen_detail_id_to \r\n" + 
				"	AND i.agendetail_company_id = A.company_id\r\n" + 
				"	LEFT JOIN m_weight j ON j.weight_id = C.m_weight_id \r\n" + 
				"	AND j.weight_company_id = C.fleet_company_id\r\n" + 
				"	LEFT JOIN m_volume K ON K.volume_id = C.m_volume_id \r\n" + 
				"	AND K.volume_company_id = C.fleet_company_id \r\n" + 
				"WHERE\r\n" + 
				"	A.tanggal = :tanggal \r\n" + 
				"	AND A.created_by = :user_uuid \r\n" + 
				"	AND A.company_id = :companyId " + 
				"").setParameter("tanggal", date)
				.setParameter("companyId", string)
				.setParameter("user_uuid", user_uuid)
				.getResultList();
	}

	@Override
	public List<Object[]> detailManifest(String string, int parseInt) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	a.order_id, a.tanggal , b.m_contact_customer_to, a.cabang_id_to, a.agen_id_to, a.agen_detail_id_to, ser.service_name, ( SELECT count(*) FROM trx_kolidetail  \r\n" + 
				"				where\r\n" + 
				"				tm_trx_order_id = a.order_id\r\n" + 
				"				and trx_kolidetail_company_id = a.company_id ) as jmlh,\r\n" + 
				"				b.trx_order_weight,\r\n" + 
				"				w.weight_name,\r\n" + 
				"				b.trx_order_volume,\r\n" + 
				"				v.volume_name\r\n" + 
				"				\r\n" + 
				"				\r\n" + 
				"FROM\r\n" + 
				"	trx_manifest_detail a\r\n" + 
				"	LEFT JOIN trx_order AS b ON a.order_id = b.trx_order_id\r\n" + 
				"	AND a.company_id = b.trx_order_company_id \r\n" + 
				"	 left join m_service ser on ser.service_id = b.m_service_id and ser.service_company_id =b.trx_order_company_id\r\n" + 
				"	 LEFT JOIN m_weight AS w ON b.m_weight_id = w.weight_id \r\n" + 
				"				AND b.trx_order_company_id = w.weight_company_id\r\n" + 
				"		LEFT JOIN m_volume AS v ON b.m_volume_id = v.volume_id \r\n" + 
				"				AND b.trx_order_company_id = v.volume_company_id\r\n" + 
				"				\r\n" + 
				"WHERE\r\n" + 
				"	ID = :id \r\n" + 
				"	AND company_id = :company ")
				.setParameter("company", string)
				.setParameter("id", parseInt).getResultList();
	}


}
