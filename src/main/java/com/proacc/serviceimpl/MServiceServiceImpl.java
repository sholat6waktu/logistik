package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MService;
import com.proacc.repository.MServiceRepository;
import com.proacc.serializable.MServiceSerializable;
import com.proacc.service.MServiceService;

@Service
public class MServiceServiceImpl implements MServiceService{
	private static final Logger logger = LoggerFactory.getLogger(MServiceServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	MServiceRepository mServiceRepository;

	@Override
	public List<Object[]> nextvalMService() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('service_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getallservice(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and service_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and service_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	service_id,\r\n"
				+ "	service_name,\r\n"
				+ "	service_deliverytime1,\r\n"
				+ "	service_deliverytime2,\r\n"
				+ "	service_description,\r\n"
				+ "	CAST ( service_created_by AS VARCHAR ),\r\n"
				+ "	service_created_at,\r\n"
				+ "	CAST ( service_updated_by AS VARCHAR ),\r\n"
				+ "	service_updated_at,\r\n"
				+ "	service_status,\r\n"
				+ "	service_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_service \r\n"
				+ "WHERE\r\n"
				+ "	service_company_id = :compid" + status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getservice(String lowerCase, Integer serviceId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and service_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and service_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	service_id,\r\n"
				+ "	service_name,\r\n"
				+ "	service_deliverytime1,\r\n"
				+ "	service_deliverytime2,\r\n"
				+ "	service_description,\r\n"
				+ "	CAST ( service_created_by AS VARCHAR ),\r\n"
				+ "	service_created_at,\r\n"
				+ "	CAST ( service_updated_by AS VARCHAR ),\r\n"
				+ "	service_updated_at,\r\n"
				+ "	service_status,\r\n"
				+ "	service_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_service \r\n"
				+ "WHERE\r\n"
				+ "	service_company_id = :compid\r\n"
				+ "	and service_id = :serviceid" + status).setParameter("compid", compId).setParameter("serviceid", serviceId).getResultList();
	}

	@Override
	public void saveservice(MService mService) {
		// TODO Auto-generated method stub
		mServiceRepository.save(mService);
	}

	@Override
	public Optional<MService> getdetail(Integer Id, String Company) {
		// TODO Auto-generated method stub
		return mServiceRepository.findById(new MServiceSerializable(Id, Company));
	}
}
