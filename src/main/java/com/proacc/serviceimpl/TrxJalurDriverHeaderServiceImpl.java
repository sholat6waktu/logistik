package com.proacc.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.repository.TrxJalurDriverHeaderRepository;
import com.proacc.service.TrxJalurDriverHeaderService;

@Service
public class TrxJalurDriverHeaderServiceImpl implements TrxJalurDriverHeaderService{
	private static final Logger logger = LoggerFactory.getLogger(TrxJalurDriverHeaderServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxJalurDriverHeaderRepository TrxJalurDriverHeaderRepository;

	@Override
	public List<Object[]> getDetail(String lowerCase, String string, int int1) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and d.status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and d.status = false";
		}
		else
		{
			status = "";
		}
		
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	d.trx_driverjalur_id,\r\n" + 
				"	h.m_route_id,\r\n" + 
				"	mr.route_name,\r\n" + 
				"	mrj.route_jalur_id,\r\n" + 
				"	mrj.m_agen_id,\r\n" + 
				"	mrj.m_agendetail_id,\r\n" + 
				"	mrj.m_cabang_id,\r\n" + 
				"	mc.cabang_name,\r\n" + 
				"	mc.m_kota_id AS kota_cabang,\r\n" + 
				"	mad.m_kota_id,\r\n" + 
				"	ma.agen_name, 	d.status,\r\n" + 
				"	d.status_progress,	mdr.position_cabang_id,\r\n" + 
				"	mdr.position_agen_id,\r\n" + 
				"	mdr.position_agendetail_id \r\n" + 
				"FROM\r\n" + 
				"	trx_jalur_driver_header AS h\r\n" + 
				"	LEFT JOIN trx_jalurdriver AS d ON h.trx_driverjalurheader_id = d.trx_driverjalurheader_id \r\n" + 
				"	AND h.company_id = d.company_id\r\n" + 
				"	LEFT JOIN m_route AS mr ON h.m_route_id = mr.route_id \r\n" + 
				"	AND mr.route_company_id = h.company_id\r\n" + 
				"	LEFT JOIN m_route_jalur AS mrj ON mrj.m_route_id = d.m_route_id \r\n" + 
				"	AND mrj.route_jalur_company_id = d.company_id \r\n" + 
				"	AND mrj.route_jalur_id = d.m_routejalur_id\r\n" + 
				"	LEFT JOIN m_cabang AS mc ON mc.cabang_id = mrj.m_cabang_id \r\n" + 
				"	AND mc.cabang_company_id = mrj.route_jalur_company_id\r\n" + 
				"	LEFT JOIN m_agendetail mad ON mad.m_agen_id = mrj.m_agen_id \r\n" + 
				"	AND mad.agendetail_id = mrj.m_agendetail_id \r\n" + 
				"	AND mad.agendetail_company_id = mrj.route_jalur_company_id\r\n" + 
				"	LEFT JOIN m_agen AS ma ON ma.agen_id = mrj.m_agen_id \r\n" + 
				"	AND ma.agen_company_id = mrj.route_jalur_company_id "
				+ " left join m_driver mdr on mdr.trx_jalurdriverheader_id = h.trx_driverjalurheader_id and mdr.driver_company_id = h.company_id\r\n" + 
				"WHERE\r\n" + 
				"	h.trx_driverjalurheader_id = :id \r\n" + status + 
				"	AND h.company_id = :company order by trx_driverjalur_id asc")
				.setParameter("id", int1)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public List<Object[]> nextval() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('jalurdriverheader_sequence')").getResultList();
	}

}
