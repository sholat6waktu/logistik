package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MCabang;
import com.proacc.entity.MCabangdetail;
import com.proacc.repository.MCabangRepository;
import com.proacc.repository.MCabangdetailRepository;
import com.proacc.serializable.MCabangSerializable;
import com.proacc.serializable.MCabangdetailSerializable;
import com.proacc.service.MCabangdetailService;

@Service
public class MCabangdetailServiceImpl implements MCabangdetailService{

	private static final Logger logger = LoggerFactory.getLogger(MCabangdetailServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MCabangdetailRepository mCabangdetailRepository;
	
	@Autowired
	MCabangRepository MCabangRepository;

	@Override
	public List<Object[]> getallCabangdetail(String lowerCase, Integer cabangId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and cd.cabangdetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and cd.cabangdetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "cd.m_cabang_id,\r\n"
				+ "cd.cabangdetail_id,\r\n"
				+ "cd.m_province_id,\r\n"
				+ "cd.m_kotadetail_id,\r\n"
				+ "cd.m_agen_id,\r\n"
				+ "a.agen_name,\r\n"
				+ "cd.m_parent_id,\r\n"
				+ "cd.cabangdetail_agenttype,\r\n"
				+ "CAST ( cd.cabangdetail_created_by AS VARCHAR ),\r\n"
				+ "cd.cabangdetail_created_at,\r\n"
				+ "CAST ( cd.cabangdetail_updated_by AS VARCHAR ),\r\n"
				+ "cd.cabangdetail_updated_at,\r\n"
				+ "cd.cabangdetail_status,\r\n"
				+ "cd.cabangdetail_company_id\r\n"
				+ "FROM\r\n"
				+ "	m_cabang\r\n"
				+ "	AS C LEFT JOIN m_cabangdetail AS cd ON C.cabang_id = cd.m_cabang_id \r\n"
				+ "	AND C.cabang_company_id = cd.cabangdetail_company_id\r\n"
				+ "	LEFT JOIN m_agen AS A ON A.agen_id = cd.m_agen_id \r\n"
				+ "	AND A.agen_company_id = cd.cabangdetail_company_id\r\n"
				+ "WHERE\r\n"
				+ "cd.m_cabang_id = :cabangid\r\n"
				+ "AND cd.cabangdetail_company_id = :compid" + status).setParameter("compid", compId).setParameter("cabangid", cabangId).getResultList();
	}

	@Override
	public List<Object[]> getCabangdetail(String lowerCase, Integer cabangdetailId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and cd.cabangdetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and cd.cabangdetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "cd.m_cabang_id,\r\n"
				+ "cd.cabangdetail_id,\r\n"
				+ "cd.m_province_id,\r\n"
				+ "cd.m_kotadetail_id,\r\n"
				+ "cd.m_agen_id,\r\n"
				+ "a.agen_name,\r\n"
				+ "cd.m_parent_id,\r\n"
				+ "cd.cabangdetail_agenttype,\r\n"
				+ "CAST ( cd.cabangdetail_created_by AS VARCHAR ),\r\n"
				+ "cd.cabangdetail_created_at,\r\n"
				+ "CAST ( cd.cabangdetail_updated_by AS VARCHAR ),\r\n"
				+ "cd.cabangdetail_updated_at,\r\n"
				+ "cd.cabangdetail_status,\r\n"
				+ "cd.cabangdetail_company_id\r\n"
				+ "FROM\r\n"
				+ "	m_cabang\r\n"
				+ "	AS C LEFT JOIN m_cabangdetail AS cd ON C.cabang_id = cd.m_cabang_id \r\n"
				+ "	AND C.cabang_company_id = cd.cabangdetail_company_id\r\n"
				+ "	LEFT JOIN m_agen AS A ON A.agen_id = cd.m_agen_id \r\n"
				+ "	AND A.agen_company_id = cd.cabangdetail_company_id\r\n"
				+ "WHERE\r\n"
				+ "cd.cabangdetail_id = :cabangdetailid\r\n"
				+ "AND cd.cabangdetail_company_id = :compid" + status).setParameter("compid", compId).setParameter("cabangdetailid", cabangdetailId).getResultList();
	}

	@Override
	public void saveallCabangdetail(ArrayList<MCabangdetail> MCabangdetailArray) {
		// TODO Auto-generated method stub
		mCabangdetailRepository.saveAll(MCabangdetailArray);
	}

	@Override
	public Optional<MCabangdetail> getdetail(Integer cabangId, Integer cabangdetailId, String cabangdetailCompanyId) {
		// TODO Auto-generated method stub
		return mCabangdetailRepository.findById(new MCabangdetailSerializable(cabangId, cabangdetailId, cabangdetailCompanyId));
	}

	@Override
	public List<Object[]> getalldetail(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and cd.cabangdetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and cd.cabangdetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	cd.m_cabang_id,\r\n"
				+ "	c.m_kota_id,\r\n"
				+ "	cd.m_province_id,\r\n"
				+ "		string_agg(CAST ( cd.m_kotadetail_id AS VARCHAR ) || '-' || COALESCE(CAST ( a.agen_name AS VARCHAR ), 'internal') ,',') listkota,\r\n"
				+ "	c.cabang_status,\r\n"
				+ "	cd.cabangdetail_status,\r\n"
				+ "	cd.cabangdetail_company_id\r\n"
				+ "FROM\r\n"
				+ "	m_cabang\r\n"
				+ "	AS c LEFT JOIN m_cabangdetail AS cd ON c.cabang_id = cd.m_cabang_id \r\n"
				+ "	AND c.cabang_company_id = cd.cabangdetail_company_id\r\n"
				+ "	LEFT JOIN m_agen as a ON cd.m_agen_id = a.agen_id AND cd.cabangdetail_company_id = a.agen_company_id\r\n"
				+ "	WHERE\r\n"
				+ "	cd.cabangdetail_company_id = :compid\r\n"
				+ status
				+ "	GROUP BY\r\n"
				+ "		cd.m_cabang_id,\r\n"
				+ "	c.m_kota_id,\r\n"
				+ "	cd.m_province_id,\r\n"
				+ "	c.cabang_status,\r\n"
				+ "	cd.cabangdetail_status,\r\n"
				+ "	cd.cabangdetail_company_id order by\r\n" + 
				" cd.m_cabang_id asc").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getdetailprovinsiforupdate(String lowerCase, Integer cabangId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and cabangdetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and cabangdetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	m_province_id,\r\n"
				+ "	22222 \r\n"
				+ "FROM\r\n"
				+ "	m_cabangdetail\r\n"
				+ "WHERE\r\n"
				+ "	cabangdetail_company_id = :compid\r\n"
				+ "	AND m_cabang_id = :cabangid\r\n"
				+ status
				+ " GROUP BY\r\n"
				+ "	m_province_id" ).setParameter("compid", compId).setParameter("cabangid", cabangId).getResultList();
	}

	@Override
	public List<Object[]> getdetailkotaforupdate(String lowerCase, Integer cabangId, Integer provinsiId,
			String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and cabangdetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and cabangdetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "				 	cd.m_province_id,\r\n"
				+ "				 	cd.m_kotadetail_id,\r\n"
				+ "				 	cd.cabangdetail_priority,\r\n"
				+ "				 	cd.cabangdetail_agenttype,\r\n"
				+ "				 	cd.cabangdetail_id,\r\n"
				+ "				 	cd.m_agen_id,\r\n"
				+ "				 	a.agen_name,\r\n"
				+ "				 	cd.m_parent_id,\r\n"
				+ "				 	cd.cabangdetail_status,\r\n"
				+ "					c.cabang_status,\r\n"
				+ "				 	cd.cabangdetail_company_id,"
				+ " cd.m_agendetail_id"
		
				+ "				 FROM\r\n"
				+ "				 	m_cabangdetail as cd LEFT JOIN m_agen as a ON cd.m_agen_id = a.agen_id AND cd.cabangdetail_company_id = a.agen_company_id LEFT JOIN m_cabang as c ON cd.m_cabang_id = c.cabang_id AND c.cabang_company_id = cd.cabangdetail_company_id\r\n"
				+ "				 WHERE\r\n"
				+ "				 	cd.cabangdetail_company_id = :compid\r\n"
				+ "				 	AND cd.m_cabang_id = :cabangid\r\n"
				+ "				 	AND cd.m_province_id = :provinsiid"
				+ status).setParameter("compid", compId).setParameter("cabangid", cabangId).setParameter("provinsiid", provinsiId).getResultList();
	}

	@Override
	public List<Object[]> getdetailall(Integer cabangId, String compId) {
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	m_cabang_id,\r\n"
				+ "	cabangdetail_id,\r\n"
				+ "	cabangdetail_status \r\n"
				+ "FROM\r\n"
				+ "	m_cabangdetail \r\n"
				+ "WHERE\r\n"
				+ "	m_cabang_id = :cabangid \r\n"
				+ "	AND cabangdetail_company_id = :compid").setParameter("compid", compId).setParameter("cabangid",cabangId).getResultList();
	}

	@Override
	public List<Object[]> getCabangIdWithCityAndAgenId(int agenId, Integer fromkota) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select m_cabang_id, m_kotadetail_id from m_cabangdetail where m_agen_id= "+agenId+" and m_kotadetail_id= "+fromkota+" and cabangdetail_status= true and cabangdetail_company_id = '1'")
				.getResultList();
	}

	@Override
	public Optional<MCabang> getDetailCabang(int parseInt, String string) {
		// TODO Auto-generated method stub
		return MCabangRepository.findById(new MCabangSerializable(parseInt, string));
	}

	@Override
	public List<Object[]> getCabangIdByCityId(Integer fromkota, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	mc.m_kota_id,\r\n" + 
				"	mc.cabang_id \r\n" + 
				"FROM\r\n" + 
				"	m_cabang mc\r\n" + 
				"	LEFT JOIN m_cabangdetail mcd ON mc.cabang_id = mcd.m_cabang_id \r\n" + 
				"	AND mc.cabang_company_id = mcd.cabangdetail_company_id\r\n" + 
				"	where\r\n" + 
				"	mcd.m_kotadetail_id = :fromkota\r\n" + 
				"	and mc.cabang_status = true\r\n" + 
				"	and mcd.cabangdetail_status = true"
				+ " and mc.cabang_company_id = :company")
				.setParameter("fromkota", fromkota)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public List<Object[]> getCabangHeaderIdByCityId(Integer fromkota, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	m_kota_id,\r\n" + 
				"	cabang_id \r\n" + 
				"FROM\r\n" + 
				"	m_cabang mc\r\n" + 
				"	LEFT JOIN m_cabangdetail mcd ON mc.cabang_id = mcd.m_cabang_id \r\n" + 
				"	AND mc.cabang_company_id = mcd.cabangdetail_company_id\r\n" + 
				"	where\r\n" + 
				"	mc.m_kota_id = :fromkota\r\n" + 
				"	and mc.cabang_status = true\r\n" + 
				"	and mcd.cabangdetail_status = true"
				+ " and mc.cabang_company_id = :company")
				.setParameter("fromkota", fromkota)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public List<Object[]> checkActor(String string, UUID fromString) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select * from (select agen_name cd, agen_status status, agen_company_id company from m_agen union select cabang_name cd, cabang_status st, cabang_company_id company from m_cabang) s\r\n" + 
				"where status = true\r\n" + 
				"and company = :company\r\n" + 
				"and cd = :user")
				.setParameter("user", fromString.toString())
				.setParameter("company", string)
				.getResultList();
		
	}

	@Override
	public List<Object[]> getDetailKhususAgen(String lowerCase, int int1, String string) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and a.cabangdetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and a.cabangdetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	a.m_cabang_id,\r\n" + 
				"	a.cabangdetail_id,\r\n" + 
				"	a.m_agen_id,\r\n" + 
				"	a.m_agendetail_id,\r\n" + 
				"	a.cabangdetail_company_id,\r\n" + 
				"	b.m_province_id,\r\n" + 
				"	b.m_kota_id,"
				+ " cast(c.agen_name as varchar ) \r\n" + 
				"FROM\r\n" + 
				"	m_cabangdetail a\r\n" + 
				"	left join m_agendetail b on b.m_agen_id = a.m_agen_id and b.agendetail_id = a.m_agendetail_id and b.agendetail_company_id = a.cabangdetail_company_id"
				+ " left join m_agen c on c.agen_id = b.m_agen_id and c.agen_company_id = b.agendetail_company_id \r\n" + 
				"WHERE\r\n" + 
				"	a.m_cabang_id = :cabangId \r\n" + 
				"	AND a.m_agen_id IS NOT NULL "
				+ " and a.cabangdetail_company_id = :companyId \r\n" + status + 
				" ORDER BY \r\n" + 
				"	a.cabangdetail_id ").setParameter("cabangId", int1)
				.setParameter("companyId", string)
				.getResultList();
	}

	@Override
	public List<Object[]> getCoverArea(int parseInt, String string) {
		// TODO Auto-generated method stub
		return null;
	}
}
