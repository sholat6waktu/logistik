package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MFleet;
import com.proacc.repository.MFleetRepository;
import com.proacc.serializable.MFleetSerializable;
import com.proacc.service.MFleetService;

@Service
public class MFleetServiceImpl implements MFleetService{

	private static final Logger logger = LoggerFactory.getLogger(MFleetServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MFleetRepository mFleetRepository;

	@Override
	public List<Object[]> nextvalMFleet() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('fleet_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getallFleet(String lowerCase, String compId, UUID user_uuid) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and fleet_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and fleet_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	f.fleet_id,\r\n"
				+ "	f.fleet_name,\r\n"
				+ "	f.fleet_vehicle,\r\n"
				+ "	vt.vehicletype_name,\r\n"
				+ "	f.fleet_weight || ' ' || w.weight_name AS weight,\r\n"
				+ "	f.fleet_height || ' ' || 	l1.lwh_name  AS height,\r\n"
				+ "	f.fleet_width || ' ' || 	l2.lwh_name  AS width,\r\n"
				+ "	f.fleet_length || ' ' || 	l3.lwh_name  AS length,\r\n"
				+ "	f.fleet_volume || ' ' || 	v.volume_name  AS volume,\r\n"
				+ "	f.fleet_status,\r\n"
				+ "	f.fleet_company_id\r\n"
				+ "FROM\r\n"
				+ "	m_fleet AS f\r\n"
				+ "	LEFT JOIN m_vehicletype AS vt ON f.m_vehicletype_id = vt.vehicletype_id \r\n"
				+ "	AND f.fleet_company_id = vt.vehicletype_company_id\r\n"
				+ "	LEFT JOIN m_weight AS w ON f.m_weight_id = w.weight_id \r\n"
				+ "	AND f.fleet_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_lwh AS l1 ON f.m_height_id = l1.lwh_id \r\n"
				+ "	AND f.fleet_company_id = l1.lwh_company_id\r\n"
				+ "	LEFT JOIN m_lwh AS l2 ON f.m_width_id = l2.lwh_id \r\n"
				+ "	AND f.fleet_company_id = l2.lwh_company_id\r\n"
				+ "		LEFT JOIN m_lwh AS l3 ON f.m_length_id = l3.lwh_id \r\n"
				+ "	AND f.fleet_company_id = l3.lwh_company_id\r\n"
				+ "	LEFT JOIN m_volume as v ON f.m_volume_id = v.volume_id\r\n"
				+ "	AND f.fleet_company_id = v.volume_company_id\r\n"
				+ "	WHERE\r\n"
				+ "	f.fleet_company_id = :compid and f.fleet_created_by = :user_uuid " + status).setParameter("compid", compId).setParameter("user_uuid", user_uuid).getResultList();
	}

	@Override
	public List<Object[]> getFleet(String lowerCase, Integer fleetId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and fleet_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and fleet_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	f.fleet_id,\r\n"
				+ "	f.fleet_name,\r\n"
				+ "	f.fleet_vehicle,\r\n"
				+ "	f.m_vehicletype_id,\r\n"
				+ "	f.fleet_weight,\r\n"
				+ "	f.m_weight_id,\r\n"
				+ "	f.fleet_height,\r\n"
				+ "	f.m_height_id,\r\n"
				+ "	f.fleet_width,\r\n"
				+ "	f.m_width_id,\r\n"
				+ "	f.fleet_length,\r\n"
				+ "	f.m_length_id,\r\n"
				+ "	f.fleet_volume,\r\n"
				+ "	f.m_volume_id,\r\n"
				+ "	f.fleet_status,\r\n"
				+ "	f.fleet_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_fleet AS f"
				+ "	WHERE\r\n"
				+ "	f.fleet_company_id = :compid"
				+ "	and fleet_id = :fleetid"+ status).setParameter("compid", compId).setParameter("fleetid", fleetId).getResultList();
	}

	@Override
	public void saveFleet(MFleet mFleet) {
		// TODO Auto-generated method stub
		mFleetRepository.save(mFleet);
	}

	@Override
	public Optional<MFleet> getdetail(Integer Id, String Company) {
		// TODO Auto-generated method stub
		return mFleetRepository.findById(new MFleetSerializable(Id, Company));
	}
	
	
}
