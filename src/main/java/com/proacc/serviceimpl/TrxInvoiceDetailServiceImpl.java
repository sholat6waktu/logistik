package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxInvoiceDetail;
import com.proacc.repository.TrxInvoiceDetailRepository;
import com.proacc.serializable.TrxInvoiceDetailSerailzable;
import com.proacc.service.TrxInvoiceDetailService;
@Service
public class TrxInvoiceDetailServiceImpl implements TrxInvoiceDetailService{

	private static final Logger logger = LoggerFactory.getLogger(TrxInvoiceDetailServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxInvoiceDetailRepository TrxInvoiceDetailRepository;

	@Override
	public List<Object[]> detailInvoice(String paramAktif, String string, int invoice) {
		// TODO Auto-generated method stub
		String status = "";
		if(paramAktif.equals("y"))
		{
			status = " and a.status = true";
		}
		else if ( paramAktif.equals("n") )
		{
			status = " and a.status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT b.m_contact_customer_from, b.m_contact_customer_from_city, b.trx_order_date, a.order_id, b.trx_order_estimated_cost, b.m_payment_id, b.trx_order_status, a.invoice_header_id, a.invoice_detail_id, a.status, b.m_contact_customer_to_city FROM \"trx_invoice_detail\" a \r\n" + 
				"left join trx_order b on a.order_id = b.trx_order_id and a.company_id = b.trx_order_company_id\r\n" + 
				"where 1=1 " + 
				status + 
				" and a.company_id = :company"
				+ " and a.invoice_header_id = :invoice")
				.setParameter("company", string)
				.setParameter("invoice", invoice)
				.getResultList();
				
	}

	@Override
	public void saveDetail(ArrayList<TrxInvoiceDetail> dataDetail) {
		// TODO Auto-generated method stub
		TrxInvoiceDetailRepository.saveAll(dataDetail);
	}

	@Override
	public Optional<TrxInvoiceDetail> getDetail(int parseInt, int parseInt2, String string) {
		// TODO Auto-generated method stub
		return TrxInvoiceDetailRepository.findById(new TrxInvoiceDetailSerailzable(parseInt, parseInt2, string));
	}

	@Override
	public void save(TrxInvoiceDetail trxInvoiceDetail) {
		// TODO Auto-generated method stub
		TrxInvoiceDetailRepository.save(trxInvoiceDetail);
	}
}
