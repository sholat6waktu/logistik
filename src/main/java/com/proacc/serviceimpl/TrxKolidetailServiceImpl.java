package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxKolidetail;
import com.proacc.repository.TrxKolidetailRepository;
import com.proacc.service.TrxKolidetailService;

@Service
public class TrxKolidetailServiceImpl implements TrxKolidetailService{

	private static final Logger logger = LoggerFactory.getLogger(TrxKolidetailServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxKolidetailRepository trxKolidetailRepository;

	@Override
	public void saveallKoli(ArrayList<TrxKolidetail> trxKolidetailArray) {
		// TODO Auto-generated method stub
		trxKolidetailRepository.saveAll(trxKolidetailArray);
	}

	@Override
	public List<Object[]> getKoli(String lowerCase, String OrderId, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	koli.tm_trx_order_id,\r\n"
				+ "	koli.trx_kolidetail_id,\r\n"
				+ "	koli.trx_kolidetail_weight || ' ' || w.weight_name AS weight,\r\n"
				+ "	koli.trx_kolidetail_height || ' ' || he.lwh_name AS height,\r\n"
				+ "	koli.trx_kolidetail_width || ' ' || wi.lwh_name AS width,\r\n"
				+ "	koli.trx_kolidetail_length || ' ' || le.lwh_name AS LENGTH,\r\n"
				+ "	koli.trx_kolidetail_declareditem,\r\n"
				+ "	koli.trx_kolidetail_status,\r\n"
				+ "	koli.trx_kolidetail_company_id, \r\n"
				+ "	koli.trx_kolidetail_weight, \r\n"
				+ "	w.weight_name, \r\n"
				+ "	koli.trx_kolidetail_height, \r\n"
				+ "	he.lwh_name, \r\n"
				+ "	koli.trx_kolidetail_width, \r\n"
				+ "	wi.lwh_name as namawid, \r\n"
				+ "	koli.trx_kolidetail_length, \r\n"
				+ "	le.lwh_name as namaleng \r\n"
				+ "FROM\r\n"
				+ "	trx_kolidetail AS koli\r\n"
				+ "	LEFT JOIN m_weight AS w ON koli.m_weight_id = w.weight_id \r\n"
				+ "	AND koli.trx_kolidetail_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_lwh AS he ON koli.m_height_id = he.lwh_id \r\n"
				+ "	AND koli.trx_kolidetail_company_id = he.lwh_company_id\r\n"
				+ "	LEFT JOIN m_lwh AS wi ON koli.m_width_id = wi.lwh_id \r\n"
				+ "	AND koli.trx_kolidetail_company_id = wi.lwh_company_id\r\n"
				+ "	LEFT JOIN m_lwh AS le ON koli.m_length_id = le.lwh_id \r\n"
				+ "	AND koli.trx_kolidetail_company_id = le.lwh_company_id "
				+ "WHERE\r\n"
				+ "	trx_kolidetail_company_id = :compid\r\n"
				+ "	AND tm_trx_order_id = :orderid").setParameter("compid", compId).setParameter("orderid", OrderId).getResultList();
	}
	
	
}
