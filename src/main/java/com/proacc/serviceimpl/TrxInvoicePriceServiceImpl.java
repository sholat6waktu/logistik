package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxInvoiceDetail;
import com.proacc.entity.TrxInvoicePrice;
import com.proacc.repository.TrxInvoicePriceRepository;
import com.proacc.serializable.TrxInvoiceDetailSerailzable;
import com.proacc.serializable.TrxInvoicePriceSerializable;
import com.proacc.service.TrxInvoicePriceService;
@Service
public class TrxInvoicePriceServiceImpl implements TrxInvoicePriceService{

	private static final Logger logger = LoggerFactory.getLogger(TrxInvoiceDetailServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxInvoicePriceRepository TrxInvoicePriceRepository;


	@Override
	public void saveDetail(ArrayList<TrxInvoicePrice> dataDetail) {
		// TODO Auto-generated method stub
		TrxInvoicePriceRepository.saveAll(dataDetail);
	}

	@Override
	public Optional<TrxInvoicePrice> getDetail(int parseInt, int parseInt2, String string) {
		// TODO Auto-generated method stub
		return TrxInvoicePriceRepository.findById(new TrxInvoicePriceSerializable(parseInt, parseInt2, string));
	}

	@Override
	public void save(TrxInvoicePrice trxInvoiceDetail) {
		// TODO Auto-generated method stub
		TrxInvoicePriceRepository.save(trxInvoiceDetail);
	}

	@Override
	public List<Object[]> detailPrice(String paramAktif, String company, int id) {
		// TODO Auto-generated method stub
		String status = "";
		if(paramAktif.equals("y"))
		{
			status = " and a.status = true";
		}
		else if ( paramAktif.equals("n") )
		{
			status = " and a.status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT * FROM trx_invoice_price a \r\n" + 
				 
				"where 1=1 " + 
				status + 
				" and a.company_id = :company"
				+ " and a.invoice_header_id = :invoice")
				.setParameter("company", company)
				.setParameter("invoice", id)
				.getResultList();
	}

}
