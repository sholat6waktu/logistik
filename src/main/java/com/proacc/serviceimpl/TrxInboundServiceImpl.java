package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.proacc.entity.MAgendetail;
import com.proacc.entity.TrxInbound;
import com.proacc.repository.TrxInboundRepository;
import com.proacc.serializable.MAgendetailSerializable;
import com.proacc.service.TrxInboundService;

@Service
public class TrxInboundServiceImpl implements TrxInboundService{

	private static final Logger logger = LoggerFactory.getLogger(TrxInboundServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxInboundRepository TrxInboundRepository;

	@Override
	public List<Object[]> getId() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('inbound_history_sequence')").getResultList();
	}

	@Override
	public void save(TrxInbound dataInboundHistory) {
		// TODO Auto-generated method stub
		TrxInboundRepository.save(dataInboundHistory);
	}

	@Override
	public List<Object[]> getList(String date, String order, String koli, String weight, String header) {
		// TODO Auto-generated method stub
		String tanggal = "";
		String orderan = "";
		String koliSearch = "";
		String weightSearch = "";
		if(!date.equals("") || !date.isEmpty())
		{
			tanggal = "and\r\n" + 
					"		date(mn.date_inbound) = '"+date+"'";
		}
		
		if(!order.equals("") || !order.isEmpty())
		{
			orderan = "and\r\n" + 
					"	mn.trx_order_id LIKE'%"+order+"%'";
		}
		
		if(!koli.equals("") || !koli.isEmpty())
		{
			koliSearch = "	and\r\n" + 
					"	mn.trx_order_koli = "+Integer.parseInt(koli)+"";
		}
		
		if(!weight.equals("") || !weight.isEmpty())
		{
			weightSearch = "and mn.total_berat like '%"+weight+"%'";
		}
		return this.em.createNativeQuery("select * from (\r\n" + 
				"SELECT A\r\n" + 
				"	.date_inbound,\r\n" + 
				"	A.trx_order_id,\r\n" + 
				"	concat ( C.trx_order_weight, ' ', d.weight_name ) total_berat,\r\n" + 
				"	c.trx_order_koli,\r\n" + 
				"-- 	customer from from\r\n" + 
				"	e.m_contact_customer_from_id,\r\n" + 
				"-- 	customer from to\r\n" + 
				"	e.m_contact_customer_to_id asdf,\r\n" + 
				"-- 	agen from\r\n" + 
				"	f_from.agen_name agen_name_from,\r\n" + 
				"	g_from.m_kota_id agen_kota_from,\r\n" + 
				"	g_from.m_province_id agen_province_from,\r\n" + 
				"-- 	cabang from\r\n" + 
				"h_from.cabang_name cabang_name_from,\r\n" + 
				"h_from.m_province_id cabang_province_from,\r\n" + 
				"h_from.m_kota_id cabang_kota_from,\r\n" + 
				"\r\n" + 
				"-- customer to from\r\n" + 
				"f.m_contact_customer_from_id customer_to_from,\r\n" + 
				"-- customer to to\r\n" + 
				"f.m_contact_customer_to_id,\r\n" + 
				"-- agen to\r\n" + 
				"	f_to.agen_name agen_name_to,\r\n" + 
				"	g_to.m_kota_id agen_kota_to,\r\n" + 
				"	g_to.m_province_id agen_province_to,\r\n" + 
				"-- 	cabang to\r\n" + 
				"h_to.cabang_name cabang_name_to,\r\n" + 
				"h_to.m_province_id cabang_province_to,\r\n" + 
				"h_to.m_kota_id cabang_kota_to,\r\n" + 
				"A.trx_tracking_from_id,\r\n" + 
				"A.trx_tracking_to_id,\r\n" + 
				"c.m_contact_customer_from,\r\n" + 
				"c.m_contact_customer_to,"
				+ " cast (n.m_contact_driver as varchar),\r\n" + 
				"cast (n.m_contact_helper as varchar ) \r\n" + 
				"FROM\r\n" + 
				"	\"trx_inbound\"\r\n" + 
				"	A inner JOIN trx_order C ON C.trx_order_id = A.trx_order_id \r\n" + 
				"	AND C.trx_order_company_id = A.company_id\r\n" + 
				"	LEFT JOIN m_weight d ON d.weight_id = C.m_weight_id \r\n" + 
				"	AND d.weight_company_id = C.trx_order_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN trx_tracking e ON A.trx_tracking_from_id = e.trx_tracking_id \r\n" + 
				"	AND A.trx_order_id = e.tm_trx_order_id \r\n" + 
				"	AND A.company_id = e.trx_tracking_company_id\r\n" + 
				"	LEFT JOIN m_agen f_from ON f_from.agen_id = e.m_agen_id \r\n" + 
				"	AND f_from.agen_company_id = e.trx_tracking_company_id\r\n" + 
				"	LEFT JOIN m_agendetail g_from ON g_from.agendetail_id = e.m_agendetail_id \r\n" + 
				"	AND g_from.m_agen_id = e.m_agen_id \r\n" + 
				"	AND g_from.agendetail_company_id = e.trx_tracking_company_id\r\n" + 
				"	LEFT JOIN m_cabang h_from ON h_from.cabang_id = e.m_cabang_id \r\n" + 
				"	AND h_from.cabang_company_id = e.trx_tracking_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN trx_tracking f ON A.trx_tracking_to_id = f.trx_tracking_id \r\n" + 
				"	AND A.trx_order_id = f.tm_trx_order_id \r\n" + 
				"	AND A.company_id = f.trx_tracking_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_agen f_to ON f_to.agen_id = f.m_agen_id \r\n" + 
				"	AND f_to.agen_company_id = f.trx_tracking_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_agendetail g_to ON g_to.agendetail_id = f.m_agendetail_id \r\n" + 
				"	AND g_to.m_agen_id = f.m_agen_id \r\n" + 
				"	AND g_to.agendetail_company_id = f.trx_tracking_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_cabang h_to ON h_to.cabang_id = f.m_cabang_id \r\n" + 
				"	AND h_to.cabang_company_id = f.trx_tracking_company_id "
				+ " 	left join trx_driverschedule m on a.trx_order_id = m.tm_trx_order_id and a.trx_tracking_from_id = m.tm_trx_tracking_id_from and a.trx_tracking_to_id = m.tm_trx_tracking_id_to and a.company_id = m.trx_driverschedule_company_id\r\n" + 
				"	left join m_driver n on n.driver_id = m.m_driver_id and n.driver_company_id = m.trx_driverschedule_company_id \r\n" + 
				"-- 	WHERE\r\n" + 
				"-- 	date(a.date_inbound) = '2021-05-28'\r\n" + 
				"-- and\r\n" + 
				"-- 	A.trx_order_id LIKE'%INV/1/ 0003%'\r\n" + 
				"-- 	and\r\n" + 
				"-- 	c.trx_order_koli = 23\r\n" + 
				"	) mn \r\n" + 
				"	where 1=1" + 
				"	" +tanggal+orderan+koliSearch+weightSearch + " and (mn.agen_name_to = :header or mn.cabang_name_to = :header)").setParameter("header", header).getResultList();
	}


	
}
