package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxJalurDriver;
import com.proacc.repository.TrxJalurDriverRepository;
import com.proacc.serializable.TrxJalurDriverSerializable;
import com.proacc.service.TrxJalurDriverService;

@Service
public class TrxJalurDriverServiceImpl implements TrxJalurDriverService{
	private static final Logger logger = LoggerFactory.getLogger(TrxJalurDriverServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxJalurDriverRepository TrxJalurDriverRepository;

	@Override
	public Optional<TrxJalurDriver> getDetail(Integer trxDriverjalurheaderId, Integer trxDriverjalurId,
			String trxTrackingCompanyId) {
		// TODO Auto-generated method stub
		return TrxJalurDriverRepository.findById(new TrxJalurDriverSerializable(trxDriverjalurheaderId, trxDriverjalurId, trxTrackingCompanyId));
	}

	@Override
	public List<Object[]> findByJalurHeaderId(int parseInt, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT C\r\n" + 
				"	.cabang_name,\r\n" + 
				"	C.m_province_id,\r\n" + 
				"	C.m_kota_id,\r\n" + 
				"	d.agen_name,\r\n" + 
				"	e.m_province_id a,\r\n" + 
				"	e.m_kota_id b,\r\n" + 
				"	A.status_progress,\r\n" + 
				"	A.date_tracking,\r\n" + 
				"	A.date_kantor \r\n" + 
				"FROM\r\n" + 
				"	trx_jalurdriver\r\n" + 
				"	A LEFT JOIN m_route_jalur b ON b.m_route_id = A.m_route_id \r\n" + 
				"	AND b.route_jalur_id = A.m_routejalur_id \r\n" + 
				"	AND b.route_jalur_company_id = A.company_id\r\n" + 
				"	LEFT JOIN m_cabang C ON C.cabang_id = b.m_cabang_id \r\n" + 
				"	AND C.cabang_company_id = b.route_jalur_company_id\r\n" + 
				"	LEFT JOIN m_agen d ON b.m_agen_id = d.agen_id \r\n" + 
				"	AND b.route_jalur_company_id = d.agen_company_id\r\n" + 
				"	LEFT JOIN m_agendetail e ON e.m_agen_id = b.m_agen_id \r\n" + 
				"	AND e.agendetail_id = b.m_agendetail_id \r\n" + 
				"	AND e.agendetail_company_id = b.route_jalur_company_id\r\n" + 
				"	where \r\n" + 
				"	a.trx_driverjalurheader_id = :headerId \r\n" + 
				"	and a.company_id = :companyId \r\n" + 
				"	order by a.trx_driverjalur_id asc")
				.setParameter("headerId", parseInt)
				.setParameter("companyId", string)
				.getResultList();
	}

}
