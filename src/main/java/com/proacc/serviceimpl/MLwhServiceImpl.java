package com.proacc.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.service.MLwhService;

@Service
public class MLwhServiceImpl implements MLwhService{

	private static final Logger logger = LoggerFactory.getLogger(MLwhServiceImpl.class);
	
	@Autowired
	EntityManager em;

	@Override
	public List<Object[]> getallLwh(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and lwh_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and lwh_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	lwh_id,\r\n"
				+ "	lwh_name,\r\n"
				+ "	lwh_tocm,\r\n"
				+ "	lwh_description,\r\n"
				+ "	CAST ( lwh_created_by AS VARCHAR ),\r\n"
				+ "	lwh_created_at,\r\n"
				+ "	CAST ( lwh_updated_by AS VARCHAR ),\r\n"
				+ "	lwh_updated_at,\r\n"
				+ "	lwh_status,\r\n"
				+ "	lwh_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_lwh \r\n"
				+ "WHERE\r\n"
				+ "	lwh_company_id = :compid" + status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getLwh(String lowerCase, Integer lwhId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and lwh_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and lwh_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	lwh_id,\r\n"
				+ "	lwh_name,\r\n"
				+ "	lwh_tocm,\r\n"
				+ "	lwh_description,\r\n"
				+ "	CAST ( lwh_created_by AS VARCHAR ),\r\n"
				+ "	lwh_created_at,\r\n"
				+ "	CAST ( lwh_updated_by AS VARCHAR ),\r\n"
				+ "	lwh_updated_at,\r\n"
				+ "	lwh_status,\r\n"
				+ "	lwh_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_lwh \r\n"
				+ "WHERE\r\n"
				+ "	lwh_company_id = :compid"
				+ "	and lwh_id = :lwhid" + status).setParameter("compid", compId).setParameter("lwhid", lwhId).getResultList();
	}
}
