package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxOrder;
import com.proacc.repository.TrxOrderRepository;
import com.proacc.serializable.TrxOrderSerializable;
import com.proacc.service.TrxOrderService;

@Service
public class TrxOrderServiceImpl implements TrxOrderService{

	private static final Logger logger = LoggerFactory.getLogger(TrxOrderServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxOrderRepository trxOrderRepository;

	@Override
	public List<Object[]> getallOrder(String lowerCase, String compId, String status, UUID customer) {
		//String status2 = status.equals("on progress") == true ? 'and (trx_order_status = :status or trx_order_status = :status)' : 'and trx_order_status = :status';
		String stats = "";
		if(status.equals("on Proggress"))
		{
			 stats = " 	AND (\r\n"
			 		+ "		( lower(trxo.trx_order_status) = 'data entry' AND trxt.trx_tracking_id = 2 AND ( C.cabang_name = '"+ customer.toString() +"' OR (A.agen_name = '"+ customer.toString() +"' and a.agen_type = 1) ) ) \r\n"
			 		+ "	OR ( trxo.trx_order_status = 'on Proggress'  AND trxt.trx_tracking_statusinbound = 'ware house' AND ( C.cabang_name = '"+ customer.toString() +"' OR (A.agen_name = '"+ customer.toString() +"' and a.agen_type = 1))) \r\n"
			 		+ "	)";
		}
		else if(status.equals("data Entry"))
		{
			 stats = " AND trx_order_status = 'data Entry'";
		}
		return this.em.createNativeQuery(" select * from ( SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.trx_order_pickup_address,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	ser.service_name,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "		case when (SELECT\r\n" + 
				"	a.trx_tracking_statusinbound \r\n" + 
				"FROM\r\n" + 
				"	trx_tracking a\r\n" + 
				"WHERE\r\n" + 
				"	a.tm_trx_order_id = trxo.trx_order_id \r\n" + 
				"	and a.trx_tracking_company_id = trxo.trx_order_company_id\r\n" + 
				"	AND a.trx_tracking_id = ( SELECT b.trx_tracking_id FROM trx_tracking b WHERE b.tm_trx_order_id = trxo.trx_order_id  and b.trx_tracking_company_id =trxo.trx_order_company_id ORDER BY b.trx_tracking_id DESC LIMIT 1 ) - 1 ) = 'ware house'  and\r\n" + 
				"	(SELECT\r\n" + 
				"	a.trx_tracking_id \r\n" + 
				"FROM\r\n" + 
				"	trx_tracking a\r\n" + 
				"WHERE\r\n" + 
				"	a.tm_trx_order_id = trxo.trx_order_id \r\n" + 
				"	and a.trx_tracking_company_id = trxo.trx_order_company_id\r\n" + 
				"	AND a.trx_tracking_id = ( SELECT b.trx_tracking_id FROM trx_tracking b WHERE b.tm_trx_order_id = trxo.trx_order_id  and b.trx_tracking_company_id =trxo.trx_order_company_id ORDER BY b.trx_tracking_id DESC LIMIT 1 ) - 1) = 1\r\n" + 
				"	then 'data entry' \r\n" + 
				"	when\r\n" + 
				"	(SELECT\r\n" + 
				"	a.trx_tracking_statusinbound \r\n" + 
				"FROM\r\n" + 
				"	trx_tracking a\r\n" + 
				"WHERE\r\n" + 
				"	a.tm_trx_order_id = trxo.trx_order_id \r\n" + 
				"	and a.trx_tracking_company_id = trxo.trx_order_company_id\r\n" + 
				"	AND a.trx_tracking_id = ( SELECT b.trx_tracking_id FROM trx_tracking b WHERE b.tm_trx_order_id = trxo.trx_order_id  and b.trx_tracking_company_id =trxo.trx_order_company_id ORDER BY b.trx_tracking_id DESC LIMIT 1 ) - 1 ) = 'ware house'  and\r\n" + 
				"	(SELECT\r\n" + 
				"	a.trx_tracking_id \r\n" + 
				"FROM\r\n" + 
				"	trx_tracking a\r\n" + 
				"WHERE\r\n" + 
				"	a.tm_trx_order_id = trxo.trx_order_id \r\n" + 
				"	and a.trx_tracking_company_id = trxo.trx_order_company_id\r\n" + 
				"	AND a.trx_tracking_id = ( SELECT b.trx_tracking_id FROM trx_tracking b WHERE b.tm_trx_order_id = trxo.trx_order_id  and b.trx_tracking_company_id =trxo.trx_order_company_id ORDER BY b.trx_tracking_id DESC LIMIT 1 ) - 1) != 1\r\n" + 
				"	then 'data entry2' else\r\n" + 
				"	trxo.trx_order_status end,\r\n"
				+ "	trxo.trx_order_company_id,\r\n"
				+ "	trxo.trx_order_weight || ' ' || w.weight_name as berat,\r\n"
				+ "	trxo.trx_order_weight,\r\n"
				+ "	w.weight_id,\r\n"
				+ "	w.weight_name,\r\n"
				+ "	trxo.trx_order_volume || ' ' || v.volume_name as volume,\r\n"
				+ "	trxo.trx_order_volume,\r\n"
				+ "	v.volume_id,\r\n"
				+ "	v.volume_name,"
				+ " trxt.trx_tracking_id,"
				+ " case when lower(trxo.trx_order_status) = 'data entry' then \r\n" + 
				"	trxt.trx_tracking_id -1 else trxt.trx_tracking_id end trx_kota_from,\r\n" + 
				"	case when lower(trxo.trx_order_status) = 'data entry' then \r\n" + 
				"	trxt.trx_tracking_id  else trxt.trx_tracking_id+1 end trx_kota_to,\r\n" + 
				"	(select trx_tracking_id from trx_tracking where trx_order_id = trxo.trx_order_id order by trx_tracking_id desc limit 1  ) last_tracking_id  \r\n"
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n"
				+ "	AND trxo.trx_order_company_id = ser.service_company_id\r\n"
				+ "	LEFT JOIN m_weight AS w ON trxo.m_weight_id = w.weight_id \r\n"
				+ "	AND trxo.trx_order_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_volume AS v ON trxo.m_volume_id = v.volume_id \r\n"
				+ "	AND trxo.trx_order_company_id = v.volume_company_id\r\n"
				+ "	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n"
				+ "	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n"
				+ "	LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n"
				+ "	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = A.agen_company_id "
				+ "WHERE\r\n"
				+ "	trx_order_company_id = :compid "
				+ stats + "	) p\r\n" + 
//						"	where\r\n" + 
//						"	trx_kota_to != last_tracking_id\r\n" + 
						"	").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getOrder(String lowerCase, String OrderId, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.m_contact_attedance_from,\r\n"
				+ "	trxo.m_contact_customer_from_city,\r\n"
				+ "	trxo.trx_order_pickup_address,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	trxo.m_contact_attedance_to,\r\n"
				+ "	trxo.m_contact_customer_to_city,\r\n"
				+ "	trxo.m_service_id,\r\n"
				+ "	trxo.trx_order_sendtype,\r\n"
				+ "	trxo.m_packing_id,\r\n"
				+ "	trxo.trx_order_nostt,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "	trxo.trx_order_estimated_cost,\r\n"
				+ "	trxo.m_payment_id,\r\n"
				+ "	trxo.trx_order_kolitype,\r\n"
				+ "	trxo.trx_order_weight,\r\n"
				+ "	trxo.m_weight_id,\r\n"
				+ "	trxo.trx_order_connocement,\r\n"
				+ "	trxo.trx_order_receiving_date,\r\n"
				+ "	trxo.trx_order_instruction,\r\n"
				+ "	trxo.trx_order_volume,\r\n"
				+ "	trxo.m_volume_id,\r\n"
				+ "	trxo.tm_trx_invoice_id,\r\n"
				+ "	trxo.trx_order_complete_date,\r\n"
				+ "	trxo.trx_order_nettprice,\r\n"
				+ "	trxo.trx_order_ordertype,\r\n"
				+ "	trxo.trx_order_solution,\r\n"
				+ "	trxo.trx_order_returtype,\r\n"
				+ "	trxo.trx_order_status,\r\n"
				+ "	trxo.trx_order_company_id,\r\n"
				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ),\r\n"
				+ "	trxo.trx_order_created_at,\r\n"
				+ "	trxo.trx_kota_from,\r\n"
				+ "	trxo.trx_kota_to,\r\n"
				+ "	s.service_name,\r\n"
				+ "	p.packing_name, \r\n"
				+ "	trxo.m_contact_attedance_pickup \r\n"
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN m_service AS s ON trxo.m_service_id = s.service_id \r\n"
				+ "	AND trxo.trx_order_company_id = s.service_company_id\r\n"
				+ "	LEFT JOIN m_packing AS P ON trxo.m_packing_id = P.packing_id\r\n"
				+ "	AND trxo.trx_order_company_id = p.packing_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trx_order_company_id = :compid\r\n"
				+ "	AND trx_order_id = :orderid").setParameter("compid", compId).setParameter("orderid", OrderId).getResultList();
	}

	@Override
	public void saveOrder(TrxOrder trxOrder) {
		// TODO Auto-generated method stub
		trxOrderRepository.save(trxOrder);
	}

	@Override
	public Optional<TrxOrder> getdetail(String Id, String Company) {
		// TODO Auto-generated method stub
		return trxOrderRepository.findById(new TrxOrderSerializable(Id, Company));
	}

	@Override
	public List<Object[]> estcost(Integer customerId, Integer cityFrom, Integer cityTo, Integer service, String vehicle,
			Float weight, String company) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select estimetedcost, 123 sok from ( select estimetedcost(:customerid, :cityfrom, :cityto, :service, :vehicle, :weight, :company) ) as q")
				.setParameter("customerid", customerId).setParameter("cityfrom", cityFrom).setParameter("cityto", cityTo).setParameter("service", service).setParameter("vehicle", vehicle)
				.setParameter("weight", weight).setParameter("company", company).getResultList();
	}

	@Override
	public List<Object[]> getmyOrder(String lowerCase, UUID customerFrom, String compId) {
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.trx_order_pickup_address,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	ser.service_name,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "	trxo.trx_order_status,\r\n"
				+ "	trxo.trx_order_company_id, \r\n"
				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ) \r\n"
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n"
				+ "	AND trxo.trx_order_company_id = ser.service_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trx_order_company_id = :compid \r\n"
				+ " and trxo.trx_order_created_by = :customerfrom").setParameter("compid", compId).setParameter("customerfrom", customerFrom).getResultList();
	}

	@Override
	public List<Object[]> getIncomingOrder(String lowerCase, String OrderId, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	trxo.trx_order_nostt,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "	trxo.trx_order_weight || ' ' || w.weight_name AS berat,\r\n"
				+ "	trxo.trx_order_volume || ' ' || v.volume_name AS volume,\r\n"
				+ "	trxo.trx_order_company_id, \r\n"
				+ "	trxo.trx_order_status "
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN m_weight AS w ON trxo.m_weight_id = w.weight_id \r\n"
				+ "	AND trxo.trx_order_company_id = w.weight_company_id \r\n"
				+ "	LEFT JOIN m_volume as v ON trxo.m_volume_id = v.volume_id\r\n"
				+ "	AND trxo.trx_order_company_id = v.volume_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trx_order_company_id = :compid\r\n"
				+ "	AND trx_order_id = :orderid").setParameter("compid", compId).setParameter("orderid", OrderId).getResultList();
	}

	@Override
	public List<Object[]> getmyinbound(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno) {
		// TODO Auto-generated method stub
		String status = "";
		String statusDate = " ";
		String kotaToStatus = " ";
		String kurirStatus = " ";
		if(kotato.isEmpty() == false)
		{
			kotaToStatus = " AND trxo.trx_kota_to LIKE '" + kotato.toUpperCase() + "%'";
		}
		if(kurirname.isEmpty() == false)
		{
			kurirStatus = " AND d.driver_name LIKE '" + kurirname + "%'";
		}
		
		if(orderdate.isEmpty() == false)
		{
			statusDate = " AND trxo.trx_order_date = '" + orderdate + "'";
			logger.info(statusDate + " <==status date");
		}
		else
		{
			statusDate = " ";
		}
		if(conno.isEmpty() == false)
		{
			status = " AND trxo.trx_order_connocement LIKE '" + conno + "%'";
		}
		else
		{
			status = " ";
		}
//		return this.em.createNativeQuery("SELECT\r\n" 
//				+ "	trxo.trx_order_id,\r\n"
//				+ "	trxo.trx_order_date,\r\n"
//				+ "	trxo.m_contact_customer_from,\r\n"
//				+ "	trxo.trx_order_pickup_address,\r\n"
//				+ "	trxo.m_contact_customer_to,\r\n"
//				+ "	ser.service_name,\r\n"
//				+ "	trxo.trx_order_koli,\r\n"
//				+ "	trxt.trx_tracking_statusinbound,\r\n"
//				+ "	trxo.trx_order_company_id,\r\n"
//				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n"
//				+ "	trxt.trx_tracking_id "
//				+ "FROM\r\n"
//				+ "	trx_order AS trxo\r\n"
//				+ "	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n"
//				+ "	AND trxo.trx_order_company_id = ser.service_company_id\r\n"
//				+ "	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n"
//				+ "	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n"
//				+ "	LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n"
//				+ "	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n"
//				+ "	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n"
//				+ "	AND trxt.m_agen_id = A.agen_id\r\n"
//				+ "	LEFT JOIN trx_driverschedule AS trxd ON trxt.tm_trx_order_id = trxd.tm_trx_order_id \r\n"
//				+ "	AND ( trxt.trx_tracking_id = trxd.tm_trx_tracking_id_from ) \r\n"
//				+ "	AND trxt.trx_tracking_company_id = trxd.trx_driverschedule_company_id \r\n"
//				+ "	LEFT JOIN m_driver as d ON trxd.m_driver_id = d.driver_id\r\n"
//				+ "	AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n"
//				+ "WHERE\r\n"
//				+ "	trxo.trx_order_company_id = :compid \r\n"
//				+ "	AND trxt.trx_tracking_statusinbound IS NOT NULL \r\n"
//				//disini ada penambahan jika diinsert dua dua nya antara parent id dan agent id maka yang di filter adalah yang cabang
//				+ "	and case when c.cabang_name is not null and a.agen_name is not null then\r\n" + 
//				" c.cabang_name = :customer\r\n" + 
//				//end --
//				"else\r\n" + 
//				" ( C.cabang_name = :customer OR A.agen_name = :customer)  end " 
//				+ status + statusDate + kotaToStatus + kurirStatus
//				+ "GROUP BY\r\n"
//				+ "	trxo.trx_order_id,\r\n"
//				+ "	trxo.trx_order_date,\r\n"
//				+ "	trxo.m_contact_customer_from,\r\n"
//				+ "	trxo.trx_order_pickup_address,\r\n"
//				+ "	trxo.m_contact_customer_to,\r\n"
//				+ "	ser.service_name,\r\n"
//				+ "	trxo.trx_order_koli,\r\n"
//				+ "	trxt.trx_tracking_statusinbound,\r\n"
//				+ "	trxo.trx_order_company_id,\r\n"
//				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), "
//				+ "	trxt.trx_tracking_id ").setParameter("compid", compId).setParameter("customer", customer.toString()).getResultList();
		
		return this.em.createNativeQuery("select q.*,	\r\n" + 
				"-- 	from\r\n" + 
				"c2.m_kota_id from_kota_cabang,\r\n" + 
				"c2.m_province_id from_provinsi_cabang,\r\n" + 
				"e2.m_kota_id from_kota_agen,\r\n" + 
				"e2.m_province_id from_provinsi_agen,\r\n" + 
				"	trxt_from.m_contact_customer_from_id from_cust_from,\r\n" + 
				"	trxt_from.m_contact_customer_to_id from_cust_to\r\n" + 
				"	from (SELECT\r\n" + 
				"	trxo.trx_order_id,\r\n" + 
				"	trxo.trx_order_date,\r\n" + 
				"	trxo.m_contact_customer_from,\r\n" + 
				"	trxo.trx_order_pickup_address,\r\n" + 
				"	trxo.m_contact_customer_to,\r\n" + 
				"	ser.service_name,\r\n" + 
				"	trxo.trx_order_koli,\r\n" + 
				"	trxt.trx_tracking_statusinbound,\r\n" + 
				"	trxo.trx_order_company_id,\r\n" + 
				"	CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n" + 
				
				"	trxt.trx_tracking_id tracking_to,\r\n" +
				"	(trxt.trx_tracking_id-1) tracking_from,\r\n" + 
				"c.m_kota_id to_kota_cabang,\r\n" + 
				"c.m_province_id to_provinsi_cabang,\r\n" + 
				"e.m_kota_id to_kota_agen,\r\n" + 
				"e.m_province_id to_provinsi_agen,\r\n" + 
				"	trxt.m_contact_customer_from_id to_cust_from,\r\n" + 
				"	trxt.m_contact_customer_to_id to_cust_to\r\n" + 
				"\r\n" + 
				"	FROM\r\n" + 
				"	trx_order AS trxo\r\n" + 
				"	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n" + 
				"	AND trxo.trx_order_company_id = ser.service_company_id\r\n" + 
				"\r\n" + 
				"	\r\n" + 
				"-- 	to\r\n" + 
				"	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n" + 
				"	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n" + 
				"-- 	to cabang / agen\r\n" + 
				"	LEFT JOIN m_cabang AS C ON trxt.m_cabang_id = C.cabang_id \r\n" + 
				"	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n" + 
				"	AND trxt.m_agen_id = A.agen_id\r\n" + 
				"	left join m_agendetail as e on trxt.m_agen_id = e.m_agen_id and trxt.m_agendetail_id = e.agendetail_id and trxt.trx_tracking_company_id = e.agendetail_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN trx_driverschedule AS trxd ON trxt.tm_trx_order_id = trxd.tm_trx_order_id \r\n" + 
				"	AND ( trxt.trx_tracking_id = trxd.tm_trx_tracking_id_from ) \r\n" + 
				"	AND trxt.trx_tracking_company_id = trxd.trx_driverschedule_company_id \r\n" + 
				"	LEFT JOIN m_driver as d ON trxd.m_driver_id = d.driver_id\r\n" + 
				"	AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n" + 
				"WHERE\r\n" + 
				"	trxo.trx_order_company_id = :compid \r\n" + 
				"	AND trxt.trx_tracking_statusinbound IS NOT NULL \r\n" + 
				"	and case when c.cabang_name is not null and a.agen_name is not null then\r\n" + 
				" c.cabang_name = :customer\r\n" + 
				"else\r\n" + 
				" ( C.cabang_name = :customer OR A.agen_name = :customer)  end     \r\n" 
				+ status + statusDate + kotaToStatus + kurirStatus +
				" GROUP BY\r\n" + 
				"	trxo.trx_order_id,\r\n" + 
				"	trxo.trx_order_date,\r\n" + 
				"	trxo.m_contact_customer_from,\r\n" + 
				"	trxo.trx_order_pickup_address,\r\n" + 
				"	trxo.m_contact_customer_to,\r\n" + 
				"	ser.service_name,\r\n" + 
				"	trxo.trx_order_koli,\r\n" + 
				"	trxt.trx_tracking_statusinbound,\r\n" + 
				"	trxo.trx_order_company_id,\r\n" + 
				"	CAST ( trxo.trx_order_created_by AS VARCHAR ), 	trxt.trx_tracking_id ,\r\n" + 
				"	c.m_kota_id,\r\n" + 
				"	c.m_province_id,\r\n" + 
				"	e.m_kota_id,\r\n" + 
				"	e.m_province_id,\r\n" + 
				"	trxt.m_contact_customer_from_id,\r\n" + 
				"	trxt.m_contact_customer_to_id ) q\r\n" + 
				"-- 		from\r\n" + 
				"LEFT JOIN trx_tracking AS trxt_from ON q.trx_order_id = trxt_from.tm_trx_order_id \r\n" + 
				"	AND trxt_from.trx_tracking_company_id = q.trx_order_company_id\r\n" + 
				"	and trxt_from.trx_tracking_id = q.tracking_from\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_cabang AS C2 ON trxt_from.m_cabang_id = C2.cabang_id \r\n" + 
				"	AND trxt_from.trx_tracking_company_id = C2.cabang_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_agen AS A2 ON trxt_from.m_agen_id = A2.agen_id \r\n" + 
				"	and trxt_from.trx_tracking_company_id = a2.agen_company_id\r\n" + 
				"	left join m_agendetail as e2 on trxt_from.m_agen_id = e2.m_agen_id and trxt_from.m_agendetail_id = e2.agendetail_id and trxt_from.trx_tracking_company_id = e2.agendetail_company_id").setParameter("compid", compId).setParameter("customer", customer.toString()).getResultList();
		
		
	}

	@Override
	public List<Object[]> getmyoutbound(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno) {
		
		String status = "";
		String statusDate = " ";
		String kotaToStatus = " ";
		String kurirStatus = " ";
		if(kotato.isEmpty() == false)
		{
			kotaToStatus = " AND trxo.trx_kota_to LIKE '" + kotato.toUpperCase() + "%'";
		}
		if(kurirname.isEmpty() == false)
		{
			kurirStatus = " AND d.driver_name LIKE '" + kurirname + "%'";
		}
		
		if(orderdate.isEmpty() == false)
		{
			statusDate = " AND trxo.trx_order_date = '" + orderdate + "'";
			logger.info(statusDate + " <==status date");
		}
		else
		{
			statusDate = " ";
		}
		if(conno.isEmpty() == false)
		{
			status = " AND trxo.trx_order_connocement LIKE '" + conno + "%'";
		}
		else
		{
			status = " ";
		}
//		return this.em.createNativeQuery("SELECT\r\n"
//				+ "	trxo.trx_order_id,\r\n"
//				+ "	trxo.trx_order_date,\r\n"
//				+ "	trxo.m_contact_customer_from,\r\n"
//				+ "	trxo.trx_order_pickup_address,\r\n"
//				+ "	trxo.m_contact_customer_to,\r\n"
//				+ "	ser.service_name,\r\n"
//				+ "	trxo.trx_order_koli,\r\n"
//				+ "	trxt.trx_tracking_statusoutbound,\r\n"
//				+ "	trxo.trx_order_company_id,\r\n"
//				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n"
//				+ "	trxt.trx_tracking_id "
//				+ "FROM\r\n"
//				+ "	trx_order AS trxo\r\n"
//				+ "	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n"
//				+ "	AND trxo.trx_order_company_id = ser.service_company_id\r\n"
//				+ "	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n"
//				+ "	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n"
//				+ "	LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n"
//				+ "	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n"
//				+ "	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n"
//				+ "	AND trxt.m_agen_id = A.agen_id\r\n"
//				+ "	LEFT JOIN trx_driverschedule AS trxd ON trxt.tm_trx_order_id = trxd.tm_trx_order_id \r\n"
//				+ "	AND ( trxt.trx_tracking_id = trxd.tm_trx_tracking_id_from OR trxt.trx_tracking_id = trxd.tm_trx_tracking_id_to ) \r\n"
//				+ "	AND trxt.trx_tracking_company_id = trxd.trx_driverschedule_company_id \r\n"
//				+ "	LEFT JOIN m_driver as d ON trxd.m_driver_id = d.driver_id\r\n"
//				+ "	AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n"
//				+ "WHERE\r\n"
//				+ "	trxo.trx_order_company_id = :compid \r\n"
//				+ "	AND trxt.trx_tracking_statusoutbound IS NOT NULL \r\n"
//				+ "	AND ( C.cabang_name = :customer OR A.agen_name = :customer) " 
//				+ status + statusDate + kotaToStatus + kurirStatus
//				+ " GROUP BY\r\n"
//				+ "	trxo.trx_order_id,\r\n"
//				+ "	trxo.trx_order_date,\r\n"
//				+ "	trxo.m_contact_customer_from,\r\n"
//				+ "	trxo.trx_order_pickup_address,\r\n"
//				+ "	trxo.m_contact_customer_to,\r\n"
//				+ "	ser.service_name,\r\n"
//				+ "	trxo.trx_order_koli,\r\n"
//				+ "	trxt.trx_tracking_statusoutbound,\r\n"
//				+ "	trxo.trx_order_company_id,\r\n"
//				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), "
//				+ "	trxt.trx_tracking_id ").setParameter("compid", compId).setParameter("customer", customer.toString()).getResultList();
		
		return this.em.createNativeQuery("select q.*,	\r\n" + 
				"-- 	from\r\n" + 
				"c2.m_kota_id from_kota_cabang,\r\n" + 
				"c2.m_province_id from_provinsi_cabang,\r\n" + 
				"e2.m_kota_id from_kota_agen,\r\n" + 
				"e2.m_province_id from_provinsi_agen,\r\n" + 
				"	trxt_from.m_contact_customer_from_id from_cust_from,\r\n" + 
				"	trxt_from.m_contact_customer_to_id from_cust_to\r\n" + 
				"	from (SELECT\r\n" + 
				"	trxo.trx_order_id,\r\n" + 
				"	trxo.trx_order_date,\r\n" + 
				"	trxo.m_contact_customer_from,\r\n" + 
				"	trxo.trx_order_pickup_address,\r\n" + 
				"	trxo.m_contact_customer_to,\r\n" + 
				"	ser.service_name,\r\n" + 
				"	trxo.trx_order_koli,\r\n" + 
				"	trxt.trx_tracking_statusoutbound,\r\n" + 
				"	trxo.trx_order_company_id,\r\n" + 
				"	CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n" + 
				
				"	trxt.trx_tracking_id tracking_to,\r\n" +
				"	(trxt.trx_tracking_id-1) tracking_from,\r\n" + 
				"c.m_kota_id to_kota_cabang,\r\n" + 
				"c.m_province_id to_provinsi_cabang,\r\n" + 
				"e.m_kota_id to_kota_agen,\r\n" + 
				"e.m_province_id to_provinsi_agen,\r\n" + 
				"	trxt.m_contact_customer_from_id to_cust_from,\r\n" + 
				"	trxt.m_contact_customer_to_id to_cust_to\r\n" + 
				"\r\n" + 
				"	FROM\r\n" + 
				"	trx_order AS trxo\r\n" + 
				"	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n" + 
				"	AND trxo.trx_order_company_id = ser.service_company_id\r\n" + 
				"\r\n" + 
				"	\r\n" + 
				"-- 	to\r\n" + 
				"	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n" + 
				"	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n" + 
				"-- 	to cabang / agen\r\n" + 
				"	LEFT JOIN m_cabang AS C ON trxt.m_cabang_id = C.cabang_id \r\n" + 
				"	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n" + 
				"	AND trxt.m_agen_id = A.agen_id\r\n" + 
				"	left join m_agendetail as e on trxt.m_agen_id = e.m_agen_id and trxt.m_agendetail_id = e.agendetail_id and trxt.trx_tracking_company_id = e.agendetail_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN trx_driverschedule AS trxd ON trxt.tm_trx_order_id = trxd.tm_trx_order_id \r\n" + 
				"	AND ( trxt.trx_tracking_id = trxd.tm_trx_tracking_id_from ) \r\n" + 
				"	AND trxt.trx_tracking_company_id = trxd.trx_driverschedule_company_id \r\n" + 
				"	LEFT JOIN m_driver as d ON trxd.m_driver_id = d.driver_id\r\n" + 
				"	AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n" + 
				"WHERE\r\n" + 
				"	trxo.trx_order_company_id = :compid \r\n" + 
				"	AND trxt.trx_tracking_statusoutbound IS NOT NULL \r\n" + 
				"	and case when c.cabang_name is not null and a.agen_name is not null then\r\n" + 
				" c.cabang_name = :customer\r\n" + 
				"else\r\n" + 
				" ( C.cabang_name = :customer OR A.agen_name = :customer)  end     \r\n" 
				+ status + statusDate + kotaToStatus + kurirStatus +
				" GROUP BY\r\n" + 
				"	trxo.trx_order_id,\r\n" + 
				"	trxo.trx_order_date,\r\n" + 
				"	trxo.m_contact_customer_from,\r\n" + 
				"	trxo.trx_order_pickup_address,\r\n" + 
				"	trxo.m_contact_customer_to,\r\n" + 
				"	ser.service_name,\r\n" + 
				"	trxo.trx_order_koli,\r\n" + 
				"	trxt.trx_tracking_statusoutbound,\r\n" + 
				"	trxo.trx_order_company_id,\r\n" + 
				"	CAST ( trxo.trx_order_created_by AS VARCHAR ), 	trxt.trx_tracking_id ,\r\n" + 
				"	c.m_kota_id,\r\n" + 
				"	c.m_province_id,\r\n" + 
				"	e.m_kota_id,\r\n" + 
				"	e.m_province_id,\r\n" + 
				"	trxt.m_contact_customer_from_id,\r\n" + 
				"	trxt.m_contact_customer_to_id ) q\r\n" + 
				"-- 		from\r\n" + 
				"LEFT JOIN trx_tracking AS trxt_from ON q.trx_order_id = trxt_from.tm_trx_order_id \r\n" + 
				"	AND trxt_from.trx_tracking_company_id = q.trx_order_company_id\r\n" + 
				"	and trxt_from.trx_tracking_id = q.tracking_from\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_cabang AS C2 ON trxt_from.m_cabang_id = C2.cabang_id \r\n" + 
				"	AND trxt_from.trx_tracking_company_id = C2.cabang_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_agen AS A2 ON trxt_from.m_agen_id = A2.agen_id \r\n" + 
				"	AND trxt_from.m_agen_id = A2.agen_id\r\n" + 
				"	left join m_agendetail as e2 on trxt_from.m_agen_id = e2.m_agen_id and trxt_from.m_agendetail_id = e2.agendetail_id and trxt_from.trx_tracking_company_id = e2.agendetail_company_id").setParameter("compid", compId).setParameter("customer", customer.toString()).getResultList();
	}

	@Override
	public List<Object[]> getmynotif(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno) {
		// TODO Auto-generated method stub
		String status = "";
		String statusDate = " ";
		String kotaToStatus = " ";
		String kurirStatus = " ";
		if(kotato.isEmpty() == false)
		{
			kotaToStatus = " AND trxo.trx_kota_to LIKE '" + kotato.toUpperCase() + "%'";
		}
		if(kurirname.isEmpty() == false)
		{
			kurirStatus = " AND d.driver_name LIKE '" + kurirname + "%'";
		}
		
		if(orderdate.isEmpty() == false)
		{
			statusDate = " AND trxo.trx_order_date = '" + orderdate + "'";
			logger.info(statusDate + " <==status date");
		}
		else
		{
			statusDate = " ";
		}
		if(conno.isEmpty() == false)
		{
			status = " AND trxo.trx_order_connocement LIKE '" + conno + "%'";
		}
		else
		{
			status = " ";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.trx_order_pickup_address,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	ser.service_name,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "	trxt.trx_tracking_status,\r\n"
				+ "	trxo.trx_order_company_id,\r\n"
				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n"
				+ "	trxt.trx_tracking_id "
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n"
				+ "	AND trxo.trx_order_company_id = ser.service_company_id\r\n"
				+ "	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n"
				+ "	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n"
				+ "	LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n"
				+ "	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n"
				+ "	AND trxt.m_agen_id = A.agen_id\r\n"
				+ "	LEFT JOIN trx_driverschedule AS trxd ON trxt.tm_trx_order_id = trxd.tm_trx_order_id \r\n"
				+ "	AND ( trxt.trx_tracking_id = trxd.tm_trx_tracking_id_from OR trxt.trx_tracking_id = trxd.tm_trx_tracking_id_to ) \r\n"
				+ "	AND trxt.trx_tracking_company_id = trxd.trx_driverschedule_company_id \r\n"
				+ "	LEFT JOIN m_driver as d ON trxd.m_driver_id = d.driver_id\r\n"
				+ "	AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trxo.trx_order_company_id = :compid \r\n"
				+ "	AND trxt.trx_tracking_status IS NOT NULL \r\n"
				+ "	AND ( C.cabang_name = :customer OR A.agen_name = :customer) " 
				+ status + statusDate + kotaToStatus + kurirStatus
				+ " GROUP BY\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.trx_order_pickup_address,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	ser.service_name,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "	trxt.trx_tracking_status,\r\n"
				+ "	trxo.trx_order_company_id,\r\n"
				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), "
				+ "	trxt.trx_tracking_id ").setParameter("compid", compId).setParameter("customer", customer.toString()).getResultList();
	}

	@Override
	public List<Object[]> getdeliveryOrder(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	trxo.trx_order_nostt,\r\n"
				+ "	s.service_name,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "	trxt.trx_tracking_statusinbound,\r\n"
				+ "	C.cabang_name,\r\n"
				+ "	C.m_kota_id,\r\n"
				+ "	A.agen_name,\r\n"
				+ "	trxo.trx_order_company_id \r\n"
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n"
				+ "	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n"
				+ "	LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n"
				+ "	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = A.agen_company_id\r\n"
				+ "	LEFT JOIN m_service AS s ON trxo.m_service_id = s.service_id \r\n"
				+ "	AND trxo.trx_order_company_id = s.service_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trxo.trx_order_company_id = :compid\r\n"
				+ "	AND trxt.trx_tracking_statusinbound = 'ware house' "
				+ "ORDER BY\r\n"
				+ "	trxt.trx_tracking_id ASC ").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getComplete(String string2,UUID user_uuid) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select m_contact_customer_from, trx_order_id, trx_order_date, cast (trx_order_estimated_cost as decimal), m_payment_id, trx_order_status  from trx_order\r\n" + 
				"where\r\n" + 
				"trx_order_status = 'complete' and trx_invoice_header_id is null" + 
				" and trx_order_company_id = :company and trx_order_created_by = :user_uuid").setParameter("user_uuid", user_uuid).setParameter("company", string2)
				.getResultList();
	}
}