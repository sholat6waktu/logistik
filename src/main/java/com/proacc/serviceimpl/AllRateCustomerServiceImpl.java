package com.proacc.serviceimpl;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRateCustomer;
import com.proacc.entity.MRateHarga;
import com.proacc.service.AllRateCustomerService;
import com.proacc.service.MRateCustomerService;
import com.proacc.service.MRateHargaService;

@Service
public class AllRateCustomerServiceImpl implements AllRateCustomerService{
	private static final Logger logger = LoggerFactory.getLogger(AllRateCustomerServiceImpl.class);
	
	@Autowired
	MRateCustomerService mRateCustomerService;
	@Autowired
	MRateHargaService mRateHargaService;
	
	@Override
	@Transactional
	public void saveallRateCustomer(MRateCustomer mRateCustomer, ArrayList<MRateHarga> mRatehargaArray) {
		// TODO Auto-generated method stub
		mRateCustomerService.saveallrateharga(mRateCustomer);
		if(mRatehargaArray.size() > 0)
		{
			mRateHargaService.saveallrateharga(mRatehargaArray);
		}
	}
}
