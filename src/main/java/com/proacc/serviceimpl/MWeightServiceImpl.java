package com.proacc.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.service.MWeightService;

@Service
public class MWeightServiceImpl implements MWeightService{

	private static final Logger logger = LoggerFactory.getLogger(MWeightServiceImpl.class);
	@Autowired
	EntityManager em;
	@Override
	public List<Object[]> getallWeight(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and weight_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and weight_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	weight_id,\r\n"
				+ "	weight_name,\r\n"
				+ "	weight_tokg,\r\n"
				+ "	weight_description,\r\n"
				+ "	CAST ( weight_created_by AS VARCHAR ),\r\n"
				+ "	weight_created_at,\r\n"
				+ "	CAST ( weight_updated_by AS VARCHAR ),\r\n"
				+ "	weight_updated_at,\r\n"
				+ "	weight_status,\r\n"
				+ "	weight_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_weight \r\n"
				+ "WHERE\r\n"
				+ "	weight_company_id = :compid" + status).setParameter("compid", compId).getResultList();
	}
	@Override
	public List<Object[]> getWeight(String lowerCase, Integer WeightId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and weight_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and weight_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	weight_id,\r\n"
				+ "	weight_name,\r\n"
				+ "	weight_tokg,\r\n"
				+ "	weight_description,\r\n"
				+ "	CAST ( weight_created_by AS VARCHAR ),\r\n"
				+ "	weight_created_at,\r\n"
				+ "	CAST ( weight_updated_by AS VARCHAR ),\r\n"
				+ "	weight_updated_at,\r\n"
				+ "	weight_status,\r\n"
				+ "	weight_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_weight \r\n"
				+ "WHERE\r\n"
				+ "	weight_company_id = :compid"
				+ "	and weight_id = :weightid" + status).setParameter("compid", compId).setParameter("weightid", WeightId).getResultList();
	}
}
