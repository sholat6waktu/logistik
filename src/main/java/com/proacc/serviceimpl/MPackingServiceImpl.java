package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MPacking;
import com.proacc.repository.MPackingRepository;
import com.proacc.serializable.MPackingSerializable;
import com.proacc.service.MPackingService;

@Service
public class MPackingServiceImpl implements MPackingService{
	private static final Logger logger = LoggerFactory.getLogger(MPackingServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	MPackingRepository mPackingRepository;

	@Override
	public List<Object[]> nextvalMPacking() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('packing_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getallpacking(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and packing_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and packing_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	packing_id,\r\n"
				+ "	packing_name,\r\n"
				+ "	packing_description,\r\n"
				+ "	CAST ( packing_created_by AS VARCHAR ),\r\n"
				+ "	packing_created_at,\r\n"
				+ "	CAST ( packing_updated_by AS VARCHAR ),\r\n"
				+ "	packing_updated_at,\r\n"
				+ "	packing_status,\r\n"
				+ "	packing_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_packing \r\n"
				+ "WHERE\r\n"
				+ "	packing_company_id = :compid" + status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getpacking(String lowerCase, Integer packingId, String compId) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and packing_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and packing_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	packing_id,\r\n"
				+ "	packing_name,\r\n"
				+ "	packing_description,\r\n"
				+ "	CAST ( packing_created_by AS VARCHAR ),\r\n"
				+ "	packing_created_at,\r\n"
				+ "	CAST ( packing_updated_by AS VARCHAR ),\r\n"
				+ "	packing_updated_at,\r\n"
				+ "	packing_status,\r\n"
				+ "	packing_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_packing \r\n"
				+ "WHERE\r\n"
				+ "	packing_company_id = :compid\r\n"
				+ "	and packing_id = :packingid" + status).setParameter("compid", compId).setParameter("packingid", packingId).getResultList();
	}

	@Override
	public void savepacking(MPacking mPacking) {
		// TODO Auto-generated method stub
		mPackingRepository.save(mPacking);
		
	}

	@Override
	public Optional<MPacking> getdetail(Integer Id, String Company) {
		// TODO Auto-generated method stub
		return mPackingRepository.findById(new MPackingSerializable(Id, Company));
	}
}
