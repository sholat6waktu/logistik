package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MCabang;
import com.proacc.repository.MCabangRepository;
import com.proacc.serializable.MCabangSerializable;
import com.proacc.service.MCabangService;

@Service
public class MCabangServiceImpl implements MCabangService{

	private static final Logger logger = LoggerFactory.getLogger(MCabangServiceImpl.class);
	
	@Autowired
	EntityManager em;

	@Autowired
	MCabangRepository mCabangRepository;

	@Override
	public List<Object[]> nextvalMCabang() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('Cabang_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getallCabang(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and m_cabang.cabang_status = true \r\n";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and m_cabang.cabang_status = false \r\n";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	cabang_id,\r\n"
				+ "	m_kota_id,\r\n"
				+ "	CAST ( cabang_created_by AS VARCHAR ),\r\n"
				+ "	cabang_created_at,\r\n"
				+ "	CAST ( cabang_updated_by AS VARCHAR ),\r\n"
				+ "	cabang_updated_at,\r\n"
				+ "	cabang_status,\r\n"
				+ "	cabang_company_id, \r\n"
				+ "	cabang_name,"
				+ " m_province_id \r\n"
				+ "FROM\r\n"
				+ "	m_cabang\r\n"
				+ "WHERE\r\n"
				+ "	cabang_company_id = :compid" + status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getCabang(String lowerCase, Integer cabangId, String compId) {
		String status = "";
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	cabang_id,\r\n"
				+ "	m_kota_id,\r\n"
				+ "	CAST ( cabang_created_by AS VARCHAR ),\r\n"
				+ "	cabang_created_at,\r\n"
				+ "	CAST ( cabang_updated_by AS VARCHAR ),\r\n"
				+ "	cabang_updated_at,\r\n"
				+ "	cabang_status,\r\n"
				+ "	cabang_company_id, \r\n"
				+ "	cabang_name,"
				+ " kode, " + 
				"				type_kode, " +
				"				 m_cabang_id_kode," + 
				"				m_agen_id_kode, " + 
				"				 agendetail_id_kode "
				+ "FROM\r\n"
				+ "	m_cabang\r\n"
				+ "WHERE\r\n"
				+ "	cabang_company_id = :compid\r\n"
				+ "	and cabang_id = :cabangid" + status).setParameter("compid", compId).setParameter("cabangid", cabangId).getResultList();
	}

	@Override
	public void saveCabang(MCabang mCabang) {
		// TODO Auto-generated method stub
		mCabangRepository.save(mCabang);
	}

	@Override
	public Optional<MCabang> getDetail(Integer Id, String Company) {
		// TODO Auto-generated method stub
		return mCabangRepository.findById(new MCabangSerializable(Id, Company));
	}

	@Override
	public List<Object[]> getdetailbyprovince(String lowerCase, Integer provinsiId, String compId) {
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	m_province_id,\r\n"
				+ "	m_kota_id \r\n"
				+ "FROM\r\n"
				+ "	m_cabang\r\n"
				+ "WHERE\r\n"
				+ "	cabang_company_id = :compid\r\n"
				+ "	AND m_province_id = :provinsiid\r\n"
				+ "GROUP BY\r\n"
				+ "	m_province_id,\r\n"
				+ "	m_kota_id").setParameter("compid", compId).setParameter("provinsiid", provinsiId).getResultList();
	}

	@Override
	public List<Object[]> getcabangbyprovince(String lowerCase, Integer provinsiId, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	m_province_id,\r\n"
				+ "	m_kota_id, \r\n"
				+ "	cabang_id,\r\n"
				+ "	cabang_name \r\n"
				+ "FROM\r\n"
				+ "	m_cabang\r\n"
				+ "WHERE\r\n"
				+ "	cabang_company_id = :compid\r\n"
				+ "	AND m_province_id = :provinsiid\r\n"
				+ "GROUP BY\r\n"
				+ "	m_province_id,\r\n"
				+ "	m_kota_id, \r\n"
				+ "	cabang_id,\r\n"
				+ "	cabang_name").setParameter("compid", compId).setParameter("provinsiid", provinsiId).getResultList();
	}

	@Override
	public List<Object[]> getCoverArea(int parseInt, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT m_cabang_id, cabangdetail_id, m_province_id, m_kotadetail_id, cabangdetail_status, cabangdetail_company_id FROM \"m_cabangdetail\"\r\n" + 
				"where m_cabang_id = :cabangId \r\n" + 
				"and cabangdetail_company_id = :company ")
				.setParameter("cabangId", parseInt)
				.setParameter("company", string)
				.getResultList();
	}
	
	
}
