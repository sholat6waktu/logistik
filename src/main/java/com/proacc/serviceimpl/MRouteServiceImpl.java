package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRoute;
import com.proacc.repository.MRouteRepository;
import com.proacc.serializable.MRouteSerializable;
import com.proacc.service.MRouteService;

@Service
public class MRouteServiceImpl implements MRouteService {
	
	private static final Logger logger = LoggerFactory.getLogger(MRouteServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MRouteRepository MRouteRepository;

	@Override
	public List<Object[]> listRouteByStatusAndCreatedBy(String lowerCase, String string, String companyId) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and route_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and route_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("select route_id, \r\n" + 
				"route_name, \r\n" + 
				"route_from_date, \r\n" + 
				"route_to_date, \r\n" + 
				"route_status, \r\n" + 
				"route_company_id, \r\n" + 
				"cast ( route_created_by as varchar )\r\n" + 
				" from m_route\r\n" + 
				"where\r\n" + 
				"1=1 \r\n" +
				status+
				"and m_route.route_created_by = '"+string+"' and route_company_id = :companyId").setParameter("companyId", companyId).getResultList();
		
	}

	@Override
	public Optional<MRoute> detailRoute(int int1, String string) {
		// TODO Auto-generated method stub
		return MRouteRepository.findById(new MRouteSerializable(int1, string));
	}

	@Override
	public List<Object[]> detailJalur(int int1, String string, String lowerCase) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and route_jalur_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and route_jalur_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n" + 
				"mrj.m_route_id,\r\n" + 
				"mrj.route_jalur_id,\r\n" + 
				"	case when \r\n" + 
				"	ag.agen_name is not null then ag.agen_name else mc.cabang_name end as name,\r\n" + 
				"	case when \r\n" + 
				"	mc.m_kota_id is not null then mc.m_kota_id else agd.m_kota_id end as kota_id,\r\n" + 
				"	mrj.route_jalur_status,\r\n" + 
				"	mrj.route_jalur_company_id,"
				+ " mrj.m_cabang_id,\r\n" + 
				"	mrj.m_agen_id,\r\n" + 
				"	mrj.m_agendetail_id\r\n" + 
				"FROM\r\n" + 
				"	\"m_route_jalur\" mrj\r\n" + 
				"	LEFT JOIN m_cabang mc ON mrj.m_cabang_id = mc.cabang_id \r\n" + 
				"	AND mrj.route_jalur_company_id = mc.cabang_company_id\r\n" + 
				"	LEFT JOIN m_agendetail agd ON mrj.m_agen_id = agd.m_agen_id \r\n" + 
				"	AND mrj.m_agendetail_id = agd.agendetail_id \r\n" + 
				"	AND mrj.route_jalur_company_id = agd.agendetail_company_id\r\n" + 
				"	left join m_agen ag on ag.agen_id = mrj.m_agen_id and ag.agen_company_id = mrj.route_jalur_company_id\r\n" + 
				"	where\r\n" + 
				"	m_route_id = :routeId\r\n" + 
				"	and route_jalur_company_id = :compId \r\n" + 
				status+
				"	order by mrj.route_jalur_id asc")
				.setParameter("routeId", int1)
				.setParameter("compId", string)
				.getResultList();
	}

	@Override
	public List<Object[]> detailJalurDetail(int int1, String string, String lowerCase, Integer routeJalurId) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and route_jalur_detail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and route_jalur_detail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n" + 
				"mrj.m_route_id,\r\n" + 
				"mrj.m_route_jalur_id,\r\n" + 
				"mrj.route_jalur_detail_id,\r\n" + 
				"	case when \r\n" + 
				"	ag.agen_name is not null then ag.agen_name else mc.cabang_name end as name,\r\n" + 
				"	case when \r\n" + 
				"	mc.m_kota_id is not null then mc.m_kota_id else agd.m_kota_id end as kota_id,\r\n" + 
				"	mrj.route_jalur_detail_status,\r\n" + 
				"	mrj.route_jalur_detail_company_id,"
				+ " mrj.m_cabang_id,\r\n" + 
				"	mrj.m_cabangdetail_id,\r\n" + 
				"	mrj.m_agen_id,\r\n" + 
				"	mrj.m_agendetail_id \r\n" + 
				"FROM\r\n" + 
				"	\"m_route_jalur_detail\" mrj\r\n" + 
				"	LEFT JOIN m_cabang mc ON mrj.m_cabang_id = mc.cabang_id \r\n" + 
				"	AND mrj.route_jalur_detail_company_id = mc.cabang_company_id"
				+ " left join m_cabangdetail mcd on mrj.m_cabang_id = mcd.m_cabang_id \r\n" + 
				"	and mcd.cabangdetail_id = mrj.m_cabangdetail_id\r\n" + 
				"	AND mrj.route_jalur_detail_company_id = mcd.cabangdetail_company_id \r\n" + 
				"	LEFT JOIN m_agendetail agd ON mrj.m_agen_id = agd.m_agen_id \r\n" + 
				"	AND mrj.m_agendetail_id = agd.agendetail_id \r\n" + 
				"	AND mrj.route_jalur_detail_company_id = agd.agendetail_company_id\r\n" + 
				"	left join m_agen ag on ag.agen_id = agd.m_agen_id and ag.agen_company_id = agd.agendetail_company_id\r\n" + 
				"	where\r\n" + 
				"	m_route_id = :routeId\r\n" + 
				"	and route_jalur_detail_company_id = :compId "
				+ " and m_route_jalur_id = :routeJalurId \r\n" + 
				status + 
				"	order by mrj.route_jalur_detail_id asc")
				.setParameter("routeId", int1)
				.setParameter("compId", string)
				.setParameter("routeJalurId", routeJalurId)
				.getResultList();
	}

	@Override
	public List<Object[]> nextvalRoute() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('route_sequence')").getResultList();
	}

	@Override
	public void save(MRoute route) {
		// TODO Auto-generated method stub
		MRouteRepository.save(route);
	}

	@Override
	public Optional<MRoute> getDetail(int int1, String string) {
		// TODO Auto-generated method stub
		return MRouteRepository.findById(new MRouteSerializable(int1, string));
	}
}
