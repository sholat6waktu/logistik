package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRateCustomer;
import com.proacc.repository.MRateCustomerRepository;
import com.proacc.serializable.MRateCustomerSerializable;
import com.proacc.service.MRateCustomerService;

@Service
public class MRateCustomerServiceImpl implements MRateCustomerService{
	
	private static final Logger logger = LoggerFactory.getLogger(MRateCustomerServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MRateCustomerRepository mRateCustomerRepository;

	@Override
	public List<Object[]> nextvalMRateCustomer() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('ratecustomer_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getallRateCustomer(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	ratecustomer_id,\r\n"
				+ "	m_customer_id,\r\n"
				+ "	ratecustomer_startdate,\r\n"
				+ "	ratecustomer_enddate,\r\n"
				+ "	CAST(ratecustomer_created_by as VARCHAR),\r\n"
				+ "	ratecustomer_created_at,\r\n"
				+ "	CAST(ratecustomer_updated_by as VARCHAR),\r\n"
				+ "	ratecustomer_updated_at,\r\n"
				+ "	ratecustomer_status,\r\n"
				+ "	ratecustomer_company_id\r\n"
				+ "FROM\r\n"
				+ "	m_ratecustomer\r\n"
				+ "	WHERE\r\n"
				+ "	ratecustomer_company_id = :compid").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getRateCustomer(String lowerCase, Integer RateCustomerId, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	ratecustomer_id,\r\n"
				+ "	m_customer_id,\r\n"
				+ "	ratecustomer_startdate,\r\n"
				+ "	ratecustomer_enddate,\r\n"
				+ "	CAST(ratecustomer_created_by as VARCHAR),\r\n"
				+ "	ratecustomer_created_at,\r\n"
				+ "	CAST(ratecustomer_updated_by as VARCHAR),\r\n"
				+ "	ratecustomer_updated_at,\r\n"
				+ "	ratecustomer_status,\r\n"
				+ "	ratecustomer_company_id\r\n"
				+ "FROM\r\n"
				+ "	m_ratecustomer\r\n"
				+ "	WHERE\r\n"
				+ "	ratecustomer_company_id = :compid\r\n"
				+ "	and ratecustomer_id = :ratecustomerid").setParameter("compid", compId).setParameter("ratecustomerid", RateCustomerId).getResultList();
	}

	@Override
	public void saveallrateharga(MRateCustomer mRateCustomer) {
		// TODO Auto-generated method stub
		mRateCustomerRepository.save(mRateCustomer);
	}

	@Override
	public Optional<MRateCustomer> getDetail(Integer RateCustomerId, String compId) {
		// TODO Auto-generated method stub
		return mRateCustomerRepository.findById(new MRateCustomerSerializable(RateCustomerId, compId));
	}
}
