package com.proacc.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxDeliveryorder;
import com.proacc.repository.TrxDeliveryorderRepository;
import com.proacc.service.TrxDeliveryorderService;

@Service
public class TrxDeliveryorderServiceImpl implements TrxDeliveryorderService{
	private static final Logger logger = LoggerFactory.getLogger(TrxDeliveryorderServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxDeliveryorderRepository TrxDeliveryorderRepository;

	@Override
	public List<Object[]> getallDeliveryorder(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxde.tm_trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.m_contact_attedance_from,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	trxo.m_contact_attedance_to,"
				+ "	trxo.trx_order_connocement,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "	trxo.trx_order_weight,\r\n"
				+ "	trxo.m_weight_id,\r\n"
				+ "	trxo.trx_order_weight || w.weight_name as berat, \r\n"
				+ "	trxde.trx_deliveryorder_bukti,\r\n"
				+ "	trxde.trx_deliveryorder_company_id \r\n"
				+ "FROM\r\n"
				+ "	trx_deliveryorder AS trxde\r\n"
				+ "	LEFT JOIN trx_order AS trxo ON trxde.tm_trx_order_id = trxo.trx_order_id\r\n"
				+ "	AND trxde.trx_deliveryorder_company_id = trxo.trx_order_company_id\r\n"
				+ "	LEFT JOIN m_weight as w ON trxo.m_weight_id = w.weight_id\r\n"
				+ "	AND trxo.trx_order_company_id = w.weight_company_id\r\n"
				+ "	WHERE\r\n"
				+ "	trxde.trx_deliveryorder_company_id = :compid ").setParameter("compid", compId).getResultList();
	}

	@Override
	public void savedeliveryorder(TrxDeliveryorder Trxdeliveryorder) {
		// TODO Auto-generated method stub
		TrxDeliveryorderRepository.save(Trxdeliveryorder);
	}

	@Override
	public List<Object[]> searchDoneOrder(String string, String string2) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select trx_order.m_contact_customer_from as from, trx_order.trx_order_date, trx_order.m_contact_attedance_from from trx_tracking\r\n" + 
				"left join trx_order on trx_order_id = trx_tracking.tm_trx_order_id and trx_order_company_id = trx_tracking.trx_tracking_company_id\r\n" + 
				"where\r\n" + 
				"trx_tracking_id = (select (trx_tracking_id ) num from trx_tracking\r\n" + 
				"where\r\n" + 
				"tm_trx_order_id = :orderId \r\n" + 
				"and trx_tracking_company_id = :companyId \r\n" + 
				"order by trx_tracking_id desc\r\n" + 
				"limit 1)\r\n" + 
				"and tm_trx_order_id =  :orderId \r\n" + 
				"and trx_tracking_statusinbound='waiting driver'\r\n" + 
				"and trx_tracking_company_id = :companyId ")
				.setParameter("orderId", string)
				.setParameter("companyId", string2)
				.getResultList();
	}
}
