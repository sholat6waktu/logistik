package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxPartdetail;
import com.proacc.repository.TrxPartdetailRepository;
import com.proacc.service.TrxPartdetailService;

@Service
public class TrxPartdetailServiceImpl implements TrxPartdetailService{

	private static final Logger logger = LoggerFactory.getLogger(TrxPartdetailServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxPartdetailRepository trxPartdetailRepository;

	@Override
	public void saveallPart(ArrayList<TrxPartdetail> trxPartdetailArray) {
		// TODO Auto-generated method stub
		trxPartdetailRepository.saveAll(trxPartdetailArray);
	}

	@Override
	public List<Object[]> getPart(String lowerCase, String orderId, Integer KoliId, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	part.tm_trx_order_id,\r\n"
				+ "	part.tm_trx_kolidetail_id,\r\n"
				+ "	part.trx_partdetail_id,\r\n"
				+ "	part.trx_partdetail_partnumber,\r\n"
				+ "	part.trx_partdetail_quantity,\r\n"
				+ "	w.weight_name,\r\n"
				+ "	part.trx_partdetail_returquantity,\r\n"
				+ "	part.trx_partdetail_returdescription,\r\n"
				+ "	part.trx_partdetail_status,\r\n"
				+ "	part.trx_partdetail_company_id, \r\n"
				+ "	part.trx_partdetail_name \r\n"
				+ "FROM\r\n"
				+ "	trx_partdetail AS part\r\n"
				+ "	LEFT JOIN m_weight AS w ON part.m_unit_id = w.weight_id \r\n"
				+ "	AND part.trx_partdetail_company_id = w.weight_company_id "
				+ "WHERE\r\n"
				+ "	trx_partdetail_company_id = :compid\r\n"
				+ "	AND tm_trx_kolidetail_id = :koliid\r\n"
				+ "	AND tm_trx_order_id = :orderid").setParameter("compid", compId).setParameter("koliid", KoliId).setParameter("orderid", orderId).getResultList();
	}	
}