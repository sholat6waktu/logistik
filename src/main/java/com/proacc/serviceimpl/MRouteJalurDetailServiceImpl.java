package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRoute;
import com.proacc.entity.MRouteJalurDetail;
import com.proacc.entity.MagenCoverArea;
import com.proacc.repository.MRouteJalurDetailRepository;
import com.proacc.serializable.MRouteJalurDetailSerializable;
import com.proacc.serializable.MRouteSerializable;
import com.proacc.service.MRouteJalurDetailService;

@Service
public class MRouteJalurDetailServiceImpl implements MRouteJalurDetailService {
	
	private static final Logger logger = LoggerFactory.getLogger(MRouteJalurDetailServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MRouteJalurDetailRepository MRouteJalurDetailRepository;

	@Override
	public void saveAll(ArrayList<MRouteJalurDetail> routeJalurDetail) {
		// TODO Auto-generated method stub
		MRouteJalurDetailRepository.saveAll(routeJalurDetail);
	}

	@Override
	public List<Object[]> getAll(int int1, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select m_route_id, \r\n" + 
				"m_route_jalur_id, \r\n" + 
				"route_jalur_detail_id from m_route_jalur_detail where m_route_id = :routeId and route_jalur_detail_company_id = :company\r\n" + 
				"")
				.setParameter("routeId", int1)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public Optional<MRouteJalurDetail> getDetail(int parseInt, int parseInt2, int parseInt3, String string) {
		// TODO Auto-generated method stub
		return MRouteJalurDetailRepository.findById(new MRouteJalurDetailSerializable(parseInt, parseInt2, parseInt3, string));
	}

}
