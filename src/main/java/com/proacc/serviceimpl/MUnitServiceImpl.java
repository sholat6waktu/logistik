package com.proacc.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.service.MUnitService;

@Service
public class MUnitServiceImpl implements MUnitService{
	
	private static final Logger logger = LoggerFactory.getLogger(MUnitServiceImpl.class);
	@Autowired
	EntityManager em;
	@Override
	public List<Object[]> getallUnit(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and unit_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and unit_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	unit_id,\r\n"
				+ "	unit_name,\r\n"
				+ "	unit_description,\r\n"
				+ "	CAST ( unit_created_by AS VARCHAR ),\r\n"
				+ "	unit_created_at,\r\n"
				+ "	CAST ( unit_updated_by AS VARCHAR ),\r\n"
				+ "	unit_updated_at,\r\n"
				+ "	unit_status,\r\n"
				+ "	unit_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_unit \r\n"
				+ "WHERE\r\n"
				+ "	unit_company_id = :compid" + status).setParameter("compid", compId).getResultList();
	}
	@Override
	public List<Object[]> getUnit(String lowerCase, Integer unitId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and unit_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and unit_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	unit_id,\r\n"
				+ "	unit_name,\r\n"
				+ "	unit_description,\r\n"
				+ "	CAST ( unit_created_by AS VARCHAR ),\r\n"
				+ "	unit_created_at,\r\n"
				+ "	CAST ( unit_updated_by AS VARCHAR ),\r\n"
				+ "	unit_updated_at,\r\n"
				+ "	unit_status,\r\n"
				+ "	unit_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_unit \r\n"
				+ "WHERE\r\n"
				+ "	unit_company_id = :compid"
				+ " and unit_id = :unitid" + status).setParameter("compid", compId).setParameter("unitid", unitId).getResultList();
	}
}
