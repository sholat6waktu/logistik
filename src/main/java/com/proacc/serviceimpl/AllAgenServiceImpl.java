package com.proacc.serviceimpl;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MAgen;
import com.proacc.entity.MAgendetail;
import com.proacc.entity.MagenCoverArea;
import com.proacc.repository.MagenDetailCoverAreaRepository;
import com.proacc.service.AllAgenService;
import com.proacc.service.MAgenService;
import com.proacc.service.MAgendetailService;

@Service
public class AllAgenServiceImpl implements AllAgenService{
	private static final Logger logger = LoggerFactory.getLogger(AllAgenServiceImpl.class);
	
	@Autowired
	MAgenService mAgenService;
	@Autowired
	MAgendetailService mAgendetailService;
	
	@Autowired
	MagenDetailCoverAreaRepository MagenDetailCoverAreaRepository;
	@Override
	@Transactional
	public void saveallAgen(MAgen mAgen, ArrayList<MAgendetail> MAgendetailArray, ArrayList<MagenCoverArea> cover) {
		// TODO Auto-generated method stub
		mAgenService.saveAgen(mAgen);
		if(MAgendetailArray.size() > 0)
		{
			mAgendetailService.saveallAgendetail(MAgendetailArray);
		}
		MagenDetailCoverAreaRepository.saveAll(cover);
	}
	
	
}
