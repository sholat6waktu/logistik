package com.proacc.serviceimpl;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRateCustomer;
import com.proacc.entity.MRateHarga;
import com.proacc.entity.MRoute;
import com.proacc.entity.MRouteJalur;
import com.proacc.entity.MRouteJalurDetail;
import com.proacc.service.AllRateCustomerService;
import com.proacc.service.MRateCustomerService;
import com.proacc.service.MRateHargaService;
import com.proacc.service.MRouteJalurDetailService;
import com.proacc.service.AllRouteService;
import com.proacc.service.MRouteJalurService;
import com.proacc.service.MRouteService;

@Service
public class AllRouteServiceImpl implements AllRouteService{
	private static final Logger logger = LoggerFactory.getLogger(AllRouteServiceImpl.class);
	
	@Autowired
	MRouteService MRouteService;
	
	@Autowired
	MRouteJalurService MRouteJalurService;
	
	@Autowired
	MRouteJalurDetailService MRouteJalurDetailService;

	@Override
	@Transactional
	public void saveallRoute(MRoute route, ArrayList<MRouteJalur> routeJalur,
			ArrayList<MRouteJalurDetail> routeJalurDetail) {
		// TODO Auto-generated method stub
		MRouteService.save(route);
		MRouteJalurService.saveAll(routeJalur);
		MRouteJalurDetailService.saveAll(routeJalurDetail);
	}
	
	

//	@Override
//	public void saveallRoute(MRoute route, ArrayList<MRouteJalur> routeJalur,
//			ArrayList<MRouteJalurDetail> routeJalurDetail) {
//		// TODO Auto-generated method stub
//		
//	}
	
	
//	@Override
//	@Transactional
//	public void saveallRateCustomer(MRateCustomer mRateCustomer, ArrayList<MRateHarga> mRatehargaArray) {
//		// TODO Auto-generated method stub
//		mRateCustomerService.saveallrateharga(mRateCustomer);
//		if(mRatehargaArray.size() > 0)
//		{
//			mRateHargaService.saveallrateharga(mRatehargaArray);
//		}
//	}
}
