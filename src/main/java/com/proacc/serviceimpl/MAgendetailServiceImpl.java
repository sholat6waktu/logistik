package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.proacc.entity.MAgendetail;
import com.proacc.repository.MAgendetailRepository;
import com.proacc.serializable.MAgendetailSerializable;
import com.proacc.service.MAgendetailService;

@Service
public class MAgendetailServiceImpl implements MAgendetailService{

	private static final Logger logger = LoggerFactory.getLogger(MAgendetailServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	MAgendetailRepository mAgendetailRepository;

	@Override
	public List<Object[]> getallAgendetail(String lowerCase, Integer agenId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and ad.agendetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and ad.agendetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	a.agen_id,\r\n"
				+ "	a.agen_name,"
				+ "	ad.m_province_id,\r\n"
				+ "	ad.m_transferpoint_id,\r\n"
				+ "	string_agg(CAST ( ad.m_kota_id AS VARCHAR ) || '-' || ad.agendetail_task,',') listkota,\r\n"
				+ "	CAST ( ad.agendetail_created_by AS VARCHAR ),\r\n"
				+ "	ad.agendetail_created_at,\r\n"
				+ "	CAST ( ad.agendetail_updated_by AS VARCHAR ),\r\n"
				+ "	ad.agendetail_updated_at,\r\n"
				+ "	ad.agendetail_status,\r\n"
				+ "	ad.agendetail_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_agen\r\n"
				+ "	AS A LEFT JOIN m_agendetail AS ad ON A.agen_id = ad.m_agen_id \r\n"
				+ "	AND A.agen_company_id = ad.agendetail_company_id \r\n"
				+ "WHERE\r\n"
				+ "	ad.agendetail_company_id = :compid\r\n"
				+ "	AND ad.m_agen_id = :agenid\r\n"
				+ status
				+ "		GROUP BY\r\n"
				+ "	a.agen_id,\r\n"
				+ "	a.agen_name,"
				+ "	ad.m_province_id,\r\n"
				+ "	ad.m_transferpoint_id,\r\n"
				+ "	CAST ( ad.agendetail_created_by AS VARCHAR ),\r\n"
				+ "	ad.agendetail_created_at,\r\n"
				+ "	CAST ( ad.agendetail_updated_by AS VARCHAR ),\r\n"
				+ "	ad.agendetail_updated_at,\r\n"
				+ "	ad.agendetail_status,\r\n"
				+ "	ad.agendetail_company_id "
				+ "	ORDER BY a.agen_id, ad.m_province_id, ad.m_transferpoint_id ASC ").setParameter("compid", compId).setParameter("agenid", agenId).getResultList();
	}

	@Override
	public List<Object[]> getAgendetail(String lowerCase, Integer agendetailId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and ad.agendetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and ad.agendetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	a.agen_id,\r\n"
				+ "	a.agen_name,"
				+ "	ad.m_province_id,\r\n"
				+ "	ad.m_transferpoint_id,\r\n"
				+ "	string_agg(CAST ( ad.m_kota_id AS VARCHAR ) || '-' || ad.agendetail_task,',') listkota,\r\n"
				+ "	CAST ( ad.agendetail_created_by AS VARCHAR ),\r\n"
				+ "	ad.agendetail_created_at,\r\n"
				+ "	CAST ( ad.agendetail_updated_by AS VARCHAR ),\r\n"
				+ "	ad.agendetail_updated_at,\r\n"
				+ "	ad.agendetail_status,\r\n"
				+ "	ad.agendetail_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_agen\r\n"
				+ "	AS A LEFT JOIN m_agendetail AS ad ON A.agen_id = ad.m_agen_id \r\n"
				+ "	AND A.agen_company_id = ad.agendetail_company_id \r\n"
				+ "WHERE\r\n"
				+ "	ad.agendetail_company_id = :compid\r\n"
				+ "	AND ad.agendetail_id = :agendetailid\r\n"
				+ status
				+ "		GROUP BY\r\n"
				+ "	a.agen_id,\r\n"
				+ "	a.agen_name,"
				+ "	ad.m_province_id,\r\n"
				+ "	ad.m_transferpoint_id,\r\n"
				+ "	CAST ( ad.agendetail_created_by AS VARCHAR ),\r\n"
				+ "	ad.agendetail_created_at,\r\n"
				+ "	CAST ( ad.agendetail_updated_by AS VARCHAR ),\r\n"
				+ "	ad.agendetail_updated_at,\r\n"
				+ "	ad.agendetail_status,\r\n"
				+ "	ad.agendetail_company_id "
				+ "	ORDER BY a.agen_id, ad.m_province_id, ad.m_transferpoint_id ASC").setParameter("compid", compId).setParameter("agendetailid", agendetailId).getResultList();
	}

	@Override
	public void saveallAgendetail(ArrayList<MAgendetail> MAgendetailArray) {
		// TODO Auto-generated method stub
		mAgendetailRepository.saveAll(MAgendetailArray);
	}

	@Override
	public Optional<MAgendetail> getdetail(Integer agenId, Integer agendetailId, String agendetailCompanyId) {
		// TODO Auto-generated method stub
		return mAgendetailRepository.findById(new MAgendetailSerializable(agenId, agendetailId, agendetailCompanyId));
	}

	@Override
	public List<Object[]> getalldetail(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and ad.agendetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and ad.agendetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT A\r\n" + 
				"	.agen_id,\r\n" + 
				"	A.agen_name,\r\n" + 
				"	ad.m_province_id,\r\n" + 
				"	ad.m_kota_id,\r\n" + 
				"	 (select string_agg(cast ( q.cong as varchar), ',') from (\r\n" + 
				" select z.m_kota_id cong from m_agen_cover_area z where z.m_agen_id = a.agen_id and z.agen_cover_area_status = true  and z.m_agendetail_id = ad.agendetail_id and z.agen_cover_area_company_id = ad.agendetail_company_id ) as q) listkota,\r\n" + 
				"	ad.agendetail_status,\r\n" + 
				"	A.agen_status,\r\n" + 
				"	ad.agendetail_company_id \r\n" + 
				"FROM\r\n" + 
				"	m_agen\r\n" + 
				"	AS A LEFT JOIN m_agendetail AS ad ON A.agen_id = ad.m_agen_id \r\n" + 
				"	AND A.agen_company_id = ad.agendetail_company_id \r\n" + 
				"WHERE\r\n" + 
				"	ad.agendetail_company_id =:compid \r\n" + status +
				" GROUP BY\r\n" + 
				"	A.agen_id,\r\n" + 
				"	A.agen_name,\r\n" + 
				"	ad.m_province_id,\r\n" + 
				"	ad.m_kota_id,\r\n" + 
				"	ad.agendetail_id,\r\n" + 
				"	ad.agendetail_status,\r\n" + 
				"	A.agen_status,\r\n" + 
				"	ad.agendetail_company_id \r\n" + 
				"ORDER BY\r\n" + 
				"	A.agen_id,\r\n" + 
				"	ad.m_province_id,\r\n" + 
				"	ad.m_kota_id ASC").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getdetailkotaforupdate(String lowerCase, Integer agenId, Integer provinsiId, Integer transferpointId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and agendetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and agendetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	m_province_id,\r\n"
				+ "	m_transferpoint_id,\r\n"
				+ "	m_kota_id,\r\n"
				+ "	agendetail_task,\r\n"
				+ "	agendetail_id, "
				+ " agendetail_status, "
				+ "agendetail_company_id,"
				+ " kode,"
				+ " type_kode,"
				+ " m_cabang_id_kode, "
				+ " m_agen_id_kode,"
				+ " agendetail_id_kode"
				+ " FROM\r\n"
				+ "m_agendetail\r\n"
				+ "WHERE\r\n"
				+ "	agendetail_company_id = :compid\r\n"
				+ "	AND m_agen_id = :agenid\r\n"
				+ "	AND m_province_id = :provinsiid\r\n"
				+ "	AND m_transferpoint_id = :transferpointid\r\n"
				+ status ).setParameter("compid", compId).setParameter("agenid", agenId).setParameter("provinsiid", provinsiId).setParameter("transferpointid", transferpointId).getResultList();
	}

	@Override
	public List<Object[]> getdetailprovinsiforupdate(String lowerCase, Integer agenId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and agendetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and agendetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	m_province_id,"
				+ "22222 "
				+ "FROM\r\n"
				+ "m_agendetail\r\n"
				+ "WHERE\r\n"
				+ "	agendetail_company_id = :compid\r\n"
				+ "	AND m_agen_id = :agenid\r\n"
				+ status
				+ " GROUP BY\r\n"
				+ "	m_province_id" ).setParameter("compid", compId).setParameter("agenid", agenId).getResultList();
	}

	@Override
	public List<Object[]> getdetailtransferpointforupdate(String lowerCase, Integer agenId, Integer provinsiId,
			String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and agendetail_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and agendetail_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	m_province_id,\r\n"
				+ "	m_transferpoint_id\r\n"
				+ "FROM\r\n"
				+ "m_agendetail\r\n"
				+ "WHERE\r\n"
				+ "	agendetail_company_id = :compid\r\n"
				+ "	AND m_agen_id = :agenid\r\n"
				+ "	AND m_province_id = :provinsiid\r\n"
				+ status
				+ " GROUP BY\r\n"
				+ "	m_province_id,\r\n"
				+ "	m_transferpoint_id").setParameter("compid", compId).setParameter("agenid", agenId).setParameter("provinsiid", provinsiId).getResultList();
	}

	@Override
	public List<Object[]> getdetailall(Integer agenId, String compId) {
		return this.em.createNativeQuery("SELECT\r\n"
				+ "m_agen_id,\r\n"
				+ "agendetail_id,\r\n"
				+ "agendetail_status\r\n"
				+ "FROM\r\n"
				+ "m_agendetail\r\n"
				+ "WHERE\r\n"
				+ "m_agen_id = :agenid\r\n"
				+ "AND agendetail_company_id = :compid").setParameter("compid", compId).setParameter("agenid", agenId).getResultList();
	}

	@Override
	public List<Object[]> getAgendetailbyprovince(String lowerCase, Integer provinceId, String compId) {
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	ad.m_agen_id,\r\n"
				+ "	ad.m_province_id,\r\n"
				+ "	ad.m_kota_id,\r\n"
				+ "	A.agen_name,"
				+ " ad.agendetail_id \r\n"
				+ "FROM\r\n"
				+ "	m_agendetail AS ad\r\n"
				+ "	LEFT JOIN m_agen AS A ON ad.m_agen_id = A.agen_id \r\n"
				+ "	AND ad.m_agen_id = A.agen_id \r\n"
				+ "WHERE\r\n"
				+ "	agendetail_company_id = :compid \r\n"
				+ "	AND m_province_id = :provinceid \r\n"
				+ "GROUP BY\r\n"
				+ "	m_agen_id,\r\n"
				+ "	m_province_id,\r\n"
				+ "	m_kota_id,\r\n"
				+ "	A.agen_name,ad.agendetail_id").setParameter("compid", compId).setParameter("provinceid", provinceId).getResultList();
	}

	@Override
	public List<Object[]> getAgenByName(String header) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select agen_id, agen_name from m_agen where agen_name = :agen and agen_company_id='1' and agen_status=true").setParameter("agen", header).getResultList();
	}

	@Override
	public List<Object[]> getAgenDetailByIdAndMKotaId(int parseInt, Integer fromkota) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select agendetail_id, m_kota_id from m_agendetail where m_agen_id =:agenId and agendetail_company_id='1' and m_kota_id=:kota and agendetail_status=true")
				.setParameter("agenId", parseInt)
				.setParameter("kota", fromkota).getResultList();
	}

	@Override
	public List<Object[]> getCustomerCityByAgenTransferPoint(String string, Integer fromkota) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	agendetail_id,\r\n" + 
				"	m_kota_id \r\n" + 
				"FROM\r\n" + 
				"	m_agendetail \r\n" + 
				"WHERE\r\n" + 
				"	m_agen_id = "+Integer.parseInt(string)+" \r\n" + 
				"	AND agendetail_company_id = '1' \r\n" + 
				"	AND agendetail_status = TRUE\r\n" + 
				"	and m_transferpoint_id = "+fromkota+" \r\n" + 
				"\r\n" + 
				"	")
				.getResultList();
	}

	@Override
	public List<Object[]> getAgenByCityId(Integer fromkota, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT \r\n" + 
				"					m_agen_id, \r\n" + 
				"					m_kota_id, "
				+ " agen_type \r\n" + 
				"				FROM \r\n" + 
				"					m_agendetail "
				+ " left join m_agen on agen_id = m_agen_id and agen_company_id = agendetail_company_id " + 
				"				WHERE \r\n" + 
				"					(m_kota_id = :fromkota or m_transferpoint_id = :fromkota )\r\n" + 
				"					AND agendetail_company_id = :company  \r\n" + 
				"					AND agendetail_status = TRUE \r\n" + 
				"					")
				.setParameter("fromkota", fromkota)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public List<Object[]> listAgen(String lowerCase, String string) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and magen.agen_status = true \r\n" + 
					" and ma.agendetail_status = true ";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and magen.agen_status = false\r\n" + 
					" and ma.agendetail_status = false ";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("select ma.m_agen_id, ma.agendetail_id, ma.agendetail_company_id, ma.m_kota_id, magen.agen_name, ma.agendetail_task from m_agendetail as ma\r\n" + 
				"left join m_agen as magen on magen.agen_id = ma.m_agen_id and magen.agen_company_id = ma.agendetail_company_id\r\n" + 
				"where\r\n" + 
				" 1=1" +
				status+
				"and ma.agendetail_company_id= :string ").setParameter("string", string).getResultList();
	}

	@Override
	public List<Object[]> listDetail(String string, String lowerCase) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and adv.agen_cover_area_status = true \r\n" ;
		}
		else if (lowerCase.equals("n"))
		{
			status = " and adv.agen_cover_area_status = false \r\n";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	ad.m_agen_id,\r\n" + 
				"	ad.agendetail_id,\r\n" + 
				"	adv.agen_cover_area_id,\r\n" + 
				"	ad.m_province_id as p,\r\n" + 
				"	ad.m_kota_id as v,\r\n" + 
				"	adv.m_province_id,\r\n" + 
				"	adv.m_kota_id \r\n" + 
				"FROM\r\n" + 
				"	\"m_agen_cover_area\" adv\r\n" + 
				"	LEFT JOIN m_agendetail AS ad ON adv.m_agen_id = ad.m_agen_id \r\n" + 
				"	AND adv.m_agendetail_id = ad.agendetail_id \r\n" + 
				"	AND adv.agen_cover_area_company_id = ad.agendetail_company_id \r\n" + 
				"WHERE\r\n" + 
				"	 \r\n" + 
				"	adv.agen_cover_area_company_id = :company \r\n" + status+
				"ORDER BY\r\n" + 
				"	ad.m_agen_id,\r\n" + 
				"	ad.agendetail_id,\r\n" + 
				"	adv.agen_cover_area_id ASC")
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public List<Object[]> listAgenKantor(String lowerCase, String string) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and ad.agendetail_status = true \r\n" ;
		}
		else if (lowerCase.equals("n"))
		{
			status = " and ad.agendetail_status = false \r\n";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT A\r\n" + 
				"	.agen_id,"
				+ " ad.agendetail_id,\r\n" + 
				"	A.agen_name,\r\n" + 
				"	ad.m_province_id,\r\n" + 
				"	ad.m_transferpoint_id,\r\n" + 
				"	ad.m_kota_id,\r\n" + 
				"	CAST ( ad.agendetail_created_by AS VARCHAR ),\r\n" + 
				"	ad.agendetail_created_at,\r\n" + 
				"	CAST ( ad.agendetail_updated_by AS VARCHAR ),\r\n" + 
				"	ad.agendetail_updated_at,\r\n" + 
				"	ad.agendetail_status,\r\n" + 
				"	ad.agendetail_company_id \r\n" + 
				"FROM\r\n" + 
				"	m_agen\r\n" + 
				"	AS A LEFT JOIN m_agendetail AS ad ON A.agen_id = ad.m_agen_id \r\n" + 
				"	AND A.agen_company_id = ad.agendetail_company_id \r\n" + 
				"WHERE\r\n" + 
				"	ad.agendetail_company_id = :company " + status)
				.setParameter("company", string).getResultList();
	}

	@Override
	public List<Object[]> getCoverArea(int parseInt, int parseInt2, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	* \r\n" + 
				"FROM\r\n" + 
				"	\"m_agen_cover_area\" \r\n" + 
				"WHERE\r\n" + 
				"	m_agen_id = :agenId \r\n" + 
				"	AND m_agendetail_id = :agenDetail \r\n" + 
				"	AND agen_cover_area_status = TRUE \r\n" + 
				"	AND agen_cover_area_company_id = :company ")
				.setParameter("agenId", parseInt)
				.setParameter("agenDetail", parseInt2)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public List<Object[]> listkode(String paramAktif, String company) {
		// TODO Auto-generated method stub
		String status = "";
		if(paramAktif.toLowerCase().equals("y"))
		{
			status = " and p.status = true";
		}
		else if (paramAktif.toLowerCase().equals("n"))
		{
			status = " and p.status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	* \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT \r\n" + 
				"	cast ( ag.agen_name  as varchar)as name,\r\n" + 
				"	NULL\r\n" + 
				"		m_cabang_id,\r\n" + 
				"		magd.m_agen_id,\r\n" + 
				"		magd.agendetail_id,\r\n" + 
				"		magd.m_kota_id,\r\n" + 
				"		magd.agendetail_company_id company_id,\r\n" + 
				"		magd.kode, \r\n" + 
				"		ag.agen_company_id company,\r\n" + 
				"		magd.agendetail_status status\r\n" + 
				"	FROM\r\n" + 
				"		m_agendetail magd  \r\n" + 
				"			left join m_agen as ag on ag.agen_id = magd.m_agen_id and ag.agen_company_id = magd.agendetail_company_id\r\n" + 
				"		 UNION\r\n" + 
				"		 SELECT \r\n" + 
				"		 cast (c.cabang_name  as varchar)as name,\r\n" + 
				"		 C\r\n" + 
				"		.cabang_id m_cabang_id,\r\n" + 
				"		NULL m_agen_id,\r\n" + 
				"		NULL agendetail_id,\r\n" + 
				"		C.m_kota_id m_kota_id,\r\n" + 
				"		C.cabang_company_id company_id,\r\n" + 
				"		c.kode,\r\n" + 
				"		c.cabang_company_id company,\r\n" + 
				"		c.cabang_status status\r\n" + 
				"	FROM\r\n" + 
				"		m_cabang C  \r\n" + 
				"		\r\n" + 
				"	) P  \r\n" + 
				"	 \r\n" + 
				"WHERE\r\n" + 
				"	1=1 \r\n"+ status + 
				"	and P.company_id = :company ")
				.setParameter("company", company).getResultList();
	}
	
}
