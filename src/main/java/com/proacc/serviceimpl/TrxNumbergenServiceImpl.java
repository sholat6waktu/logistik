package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxNumbergen;
import com.proacc.repository.TrxNumbergenRepository;
import com.proacc.serializable.TrxNumbergenSerializable;
import com.proacc.service.TrxNumbergenService;

@Service
public class TrxNumbergenServiceImpl implements TrxNumbergenService{

	private static final Logger logger = LoggerFactory.getLogger(TrxNumbergenServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxNumbergenRepository trxNumbergenRepository;

	@Override
	public List<Object[]> getallNumbergen(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void savenumbergen(TrxNumbergen trxNumbergen) {
		// TODO Auto-generated method stub
		trxNumbergenRepository.save(trxNumbergen);
	}

	@Override
	public Optional<TrxNumbergen> getdetail(Integer numberId, String numbergenId, String Company) {
		// TODO Auto-generated method stub
		return trxNumbergenRepository.findById(new TrxNumbergenSerializable(numberId, numbergenId,  Company));
	}
}
