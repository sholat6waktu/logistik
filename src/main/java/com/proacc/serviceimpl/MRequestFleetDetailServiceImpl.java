package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRequestFleetDetail;
import com.proacc.repository.MRequestFleetDetailRepository;
import com.proacc.serializable.MRequestFleetDetailSerializable;
import com.proacc.service.MRequestFleetDetailService;

@Service
public class MRequestFleetDetailServiceImpl implements MRequestFleetDetailService{

	private static final Logger logger = LoggerFactory.getLogger(MRequestFleetDetailServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MRequestFleetDetailRepository MRequestFleetDetailRepository;

	@Override
	public List<Object[]> getAll(int int1, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select * from m_request_fleet_detail where id = :id and company_id =:company").setParameter("id", int1).setParameter("company", string).getResultList();
	}

	@Override
	public Optional<MRequestFleetDetail> getdetailById(int parseInt, int parseInt2, String string) {
		// TODO Auto-generated method stub
		return MRequestFleetDetailRepository.findById(new MRequestFleetDetailSerializable(parseInt, parseInt2, string));
	}
}
