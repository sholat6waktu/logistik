package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxDriverTracking;
import com.proacc.repository.TrxDriverTrackingRepository;
import com.proacc.serializable.TrxDriverTrackingSerializable;
import com.proacc.service.TrxDriverTrackingService;

@Service
public class TrxDriverTrackingServiceImpl implements TrxDriverTrackingService{
	private static final Logger logger = LoggerFactory.getLogger(TrxDriverTrackingServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxDriverTrackingRepository TrxDriverTrackingRepository;

	@Override
	public void save(TrxDriverTracking data1) {
		// TODO Auto-generated method stub
		TrxDriverTrackingRepository.save(data1);
	}

	@Override
	public Optional<TrxDriverTracking> getDetail(String string, Integer tmTrxTrackingIdFrom,
			String trxDriverscheduleCompanyId) {
		// TODO Auto-generated method stub
		return TrxDriverTrackingRepository.findById(new TrxDriverTrackingSerializable(tmTrxTrackingIdFrom, string, trxDriverscheduleCompanyId));
	}

	@Override
	public List<Object[]> findByDriver(int parseInt, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	b.m_contact_customer_from_id,\r\n" + 
				"	c.cabang_name,\r\n" + 
				"	b.m_cabang_id,\r\n" + 
				"	c.m_province_id,\r\n" + 
				"	c.m_kota_id,\r\n" + 
				"	d.agen_name,\r\n" + 
				"	b.m_agen_id,\r\n" + 
				"	b.m_agendetail_id,\r\n" + 
				"	e.m_province_id a,\r\n" + 
				"	e.m_kota_id b,\r\n" + 
				"	b.trx_tracking_company_id,"
				+ " a.status,"
				+ " a.date_tracking,"
				+ " a.date_kantor,"
				+ " a.trx_order_id, "
				+ " a.id \r\n" + 
				"FROM\r\n" + 
				"	trx_drivertracking\r\n" + 
				"	A LEFT JOIN trx_tracking b ON A.trx_order_id = b.tm_trx_order_id \r\n" + 
				"	AND A.tracking_id = b.trx_tracking_id \r\n" + 
				"	left join m_cabang c on c.cabang_id = b.m_cabang_id and c.cabang_company_id = b.trx_tracking_company_id\r\n" + 
				"	left join m_agen d on d.agen_id = b.m_agen_id and d.agen_company_id = b.trx_tracking_company_id\r\n" + 
				"	left join m_agendetail e on e.m_agen_id = b.m_agen_id and e.agendetail_id = b.m_agendetail_id and e.agendetail_company_id = b.trx_tracking_company_id\r\n" + 
				"	\r\n" + 
				"WHERE\r\n" + 
				"	m_driver_id = :driverId \r\n" + 
				"	AND company_id = :companyId "
				+ " order by a.trx_order_id, a.id asc ")
				.setParameter("driverId", parseInt)
				.setParameter("companyId", string)
				.getResultList();
	}

}
