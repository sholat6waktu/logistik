package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MJenispaket;
import com.proacc.repository.MJenispaketRepository;
import com.proacc.serializable.MJenispaketSerializable;
import com.proacc.service.MJenispaketService;

@Service
public class MJenispaketServiceImpl implements MJenispaketService{

	private static final Logger logger = LoggerFactory.getLogger(MJenispaketServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MJenispaketRepository mJenispaketRepository;

	@Override
	public List<Object[]> nextvalMJenispaket() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('jenispaket_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getalljenispaket(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and jenispaket_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and jenispaket_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	jenispaket_id,\r\n"
				+ "	jenispaket_name,\r\n"
				+ "	jenispaket_description,\r\n"
				+ "	CAST ( jenispaket_created_by AS VARCHAR ),\r\n"
				+ "	jenispaket_created_at,\r\n"
				+ "	CAST ( jenispaket_updated_by AS VARCHAR ),\r\n"
				+ "	jenispaket_updated_at,\r\n"
				+ "	jenispaket_status,\r\n"
				+ "	jenispaket_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_jenispaket\r\n"
				+ "WHERE\r\n"
				+ "	jenispaket_company_id = :compid" + status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getjenispaket(String lowerCase, Integer jenispaketId, String compId) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and jenispaket_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and jenispaket_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	jenispaket_id,\r\n"
				+ "	jenispaket_name,\r\n"
				+ "	jenispaket_description,\r\n"
				+ "	CAST ( jenispaket_created_by AS VARCHAR ),\r\n"
				+ "	jenispaket_created_at,\r\n"
				+ "	CAST ( jenispaket_updated_by AS VARCHAR ),\r\n"
				+ "	jenispaket_updated_at,\r\n"
				+ "	jenispaket_status,\r\n"
				+ "	jenispaket_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_jenispaket\r\n"
				+ "WHERE\r\n"
				+ "	jenispaket_company_id = :compid\r\n"
				+ "	and jenispaket_id = :jenispaketid" + status).setParameter("compid", compId).setParameter("jenispaketid", jenispaketId).getResultList();
	}

	@Override
	public void savejenispaket(MJenispaket mJenispaket) {
		// TODO Auto-generated method stub
		mJenispaketRepository.save(mJenispaket);
		
	}

	@Override
	public Optional<MJenispaket> getdetail(Integer Id, String Company) {
		// TODO Auto-generated method stub
		return mJenispaketRepository.findById(new MJenispaketSerializable(Id, Company));
	}
}
