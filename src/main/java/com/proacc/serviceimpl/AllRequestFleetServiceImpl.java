package com.proacc.serviceimpl;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRequestFleet;
import com.proacc.entity.MRequestFleetDetail;
import com.proacc.service.AllRequestFleetService;
import com.proacc.repository.MRequestFleetRepository;
import com.proacc.repository.MRequestFleetDetailRepository;

@Service
public class AllRequestFleetServiceImpl implements AllRequestFleetService{
	private static final Logger logger = LoggerFactory.getLogger(AllRequestFleetServiceImpl.class);
	
	@Autowired
	MRequestFleetRepository MRequestFleetRepository;
	@Autowired
	MRequestFleetDetailRepository MRequestFleetDetailRepository;
	@Override
	@Transactional
	public void saveAll(MRequestFleet data, ArrayList<MRequestFleetDetail> dataDetailRequest) {
		// TODO Auto-generated method stub
		MRequestFleetRepository.save(data);
		MRequestFleetDetailRepository.saveAll(dataDetailRequest);
	}

}
