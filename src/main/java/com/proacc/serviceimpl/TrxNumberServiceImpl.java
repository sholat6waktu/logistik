package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxNumber;
import com.proacc.repository.TrxNumberRepository;
import com.proacc.serializable.TrxNumberSerializable;
import com.proacc.service.TrxNumberService;

@Service
public class TrxNumberServiceImpl implements TrxNumberService{

	private static final Logger logger = LoggerFactory.getLogger(TrxNumberServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxNumberRepository trxNumberRepository;

	@Override
	public List<Object[]> getallNumber(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ " *,\r\n"
				+ "	trx_number_prefix || '/' || trx_number_period || '/' || to_char( trx_number_runningnumber, '099999' ) || '/' || to_char( trx_number_date, 'yyyy' ) AS idorder\r\n"
				+ "FROM\r\n"
				+ "	trx_number \r\n"
				+ "WHERE\r\n"
				+ "	trx_number_company_id = :compid \r\n").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getNumber(String lowerCase, Integer NumberId, String compId) {
		return this.em.createNativeQuery("SELECT\r\n"
				+ " *,\r\n"
				+ " 9 || LPAD(cast(trx_number_runningnumber as text), 7, '0') idorder "
//				+ "	trx_number_prefix || '/' || trx_number_period || '/' || to_char( trx_number_runningnumber, '099999' ) || '/' || to_char( trx_number_date, 'yyyy' ) AS idorder\r\n"
				+ "FROM\r\n"
				+ "	trx_number \r\n"
				+ "WHERE\r\n"
				+ "	trx_number_company_id = '"+compId+"' \r\n"
				+ "	AND trx_number_id = "+NumberId+" ").getResultList();
	}

	@Override
	public void saveNumber(TrxNumber trxNumber) {
		// TODO Auto-generated method stub
		trxNumberRepository.save(trxNumber);
	}

	@Override
	public Optional<TrxNumber> getdetail(Integer Id, String Company) {
		// TODO Auto-generated method stub
		return trxNumberRepository.findById(new TrxNumberSerializable(Id, Company));
	}
}