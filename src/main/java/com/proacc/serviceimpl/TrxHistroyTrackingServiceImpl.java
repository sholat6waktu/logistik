package com.proacc.serviceimpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxHistroyTracking;
import com.proacc.repository.TrxHistroyTrackingRepository;
import com.proacc.service.TrxHistroyTrackingService;

@Service
public class TrxHistroyTrackingServiceImpl implements TrxHistroyTrackingService{
	private static final Logger logger = LoggerFactory.getLogger(TrxHistroyTrackingServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxHistroyTrackingRepository TrxHistroyTrackingRepository;

	@Override
	public List<Object[]> getLatestId(String string, String string2) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select id, order_id from trx_history_tracking where order_id = :orderId and company_id = :company order by id desc limit 1")
				.setParameter("orderId", string)
				.setParameter("company", string2)
				.getResultList();
	}

	@Override
	public void save(TrxHistroyTracking dataHistory) {
		// TODO Auto-generated method stub
		TrxHistroyTrackingRepository.save(dataHistory);
	}

	@Override
	public List<Object[]> getByOrderId(String string, String string2) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT a.order_id, a.id, a.status, cast (cb.cabang_name as varchar), cb.m_province_id, cb.m_kota_id, cast (ag.agen_name as varchar), agd.m_province_id agenprovince, agd.m_kota_id agenkota, a.waktu FROM \"trx_history_tracking\" a\r\n" + 
				"left join m_cabang cb on cb.cabang_id = a.cabang_id and cb.cabang_company_id = a.company_id\r\n" + 
				"left join m_agen ag on ag.agen_id = a.agen_id and ag.agen_company_id = a.company_id\r\n" + 
				"left join m_agendetail agd on agd.m_agen_id = a.agen_id and agd.agendetail_id = a.agendetail_id and agd.agendetail_company_id = a.company_id \r\n" + 
				"where order_id = :orderId \r\n" + 
				"and company_id = :company\r\n" + 
				"order by a.id")
				.setParameter("orderId", string)
				.setParameter("company", string2)
				.getResultList();
	}


}
