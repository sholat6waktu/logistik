package com.proacc.serviceimpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRate;
import com.proacc.repository.MRateRepository;
import com.proacc.serializable.MRateSerializable;
import com.proacc.service.MRateService;

@Service
public class MRateServiceImpl implements MRateService{

	private static final Logger logger = LoggerFactory.getLogger(MRateServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MRateRepository mRateRepository;

	@Override
	public List<Object[]> nextvalMRate() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('rate_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getallRate(String lowerCase, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and r.rate_draftstatus = 'complete'";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and r.rate_draftstatus = 'draft'";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	r.rate_id1,\r\n"
				+ "	r.rate_id2,\r\n"
				+ "	r.m_kota_from_id,\r\n"
				+ "	r.m_province_from_id,\r\n"
				+ "	r.m_kota_to_id,\r\n"
				+ "	r.m_province_to_id,\r\n"
				+ "	s.service_name,\r\n"
				+ "	r.rate_vehicle,\r\n"
				+ "	r.rate_price,\r\n"
				+ "	r.rate_startdate,\r\n"
				+ "	r.rate_enddate,\r\n"
				+ "	r.rate_draftstatus,\r\n"
				+ "	r.rate_status,\r\n"
				+ "	r.rate_company_id,\r\n"
				+ " r.m_service_id \r\n"
				+ "FROM\r\n"
				+ "	m_rate AS r INNER JOIN m_service as s ON r.m_service_id = s.service_id AND r.rate_company_id = s.service_company_id\r\n"
				+ "WHERE\r\n"
				+ "	r.rate_company_id = :compid"
				+ " ORDER BY r.rate_id1 ASC" + status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getRate(String lowerCase, Integer RateId1, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and r.rate_draftstatus = 'complete'";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and r.rate_draftstatus = 'draft'";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	r.rate_id1,\r\n"
				+ "	r.rate_id2,\r\n"
				+ "	r.m_kota_from_id,\r\n"
				+ "	r.m_province_from_id,\r\n"
				+ "	r.m_kota_to_id,\r\n"
				+ "	r.m_province_to_id,\r\n"
				+ "	s.service_name,\r\n"
				+ "	r.rate_vehicle,\r\n"
				+ "	r.rate_price,\r\n"
				+ "	r.rate_startdate,\r\n"
				+ "	r.rate_enddate,\r\n"
				+ "	r.rate_draftstatus,\r\n"
				+ "	r.rate_status,\r\n"
				+ "	r.rate_company_id,\r\n"
				+ " r.m_service_id \r\n"
				+ "FROM\r\n"
				+ "	m_rate AS r INNER JOIN m_service as s ON r.m_service_id = s.service_id AND r.rate_company_id = s.service_company_id\r\n"
				+ "WHERE\r\n"
				+ "	r.rate_company_id = :compid\r\n"
				+ "	and r.rate_id1 = :rateid1"
				+ "	ORDER BY r.rate_id1 ASC" + status).setParameter("compid", compId).setParameter("rateid1", RateId1).getResultList();
	}

	@Override
	public List<Object[]> getRateDetail(String lowerCase, Integer RateId1, Integer RateId2, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and r.rate_draftstatus = 'complete'";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and r.rate_draftstatus = 'draft'";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	r.rate_id1,\r\n"
				+ "	r.rate_id2,\r\n"
				+ "	r.m_kota_from_id,\r\n"
				+ "	r.m_province_from_id,\r\n"
				+ "	r.m_kota_to_id,\r\n"
				+ "	r.m_province_to_id,\r\n"
				+ "	s.service_name,\r\n"
				+ "	r.rate_vehicle,\r\n"
				+ "	r.rate_price,\r\n"
				+ "	r.rate_startdate,\r\n"
				+ "	r.rate_enddate,\r\n"
				+ "	r.rate_draftstatus,\r\n"
				+ "	r.rate_status,\r\n"
				+ "	r.rate_company_id, \r\n"
				+ " r.m_service_id \r\n"
				+ "FROM\r\n"
				+ "	m_rate AS r INNER JOIN m_service as s ON r.m_service_id = s.service_id AND r.rate_company_id = s.service_company_id\r\n"
				+ "WHERE\r\n"
				+ "	r.rate_company_id = :compid\r\n"
				+ "	and r.rate_id1 = :rateid1\r\n"
				+ "	and r.rate_id2 = :rateid2"
				+ " ORDER BY r.rate_id1 ASC" + status).setParameter("compid", compId).setParameter("rateid1", RateId1).setParameter("rateid2", RateId2).getResultList();
	}

	@Override
	public void saveallRate(ArrayList<MRate> mRateArray) {
		// TODO Auto-generated method stub
		mRateRepository.saveAll(mRateArray);
	}

	@Override
	public Optional<MRate> getdetail(Integer Id1, Integer Id2, String Company) {
		// TODO Auto-generated method stub
		return mRateRepository.findById(new MRateSerializable(Id1, Id2, Company));
	}

	@Override
	public List<Object[]> checkinsert(Integer cityfrom, Integer cityto, Integer service, String vehicle,
			String startdate, String enddate, String company) {
		return this.em.createNativeQuery("select ratecheckdate, 123 sok from ( select ratecheckdate(:cityfrom, :cityto, :service, :vehicle, :startdate,:enddate, :company) ) as q")
				.setParameter("cityfrom", cityfrom).setParameter("cityto", cityto).setParameter("service", service).setParameter("vehicle", vehicle)
				.setParameter("startdate", startdate).setParameter("enddate", enddate).setParameter("company", company).getResultList();
	}

	@Override
	public List<Object[]> getRatelastindex(String lowerCase, Integer RateId1, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and r.rate_draftstatus = 'complete'";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and r.rate_draftstatus = 'draft'";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	r.rate_id1,\r\n"
				+ "	r.rate_id2,\r\n"
				+ "	r.m_kota_from_id,\r\n"
				+ "	r.m_province_from_id,\r\n"
				+ "	r.m_kota_to_id,\r\n"
				+ "	r.m_province_to_id,\r\n"
				+ "	s.service_name,\r\n"
				+ "	r.rate_vehicle,\r\n"
				+ "	r.rate_price,\r\n"
				+ "	r.rate_startdate,\r\n"
				+ "	r.rate_enddate,\r\n"
				+ "	r.rate_draftstatus,\r\n"
				+ "	r.rate_status,\r\n"
				+ "	r.rate_company_id,\r\n"
				+ " r.m_service_id \r\n"
				+ "FROM\r\n"
				+ "	m_rate AS r INNER JOIN m_service as s ON r.m_service_id = s.service_id AND r.rate_company_id = s.service_company_id\r\n"
				+ "WHERE\r\n"
				+ "	r.rate_company_id = :compid\r\n"
				+ "	and r.rate_id1 = :rateid1" 
				+ status
				+ " ORDER BY r.rate_id2 DESC LIMIT 1").setParameter("compid", compId).setParameter("rateid1", RateId1).getResultList();
	}

	@Override
	public void deleteRate(Integer Id1, Integer Id2, String Company) {
		// TODO Auto-generated method stub
		mRateRepository.deleteById((new MRateSerializable(Id1, Id2, Company)));
	}

	@Override
	public List<Object[]> getallRatebystats(String lowerCase, String compId, String stats) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and r.rate_draftstatus = 'complete'";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and r.rate_draftstatus = 'draft'";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	r.rate_id1,\r\n"
				+ "	r.rate_id2,\r\n"
				+ "	r.m_kota_from_id,\r\n"
				+ "	r.m_province_from_id,\r\n"
				+ "	r.m_kota_to_id,\r\n"
				+ "	r.m_province_to_id,\r\n"
				+ "	s.service_name,\r\n"
				+ "	r.rate_vehicle,\r\n"
				+ "	r.rate_price,\r\n"
				+ "	r.rate_startdate,\r\n"
				+ "	r.rate_enddate,\r\n"
				+ "	r.rate_draftstatus,\r\n"
				+ "	r.rate_status,\r\n"
				+ "	r.rate_company_id,\r\n"
				+ " r.m_service_id \r\n"
				+ "FROM\r\n"
				+ "	m_rate AS r INNER JOIN m_service as s ON r.m_service_id = s.service_id AND r.rate_company_id = s.service_company_id\r\n"
				+ "WHERE\r\n"
				+ "	r.rate_company_id = :compid\r\n"
				+ "	and r.rate_draftstatus = :stats order by r.rate_id1" + status ).setParameter("compid", compId).setParameter("stats", stats).getResultList();
	}

	@Override
	public List<Object[]> checkupdate(Integer cityfrom, Integer cityto, Integer service, String vehicle,
			String startdate, String enddate, String company, Integer rateId2) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select ratecheckupdate, 123 sok from ( select ratecheckupdate(:cityfrom, :cityto, :service, :vehicle, :startdate,:enddate, :company, :rateid2) ) as q")
				.setParameter("cityfrom", cityfrom).setParameter("cityto", cityto).setParameter("service", service).setParameter("vehicle", vehicle)
				.setParameter("startdate", startdate).setParameter("enddate", enddate).setParameter("company", company).setParameter("rateid2", rateId2).getResultList();
	}
}