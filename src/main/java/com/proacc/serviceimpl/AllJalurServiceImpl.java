package com.proacc.serviceimpl;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.service.TrxJalurDriverHeaderService;
import com.proacc.service.TrxJalurDriverService;
import com.proacc.entity.TrxJalurDriver;
import com.proacc.entity.TrxJalurDriverHeader;
import com.proacc.service.AllJalurService;
import com.proacc.repository.TrxJalurDriverHeaderRepository;
import com.proacc.repository.TrxJalurDriverRepository;

@Service
public class AllJalurServiceImpl implements AllJalurService{
	private static final Logger logger = LoggerFactory.getLogger(AllJalurServiceImpl.class);
	
	@Autowired
	TrxJalurDriverHeaderRepository TrxJalurDriverHeaderRepository;
	
	@Autowired
	TrxJalurDriverRepository TrxJalurDriverRepository;
	
	

	@Override
	@Transactional
	public void saveAll(TrxJalurDriverHeader data, List<TrxJalurDriver> dataDetailTrx) {
		// TODO Auto-generated method stub
		TrxJalurDriverHeaderRepository.save(data);
		TrxJalurDriverRepository.saveAll(dataDetailTrx);
	}
	
	

}
