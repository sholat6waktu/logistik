package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRateHarga;
import com.proacc.repository.MRateHargaRepository;
import com.proacc.serializable.MRateHargaSerializable;
import com.proacc.service.MRateHargaService;

@Service
public class MRateHargaServiceImpl implements MRateHargaService{

	private static final Logger logger = LoggerFactory.getLogger(MRateHargaServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MRateHargaRepository mRateHargaRepository;

	@Override
	public List<Object[]> nextvalMRate() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('rateharga_sequence')").getResultList();
	}

	@Override
	public List<Object[]> getallRateHarga(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	r.rate_id1,\r\n"
				+ "	r.rate_id2,\r\n"
				+ "	rh.rateharga_id1,\r\n"
				+ "	rh.rateharga_id2,\r\n"
				+ "	rc.m_customer_id,\r\n"
				+ "	r.m_kota_from_id,\r\n"
				+ "	r.m_province_from_id,\r\n"
				+ "	r.m_kota_to_id,\r\n"
				+ "	r.m_province_to_id,\r\n"
				+ "	r.m_service_id,\r\n"
				+ "	s.service_name,\r\n"
				+ "	r.rate_vehicle,\r\n"
				+ "	r.rate_price,\r\n"
				+ "	rh.rateharga_discount,\r\n"
				+ "	rh.rateharga_nettprice,\r\n"
				+ "	rh.rateharga_startdate,\r\n"
				+ "	rh.rateharga_enddate,\r\n"
				+ "	rh.rateharga_status,\r\n"
				+ "	rh.rateharga_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_rate AS r\r\n"
				+ "	LEFT JOIN m_rateharga AS rh ON rh.m_rate_id1 = r.rate_id1 \r\n"
				+ "	AND rh.m_rate_id2 = r.rate_id2 \r\n"
				+ "	AND rh.rateharga_company_id = r.rate_company_id\r\n"
				+ "	LEFT JOIN m_service AS s ON r.m_service_id = s.service_id \r\n"
				+ "	AND r.rate_company_id = s.service_company_id\r\n"
				+ "	LEFT JOIN m_ratecustomer AS rc ON rh.m_ratecustomer_id = rc.m_customer_id \r\n"
				+ "	AND rh.rateharga_company_id = rc.ratecustomer_company_id \r\n"
				+ "WHERE\r\n"
				+ "	rh.rateharga_company_id = :compid").setParameter("compid", compId).getResultList();
	}

	@Override
	public void saveallrateharga(ArrayList <MRateHarga> mRatehargaArray) {
		// TODO Auto-generated method stub
		mRateHargaRepository.saveAll(mRatehargaArray);
	}

	@Override
	public List<Object[]> getRateHarga(String lowerCase, Integer RateHargaId1, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	r.rate_id1,\r\n"
				+ "	r.rate_id2,\r\n"
				+ "	rh.rateharga_id1,\r\n"
				+ "	rh.rateharga_id2,\r\n"
				+ "	rc.m_customer_id,\r\n"
				+ "	r.m_kota_from_id,\r\n"
				+ "	r.m_province_from_id,\r\n"
				+ "	r.m_kota_to_id,\r\n"
				+ "	r.m_province_to_id,\r\n"
				+ "	r.m_service_id,\r\n"
				+ "	s.service_name,\r\n"
				+ "	r.rate_vehicle,\r\n"
				+ "	r.rate_price,\r\n"
				+ "	rh.rateharga_discount,\r\n"
				+ "	rh.rateharga_nettprice,\r\n"
				+ "	rh.rateharga_startdate,\r\n"
				+ "	rh.rateharga_enddate,\r\n"
				+ "	rh.rateharga_status,\r\n"
				+ "	rh.rateharga_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_rate AS r\r\n"
				+ "	LEFT JOIN m_rateharga AS rh ON rh.m_rate_id1 = r.rate_id1 \r\n"
				+ "	AND rh.m_rate_id2 = r.rate_id2 \r\n"
				+ "	AND rh.rateharga_company_id = r.rate_company_id\r\n"
				+ "	LEFT JOIN m_service AS s ON r.m_service_id = s.service_id \r\n"
				+ "	AND r.rate_company_id = s.service_company_id\r\n"
				+ "	LEFT JOIN m_ratecustomer AS rc ON rh.m_ratecustomer_id = rc.m_customer_id \r\n"
				+ "	AND rh.rateharga_company_id = rc.ratecustomer_company_id \r\n"
				+ "WHERE\r\n"
				+ "	rh.rateharga_company_id = :compid\r\n"
				+ "	AND rh.rateharga_id1 = :ratehargaid1").setParameter("compid", compId).setParameter("ratehargaid1", RateHargaId1).getResultList();
	}

	@Override
	public List<Object[]> getRateDetail(String lowerCase, Integer RateHargaId1, Integer RateHargaId2, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	r.rate_id1,\r\n"
				+ "	r.rate_id2,\r\n"
				+ "	rh.rateharga_id1,\r\n"
				+ "	rh.rateharga_id2,\r\n"
				+ "	rc.m_customer_id,\r\n"
				+ "	r.m_kota_from_id,\r\n"
				+ "	r.m_province_from_id,\r\n"
				+ "	r.m_kota_to_id,\r\n"
				+ "	r.m_province_to_id,\r\n"
				+ "	r.m_service_id,\r\n"
				+ "	s.service_name,\r\n"
				+ "	r.rate_vehicle,\r\n"
				+ "	r.rate_price,\r\n"
				+ "	rh.rateharga_discount,\r\n"
				+ "	rh.rateharga_nettprice,\r\n"
				+ "	rh.rateharga_startdate,\r\n"
				+ "	rh.rateharga_enddate,\r\n"
				+ "	rh.rateharga_status,\r\n"
				+ "	rh.rateharga_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_rate AS r\r\n"
				+ "	LEFT JOIN m_rateharga AS rh ON rh.m_rate_id1 = r.rate_id1 \r\n"
				+ "	AND rh.m_rate_id2 = r.rate_id2 \r\n"
				+ "	AND rh.rateharga_company_id = r.rate_company_id\r\n"
				+ "	LEFT JOIN m_service AS s ON r.m_service_id = s.service_id \r\n"
				+ "	AND r.rate_company_id = s.service_company_id\r\n"
				+ "	LEFT JOIN m_ratecustomer AS rc ON rh.m_ratecustomer_id = rc.m_customer_id \r\n"
				+ "	AND rh.rateharga_company_id = rc.ratecustomer_company_id \r\n"
				+ "WHERE\r\n"
				+ "	rh.rateharga_company_id = :compid\r\n"
				+ "	AND rh.rateharga_id1 = :ratehargaid1\r\n"
				+ "	AND rh.rateharga_id2 = :ratehargaid2").setParameter("compid", compId).setParameter("ratehargaid1", RateHargaId1).setParameter("ratehargaid2", RateHargaId2).getResultList();
	}

	@Override
	public List<Object[]> getbyCustomerRateHarga(String lowerCase, Integer CustomerId, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	r.rate_id1,\r\n"
				+ "	r.rate_id2,\r\n"
				+ "	rh.rateharga_id1,\r\n"
				+ "	rh.rateharga_id2,\r\n"
				+ "	rc.m_customer_id,\r\n"
				+ "	r.m_kota_from_id,\r\n"
				+ "	r.m_province_from_id,\r\n"
				+ "	r.m_kota_to_id,\r\n"
				+ "	r.m_province_to_id,\r\n"
				+ "	r.m_service_id,\r\n"
				+ "	s.service_name,\r\n"
				+ "	r.rate_vehicle,\r\n"
				+ "	r.rate_price, \r\n" + 
				"	rh.rateharga_discount,\r\n" + 
				"	rh.rateharga_nettprice,\r\n"
				+ "	rh.rateharga_startdate,\r\n"
				+ "	rh.rateharga_enddate,\r\n"
				+ "	rh.rateharga_status,\r\n"
				+ "	rh.rateharga_company_id \r\n"
				+ "FROM\r\n"
				+ "	m_rate AS r\r\n"
				+ "	LEFT JOIN m_rateharga AS rh ON rh.m_rate_id1 = r.rate_id1 \r\n"
				+ "	AND rh.m_rate_id2 = r.rate_id2 \r\n"
				+ "	AND rh.rateharga_company_id = r.rate_company_id "
				+ " AND rh.m_ratecustomer_id = :customerid\r\n"
				+ "	LEFT JOIN m_service AS s ON r.m_service_id = s.service_id \r\n"
				+ "	AND r.rate_company_id = s.service_company_id\r\n"
				+ "	LEFT JOIN m_ratecustomer AS rc ON rh.m_ratecustomer_id = rc.ratecustomer_id \r\n"
				+ "	AND rh.rateharga_company_id = rc.ratecustomer_company_id "
				+ " AND rc.ratecustomer_id = :customerid "
				+ " \r\n"
				+ "WHERE"
				+ "	r.rate_company_id = :compid\r\n"
//				+ "	AND rh.m_ratecustomer_id = :customerid\r\n"
				).setParameter("compid", compId).setParameter("customerid", CustomerId).getResultList();
	}

	@Override
	public Optional<MRateHarga> getDetail(Integer mRateId1, Integer mRateId2, Integer ratehargaId1,
			Integer ratehargaId2, Integer mRatecustomerId, String ratehargaCompanyId) {
		// TODO Auto-generated method stub
		return mRateHargaRepository.findById(new MRateHargaSerializable(mRateId1, mRateId2, ratehargaId1, ratehargaId2, mRatecustomerId, ratehargaCompanyId));
	}

	@Override
	public void saverateharga(MRateHarga mRateharga) {
		// TODO Auto-generated method stub
		mRateHargaRepository.save(mRateharga);
	}

	@Override
	public List<Object[]> nettdisc(Float price, String diskon, String nett) {
		if(diskon == null)
		{
			return this.em.createNativeQuery("select nettdisc, 123 sok from ( select nettdisc(:price, null, :nett) ) as q")
					.setParameter("price", price).setParameter("nett", nett).getResultList();
		}
		else
		{
			return this.em.createNativeQuery("select nettdisc, 123 sok from ( select nettdisc(:price, :diskon, null) ) as q")
					.setParameter("price", price).setParameter("diskon", diskon).getResultList();	
		}
	}
}
