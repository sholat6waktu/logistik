package com.proacc.serviceimpl;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MCabang;
import com.proacc.entity.MCabangdetail;
import com.proacc.service.AllCabangService;
import com.proacc.service.MCabangService;
import com.proacc.service.MCabangdetailService;

@Service
public class AllCabangServiceImpl implements AllCabangService{
	private static final Logger logger = LoggerFactory.getLogger(AllCabangServiceImpl.class);
	
	@Autowired
	MCabangService mCabangService;
	@Autowired
	MCabangdetailService mCabangdetailService;
	
	@Override
	@Transactional
	public void saveallCabang(MCabang mCabang, ArrayList<MCabangdetail> MCabangdetailArray) {
		// TODO Auto-generated method stub
		mCabangService.saveCabang(mCabang);
		if(MCabangdetailArray.size() > 0)
		{
			mCabangdetailService.saveallCabangdetail(MCabangdetailArray);
		}
	}
}