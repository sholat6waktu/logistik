package com.proacc.serviceimpl;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MDriver;
import com.proacc.repository.MDriverRepository;
import com.proacc.serializable.MDriverSerializable;
import com.proacc.service.MDriverService;

@Service
public class MDriverServiceImpl implements MDriverService{

	private static final Logger logger = LoggerFactory.getLogger(MDriverServiceImpl.class);
	
	@Autowired
	EntityManager em;
	@Autowired
	MDriverRepository mDriverRepository;
	@Override
	public List<Object[]> nextvalMDriver() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('driver_sequence')").getResultList();
	}
	@Override
	public List<Object[]> getallDriver(String lowerCase, LocalDate driverDate, String compId, UUID user_uuid) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and driver_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and driver_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	d.driver_id,\r\n"
				+ "	f.fleet_id,\r\n"
				+ "	f.fleet_name,\r\n"
				+ "	f.fleet_vehicle,\r\n"
				+ " CAST ( d.driver_date AS VARCHAR ),\r\n"
				+ "	CAST ( to_char(d.driver_time_from, 'HH24:MI') AS VARCHAR ) || ' - ' || CAST ( to_char(d.driver_time_to, 'HH24:MI') AS VARCHAR ) AS waktu,\r\n"
				+ " CAST ( to_char(d.driver_time_from, 'HH24:MI') AS VARCHAR ) as timefrom,\r\n"
				+ " CAST ( to_char(d.driver_time_to, 'HH24:MI') AS VARCHAR ) as timeto,\r\n"
				+ " CAST ( d.m_contact_driver AS VARCHAR ),\r\n"
				+ " CAST ( d.m_contact_helper AS VARCHAR ),"
				+ " (select count(m_driver_id) from m_share_driver where m_driver_id = d.driver_id and company_id = d.driver_company_id) simbol,"
				+ " a.cabang_id,\r\n" + 
				" a.cabang_name,\r\n" + 
				" a.m_province_id,\r\n" + 
				" a.m_kota_id,\r\n" + 
				" b.agen_id,\r\n" + 
				" c.agendetail_id,\r\n" + 
				" c.m_province_id asdf,\r\n" + 
				" c.m_kota_id kota,"
				+ " b.agen_name  \r\n"
				+ " FROM\r\n"
				+ "	m_fleet AS f\r\n"
				+ "	LEFT JOIN m_driver AS d ON f.fleet_id = d.m_fleet_id \r\n"
				+ "	AND f.fleet_company_id = d.driver_company_id\r\n"
				+ "	AND d.driver_date = :driverdate\r\n"
				+ " left join m_cabang a on a.cabang_id = d.position_cabang_id and a.cabang_company_id = d.driver_company_id\r\n" + 
				"	left join m_agen b on b.agen_id = d.position_agen_id and b.agen_company_id = d.driver_company_id\r\n" + 
				"	left join m_agendetail c on c.agendetail_id = d.position_agendetail_id and c.m_agen_id = d.position_agen_id and c.agendetail_company_id = d.driver_company_id "
				+ "	WHERE\r\n"
				+ "	f.fleet_company_id = :compid 	and f.fleet_created_by = :user_uuid \r\n" + 
				"	"+ status).setParameter("compid", compId).setParameter("driverdate", driverDate).setParameter("user_uuid", user_uuid).getResultList();
	}
	@Override
	public List<Object[]> getDriver(String lowerCase, LocalDate driverDate, Integer driverId, String compId) {
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and driver_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and driver_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	d.driver_id,\r\n"
				+ "	f.fleet_name,\r\n"
				+ "	f.fleet_vehicle,\r\n"
				+ " CAST ( d.driver_date AS VARCHAR ),\r\n"
				+ " CAST ( to_char(d.driver_time_from, 'HH24:MI') AS VARCHAR ) as timefrom,\r\n"
				+ " CAST ( to_char(d.driver_time_to, 'HH24:MI') AS VARCHAR ) as timeto,\r\n"
				+ " CAST ( d.m_contact_driver AS VARCHAR ),\r\n"
				+ " CAST ( d.m_contact_helper AS VARCHAR )\r\n"
				+ " FROM\r\n"
				+ "	m_fleet AS f\r\n"
				+ "	LEFT JOIN m_driver AS d ON f.fleet_id = d.m_fleet_id \r\n"
				+ "	AND f.fleet_company_id = d.driver_company_id\r\n"
				+ "	AND d.driver_date = :driverdate\r\n"
				+ "	WHERE\r\n"
				+ "	f.fleet_company_id = :compid\r\n"
				+ "	and d.driver_id = :driverid"+ status).setParameter("compid", compId).setParameter("driverdate", driverDate).setParameter("driverid", driverId).getResultList();
	}
	@Override
	public void saveDriver(MDriver mDriver) {
		// TODO Auto-generated method stub
		mDriverRepository.save(mDriver);
	}
	@Override
	public Optional<MDriver> getdetail(Integer Id, String Company) {
		// TODO Auto-generated method stub
		return mDriverRepository.findById(new MDriverSerializable(Id, Company));
	}
	@Override
	public List<Object[]> getDriverAlltime(String lowerCase, UUID driver, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	d.driver_id,\r\n"
				+ "	f.fleet_id,\r\n"
				+ "	f.fleet_name,\r\n"
				+ "	f.fleet_vehicle,\r\n"
				+ " CAST ( d.driver_date AS VARCHAR ),\r\n"
				+ "	CAST ( to_char(d.driver_time_from, 'HH24:MI') AS VARCHAR ) || ' - ' || CAST ( to_char(d.driver_time_to, 'HH24:MI') AS VARCHAR ) AS waktu,\r\n"
				+ " CAST ( to_char(d.driver_time_from, 'HH24:MI') AS VARCHAR ) as timefrom,\r\n"
				+ " CAST ( to_char(d.driver_time_to, 'HH24:MI') AS VARCHAR ) as timeto,\r\n"
				+ " CAST ( d.m_contact_driver AS VARCHAR ),\r\n"
				+ " CAST ( d.m_contact_helper AS VARCHAR )\r\n"
				+ " FROM\r\n"
				+ "	m_fleet AS f\r\n"
				+ "	LEFT JOIN m_driver AS d ON f.fleet_id = d.m_fleet_id \r\n"
				+ "	AND f.fleet_company_id = d.driver_company_id\r\n"
				+ "	WHERE\r\n"
				+ "	f.fleet_company_id = :compid"
				+ " AND (CAST (m_contact_driver AS VARCHAR) = CAST ( :driver AS VARCHAR ) "
				+ " OR CAST (m_contact_helper AS VARCHAR) = CAST ( :driver AS VARCHAR )) ").setParameter("compid", compId).setParameter("driver", driver).getResultList();
	}
}
