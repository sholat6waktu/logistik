package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.MRequestFleet;
import com.proacc.repository.MRequestFleetRepository;
import com.proacc.serializable.MRequestFleetSerializable;
import com.proacc.service.MRequestFleetService;

@Service
public class MRequestFleetServiceImpl implements MRequestFleetService{

	private static final Logger logger = LoggerFactory.getLogger(MRequestFleetServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	MRequestFleetRepository MRequestFleetRepository;

	@Override
	public List<Object[]> detailRequest(String lowerCase, String lowerCase2, UUID user_uuid, String string, Integer id) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and a.status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and a.status = false";
		}
		else
		{
			status = "";
		}
		
		String statusDetail = "";
		if(lowerCase2.equals("y"))
		{
			statusDetail = " and b.status = true";
		}
		else if (lowerCase2.equals("n"))
		{
			statusDetail = " and b.status = false";
		}
		else
		{
			statusDetail = "";
		}
		return this.em.createNativeQuery("SELECT A\r\n" + 
				"	.ID,\r\n" + 
				"	b.detail_id,\r\n" + 
				"	A.NO,\r\n" + 
				"	A.date_request,\r\n" + 
				"	c.cabang_id cabang_id_from,\r\n" + 
				"	C.cabang_name AS cabang_from,\r\n" + 
				"	C.m_province_id cabang_provinsi_from,\r\n" + 
				"	C.m_kota_id cabang_kota_from,\r\n" + 
				"	h.agen_id,\r\n" + 
				"	e.agendetail_id,\r\n" + 
				"	h.agen_name agen_from,\r\n" + 
				"	e.m_province_id agen_province_from,\r\n" + 
				"	e.m_kota_id agen_kota_from,\r\n" + 
				"	d.cabang_id cabang_id_to,\r\n" + 
				"	d.cabang_name AS cabang_to,\r\n" + 
				"	d.m_province_id cabang_provinsi_to,\r\n" + 
				"	d.m_kota_id cabang_kota_to,\r\n" + 
				"	i.agen_id agen_id_to,\r\n" + 
				"	f.agendetail_id agendetail_it_to,\r\n" + 
				"	i.agen_name agen_to,\r\n" + 
				"	f.m_province_id agen_province_to,\r\n" + 
				"	f.m_kota_id agen_kota_to,\r\n" + 
				"	A.status,\r\n" + 
				"	b.status as det,\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"		WHEN C.cabang_name = :user_uuid THEN\r\n" + 
				"		'owner' ELSE'requested' \r\n" + 
				"	END,"
				+ " 	g.trx_order_id,\r\n" + 
				"	g.trx_order_date,\r\n" + 
				"	g.m_contact_customer_from,\r\n" + 
				"	g.m_contact_customer_to,\r\n" + 
				"	g.trx_order_koli,\r\n" + 
				"	g.m_weight_id,\r\n" + 
				"	g.trx_order_weight,\r\n" + 
				"	j.weight_name,\r\n" + 
				"	g.trx_order_volume,\r\n" + 
				"	g.m_volume_id,\r\n" + 
				"	k.volume_name	  \r\n" + 
				"	FROM\r\n" + 
				"		m_request_fleet\r\n" + 
				"		AS A LEFT JOIN m_request_fleet_detail AS b ON b.ID = A.ID \r\n" + 
				"		AND b.company_id = A.company_id\r\n" + 
				"		LEFT JOIN m_cabang AS C ON A.m_cabang_id = C.cabang_id \r\n" + 
				"		AND A.company_id = C.cabang_company_id\r\n" + 
				"		LEFT JOIN m_cabang AS d ON A.m_cabang_id_to = d.cabang_id \r\n" + 
				"		AND A.company_id = d.cabang_company_id\r\n" + 
				"		LEFT JOIN m_agendetail AS e ON A.m_agen_id = e.m_agen_id \r\n" + 
				"		AND A.m_agendetail_id = e.agendetail_id \r\n" + 
				"		AND A.company_id = e.agendetail_company_id\r\n" + 
				"		LEFT JOIN m_agendetail AS f ON A.m_agen_id_to = f.m_agen_id \r\n" + 
				"		AND A.m_agendetail_id_to = f.agendetail_id \r\n" + 
				"		AND A.company_id = f.agendetail_company_id\r\n" + 
				"		LEFT JOIN trx_order AS G ON b.order_id = G.trx_order_id \r\n" + 
				"		AND b.company_id = G.trx_order_company_id\r\n" + 
				"		LEFT JOIN m_agen AS h ON A.m_agen_id = h.agen_id \r\n" + 
				"		AND A.company_id = h.agen_company_id\r\n" + 
				"		LEFT JOIN m_agen AS i ON A.m_agen_id_to = i.agen_id \r\n" + 
				"		AND A.company_id = i.agen_company_id \r\n"
				+ " left join m_weight as j on j.weight_id = g.m_weight_id and j.weight_company_id = g.trx_order_company_id\r\n" + 
				"		left join m_volume as k on k.volume_id = g.m_volume_id and k.volume_company_id = g.trx_order_company_id " + 
				"	WHERE\r\n" + 
				"		( C.cabang_name = :user_uuid OR d.cabang_name = :user_uuid OR h.agen_name = :user_uuid_string OR i.agen_name = :user_uuid_string ) \r\n" + 
				"	AND A.company_id = :company \r\n" + 
				"	AND A.status = TRUE and a.id = :id " + status + statusDetail)
				.setParameter("user_uuid", user_uuid.toString())
				.setParameter("user_uuid_string", user_uuid.toString())
				.setParameter("company", string)
				.setParameter("id", id)
				.getResultList();
	}

	@Override
	public List<Object[]> listRequest(String lowerCase,  UUID user_uuid, String string) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and a.status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and a.status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT A\r\n" + 
				"	.ID,\r\n" + 
				"	A.NO,\r\n" + 
				"	A.date_request,\r\n" + 
				"	c.cabang_id cabang_id_from,\r\n" + 
				"	C.cabang_name AS cabang_from,\r\n" + 
				"	C.m_province_id cabang_provinsi_from,\r\n" + 
				"	C.m_kota_id cabang_kota_from,\r\n" + 
				"	h.agen_id,\r\n" + 
				"	e.agendetail_id,\r\n" + 
				"	h.agen_name agen_from,\r\n" + 
				"	e.m_province_id agen_province_from,\r\n" + 
				"	e.m_kota_id agen_kota_from,\r\n" + 
				"	d.cabang_id cabang_id_to,\r\n" + 
				"	d.cabang_name AS cabang_to,\r\n" + 
				"	d.m_province_id cabang_provinsi_to,\r\n" + 
				"	d.m_kota_id cabang_kota_to,\r\n" + 
				"	i.agen_id agen_id_to,\r\n" + 
				"	f.agendetail_id agendetail_it_to,\r\n" + 
				"	i.agen_name agen_to,\r\n" + 
				"	f.m_province_id agen_province_to,\r\n" + 
				"	f.m_kota_id agen_kota_to,\r\n" + 
				"	A.status,\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"		WHEN C.cabang_name = :user_uuid THEN\r\n" + 
				"		'owner' ELSE'requested' \r\n" + 
				"	END,"
				+ " (select count(id) from m_request_fleet_detail where id = a.id) totalManifest  \r\n" + 
				"	FROM\r\n" + 
				"		m_request_fleet AS A \r\n" + 
				"		LEFT JOIN m_cabang AS C ON A.m_cabang_id = C.cabang_id \r\n" + 
				"		AND A.company_id = C.cabang_company_id\r\n" + 
				"		LEFT JOIN m_cabang AS d ON A.m_cabang_id_to = d.cabang_id \r\n" + 
				"		AND A.company_id = d.cabang_company_id\r\n" + 
				"		LEFT JOIN m_agendetail AS e ON A.m_agen_id = e.m_agen_id \r\n" + 
				"		AND A.m_agendetail_id = e.agendetail_id \r\n" + 
				"		AND A.company_id = e.agendetail_company_id\r\n" + 
				"		LEFT JOIN m_agendetail AS f ON A.m_agen_id_to = f.m_agen_id \r\n" + 
				"		AND A.m_agendetail_id_to = f.agendetail_id \r\n" + 
				"		AND A.company_id = f.agendetail_company_id\r\n" + 
				"		LEFT JOIN m_agen AS h ON A.m_agen_id = h.agen_id \r\n" + 
				"		AND A.company_id = h.agen_company_id\r\n" + 
				"		LEFT JOIN m_agen AS i ON A.m_agen_id_to = i.agen_id \r\n" + 
				"		AND A.company_id = i.agen_company_id \r\n" + 
				"	WHERE\r\n" + 
				"		( C.cabang_name = :user_uuid OR d.cabang_name = :user_uuid OR h.agen_name = :user_uuid_string OR i.agen_name = :user_uuid_string ) \r\n" + 
				"	AND A.company_id = :company \r\n" + 
				"	 " + status )
				.setParameter("user_uuid", user_uuid.toString())
				.setParameter("user_uuid_string", user_uuid.toString())
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public List<Object[]> nextvalRequestFleet() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('request_fleet_sequence')").getResultList();
	}

	@Override
	public Optional<MRequestFleet> getDetail(int int1, String string) {
		// TODO Auto-generated method stub
		return MRequestFleetRepository.findById(new MRequestFleetSerializable(int1, string));
	}
}
