package com.proacc.serviceimpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxDriverschedule;
import com.proacc.repository.TrxDriverscheduleRepository;
import com.proacc.serializable.TrxDriverscheduleSerializable;
import com.proacc.service.TrxDriverscheduleService;

@Service
public class TrxDriverscheduleServiceImpl implements TrxDriverscheduleService{
	private static final Logger logger = LoggerFactory.getLogger(TrxDriverscheduleServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxDriverscheduleRepository trxDriverscheduleRepository;

	@Override
	public List<Object[]> getallDriverschedule(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxd.tm_trx_order_id,\r\n"
				+ "	trxd.trx_driverschedule_id,\r\n"
				+ "	trxd.tm_trx_tracking_id_from,\r\n"
				+ "	trxd.tm_trx_tracking_id_to,\r\n"
				+ "	CAST ( to_char( trxd.trx_driverschedule_time_from, 'HH24:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( trxd.trx_driverschedule_time_to, 'HH24:MI' ) AS VARCHAR ) AS waktu,\r\n"
				+ "	trxd.trx_driverschedule_weight,\r\n"
				+ " w.weight_name AS berat,\r\n"
				+ "	trxd.trx_driverschedule_volume,\r\n"
				+ " v.volume_name AS volume,\r\n"
				+ " trxd.trx_driverschedule_status,\r\n"
				+ "	trxd.trx_driverschedule_company_id \r\n"
				+ "FROM\r\n"
				+ "	trx_driverschedule AS trxd\r\n"
				+ "	LEFT JOIN m_weight AS w ON trxd.m_weight_id = w.weight_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_volume AS v ON trxd.m_volume_id = v.volume_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = v.volume_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trxd.trx_driverschedule_company_id = :compid ").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getDriverschedulebyorder(String lowerCase, String OrderId, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxd.tm_trx_order_id,\r\n"
				+ "	trxd.trx_driverschedule_id,\r\n"
				+ "	trxd.tm_trx_tracking_id_from,\r\n"
				+ "	trxd.tm_trx_tracking_id_to,\r\n"
				+ "	CAST ( to_char( trxd.trx_driverschedule_time_from, 'HH24:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( trxd.trx_driverschedule_time_to, 'HH24:MI' ) AS VARCHAR ) AS waktu,\r\n"
				+ "	trxd.trx_driverschedule_weight,\r\n"
				+ "	w.weight_name AS berat,\r\n"
				+ "	trxd.trx_driverschedule_volume,\r\n"
				+ "	v.volume_name AS volume,\r\n"
				+ "	trxd.trx_driverschedule_status,\r\n"
				+ "	trxd.trx_driverschedule_company_id,\r\n"
				+ "	trxd.m_driver_id,\r\n"
				+ "	f.fleet_vehicle,\r\n"
				+ "	CAST ( d.m_contact_driver AS VARCHAR ),"
				+ " trxd.tgl_berangkat,"
				+ " trxd.tgl_tiba \r\n"
				+ "FROM\r\n"
				+ "	trx_driverschedule AS trxd\r\n"
				+ "	LEFT JOIN m_weight AS w ON trxd.m_weight_id = w.weight_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_volume AS v ON trxd.m_volume_id = v.volume_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = v.volume_company_id\r\n"
				+ "	LEFT JOIN m_driver as d ON trxd.m_driver_id = d.driver_id\r\n"
				+ "	AND trxd.trx_driverschedule_company_id = d.driver_company_id\r\n"
				+ "	LEFT JOIN m_fleet as f ON d.m_fleet_id = f.fleet_id\r\n"
				+ "	AND f.fleet_company_id = d.driver_company_id "
				+ "WHERE\r\n"
				+ "	trxd.trx_driverschedule_company_id = :compid \r\n"
				+ "	AND trxd.tm_trx_order_id = :orderid ").setParameter("compid", compId).setParameter("orderid", OrderId).getResultList();
	}

	@Override
	public void savedriverschedule(TrxDriverschedule trxDriverschedule) {
		// TODO Auto-generated method stub
		trxDriverscheduleRepository.save(trxDriverschedule);
	}

	@Override
	public Optional<TrxDriverschedule> getdetail(String OrderId, Integer Id, String Company) {
		// TODO Auto-generated method stub
		return trxDriverscheduleRepository.findById(new TrxDriverscheduleSerializable(OrderId, Id, Company));
	}

	@Override
	public List<Object[]> getDriverschedulebytracking(String lowerCase, String OrderId, Integer trackingfrom,
			Integer trackingto, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxd.tm_trx_order_id,\r\n"
				+ "	trxd.trx_driverschedule_id,\r\n"
				+ "	trxd.tm_trx_tracking_id_from,\r\n"
				+ "	trxd.tm_trx_tracking_id_to,\r\n"
				+ "	CAST ( to_char( trxd.trx_driverschedule_time_from, 'HH24:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( trxd.trx_driverschedule_time_to, 'HH24:MI' ) AS VARCHAR ) AS waktu,\r\n"
				+ "	trxd.trx_driverschedule_weight,\r\n"
				+ " w.weight_name AS berat,\r\n"
				+ "	trxd.trx_driverschedule_volume,\r\n"
				+ " v.volume_name AS volume,\r\n"
				+ " trxd.trx_driverschedule_status,\r\n"
				+ "	trxd.trx_driverschedule_company_id \r\n"
				+ "FROM\r\n"
				+ "	trx_driverschedule AS trxd\r\n"
				+ "	LEFT JOIN m_weight AS w ON trxd.m_weight_id = w.weight_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_volume AS v ON trxd.m_volume_id = v.volume_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = v.volume_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trxd.trx_driverschedule_company_id = :compid \r\n"
				+ "	AND trxd.tm_trx_order_id = :orderid \r\n"
				+ "	AND trxd.tm_trx_tracking_id_from = :trackingfrom \r\n"
				+ "	AND trxd.tm_trx_tracking_id_to = :trackingto ").setParameter("compid", compId).setParameter("orderid", OrderId).setParameter("trackingfrom", trackingfrom).setParameter("trackingto", trackingto).getResultList();
	}

	@Override
	public List<Object[]> getallDriverschedulebydriver(String lowerCase, String compId, Integer driverId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxd.tm_trx_order_id,\r\n"
				+ "	trxd.m_driver_id,\r\n"
				+ "	trxd.trx_driverschedule_id,\r\n"
				+ "	trxd.tm_trx_tracking_id_from,\r\n"
				+ "	trxd.tm_trx_tracking_id_to,\r\n"
				+ "	CAST ( to_char( trxd.trx_driverschedule_time_from, 'HH24:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( trxd.trx_driverschedule_time_to, 'HH24:MI' ) AS VARCHAR ) AS waktu,\r\n"
				+ "	trxd.trx_driverschedule_weight,\r\n"
				+ " w.weight_name AS berat,\r\n"
				+ "	trxd.trx_driverschedule_volume,\r\n"
				+ " v.volume_name AS volume,\r\n"
				+ " trxd.trx_driverschedule_status,\r\n"
				+ "	trxd.trx_driverschedule_company_id \r\n"
				+ "FROM\r\n"
				+ "	trx_driverschedule AS trxd\r\n"
				+ "	LEFT JOIN m_weight AS w ON trxd.m_weight_id = w.weight_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_volume AS v ON trxd.m_volume_id = v.volume_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = v.volume_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trxd.trx_driverschedule_company_id = :compid\r\n"
				+ "	and trxd.m_driver_id = :driverid").setParameter("compid", compId).setParameter("driverid", driverId).getResultList();
	}

	@Override
	public List<Object[]> getalldriver(String lowerCase, LocalDate driverDate, String compId, UUID user_uuid) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and driver_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and driver_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("select q.*, case when \r\n" + 
				"i.id is not null then case when \r\n" + 
				"cast (q.driver_created_by as varchar) =  '"+user_uuid+"' \r\n" + 
				"then 'owner' else 'requested' end  else 'owner' end actor from (SELECT\r\n"
				+ "	d.driver_id,\r\n"
				+ "	f.fleet_id,\r\n"
				+ "	f.fleet_name,\r\n"
				+ "	f.fleet_vehicle,\r\n"
				+ "	CAST ( d.driver_date AS VARCHAR ),\r\n"
				+ "	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) AS waktu,\r\n"
				+ "	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) AS timefrom,\r\n"
				+ "	CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) AS timeto,\r\n"
				+ "	CAST ( d.m_contact_driver AS VARCHAR ),\r\n"
				+ "	CAST ( d.m_contact_helper AS VARCHAR ),\r\n"
				+ "	f.fleet_weight,\r\n"
				+ " w.weight_name,\r\n"
				+ "	f.fleet_volume,\r\n"
				+ " v.volume_name,"
				+ " d.trx_jalurdriverheader_id,\r\n" + 
				" mr.route_id,\r\n" + 
				" mr.route_name,"
				+ " "
				+ " z.vehicletype_name,"
				+ " d.driver_company_id company_id,cast (d.driver_created_by as varchar)	  \r\n"
				+ "FROM\r\n"
				+ "	m_driver AS d\r\n"
				+ "	LEFT JOIN m_fleet AS f ON f.fleet_id = d.m_fleet_id \r\n"
				+ "	AND f.fleet_company_id = d.driver_company_id \r\n"
				+ "	AND d.driver_date = '"+driverDate+"' \r\n"
				+ "	LEFT JOIN m_weight AS w ON f.m_weight_id = w.weight_id \r\n"
				+ "	AND f.fleet_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_volume AS v ON f.m_volume_id = v.volume_id \r\n"
				+ "	AND f.fleet_company_id = v.volume_company_id "
				+ " left join trx_jalur_driver_header as trxjh on trxjh.trx_driverjalurheader_id = d.trx_jalurdriverheader_id\r\n" + 
				"	and trxjh.company_id = d.driver_company_id\r\n" + 
				"	left join m_route as mr on mr.route_id = trxjh.m_route_id and mr.route_company_id = trxjh.company_id "
				+ " 	left join m_share_driver as i on i.m_driver_id = d.driver_id and i.company_id = d.driver_company_id\r\n" + 
				"	left join m_cabang as j on i.m_cabang_id = j.cabang_id and i.company_id = j.cabang_company_id\r\n" + 
				"	left join m_agen as k on k.agen_id = i.m_agen_id and k.agen_company_id = i.company_id "
				+ " left join m_vehicletype as z on f.m_vehicletype_id = z.vehicletype_id and f.fleet_company_id = z.vehicletype_company_id "
				+ "	left join m_cabang jk on jk.cabang_id = d.position_cabang_id and jk.cabang_company_id = d.driver_company_id\r\n" + 
				"	left join m_agen kl on kl.agen_id = d.position_agen_id and kl.agen_company_id = d.driver_company_id\r\n"
				+ "	WHERE\r\n"
				+ "	f.fleet_company_id = '"+compId+"' " + status +"	"
								+ " 	and "
								+ " \r\n" + 
								"				f.fleet_created_by = '"+user_uuid+"' \r\n" 
//								"	case when d.position_cabang_id is null and d.position_agen_id is null then f.fleet_created_by = '"+user_uuid+"' \r\n" + 
//								"	when  d.position_cabang_id is not null then jk.cabang_name = '"+user_uuid+"' \r\n" + 
//								"	when d.position_agen_id is not null then kl.agen_name = '"+user_uuid+"' \r\n" + 
//								"	end "
//								+ "and\r\n" + 
//								"	(d.status_condition=0 or d.status_condition is null )"
								+ ""
								+ "group by\r\n" + 
								"	d.driver_id,\r\n" + 
								"	f.fleet_id,\r\n" + 
								"	f.fleet_name,\r\n" + 
								"	f.fleet_vehicle,\r\n" + 
								"	CAST ( d.driver_date AS VARCHAR ),\r\n" + 
								"	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) ,\r\n" + 
								"	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) ,\r\n" + 
								"	CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) ,\r\n" + 
								"	CAST ( d.m_contact_driver AS VARCHAR ),\r\n" + 
								"	CAST ( d.m_contact_helper AS VARCHAR ),\r\n" + 
								"	f.fleet_weight,\r\n" + 
								"	w.weight_name,\r\n" + 
								"	f.fleet_volume,\r\n" + 
								"	v.volume_name,\r\n" + 
								"	d.trx_jalurdriverheader_id,\r\n" + 
								"	mr.route_id,\r\n" + 
								"	mr.route_name ,\r\n" + 
								"	f.fleet_created_by,"
								+ " z.vehicletype_name,"
								+ " d.driver_company_id,d.driver_created_by "
								+ " union\r\n" + 
								"SELECT\r\n" + 
								"	d.driver_id,\r\n" + 
								"	f.fleet_id,\r\n" + 
								"	f.fleet_name,\r\n" + 
								"	f.fleet_vehicle,\r\n" + 
								"	CAST ( d.driver_date AS VARCHAR ),\r\n" + 
								"	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) AS waktu,\r\n" + 
								"	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) AS timefrom,\r\n" + 
								"	CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) AS timeto,\r\n" + 
								"	CAST ( d.m_contact_driver AS VARCHAR ),\r\n" + 
								"	CAST ( d.m_contact_helper AS VARCHAR ),\r\n" + 
								"	f.fleet_weight,\r\n" + 
								"	w.weight_name,\r\n" + 
								"	f.fleet_volume,\r\n" + 
								"	v.volume_name,\r\n" + 
								"	d.trx_jalurdriverheader_id,\r\n" + 
								"	mr.route_id,\r\n" + 
								"	mr.route_name,"
								+ " "
								+ " z.vehicletype_name,"
								+ " a.company_id,cast (d.driver_created_by as varchar)  \r\n" + 
								"FROM\r\n" + 
								"	m_share_driver\r\n" + 
								"	A LEFT JOIN m_driver d ON d.driver_id = A.m_driver_id \r\n" + 
								"	AND d.driver_company_id = A.company_id\r\n" + 
								"	LEFT JOIN m_fleet AS f ON f.fleet_id = d.m_fleet_id \r\n" + 
								"	AND f.fleet_company_id = d.driver_company_id \r\n" + 
								"	AND d.driver_date = '"+driverDate+"' \r\n" + 
								"	LEFT JOIN m_weight AS w ON f.m_weight_id = w.weight_id \r\n" + 
								"	AND f.fleet_company_id = w.weight_company_id\r\n" + 
								"	LEFT JOIN m_volume AS v ON f.m_volume_id = v.volume_id \r\n" + 
								"	AND f.fleet_company_id = v.volume_company_id\r\n" + 
								"	LEFT JOIN trx_jalur_driver_header AS trxjh ON trxjh.trx_driverjalurheader_id = d.trx_jalurdriverheader_id \r\n" + 
								"	AND trxjh.company_id = d.driver_company_id\r\n" + 
								"	LEFT JOIN m_route AS mr ON mr.route_id = trxjh.m_route_id \r\n" + 
								"	AND mr.route_company_id = trxjh.company_id\r\n" + 
								"	left JOIN m_share_driver AS i ON i.m_driver_id = d.driver_id \r\n" + 
								"	AND i.company_id = d.driver_company_id\r\n" + 
								"	LEFT JOIN m_cabang AS j ON i.m_cabang_id = j.cabang_id \r\n" + 
								"	AND i.company_id = j.cabang_company_id\r\n" + 
								"	LEFT JOIN m_agen AS K ON K.agen_id = i.m_agen_id \r\n" + 
								"	AND K.agen_company_id = i.company_id "
								+ " left join m_vehicletype as z on f.m_vehicletype_id = z.vehicletype_id and f.fleet_company_id = z.vehicletype_company_id  "
								+ " left join m_cabang jk on jk.cabang_id = d.position_cabang_id and jk.cabang_company_id = d.driver_company_id " + 
								"					left join m_agen kl on kl.agen_id = d.position_agen_id and kl.agen_company_id = d.driver_company_id " + 
								"	where\r\n" + 
								"		f.fleet_company_id = '"+compId+"' \r\n" 
								+ " and \r\n" + 
//								"	case when d.position_cabang_id is null and d.position_agen_id is null then \r\n" + 
//								"	j.cabang_name =  cast (d.driver_created_by as varchar) OR K.agen_name =  cast (d.driver_created_by as varchar ) \r\n" + 
//								"	when  d.position_cabang_id is not null then "
//								--jk.cabang_name = '"+user_uuid+"'
								"  "
										+ " "
//										+ " (select route_jalur_id from m_route_jalur as aa where aa.m_route_id = trxjh.m_route_id and aa.m_cabang_id = d.position_cabang_id and aa.route_jalur_company_id = d.driver_company_id) <=\r\n" + 
//										"(select route_jalur_id from m_route_jalur ba where ba.m_route_id = trxjh.m_route_id \r\n" + 
//										"and\r\n" + 
//										" CASE " + 
//										"	" + 
//										"	WHEN (select cabang_id from m_cabang where m_cabang.cabang_name = '"+user_uuid+"' and m_cabang.cabang_company_id = '"+compId+"' ) IS NOT NULL THEN " + 
//										"			ba.m_cabang_id = (select cabang_id from m_cabang where m_cabang.cabang_name = '"+user_uuid+"' and m_cabang.cabang_company_id = '"+compId+"' ) " + 
//										"	ELSE (select agen_id from m_agen where m_agen.agen_name = '"+user_uuid+"' and m_agen.agen_company_id = '"+compId+"' ) = ba.m_agen_id " + 
//										"	END" + 
//										" and \r\n" + 
//										"ba.route_jalur_company_id = a.company_id)  " + 
//								"	when d.position_agen_id is not null then "
//								--kl.agen_name = '"+user_uuid+"'
								+ " "
//										+ " (select route_jalur_id from m_route_jalur as aa where aa.m_route_id = trxjh.m_route_id and aa.m_agen_id = d.position_agen_id and aa.m_agendetail_id = d.position_agendetail_id and aa.route_jalur_company_id = d.driver_company_id) <=\r\n" + 
//										"(select route_jalur_id from m_route_jalur ba where ba.m_route_id = trxjh.m_route_id and \r\n" + 
//										"CASE\r\n" + 
//			"								\r\n" + 
//			"								WHEN (select cabang_id from m_cabang where m_cabang.cabang_name = '"+user_uuid+"' and m_cabang.cabang_company_id = '"+compId+"' ) IS NOT NULL THEN\r\n" + 
//			"								ba.m_cabang_id = (select cabang_id from m_cabang where m_cabang.cabang_name = '"+user_uuid+"' and m_cabang.cabang_company_id = '"+compId+"' )"
//					+ "						and ba.route_jalur_company_id = '"+compId+"' \r\n" + 
//			"								ELSE (select agen_id from m_agen where m_agen.agen_name = '"+user_uuid+"' and m_agen.agen_company_id = '"+compId+"' ) = ba.m_agen_id \r\n" + 
//										"							END " + 
//										"and ba.route_jalur_company_id = a.company_id)  "
//										+ " \r\n" + 
//								"	end " 
								+ "  ( " + 
								"								j.cabang_name =  '" +user_uuid+"' OR K.agen_name =  '" +user_uuid+"' " + 
								"								)" + 
								"	and ( i.start_date >= '"+driverDate+"' OR i.end_date <= '"+driverDate+"' ) and a.status = true"
//										+ "and\r\n" + 
//										"	d.status_condition=0  "
//										+ " and\r\n" + 
//										"	(j.cabang_name = '"+user_uuid+"' or k.agen_name = '"+user_uuid+"' )"
								+ " ) as q\r\n" + 
								"		left JOIN m_share_driver AS i ON i.m_driver_id = q.driver_id \r\n" + 
								"	AND i.company_id = q.company_id\r\n" + 
								"	group by q.driver_id,q.fleet_id,q.fleet_name,q.fleet_vehicle,q.driver_date,q.waktu,q.timefrom,q.timeto,q.m_contact_driver,q.m_contact_helper,q.fleet_weight,\r\n" + 
								"		q.fleet_volume,\r\n" + 
								"	q.volume_name,\r\n" + 
								"	q.trx_jalurdriverheader_id,\r\n" + 
								"	q.route_id,\r\n" + 
								"	q.route_name, \r\n" + 
								"	q.vehicletype_name ,\r\n" + 
								"	q.weight_name,\r\n" + 
								"	q.company_id,\r\n" + 
								"	i.id,q.driver_created_by ")
//				.setParameter("compid", compId)
//				.setParameter("driverdate", driverDate)
				.getResultList();
	}
	
	
	@Override
	public List<Object[]> getalldriverTracking(String lowerCase, LocalDate driverDate, String compId, UUID user_uuid) {
		// TODO Auto-generated method stub
		String status = "";
		if(lowerCase.equals("y"))
		{
			status = " and driver_status = true";
		}
		else if (lowerCase.equals("n"))
		{
			status = " and driver_status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("select q.*, case when \r\n" + 
				"i.id is not null then case when \r\n" + 
				"cast (q.driver_created_by as varchar) =  '"+user_uuid+"' \r\n" + 
				"then 'owner' else 'requested' end  else 'owner' end actor from (SELECT\r\n"
				+ "	d.driver_id,\r\n"
				+ "	f.fleet_id,\r\n"
				+ "	f.fleet_name,\r\n"
				+ "	f.fleet_vehicle,\r\n"
				+ "	CAST ( d.driver_date AS VARCHAR ),\r\n"
				+ "	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) AS waktu,\r\n"
				+ "	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) AS timefrom,\r\n"
				+ "	CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) AS timeto,\r\n"
				+ "	CAST ( d.m_contact_driver AS VARCHAR ),\r\n"
				+ "	CAST ( d.m_contact_helper AS VARCHAR ),\r\n"
				+ "	f.fleet_weight,\r\n"
				+ " w.weight_name,\r\n"
				+ "	f.fleet_volume,\r\n"
				+ " v.volume_name,"
				+ " d.trx_jalurdriverheader_id,\r\n" + 
				" mr.route_id,\r\n" + 
				" mr.route_name,"
				+ " "
				+ " z.vehicletype_name,"
				+ " d.driver_company_id company_id,cast (d.driver_created_by as varchar)	  \r\n"
				+ "FROM\r\n"
				+ "	m_driver AS d\r\n"
				+ "	LEFT JOIN m_fleet AS f ON f.fleet_id = d.m_fleet_id \r\n"
				+ "	AND f.fleet_company_id = d.driver_company_id \r\n"
				+ "	AND d.driver_date = '"+driverDate+"' \r\n"
				+ "	LEFT JOIN m_weight AS w ON f.m_weight_id = w.weight_id \r\n"
				+ "	AND f.fleet_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_volume AS v ON f.m_volume_id = v.volume_id \r\n"
				+ "	AND f.fleet_company_id = v.volume_company_id "
				+ " left join trx_jalur_driver_header as trxjh on trxjh.trx_driverjalurheader_id = d.trx_jalurdriverheader_id\r\n" + 
				"	and trxjh.company_id = d.driver_company_id\r\n" + 
				"	left join m_route as mr on mr.route_id = trxjh.m_route_id and mr.route_company_id = trxjh.company_id "
				+ " 	left join m_share_driver as i on i.m_driver_id = d.driver_id and i.company_id = d.driver_company_id\r\n" + 
				"	left join m_cabang as j on i.m_cabang_id = j.cabang_id and i.company_id = j.cabang_company_id\r\n" + 
				"	left join m_agen as k on k.agen_id = i.m_agen_id and k.agen_company_id = i.company_id "
				+ " left join m_vehicletype as z on f.m_vehicletype_id = z.vehicletype_id and f.fleet_company_id = z.vehicletype_company_id "
				+ "	left join m_cabang jk on jk.cabang_id = d.position_cabang_id and jk.cabang_company_id = d.driver_company_id\r\n" + 
				"	left join m_agen kl on kl.agen_id = d.position_agen_id and kl.agen_company_id = d.driver_company_id\r\n"
				+ "	WHERE\r\n"
				+ "	f.fleet_company_id = '"+compId+"' " + status +"	"
//								+ " 	and \r\n" + 
//								"	\r\n" + 
//								"	d.status_condition=0"
								+ ""
								+ "group by\r\n" + 
								"	d.driver_id,\r\n" + 
								"	f.fleet_id,\r\n" + 
								"	f.fleet_name,\r\n" + 
								"	f.fleet_vehicle,\r\n" + 
								"	CAST ( d.driver_date AS VARCHAR ),\r\n" + 
								"	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) ,\r\n" + 
								"	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) ,\r\n" + 
								"	CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) ,\r\n" + 
								"	CAST ( d.m_contact_driver AS VARCHAR ),\r\n" + 
								"	CAST ( d.m_contact_helper AS VARCHAR ),\r\n" + 
								"	f.fleet_weight,\r\n" + 
								"	w.weight_name,\r\n" + 
								"	f.fleet_volume,\r\n" + 
								"	v.volume_name,\r\n" + 
								"	d.trx_jalurdriverheader_id,\r\n" + 
								"	mr.route_id,\r\n" + 
								"	mr.route_name ,\r\n" + 
								"	f.fleet_created_by,"
								+ " z.vehicletype_name,"
								+ " d.driver_company_id,d.driver_created_by "
								+ " union\r\n" + 
								"SELECT\r\n" + 
								"	d.driver_id,\r\n" + 
								"	f.fleet_id,\r\n" + 
								"	f.fleet_name,\r\n" + 
								"	f.fleet_vehicle,\r\n" + 
								"	CAST ( d.driver_date AS VARCHAR ),\r\n" + 
								"	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) AS waktu,\r\n" + 
								"	CAST ( to_char( d.driver_time_from, 'HH24\\:MI' ) AS VARCHAR ) AS timefrom,\r\n" + 
								"	CAST ( to_char( d.driver_time_to, 'HH24\\:MI' ) AS VARCHAR ) AS timeto,\r\n" + 
								"	CAST ( d.m_contact_driver AS VARCHAR ),\r\n" + 
								"	CAST ( d.m_contact_helper AS VARCHAR ),\r\n" + 
								"	f.fleet_weight,\r\n" + 
								"	w.weight_name,\r\n" + 
								"	f.fleet_volume,\r\n" + 
								"	v.volume_name,\r\n" + 
								"	d.trx_jalurdriverheader_id,\r\n" + 
								"	mr.route_id,\r\n" + 
								"	mr.route_name,"
								+ " "
								+ " z.vehicletype_name,"
								+ " a.company_id,cast (d.driver_created_by as varchar)  \r\n" + 
								"FROM\r\n" + 
								"	m_share_driver\r\n" + 
								"	A LEFT JOIN m_driver d ON d.driver_id = A.m_driver_id \r\n" + 
								"	AND d.driver_company_id = A.company_id\r\n" + 
								"	LEFT JOIN m_fleet AS f ON f.fleet_id = d.m_fleet_id \r\n" + 
								"	AND f.fleet_company_id = d.driver_company_id \r\n" + 
								"	AND d.driver_date = '"+driverDate+"' \r\n" + 
								"	LEFT JOIN m_weight AS w ON f.m_weight_id = w.weight_id \r\n" + 
								"	AND f.fleet_company_id = w.weight_company_id\r\n" + 
								"	LEFT JOIN m_volume AS v ON f.m_volume_id = v.volume_id \r\n" + 
								"	AND f.fleet_company_id = v.volume_company_id\r\n" + 
								"	LEFT JOIN trx_jalur_driver_header AS trxjh ON trxjh.trx_driverjalurheader_id = d.trx_jalurdriverheader_id \r\n" + 
								"	AND trxjh.company_id = d.driver_company_id\r\n" + 
								"	LEFT JOIN m_route AS mr ON mr.route_id = trxjh.m_route_id \r\n" + 
								"	AND mr.route_company_id = trxjh.company_id\r\n" + 
								"	left JOIN m_share_driver AS i ON i.m_driver_id = d.driver_id \r\n" + 
								"	AND i.company_id = d.driver_company_id\r\n" + 
								"	LEFT JOIN m_cabang AS j ON i.m_cabang_id = j.cabang_id \r\n" + 
								"	AND i.company_id = j.cabang_company_id\r\n" + 
								"	LEFT JOIN m_agen AS K ON K.agen_id = i.m_agen_id \r\n" + 
								"	AND K.agen_company_id = i.company_id "
								+ " left join m_vehicletype as z on f.m_vehicletype_id = z.vehicletype_id and f.fleet_company_id = z.vehicletype_company_id  "
								+ " left join m_cabang jk on jk.cabang_id = d.position_cabang_id and jk.cabang_company_id = d.driver_company_id " + 
								"					left join m_agen kl on kl.agen_id = d.position_agen_id and kl.agen_company_id = d.driver_company_id " + 
								"	where\r\n" + 
								"		f.fleet_company_id = '"+compId+"' \r\n" 
								+ "  \r\n" + 
								"	" + 
								"	and ( i.start_date >= '"+driverDate+"' OR i.end_date <= '"+driverDate+"' ) "
//										+ "and\r\n" + 
//										"	d.status_condition=0  "
								+ " ) as q\r\n" + 
								"		left JOIN m_share_driver AS i ON i.m_driver_id = q.driver_id \r\n" + 
								"	AND i.company_id = q.company_id\r\n" + 
								"	group by q.driver_id,q.fleet_id,q.fleet_name,q.fleet_vehicle,q.driver_date,q.waktu,q.timefrom,q.timeto,q.m_contact_driver,q.m_contact_helper,q.fleet_weight,\r\n" + 
								"		q.fleet_volume,\r\n" + 
								"	q.volume_name,\r\n" + 
								"	q.trx_jalurdriverheader_id,\r\n" + 
								"	q.route_id,\r\n" + 
								"	q.route_name, \r\n" + 
								"	q.vehicletype_name ,\r\n" + 
								"	q.weight_name,\r\n" + 
								"	q.company_id,\r\n" + 
								"	i.id,q.driver_created_by ")
//				.setParameter("compid", compId)
//				.setParameter("driverdate", driverDate)
				.getResultList();
	}

	@Override
	public List<Object[]> availablecapacityberat(Integer driverid, String status, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select availablecapacityberat, 123 sok from ( select availablecapacityberat(:driverid, :status, :compid) ) as q").setParameter("driverid", driverid).setParameter("status", status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> availablecapacityvolume(Integer driverid, String status, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select availablecapacityvolume, 123 sok from ( select availablecapacityvolume(:driverid, :status, :compid) ) as q").setParameter("driverid", driverid).setParameter("status", status).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> schedulecheck(Integer driverId, String timefrom, String timeto, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select schedulecheck, 123 sok from ( select schedulecheck(:driverid, :timefrom, :timeto, :compid) ) as q").setParameter("driverid", driverId).setParameter("timefrom", timefrom).setParameter("timeto", timeto).setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getallDriverschedulebydriverbystatus(String lowerCase, String compId, Integer driverId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxd.tm_trx_order_id,\r\n"
				+ "	trxd.m_driver_id,\r\n"
				+ "	trxd.trx_driverschedule_id,\r\n"
				+ "	trxd.tm_trx_tracking_id_from,\r\n"
				+ "	trxd.tm_trx_tracking_id_to,\r\n"
				+ "	CAST ( to_char( trxd.trx_driverschedule_time_from, 'HH24:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( trxd.trx_driverschedule_time_to, 'HH24:MI' ) AS VARCHAR ) AS waktu,\r\n"
				+ "	trxd.trx_driverschedule_weight,\r\n"
				+ " w.weight_name AS berat,\r\n"
				+ "	trxd.trx_driverschedule_volume,\r\n"
				+ " v.volume_name AS volume,\r\n"
				+ " trxd.trx_driverschedule_status,\r\n"
				+ "	trxd.trx_driverschedule_company_id, "
				+ " trxo.m_contact_customer_from,"
				+ " a.m_parent_id a1,\r\n" + 
				"	c.cabang_name a2,\r\n" + 
				"	c.m_province_id b,\r\n" + 
				"	c.m_kota_id c,\r\n" + 
				"	a.m_agen_id d,\r\n" + 
				"	d.agen_name e,\r\n" + 
				"	e.m_province_id f,\r\n" + 
				"	e.m_kota_id g,\r\n" + 
				"	a.m_agendetail_id h,\r\n" + 
				"	a.m_contact_customer_to_id i,\r\n" + 
				"	\r\n" + 
				"	b.m_contact_customer_from_id j,\r\n" + 
				"	b.m_parent_id k,\r\n" + 
				"	f.cabang_name l,\r\n" + 
				"	f.m_province_id m,\r\n" + 
				"	f.m_kota_id n,\r\n" + 
				"	b.m_agen_id o,\r\n" + 
				"	g.agen_name p,\r\n" + 
				"	h.m_province_id q,\r\n" + 
				"	h.m_kota_id r,\r\n" + 
				"	b.m_agendetail_id s ,\r\n" + 
				"	b.m_contact_customer_to_id t,"
				+ " 	i_from.trx_driverjalurheader_id q1,\r\n" + 
				"	i_from.trx_driverjalur_id s1,\r\n" + 
				"	i_to.trx_driverjalurheader_id s4,\r\n" + 
				"	i_to.trx_driverjalur_id s5, "
				+ "trxo.m_contact_customer_to,"
				+ " 	a.m_contact_customer_from_id qw,\r\n" + 
				"	a.m_contact_customer_to_id wq,\r\n" +
				" trxo.trx_order_koli as jmlh, "+
//				"	(SELECT count(*) FROM trx_kolidetail\r\n" + 
//				"where\r\n" + 
//				"tm_trx_order_id = trxd.tm_trx_order_id\r\n" + 
//				"and trx_kolidetail_company_id = trxd.trx_driverschedule_company_id ) as jmlh,\r\n" + 
				"ser.service_name  \r\n"
				+ "FROM\r\n"
				+ "	trx_driverschedule AS trxd\r\n"
				+ "	LEFT JOIN m_weight AS w ON trxd.m_weight_id = w.weight_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_volume AS v ON trxd.m_volume_id = v.volume_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = v.volume_company_id \r\n"
				+ "	LEFT JOIN trx_order AS trxo ON trxd.tm_trx_order_id = trxo.trx_order_id\r\n"
				+ "	AND trxd.trx_driverschedule_company_id = trxo.trx_order_company_id "
				+ " left join trx_tracking a on a.trx_tracking_id = trxd.tm_trx_tracking_id_from and a.tm_trx_order_id = trxd.tm_trx_order_id and a.trx_tracking_company_id = trxd.trx_driverschedule_company_id\r\n" + 
				"	left join trx_tracking b on b.trx_tracking_id = trxd.tm_trx_tracking_id_to and b.tm_trx_order_id = trxd.tm_trx_order_id and b.trx_tracking_company_id = trxd.trx_driverschedule_company_id\r\n" + 
				"	left join m_cabang c on c.cabang_id = a.m_parent_id and c.cabang_company_id = a.trx_tracking_company_id\r\n" + 
				"	left join m_agen d on d.agen_id = a.m_agen_id and d.agen_company_id = a.trx_tracking_company_id\r\n" + 
				"	left join m_agendetail e on e.agendetail_id = a.m_agendetail_id and e.m_agen_id = a.m_agen_id and e.agendetail_company_id = a.trx_tracking_company_id\r\n" + 
				"	\r\n" + 
				"	left join m_cabang f on f.cabang_id = b.m_parent_id and f.cabang_company_id = b.trx_tracking_company_id\r\n" + 
				"	left join m_agen g on g.agen_id = b.m_agen_id and g.agen_company_id = b.trx_tracking_company_id\r\n" + 
				"	left join m_agendetail h on h.agendetail_id = b.m_agendetail_id and h.m_agen_id = b.m_agen_id and h.agendetail_company_id = b.trx_tracking_company_id "
				+ " left join trx_jalurdriver i_from on i_from.trx_driverjalurheader_id = a.trx_driverjalurheader_id and i_from.trx_driverjalur_id = a.trx_driverjalur_id and i_from.company_id = a.trx_tracking_company_id\r\n" + 
				"	left join trx_jalurdriver i_to on i_to.trx_driverjalurheader_id = b.trx_driverjalurheader_id and i_to.trx_driverjalur_id = b.trx_driverjalur_id and i_to.company_id = b.trx_tracking_company_id"
				+ " left join m_service ser on ser.service_id = trxo.m_service_id and ser.service_company_id =trxo.trx_order_company_id"
				+ " WHERE\r\n"
				+ "	trxd.trx_driverschedule_company_id = :compid\r\n"
				+ "	and trxd.m_driver_id = :driverid "
				+ " AND (trxd.trx_driverschedule_statusoutbound != 'complete' OR trxd.trx_driverschedule_statusoutbound ISNULL) ").setParameter("compid", compId).setParameter("driverid", driverId).getResultList();
	}

	@Override
	public void savealldriverschedule(ArrayList<TrxDriverschedule> trxDriverscheduleArray) {
		// TODO Auto-generated method stub
		trxDriverscheduleRepository.saveAll(trxDriverscheduleArray);
	}

	@Override
	@Transactional
	public void updatedriver(String OrderId, Integer trackto, String Company, String statsout, String statsin) {
		// TODO Auto-generated method stub
		this.em.createNativeQuery(""
				+ "UPDATE trx_driverschedule \r\n"
				+ "SET trx_driverschedule_statusoutbound = :statsout,\r\n"
				+ "trx_driverschedule_status = :statsin \r\n"
				+ "WHERE\r\n"
				+ "	tm_trx_order_id = :orderid\r\n"
				+ "	AND tm_trx_tracking_id_to = :trackto\r\n"
				+ "	AND trx_driverschedule_company_id = :compid").setParameter("orderid", OrderId).setParameter("trackto", trackto).setParameter("compid", Company).setParameter("statsout", statsout).setParameter("statsin", statsin).executeUpdate();
	}

	@Override
	public List<Object[]> getDriverschedulebytrackingtocheckstat(String lowerCase, String OrderId, Integer trackingto,
			String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxd.tm_trx_order_id,\r\n"
				+ "	trxd.trx_driverschedule_id,\r\n"
				+ "	trxd.tm_trx_tracking_id_from,\r\n"
				+ "	trxd.tm_trx_tracking_id_to,\r\n"
				+ "	CAST ( to_char( trxd.trx_driverschedule_time_from, 'HH24:MI' ) AS VARCHAR ) || ' - ' || CAST ( to_char( trxd.trx_driverschedule_time_to, 'HH24:MI' ) AS VARCHAR ) AS waktu,\r\n"
				+ "	trxd.trx_driverschedule_weight,\r\n"
				+ " w.weight_name AS berat,\r\n"
				+ "	trxd.trx_driverschedule_volume,\r\n"
				+ " v.volume_name AS volume,\r\n"
				+ " trxd.trx_driverschedule_status,\r\n"
				+ "	trxd.trx_driverschedule_company_id \r\n"
				+ "FROM\r\n"
				+ "	trx_driverschedule AS trxd\r\n"
				+ "	LEFT JOIN m_weight AS w ON trxd.m_weight_id = w.weight_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = w.weight_company_id\r\n"
				+ "	LEFT JOIN m_volume AS v ON trxd.m_volume_id = v.volume_id \r\n"
				+ "	AND trxd.trx_driverschedule_company_id = v.volume_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trxd.trx_driverschedule_company_id = :compid \r\n"
				+ "	AND trxd.tm_trx_order_id = :orderid \r\n"
				+ "	AND trxd.trx_driverschedule_statusoutbound = 'waiting response' \r\n"
				+ "	AND trxd.tm_trx_tracking_id_to = "+trackingto+" ").setParameter("compid", compId).setParameter("orderid", OrderId).getResultList();
	}

	@Override
	public List<Object[]> getmyinbound(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno) {
		// TODO Auto-generated method stub
		String status = "";
		String statusDate = " ";
		String kotaToStatus = " ";
		String kurirStatus = " ";
		logger.info("coba = " + "sd" + conno.isEmpty());
		if(kotato.isEmpty() == false)
		{
			kotaToStatus = " AND trxo.trx_kota_to LIKE '" + kotato.toUpperCase() + "%'";
		}
		if(kurirname.isEmpty() == false)
		{
			kurirStatus = " AND d.driver_name LIKE '" + kurirname + "%'";
		}
		
		if(orderdate.isEmpty() == false)
		{
			statusDate = " AND trxo.trx_order_date = '" + orderdate + "'";
			logger.info(statusDate + " <==status date");
		}
		else
		{
			statusDate = " ";
		}
		if(conno.isEmpty() == false)
		{
			status = " AND trxo.trx_order_connocement LIKE '" + conno + "%'";
		}
		else
		{
			status = " ";
		}
//		return this.em.createNativeQuery("SELECT\r\n"
//				+ "	trxo.trx_order_id,\r\n"
//				+ "	trxo.trx_order_date,\r\n"
//				+ "	trxo.m_contact_customer_from,\r\n"
//				+ "	trxo.trx_order_pickup_address,\r\n"
//				+ "	trxo.m_contact_customer_to,\r\n"
//				+ "	ser.service_name,\r\n"
//				+ "	trxo.trx_order_koli,\r\n"
//				+ "	trxd.trx_driverschedule_status,\r\n"
//				+ "	trxo.trx_order_company_id,\r\n"
//				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n"
//				+ "	trxd.trx_driverschedule_id \r\n"
//				+ "FROM\r\n"
//				+ "	trx_order AS trxo\r\n"
//				+ "	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n"
//				+ "	AND trxo.trx_order_company_id = ser.service_company_id\r\n"
//				+ "	LEFT JOIN trx_driverschedule as trxd ON trxo.trx_order_id = trxd.tm_trx_order_id\r\n"
//				+ "	AND trxo.trx_order_company_id = trxd.trx_driverschedule_company_id\r\n"
//				+ "	LEFT JOIN m_driver as D ON trxd.m_driver_id = d.driver_id\r\n"
//				+ "	AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n"
//				+ "WHERE\r\n"
//				+ "	trxo.trx_order_company_id = :compid \r\n"
//				+ "	AND trxd.trx_driverschedule_status IS NOT NULL\r\n"
//				+ "	AND ( CAST( d.m_contact_driver AS VARCHAR ) = :customer OR CAST( d.m_contact_helper AS VARCHAR ) = :customer) " 
//				+ status + statusDate + kotaToStatus + kurirStatus
//				+ " GROUP BY\r\n"
//				+ "	trxo.trx_order_id,\r\n"
//				+ "	trxo.trx_order_date,\r\n"
//				+ "	trxo.m_contact_customer_from,\r\n"
//				+ "	trxo.trx_order_pickup_address,\r\n"
//				+ "	trxo.m_contact_customer_to,\r\n"
//				+ "	ser.service_name,\r\n"
//				+ "	trxo.trx_order_koli,\r\n"
//				+ "	trxd.trx_driverschedule_status,\r\n"
//				+ "	trxo.trx_order_company_id,\r\n"
//				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), "
//				+ "	trxd.trx_driverschedule_id \r\n" ).setParameter("compid", compId).setParameter("customer", customer.toString()).getResultList();
		
		return this.em.createNativeQuery("SELECT\r\n" + 
				"					trxo.trx_order_id,\r\n" + 
				"					trxo.trx_order_date,\r\n" + 
				"					trxo.m_contact_customer_from,\r\n" + 
				"					trxo.trx_order_pickup_address,\r\n" + 
				"					trxo.m_contact_customer_to,\r\n" + 
				"					ser.service_name,\r\n" + 
				"					trxo.trx_order_koli,\r\n" + 
				"					trxd.trx_driverschedule_status,\r\n" + 
				"					trxo.trx_order_company_id,\r\n" + 
				"					CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n" + 
				"					trxd.trx_driverschedule_id ,\r\n" + 
				"					null as driver,\r\n" + 
				"					c.m_kota_id to_kota_cabang,\r\n" + 
				"c.m_province_id to_provinsi_cabang,\r\n" + 
				"e.m_kota_id to_kota_agen,\r\n" + 
				"e.m_province_id to_provinsi_agen,\r\n" + 
				"	trxt.m_contact_customer_from_id to_cust_from,\r\n" + 
				"	trxt.m_contact_customer_to_id to_cust_to,\r\n" + 
				"					c2.m_kota_id from_kota_cabang,\r\n" + 
				"c2.m_province_id from_provinsi_cabang,\r\n" + 
				"e2.m_kota_id from_kota_agen,\r\n" + 
				"e2.m_province_id from_provinsi_agen,\r\n" + 
				"	trxt_from.m_contact_customer_from_id from_cust_from,\r\n" + 
				"	trxt_from.m_contact_customer_to_id from_cust_to\r\n" + 
				"				FROM\r\n" + 
				"					trx_order AS trxo\r\n" + 
				"					LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n" + 
				"					AND trxo.trx_order_company_id = ser.service_company_id\r\n" + 
				"					LEFT JOIN trx_driverschedule as trxd ON trxo.trx_order_id = trxd.tm_trx_order_id\r\n" + 
				"					AND trxo.trx_order_company_id = trxd.trx_driverschedule_company_id\r\n" + 
				"					LEFT JOIN m_driver as D ON trxd.m_driver_id = d.driver_id\r\n" + 
				"					AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n" + 
				"					\r\n" + 
				"					-- 		from\r\n" + 
				"				LEFT JOIN trx_tracking AS trxt_from ON trxo.trx_order_id = trxt_from.tm_trx_order_id \r\n" + 
				"					AND trxt_from.trx_tracking_company_id = trxd.trx_driverschedule_company_id\r\n" + 
				"					and trxt_from.trx_tracking_id = trxd.tm_trx_tracking_id_from\r\n" + 
				"					\r\n" + 
				"					LEFT JOIN m_cabang AS C2 ON trxt_from.m_cabang_id = C2.cabang_id \r\n" + 
				"					AND trxt_from.trx_tracking_company_id = C2.cabang_company_id\r\n" + 
				"					\r\n" + 
				"					LEFT JOIN m_agen AS A2 ON trxt_from.m_agen_id = A2.agen_id \r\n" + 
				"					AND trxt_from.m_agen_id = A2.agen_id\r\n" + 
				"					left join m_agendetail as e2 on trxt_from.m_agen_id = e2.m_agen_id and trxt_from.m_agendetail_id = e2.agendetail_id and trxt_from.trx_tracking_company_id = e2.agendetail_company_id\r\n" + 
				"					\r\n" + 
				"					-- 	to\r\n" + 
				"	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n" + 
				"	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n" + 
				"	and trxt.trx_tracking_id = trxd.tm_trx_tracking_id_to\r\n" + 
				"-- 	to cabang / agen\r\n" + 
				"	LEFT JOIN m_cabang AS C ON trxt.m_cabang_id = C.cabang_id \r\n" + 
				"	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n" + 
				"	AND trxt.m_agen_id = A.agen_id\r\n" + 
				"	left join m_agendetail as e on trxt.m_agen_id = e.m_agen_id and trxt.m_agendetail_id = e.agendetail_id and trxt.trx_tracking_company_id = e.agendetail_company_id\r\n" + 
				"	\r\n" + 
				"				WHERE\r\n" + 
				"					trxo.trx_order_company_id = :compid \r\n" + 
				"					AND trxd.trx_driverschedule_status IS NOT NULL\r\n" + 
				"					AND ( CAST( d.m_contact_driver AS VARCHAR ) = :customer OR CAST( d.m_contact_helper AS VARCHAR ) = :customer) " 
								+ status + statusDate + kotaToStatus + kurirStatus + 
				"				 GROUP BY\r\n" + 
				"					trxo.trx_order_id,\r\n" + 
				"					trxo.trx_order_date,\r\n" + 
				"					trxo.m_contact_customer_from,\r\n" + 
				"					trxo.trx_order_pickup_address,\r\n" + 
				"					trxo.m_contact_customer_to,\r\n" + 
				"					ser.service_name,\r\n" + 
				"					trxo.trx_order_koli,\r\n" + 
				"					trxd.trx_driverschedule_status,\r\n" + 
				"					trxo.trx_order_company_id,\r\n" + 
				"					CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n" + 
				"					trxd.trx_driverschedule_id,\r\n" + 
				"					c2.m_kota_id ,\r\n" + 
				"c2.m_province_id ,\r\n" + 
				"e2.m_kota_id ,\r\n" + 
				"e2.m_province_id ,\r\n" + 
				"	trxt_from.m_contact_customer_from_id ,\r\n" + 
				"	trxt_from.m_contact_customer_to_id ,\r\n" + 
				"	c.m_kota_id ,\r\n" + 
				"c.m_province_id ,\r\n" + 
				"e.m_kota_id ,\r\n" + 
				"e.m_province_id ,\r\n" + 
				"	trxt.m_contact_customer_from_id ,\r\n" + 
				"	trxt.m_contact_customer_to_id " ).setParameter("compid", compId).setParameter("customer", customer.toString()).getResultList();
	}

	@Override
	public List<Object[]> getmyoutbound(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno) {
		// TODO Auto-generated method stub
		String status = "";
		String statusDate = " ";
		String kotaToStatus = " ";
		String kurirStatus = " ";
		if(kotato.isEmpty() == false)
		{
			kotaToStatus = " AND trxo.trx_kota_to LIKE '" + kotato.toUpperCase() + "%'";
		}
		if(kurirname.isEmpty() == false)
		{
			kurirStatus = " AND d.driver_name LIKE '" + kurirname + "%'";
		}
		
		if(orderdate.isEmpty() == false)
		{
			statusDate = " AND trxo.trx_order_date = '" + orderdate + "'";
			logger.info(statusDate + " <==status date");
		}
		else
		{
			statusDate = " ";
		}
		if(conno.isEmpty() == false)
		{
			status = " AND trxo.trx_order_connocement LIKE '" + conno + "%'";
		}
		else
		{
			status = " ";
		}
//		return this.em.createNativeQuery("SELECT\r\n"
//				+ "	trxo.trx_order_id,\r\n"
//				+ "	trxo.trx_order_date,\r\n"
//				+ "	trxo.m_contact_customer_from,\r\n"
//				+ "	trxo.trx_order_pickup_address,\r\n"
//				+ "	trxo.m_contact_customer_to,\r\n"
//				+ "	ser.service_name,\r\n"
//				+ "	trxo.trx_order_koli,\r\n"
//				+ "	trxd.trx_driverschedule_statusoutbound,\r\n"
//				+ "	trxo.trx_order_company_id,\r\n"
//				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n"
//				+ "	trxd.trx_driverschedule_id \r\n"
//				+ "FROM\r\n"
//				+ "	trx_order AS trxo\r\n"
//				+ "	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n"
//				+ "	AND trxo.trx_order_company_id = ser.service_company_id\r\n"
//				+ "	LEFT JOIN trx_driverschedule as trxd ON trxo.trx_order_id = trxd.tm_trx_order_id\r\n"
//				+ "	AND trxo.trx_order_company_id = trxd.trx_driverschedule_company_id\r\n"
//				+ "	LEFT JOIN m_driver as D ON trxd.m_driver_id = d.driver_id\r\n"
//				+ "	AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n"
//				+ "WHERE\r\n"
//				+ "	trxo.trx_order_company_id = :compid \r\n"
//				+ "	AND trxd.trx_driverschedule_statusoutbound IS NOT NULL\r\n"
//				+ "	AND ( CAST( d.m_contact_driver AS VARCHAR ) = :cusUUID OR CAST( d.m_contact_helper AS VARCHAR ) = :cusUUID) " 
//				+ status + statusDate + kotaToStatus + kurirStatus
//				+ " GROUP BY\r\n"
//				+ "	trxo.trx_order_id,\r\n"
//				+ "	trxo.trx_order_date,\r\n"
//				+ "	trxo.m_contact_customer_from,\r\n"
//				+ "	trxo.trx_order_pickup_address,\r\n"
//				+ "	trxo.m_contact_customer_to,\r\n"
//				+ "	ser.service_name,\r\n"
//				+ "	trxo.trx_order_koli,\r\n"
//				+ "	trxd.trx_driverschedule_statusoutbound,\r\n"
//				+ "	trxo.trx_order_company_id,\r\n"
//				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), "
//				+ "	trxd.trx_driverschedule_id \r\n" ).setParameter("compid", compId).setParameter("cusUUID", customer.toString()).getResultList();
		
		return this.em.createNativeQuery("SELECT\r\n" + 
				"					trxo.trx_order_id,\r\n" + 
				"					trxo.trx_order_date,\r\n" + 
				"					trxo.m_contact_customer_from,\r\n" + 
				"					trxo.trx_order_pickup_address,\r\n" + 
				"					trxo.m_contact_customer_to,\r\n" + 
				"					ser.service_name,\r\n" + 
				"					trxo.trx_order_koli,\r\n" + 
				"					trxd.trx_driverschedule_statusoutbound,\r\n" + 
				"					trxo.trx_order_company_id,\r\n" + 
				"					CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n" + 
				"					trxd.trx_driverschedule_id ,\r\n" + 
				"					null as driver,\r\n" + 
				"					c.m_kota_id to_kota_cabang,\r\n" + 
				"c.m_province_id to_provinsi_cabang,\r\n" + 
				"e.m_kota_id to_kota_agen,\r\n" + 
				"e.m_province_id to_provinsi_agen,\r\n" + 
				"	trxt.m_contact_customer_from_id to_cust_from,\r\n" + 
				"	trxt.m_contact_customer_to_id to_cust_to,\r\n" + 
				"					c2.m_kota_id from_kota_cabang,\r\n" + 
				"c2.m_province_id from_provinsi_cabang,\r\n" + 
				"e2.m_kota_id from_kota_agen,\r\n" + 
				"e2.m_province_id from_provinsi_agen,\r\n" + 
				"	trxt_from.m_contact_customer_from_id from_cust_from,\r\n" + 
				"	trxt_from.m_contact_customer_to_id from_cust_to\r\n" + 
				"				FROM\r\n" + 
				"					trx_order AS trxo\r\n" + 
				"					LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n" + 
				"					AND trxo.trx_order_company_id = ser.service_company_id\r\n" + 
				"					LEFT JOIN trx_driverschedule as trxd ON trxo.trx_order_id = trxd.tm_trx_order_id\r\n" + 
				"					AND trxo.trx_order_company_id = trxd.trx_driverschedule_company_id\r\n" + 
				"					LEFT JOIN m_driver as D ON trxd.m_driver_id = d.driver_id\r\n" + 
				"					AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n" + 
				"					\r\n" + 
				"					-- 		from\r\n" + 
				"				LEFT JOIN trx_tracking AS trxt_from ON trxo.trx_order_id = trxt_from.tm_trx_order_id \r\n" + 
				"					AND trxt_from.trx_tracking_company_id = trxd.trx_driverschedule_company_id\r\n" + 
				"					and trxt_from.trx_tracking_id = trxd.tm_trx_tracking_id_from\r\n" + 
				"					\r\n" + 
				"					LEFT JOIN m_cabang AS C2 ON trxt_from.m_cabang_id = C2.cabang_id \r\n" + 
				"					AND trxt_from.trx_tracking_company_id = C2.cabang_company_id\r\n" + 
				"					\r\n" + 
				"					LEFT JOIN m_agen AS A2 ON trxt_from.m_agen_id = A2.agen_id \r\n" + 
				"					AND trxt_from.m_agen_id = A2.agen_id\r\n" + 
				"					left join m_agendetail as e2 on trxt_from.m_agen_id = e2.m_agen_id and trxt_from.m_agendetail_id = e2.agendetail_id and trxt_from.trx_tracking_company_id = e2.agendetail_company_id\r\n" + 
				"					\r\n" + 
				"					-- 	to\r\n" + 
				"	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n" + 
				"	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n" + 
				"	and trxt.trx_tracking_id = trxd.tm_trx_tracking_id_to\r\n" + 
				"-- 	to cabang / agen\r\n" + 
				"	LEFT JOIN m_cabang AS C ON trxt.m_cabang_id = C.cabang_id \r\n" + 
				"	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n" + 
				"	\r\n" + 
				"	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n" + 
				"	AND trxt.m_agen_id = A.agen_id\r\n" + 
				"	left join m_agendetail as e on trxt.m_agen_id = e.m_agen_id and trxt.m_agendetail_id = e.agendetail_id and trxt.trx_tracking_company_id = e.agendetail_company_id\r\n" + 
				"	\r\n" + 
				"				WHERE\r\n" + 
				"					trxo.trx_order_company_id = :compid \r\n" + 
				"					AND trxd.trx_driverschedule_statusoutbound IS NOT NULL\r\n" + 
				"					AND ( CAST( d.m_contact_driver AS VARCHAR ) = :customer OR CAST( d.m_contact_helper AS VARCHAR ) = :customer) " 
								+ status + statusDate + kotaToStatus + kurirStatus + 
				"				 GROUP BY\r\n" + 
				"					trxo.trx_order_id,\r\n" + 
				"					trxo.trx_order_date,\r\n" + 
				"					trxo.m_contact_customer_from,\r\n" + 
				"					trxo.trx_order_pickup_address,\r\n" + 
				"					trxo.m_contact_customer_to,\r\n" + 
				"					ser.service_name,\r\n" + 
				"					trxo.trx_order_koli,\r\n" + 
				"					trxd.trx_driverschedule_statusoutbound,\r\n" + 
				"					trxo.trx_order_company_id,\r\n" + 
				"					CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n" + 
				"					trxd.trx_driverschedule_id,\r\n" + 
				"					c2.m_kota_id ,\r\n" + 
				"c2.m_province_id ,\r\n" + 
				"e2.m_kota_id ,\r\n" + 
				"e2.m_province_id ,\r\n" + 
				"	trxt_from.m_contact_customer_from_id ,\r\n" + 
				"	trxt_from.m_contact_customer_to_id ,\r\n" + 
				"	c.m_kota_id ,\r\n" + 
				"c.m_province_id ,\r\n" + 
				"e.m_kota_id ,\r\n" + 
				"e.m_province_id ,\r\n" + 
				"	trxt.m_contact_customer_from_id ,\r\n" + 
				"	trxt.m_contact_customer_to_id " ).setParameter("compid", compId).setParameter("customer", customer.toString()).getResultList();
	}

	@Override
	public List<Object[]> getmynotif(String lowerCase, UUID customer, String compId, String kotato, String kurirname, String orderdate, String conno) {
		// TODO Auto-generated method stub
		String status = "";
		String statusDate = " ";
		String kotaToStatus = " ";
		String kurirStatus = " ";
		if(kotato.isEmpty() == false)
		{
			kotaToStatus = " AND trxo.trx_kota_to LIKE '" + kotato.toUpperCase() + "%'";
		}
		if(kurirname.isEmpty() == false)
		{
			kurirStatus = " AND d.driver_name LIKE '" + kurirname + "%'";
		}
		
		if(orderdate.isEmpty() == false)
		{
			statusDate = " AND trxo.trx_order_date = '" + orderdate + "'";
			logger.info(statusDate + " <==status date");
		}
		else
		{
			statusDate = " ";
		}
		if(conno.isEmpty() == false)
		{
			status = " AND trxo.trx_order_connocement LIKE '" + conno + "%'";
		}
		else
		{
			status = " ";
		}
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.trx_order_pickup_address,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	ser.service_name,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "	trxd.trx_driverschedule_status,\r\n"
				+ "	trxo.trx_order_company_id,\r\n"
				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), \r\n"
				+ "	trxd.trx_driverschedule_id \r\n"
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN m_service AS ser ON trxo.m_service_id = service_id \r\n"
				+ "	AND trxo.trx_order_company_id = ser.service_company_id\r\n"
				+ "	LEFT JOIN trx_driverschedule as trxd ON trxo.trx_order_id = trxd.tm_trx_order_id\r\n"
				+ "	AND trxo.trx_order_company_id = trxd.trx_driverschedule_company_id\r\n"
				+ "	LEFT JOIN m_driver as D ON trxd.m_driver_id = d.driver_id\r\n"
				+ "	AND trxd.trx_driverschedule_company_id = d.driver_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trxo.trx_order_company_id = :compid \r\n"
				+ "	AND trxd.trx_driverschedule_status IS NOT NULL\r\n"
				+ "	AND ( CAST( d.m_contact_driver AS VARCHAR ) = :customer OR CAST( d.m_contact_helper AS VARCHAR ) = :customer) " 
				+ status + statusDate + kotaToStatus + kurirStatus
				+ " GROUP BY\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.m_contact_customer_from,\r\n"
				+ "	trxo.trx_order_pickup_address,\r\n"
				+ "	trxo.m_contact_customer_to,\r\n"
				+ "	ser.service_name,\r\n"
				+ "	trxo.trx_order_koli,\r\n"
				+ "	trxd.trx_driverschedule_status,\r\n"
				+ "	trxo.trx_order_company_id,\r\n"
				+ "	CAST ( trxo.trx_order_created_by AS VARCHAR ), "
				+ "	trxd.trx_driverschedule_id \r\n").setParameter("compid", compId).setParameter("customer", customer.toString()).getResultList();
	}
	@Override
	@Transactional
	public void updatedriverstatusbyorder(String OrderId, String compid) {
		// TODO Auto-generated method stub
		this.em.createNativeQuery("UPDATE trx_driverschedule \r\n"
				+ "SET trx_driverschedule_status = NULL,\r\n"
				+ "trx_driverschedule_statusoutbound = 'complete' \r\n"
				+ "WHERE\r\n"
				+ "	tm_trx_order_id = :orderid AND trx_driverschedule_company_id = :compid").setParameter("orderid", OrderId).setParameter("compid", compid).executeUpdate();
	}

	@Override
	public void delete(int int1, String string, String string2) {
		// TODO Auto-generated method stub
		boolean coba =  trxDriverscheduleRepository.existsById(new TrxDriverscheduleSerializable(string, int1, string2));
		
		if(coba == true)
		{

			trxDriverscheduleRepository.deleteById((new TrxDriverscheduleSerializable(string, int1, string2)));
		}
		
	}

	@Override
	public List<Object[]> cekKotaTujuan(Integer mcabangId, Integer mAgenId, Integer mAgenDetailId, String string, String string2) {
		// TODO Auto-generated method stub
		String status = "";
		if(mcabangId == null && mAgenId != null)
		{
			status = "	and m_agen_id = " +mAgenId+  
					"	and m_agendetail_id = "+mAgenDetailId;
		}
		else if(mcabangId != null && mAgenId != null)
		{
			status = "	and m_cabang_id = " +mcabangId;
		}
		else if(mcabangId != null && mAgenId == null)
		{
			status = "	and m_cabang_id = " +mcabangId;
		}
		else
		{
			status = "";
		}
			
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	* \r\n" + 
				"FROM\r\n" + 
				"	\"public\".\"trx_tracking\" \r\n" + 
				"WHERE\r\n" + 
				"	\"tm_trx_order_id\" = :orderId and trx_tracking_company_id = :companyId \r\n" + 
				"	AND trx_tracking_id = ( SELECT MAX ( trx_tracking_id - 1 ) FROM \"public\".\"trx_tracking\" WHERE \"tm_trx_order_id\" = :orderId  and trx_tracking_company_id = :companyId ) "+ status)
				.setParameter("orderId", string2)
				.setParameter("companyId", string)
				.getResultList();
				
	}

	@Override
	public Optional<TrxDriverschedule> getDetail(String string, int int1, String string2) {
		// TODO Auto-generated method stub
		return trxDriverscheduleRepository.findById(new TrxDriverscheduleSerializable(string, int1, string2));
	}

	@Override
	public List<Object[]> getLastDriverSchedule(String string, String string2) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	trx_driverschedule_id,\r\n" + 
				"	m_driver_id \r\n" + 
				"FROM\r\n" + 
				"	\"trx_driverschedule\" \r\n" + 
				"WHERE\r\n" + 
				"	tm_trx_order_id = :order \r\n" + 
				"	AND trx_driverschedule_company_id = :company "
				+ "  \r\n" + 
				"ORDER BY\r\n" + 
				"	trx_driverschedule_id DESC \r\n" + 
				"	LIMIT 1").setParameter("order", string).setParameter("company", string2).getResultList();
	}
}
