package com.proacc.serviceimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxTracking;
import com.proacc.repository.TrxTrackingRepository;
import com.proacc.serializable.TrxTrackingSerializable;
import com.proacc.service.TrxTrackingService;
@Service
public class TrxTrackingServiceImpl implements TrxTrackingService{
	private static final Logger logger = LoggerFactory.getLogger(TrxTrackingServiceImpl.class);
	
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxTrackingRepository trxTrackingRepository;

	@Override
	public List<Object[]> getallTracking(String lowerCase, String compId) {
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxt.tm_trx_order_id,\r\n"
				+ "	trxt.trx_tracking_id,\r\n"
				+ "	trxt.m_contact_customer_from_id,\r\n"
				+ "	trxt.m_cabang_id,\r\n"
				+ "	trxt.m_cabang_detail_id,\r\n"
				+ "	cd.m_kotadetail_id AS kotacabang,\r\n"
				+ "	trxt.m_agen_id,\r\n"
				+ "	trxt.m_parent_id,\r\n"
				+ "	C.cabang_name,\r\n"
				+ "	trxt.m_contact_customer_to_id,\r\n"
				+ "	trxt.trx_tracking_status,\r\n"
				+ "	trxt.trx_tracking_company_id \r\n"
				+ "FROM\r\n"
				+ "	trx_tracking AS trxt\r\n"
				+ "	LEFT JOIN m_cabangdetail AS cd ON trxt.m_cabang_id = cd.m_cabang_id \r\n"
				+ "	AND trxt.m_cabang_detail_id = cd.cabangdetail_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = cd.cabangdetail_company_id\r\n"
				+ "	LEFT JOIN m_cabang AS C ON cd.m_parent_id = C.cabang_id \r\n"
				+ "	AND cd.cabangdetail_company_id = C.cabang_company_id "
				+ "WHERE\r\n"
				+ "	trxt.trx_tracking_company_id = :compid \r\n"
				+ "ORDER BY\r\n"
				+ "	trxt.trx_tracking_id ASC ").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getTracking(String lowerCase, String orderId, Integer trackingId, String compId) {
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxt.tm_trx_order_id,\r\n"
				+ "	trxt.trx_tracking_id,\r\n"
				+ "	trxt.m_contact_customer_from_id,\r\n"
				+ "	trxt.m_cabang_id,\r\n"
				+ "	trxt.m_cabang_detail_id,\r\n"
				+ "	cd.m_kotadetail_id AS kotacabang,\r\n"
				+ "	trxt.m_agen_id,\r\n"
				+ "	trxt.m_parent_id,\r\n"
				+ "	C.cabang_name,\r\n"
				+ "	trxt.m_contact_customer_to_id,\r\n"
				+ "	trxt.trx_tracking_status,\r\n"
				+ "	trxt.trx_tracking_company_id \r\n"
				+ "FROM\r\n"
				+ "	trx_tracking AS trxt\r\n"
				+ "	LEFT JOIN m_cabangdetail AS cd ON trxt.m_cabang_id = cd.m_cabang_id \r\n"
				+ "	AND trxt.m_cabang_detail_id = cd.cabangdetail_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = cd.cabangdetail_company_id\r\n"
				+ "	LEFT JOIN m_cabang AS C ON cd.m_parent_id = C.cabang_id \r\n"
				+ "	AND cd.cabangdetail_company_id = C.cabang_company_id "
				+ "WHERE\r\n"
				+ "	trxt.trx_tracking_company_id = :compid \r\n"
				+ "	AND trxt.tm_trx_order_id = :orderid \r\n"
				+ "	AND trxt.trx_tracking_id = :trackingid \r\n"
				+ "ORDER BY\r\n"
				+ "	trxt.trx_tracking_id ASC").setParameter("compid", compId).setParameter("orderid", orderId).setParameter("trackingid", trackingId).getResultList();
	}

	@Override
	public List<Object[]> getTrackingByOrder(String lowerCase, String orderId, String compId) {
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxt.tm_trx_order_id,\r\n"
				+ "	trxt.trx_tracking_id,\r\n"
				+ "	trxt.m_contact_customer_from_id,\r\n"
				+ "	trxt.m_cabang_id,\r\n"
				+ "	trxt.m_cabang_detail_id,\r\n"
				+ "	cd.m_kotadetail_id AS kotacabang,\r\n"
				+ "	trxt.m_agen_id,\r\n"
				+ "	trxt.m_parent_id,\r\n"
				+ "	C.cabang_name,\r\n"
				+ "	trxt.m_contact_customer_to_id,\r\n"
				+ "	trxt.trx_tracking_status,\r\n"
				+ "	trxt.trx_tracking_company_id,\r\n"
				+ "	A.agen_name,\r\n"
				+ "	to_char(trxt.trx_tracking_estimeted_time, 'YYYY-MM-DD HH24:MI'),\r\n"
				+ "	to_char(trxt.trx_tracking_estimeted_time, 'Day') AS Day,\r\n"
				+ "	to_char(trxt.trx_tracking_estimeted_time, 'DD') as tanggal,\r\n"
				+ "	to_char(trxt.trx_tracking_estimeted_time, 'Month') as bulan,\r\n"
				+ "	to_char(trxt.trx_tracking_estimeted_time, 'YYYY') as tahun, \r\n"
				+ " "
				+ "	trxt.trx_tracking_statusinbound,"
				+ " trxt.trx_tracking_statusoutbound, "
				+ "  trxt.trx_tracking_estimeted_time,"
				+ " trxt.m_agendetail_id"
				+ " FROM\r\n"
				+ "	trx_tracking AS trxt\r\n"
				+ "	LEFT JOIN m_cabangdetail AS cd ON trxt.m_cabang_id = cd.m_cabang_id \r\n"
				+ "	AND trxt.m_cabang_detail_id = cd.cabangdetail_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = cd.cabangdetail_company_id\r\n"
				+ "	LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n"
				+ "	LEFT JOIN m_agen AS a ON trxt.m_agen_id = a.agen_id\r\n"
				+ "	AND trxt.trx_tracking_company_id = a.agen_company_id "
				+ "WHERE\r\n"
				+ "	trxt.trx_tracking_company_id = :compid \r\n"
				+ "	AND trxt.tm_trx_order_id = :orderid \r\n"
				+ "ORDER BY\r\n"
				+ "	trxt.trx_tracking_id ASC").setParameter("compid", compId).setParameter("orderid", orderId).getResultList();
	}

	@Override
	public void saveAllTracking(ArrayList<TrxTracking> trxTrackingArray) {
		// TODO Auto-generated method stub
		trxTrackingRepository.saveAll(trxTrackingArray);
	}

	@Override
	public Optional<TrxTracking> getdetail(String orderid, Integer trackingId, String compId) {
		// TODO Auto-generated method stub
		return trxTrackingRepository.findById(new TrxTrackingSerializable(orderid, trackingId, compId));
	}

	@Override
	public List<Object[]> getRoute(Integer kotaFrom, Integer KotaTo, String CompanyId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("WITH RECURSIVE findroute AS (\r\n"
				+ "				 	SELECT\r\n"
				+ "				 		dc.cabang_id,\r\n"
				+ "				 		dc.m_kota_id,\r\n"
				+ "				 		dcb.m_kotadetail_id,\r\n"
				+ "				 		dcb.m_parent_id,\r\n"
				+ "				 		ARRAY [ dcb.cabangdetail_priority ] path3,\r\n"
				+ "				 		ARRAY [ dc.m_kota_id,\r\n"
				+ "				 		dcb.m_kotadetail_id ] PATH,\r\n"
				+ "				 		ARRAY [ 'cabang' || '/id=' || dc.cabang_id || '-parent_id=' || dc.cabang_id,\r\n"
				+ "				 	CASE\r\n"
				+ "				 			\r\n"
				+ "				 			WHEN dcb.m_parent_id IS NOT NULL THEN\r\n"
				+ "				 			'cabang' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id || '-parent_id=' || dcb.m_parent_id \r\n"
				+ "				 			WHEN dcb.m_agen_id IS NOT NULL THEN\r\n"
				+ "				 			'agen' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id || '-agen_id=' || dcb.m_agen_id ELSE'customer' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id \r\n"
				+ "				 		END ] path2 \r\n"
				+ "				 FROM\r\n"
				+ "				 	m_cabang AS dc\r\n"
				+ "				 	INNER JOIN m_cabangdetail AS dcb ON dc.cabang_id = dcb.m_cabang_id \r\n"
				+ "				 	AND dc.cabang_company_id = dcb.cabangdetail_company_id \r\n"
				+ "				 WHERE\r\n"
				+ "				 	dc.m_kota_id = :kotafrom \r\n"
				+ "				 	AND dc.cabang_company_id = :compid UNION ALL\r\n"
				+ "				 SELECT\r\n"
				+ "				 	dc.cabang_id,\r\n"
				+ "				 	dc.m_kota_id,\r\n"
				+ "				 	dcb.m_kotadetail_id,\r\n"
				+ "				 	dcb.m_parent_id,\r\n"
				+ "				 	f.path3 || dcb.cabangdetail_priority,\r\n"
				+ "				 	f.PATH || dcb.m_kotadetail_id,\r\n"
				+ "				 	f.path2 ||\r\n"
				+ "				 CASE\r\n"
				+ "				 		\r\n"
				+ "				 		WHEN dcb.m_parent_id IS NOT NULL THEN\r\n"
				+ "				 		'cabang' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id || '-parent_id=' || dcb.m_parent_id \r\n"
				+ "				 		WHEN dcb.m_agen_id IS NOT NULL THEN\r\n"
				+ "				 		'agen' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id || '-agen_id=' || dcb.m_agen_id ELSE'customer' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id \r\n"
				+ "				 	END \r\n"
				+ "				 	FROM\r\n"
				+ "				 		m_cabang AS dc\r\n"
				+ "				 		INNER JOIN m_cabangdetail AS dcb ON dc.cabang_id = dcb.m_cabang_id \r\n"
				+ "				 		AND dc.cabang_company_id = dcb.cabangdetail_company_id\r\n"
				+ "				 		INNER JOIN findroute AS f ON dc.cabang_id = f.m_parent_id \r\n"
				+ "				 	WHERE\r\n"
				+ "				 		NOT dcb.m_kotadetail_id = ANY ( f.PATH ) \r\n"
				+ "				 		AND dcb.cabangdetail_company_id = :compid \r\n"
				+ "						AND dcb.cabangdetail_status = true\r\n"
				+ "				 		AND f.m_kotadetail_id <> :kotato \r\n"
				+ "				 	) SELECT\r\n"
				+ "				 	cast (findroute.path3 as text ),\r\n"
				+ "				 	cast (findroute.PATH as text ),\r\n"
				+ "				 	cast (findroute.path2  as text ) \r\n"
				+ "				 FROM\r\n"
				+ "				 	findroute \r\n"
				+ "				 WHERE\r\n"
				+ "				 	m_kotadetail_id = :kotato \r\n"
				+ "				 ORDER BY\r\n"
				+ "				 	findroute.path3").setParameter("kotafrom", kotaFrom).setParameter("kotato", KotaTo).setParameter("compid", CompanyId).getResultList();
	}

	@Override
	public Optional<TrxTracking> getupdate(String orderid, String compId) {
		// TODO Auto-generated method stub
		return trxTrackingRepository.findById(new TrxTrackingSerializable(orderid, compId));
	}

	@Override
	public List<Object[]> getTrackingViewOngoing(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.trx_kota_from,\r\n"
				+ "	trxo.trx_kota_to,\r\n"
				+ "	trxt.m_cabang_id,\r\n"
				+ "	C.m_kota_id,\r\n"
				+ "	trxt.m_agen_id,\r\n"
				+ "	A.agen_name,\r\n"
				+ "	trxt.trx_tracking_statusinbound, \r\n"
				+ "	trxt.trx_tracking_status, "
				+ "trxt.trx_tracking_id,"
				+ " case when ad.m_kota_id is null then -1 else ad.m_kota_id end as agen_kota_id,c.cabang_name "
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n"
				+ "	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n"
				+ "	LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n"
				+ "	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = A.agen_company_id "
				+ "left join m_agendetail as ad on ad.m_agen_id = trxt.m_agen_id and ad.agendetail_id = trxt.m_agendetail_id\r\n" + 
				"	and ad.agendetail_company_id = trxt.trx_tracking_company_id\r\n"
				+ "WHERE\r\n"
				+ "	trxo.trx_order_company_id = :compid \r\n"
				+ "	AND ( trxt.trx_tracking_statusinbound = 'ware house' OR ( trxt.trx_tracking_id = 2 AND (trxt.trx_tracking_status = 'data Entry' or trxt.trx_tracking_status = 'Data Entry') ) ) \r\n"
				+ "GROUP BY\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.trx_kota_from,\r\n"
				+ "	trxo.trx_kota_to,\r\n"
				+ "	trxt.m_cabang_id,\r\n"
				+ "	C.m_kota_id,\r\n"
				+ "	trxt.m_agen_id,\r\n"
				+ "	A.agen_name,\r\n"
				+ "	trxt.trx_tracking_statusinbound,"
				+ " trxt.trx_tracking_status,"
				+ " trxt.trx_tracking_id,"
				+ " ad.m_kota_id,c.cabang_name "
				+ " UNION \r\n"
				+ "SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.trx_kota_from,\r\n"
				+ "	trxo.trx_kota_to,\r\n"
				+ "	NULL,\r\n"
				+ "	NULL,\r\n"
				+ "	NULL,\r\n"
				+ "	NULL,\r\n"
				+ "	NULL,\r\n"
				+ "	'intransit',"
				+ " -1 as trx_tracking_id, 0 as agen_kota_id, null "
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN trx_driverschedule AS trxd ON trxo.trx_order_id = trxd.tm_trx_order_id \r\n"
				+ "	AND trxo.trx_order_company_id = trxd.trx_driverschedule_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trxo.trx_order_company_id = :compid \r\n"
				+ "	AND ( trxd.trx_driverschedule_statusoutbound = 'on proggress' OR trxd.trx_driverschedule_status = 'pick Up' or trxd.trx_driverschedule_statusoutbound='on Proggress' or trxd.trx_driverschedule_statusoutbound = 'waiting response') \r\n"
				+ "GROUP BY\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.trx_kota_from,\r\n"
				+ "	trxo.trx_kota_to ").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getTrackingSettingOngoing(String lowerCase, String compId, String useruuid) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n" + 
				"	* \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT\r\n" + 
				"		trxo.trx_order_id,\r\n" + 
				"		trxo.trx_order_date,\r\n" + 
				"		trxo.trx_kota_from,\r\n" + 
				"		trxo.trx_kota_to,\r\n" + 
				"		'unassigned' G,\r\n" + 
				"		C.cabang_name,\r\n" + 
				"		A.agen_name \r\n" + 
				"	FROM\r\n" + 
				"		trx_order AS trxo\r\n" + 
				"		LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n" + 
				"		AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n" + 
				"		LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n" + 
				"		AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n" + 
				"		LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n" + 
				"		AND trxt.trx_tracking_company_id = A.agen_company_id \r\n" + 
				"	WHERE\r\n" + 
				"		trxo.trx_order_company_id = :compid \r\n" + 
				"		AND (\r\n" + 
				"			trxt.trx_tracking_statusinbound = 'ware house' \r\n" + 
				"			OR ( -- 	trxt.m_contact_customer_from_id IS NOT NULL\r\n" + 
				"			trxt.trx_tracking_id = 2 AND ( trxt.trx_tracking_status = 'data Entry' OR trxt.trx_tracking_status = 'Data Entry' ) ) \r\n" + 
				"		) "
				+ " OR\r\n" + 
				"CASE\r\n" + 
				"		\r\n" + 
				"		WHEN (\r\n" + 
				"			SELECT A2\r\n" + 
				"				.agen_name \r\n" + 
				"			FROM\r\n" + 
				"				trx_order AS trxo2\r\n" + 
				"				LEFT JOIN trx_tracking AS trxt2 ON trxo2.trx_order_id = trxt2.tm_trx_order_id \r\n" + 
				"				AND trxo2.trx_order_company_id = trxt2.trx_tracking_company_id\r\n" + 
				"				LEFT JOIN m_cabang AS C2 ON trxt2.m_parent_id = C2.cabang_id \r\n" + 
				"				AND trxt2.trx_tracking_company_id = C2.cabang_company_id\r\n" + 
				"				LEFT JOIN m_agen AS A2 ON trxt2.m_agen_id = A2.agen_id \r\n" + 
				"				AND trxt2.trx_tracking_company_id = A2.agen_company_id \r\n" + 
				"			WHERE\r\n" + 
				"				trxo2.trx_order_company_id = :compid \r\n" + 
				"				AND ( trxt2.trx_tracking_statusinbound = 'ware house' ) \r\n" + 
				"				and trxo2.trx_order_id = trxo.trx_order_id\r\n" + 
				"				\r\n" + 
				"			 \r\n" + 
				"			) IS NOT NULL   THEN\r\n" + 
				"			trxt.trx_tracking_id = (\r\n" + 
				"				(\r\n" + 
				"				SELECT trxt2.trx_tracking_id\r\n" + 
				"			FROM\r\n" + 
				"				trx_order AS trxo2\r\n" + 
				"				LEFT JOIN trx_tracking AS trxt2 ON trxo2.trx_order_id = trxt2.tm_trx_order_id \r\n" + 
				"				AND trxo2.trx_order_company_id = trxt2.trx_tracking_company_id\r\n" + 
				"				LEFT JOIN m_cabang AS C2 ON trxt2.m_parent_id = C2.cabang_id \r\n" + 
				"				AND trxt2.trx_tracking_company_id = C2.cabang_company_id\r\n" + 
				"				LEFT JOIN m_agen AS A2 ON trxt2.m_agen_id = A2.agen_id \r\n" + 
				"				AND trxt2.trx_tracking_company_id = A2.agen_company_id \r\n" + 
				"			WHERE\r\n" + 
				"				trxo2.trx_order_company_id = :compid \r\n" + 
				"				AND ( trxt2.trx_tracking_statusinbound = 'ware house' ) \r\n" + 
				"				and trxo2.trx_order_id = trxo.trx_order_id\r\n" + 
				"				\r\n" + 
				"					\r\n" + 
				"				) + 1 \r\n" + 
				"			) \r\n" + 
				"		END  "
				+ ""
				+ "\r\n" + 
				"	GROUP BY\r\n" + 
				"		trxo.trx_order_id,\r\n" + 
				"		trxo.trx_order_date,\r\n" + 
				"		trxo.trx_kota_from,\r\n" + 
				"		trxo.trx_kota_to,\r\n" + 
				"		C.cabang_name,\r\n" + 
				"		A.agen_name UNION\r\n" + 
				"	SELECT\r\n" + 
				"		trxo.trx_order_id,\r\n" + 
				"		trxo.trx_order_date,\r\n" + 
				"		trxo.trx_kota_from,\r\n" + 
				"		trxo.trx_kota_to,\r\n" + 
				"		'assigned' G,\r\n" + 
				"		C.cabang_name,\r\n" + 
				"		A.agen_name \r\n" + 
				"	FROM\r\n" + 
				"		trx_order AS trxo\r\n" + 
				"		LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n" + 
				"		AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n" + 
				"		LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n" + 
				"		AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n" + 
				"		LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n" + 
				"		AND trxt.trx_tracking_company_id = A.agen_company_id \r\n" + 
				"	WHERE\r\n" + 
				"		trxo.trx_order_company_id = :compid \r\n" + 
				"		AND (trxt.trx_tracking_statusinbound != 'ware house' or trxt.trx_tracking_statusoutbound='on Proggress') \r\n" + 
				"		--AND trxt.trx_tracking_statusinbound IS NOT NULL \r\n" + 
				"		AND ( ( trxt.trx_tracking_status != 'data Entry' OR trxt.trx_tracking_status != 'Data Entry' ) OR trxt.trx_tracking_status IS NULL ) \r\n" + 
				"	GROUP BY\r\n" + 
				"		trxo.trx_order_id,\r\n" + 
				"		trxo.trx_order_date,\r\n" + 
				"		trxo.trx_kota_from,\r\n" + 
				"		trxo.trx_kota_to,\r\n" + 
				"		trxt.m_parent_id,\r\n" + 
				"		C.cabang_name,\r\n" + 
				"		A.agen_name \r\n" + 
				"	) AS K \r\n" + 
				"WHERE\r\n" + 
				"	1 = 1 \r\n" + 
				"AND\r\n" + 
				"" + 
				"	( K.cabang_name = :useruuid OR K.agen_name = :useruuid ) "
				
				).setParameter("compid", compId)
				.setParameter("useruuid", useruuid).getResultList();
	}

	@Override
	public List<Object[]> getTrackingViewDone(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("SELECT\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.trx_kota_from,\r\n"
				+ "	trxo.trx_kota_to,\r\n"
				+ "	trxt.m_parent_id,\r\n"
				+ "	C.m_kota_id,\r\n"
				+ "	trxt.m_agen_id,\r\n"
				+ "	A.agen_name,\r\n"
				+ "	trxt.trx_tracking_statusoutbound \r\n"
				+ "FROM\r\n"
				+ "	trx_order AS trxo\r\n"
				+ "	LEFT JOIN trx_tracking AS trxt ON trxo.trx_order_id = trxt.tm_trx_order_id \r\n"
				+ "	AND trxo.trx_order_company_id = trxt.trx_tracking_company_id\r\n"
				+ "	LEFT JOIN m_cabang AS C ON trxt.m_parent_id = C.cabang_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = C.cabang_company_id\r\n"
				+ "	LEFT JOIN m_agen AS A ON trxt.m_agen_id = A.agen_id \r\n"
				+ "	AND trxt.trx_tracking_company_id = A.agen_company_id \r\n"
				+ "WHERE\r\n"
				+ "	trxo.trx_order_company_id = :compid \r\n"
				+ "	AND trxt.m_contact_customer_to_id IS NOT NULL \r\n"
				+ "	AND trxt.trx_tracking_statusoutbound = 'complete' \r\n"
				+ "GROUP BY\r\n"
				+ "	trxo.trx_order_id,\r\n"
				+ "	trxo.trx_order_date,\r\n"
				+ "	trxo.trx_kota_from,\r\n"
				+ "	trxo.trx_kota_to,\r\n"
				+ "	trxt.m_parent_id,\r\n"
				+ "	C.m_kota_id,\r\n"
				+ "	trxt.m_agen_id,\r\n"
				+ "	A.agen_name,\r\n"
				+ "	trxt.trx_tracking_statusoutbound ").setParameter("compid", compId).getResultList();
	}

	@Override
	public List<Object[]> getTrackingSettingDone(String lowerCase, String compId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public void updatestrackingtatusbyorder(String OrderId, String compId) {
		// TODO Auto-generated method stub
		this.em.createNativeQuery("UPDATE trx_tracking \r\n"
				+ "SET trx_tracking_status = NULL,\r\n"
				+ "trx_tracking_statusinbound = NULL,\r\n"
				+ "trx_tracking_statusoutbound = 'complete' \r\n"
				+ "WHERE\r\n"
				+ "	tm_trx_order_id = :orderid AND trx_tracking_company_id = :compid ").setParameter("orderid", OrderId).setParameter("compid", compId).executeUpdate();
	}
	
	@Override
	@Transactional
	public void updatestrackingtatusbyorderCancell(String OrderId, String compId) {
		// TODO Auto-generated method stub
		this.em.createNativeQuery("UPDATE trx_tracking \r\n"
				+ "SET trx_tracking_status = 'Data Entry',\r\n"
				+ "trx_tracking_statusinbound = NULL,\r\n"
				+ "trx_tracking_statusoutbound = null \r\n"
				+ "WHERE\r\n"
				+ "	tm_trx_order_id = :orderid AND trx_tracking_company_id = :compid ").setParameter("orderid", OrderId).setParameter("compid", compId).executeUpdate();
	}

	@Override
	public List<Object[]> getRouteKhususGetFromCoverAreaAgent(Integer getmKotaId, Integer tokota, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("WITH RECURSIVE findroute AS (\r\n" + 
				"	SELECT P\r\n" + 
				"		.m_agen_id,\r\n" + 
				"		P.cabang_id,\r\n" + 
				"		P.m_kota_id,\r\n" + 
				"		P.m_kotadetail_id,\r\n" + 
				"		P.m_parent_id,\r\n" + 
				"		P.PATH,\r\n" + 
				"		P.path2 \r\n" + 
				"	FROM\r\n" + 
				"		(\r\n" + 
				"		SELECT NULL AS\r\n" + 
				"			m_agen_id,\r\n" + 
				"			dc.cabang_id,\r\n" + 
				"			dc.m_kota_id,\r\n" + 
				"			dcb.m_kotadetail_id,\r\n" + 
				"			dcb.m_parent_id,\r\n" + 
				"			ARRAY [ NULL ] path3,\r\n" + 
				"			ARRAY [ dc.m_kota_id,\r\n" + 
				"			dcb.m_kotadetail_id ] PATH,\r\n" + 
				"			ARRAY [ 'cabang' || '/id=' || dc.cabang_id || '-parent_id=' || dc.cabang_id,\r\n" + 
				"		CASE\r\n" + 
				"				\r\n" + 
				"				WHEN dcb.m_parent_id IS NOT NULL THEN\r\n" + 
				"				'cabang' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id || '-parent_id=' || dcb.m_parent_id \r\n" + 
				"				WHEN dcb.m_agen_id IS NOT NULL THEN\r\n" + 
				"				'agen' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id || '-agen_id=' || dcb.m_agen_id || '-agen_type=' || (select agn.agen_type from m_agen agn where agn.agen_id = dcb.m_agen_id and agn.agen_company_id = :company) ELSE 'customer' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id \r\n" + 
				"			END ] path2 \r\n" + 
				"FROM\r\n" + 
				"	m_cabang AS dc\r\n" + 
				"	INNER JOIN m_cabangdetail AS dcb ON dc.cabang_id = dcb.m_cabang_id \r\n" + 
				"	AND dc.cabang_company_id = dcb.cabangdetail_company_id \r\n" + 
				"WHERE\r\n" + 
				"	dc.cabang_company_id = :company \r\n" + 
				"	AND dc.cabang_status = TRUE \r\n" + 
				"	AND dcb.cabangdetail_status = TRUE UNION\r\n" + 
				"SELECT\r\n" + 
				"	ma.agen_id m_agen_id,\r\n" + 
				"	NULL cabang_id,\r\n" + 
				"	mad.m_kota_id m_kota_id,\r\n" + 
				"	mad.m_transferpoint_id m_kotadetail_id,\r\n" + 
				"	ma.agen_id m_parent_id,\r\n" + 
				"	ARRAY [ NULL ] path3,\r\n" + 
				"	ARRAY [ mad.m_kota_id,\r\n" + 
				"	mad.m_transferpoint_id ] PATH,\r\n" + 
				"	null path2 \r\n" + 
				"FROM\r\n" + 
				"	m_agen ma\r\n" + 
				"	INNER JOIN m_agendetail mad ON ma.agen_id = mad.m_agen_id \r\n" + 
				"WHERE\r\n" + 
				"	ma.agen_company_id = :company \r\n" + 
				"	AND mad.agendetail_status = TRUE \r\n" + 
				"	AND ma.agen_status = TRUE \r\n" + 
				"	) P \r\n" + 
				"WHERE\r\n" + 
				"	P.m_kota_id = "+getmKotaId+" \r\n" + 
				"	UNION ALL\r\n" + 
				"SELECT\r\n" + 
				"	cast ( w.m_agen_id as integer ) ,\r\n" + 
				"	w.cabang_id,\r\n" + 
				"	w.m_kota_id,\r\n" + 
				"	w.m_kotadetail_id,\r\n" + 
				"	w.m_parent_id,\r\n" + 
				"	f.PATH || w.m_kotadetail_id,\r\n" + 
				"	 f.path2 || w.path2 \r\n" + 
				"FROM\r\n" + 
				"	(\r\n" + 
				"	SELECT null AS\r\n" + 
				"		m_agen_id,\r\n" + 
				"		dc.cabang_id,\r\n" + 
				"		dc.m_kota_id,\r\n" + 
				"		dcb.m_kotadetail_id,\r\n" + 
				"		dcb.m_parent_id,\r\n" + 
				"		ARRAY [ NULL ] path3,\r\n" + 
				"		ARRAY [ dc.m_kota_id,\r\n" + 
				"		dcb.m_kotadetail_id ] PATH,\r\n" + 
				"		ARRAY [ \r\n" + 
				"	CASE\r\n" + 
				"			\r\n" + 
				"			WHEN dcb.m_parent_id IS NOT NULL THEN\r\n" + 
				"			'cabang' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id || '-parent_id=' || dcb.m_parent_id \r\n" + 
				"			WHEN dcb.m_agen_id IS NOT NULL THEN\r\n" + 
				"			'agen' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id || '-agen_id=' || dcb.m_agen_id || '-agen_type=' || (select agn.agen_type from m_agen agn where agn.agen_id = dcb.m_agen_id and agn.agen_company_id = :company) ELSE'customer' || '/id=' || dc.cabang_id || '-detail_id=' || dcb.cabangdetail_id \r\n" + 
				"		END ] path2 \r\n" + 
				"FROM\r\n" + 
				"	m_cabang AS dc\r\n" + 
				"	INNER JOIN m_cabangdetail AS dcb ON dc.cabang_id = dcb.m_cabang_id \r\n" + 
				"	AND dc.cabang_company_id = dcb.cabangdetail_company_id \r\n" + 
				"	WHERE-- 	dc.m_kota_id = 3374\r\n" + 
				"	dc.cabang_company_id = :company \r\n" + 
				"	AND dc.cabang_status = TRUE \r\n" + 
				"	AND dcb.cabangdetail_status = TRUE \r\n" + 
				"	UNION\r\n" + 
				"SELECT\r\n" + 
				"	ma.agen_id m_agen_id,\r\n" + 
				"	NULL cabang_id,\r\n" + 
				"	mad.m_kota_id m_kota_id,\r\n" + 
				"	mad.m_transferpoint_id m_kotadetail_id,\r\n" + 
				"	ma.agen_id m_parent_id,\r\n" + 
				"	ARRAY [ NULL ] path3,\r\n" + 
				"	ARRAY [ mad.m_kota_id,\r\n" + 
				"	mad.m_transferpoint_id ] PATH,\r\n" + 
				"	null path2\r\n" + 
				"-- 	ARRAY [ 'agen' || '/id=' || ma.agen_id || '-parent_id=' || ma.agen_id || '-agen_type=' || (select agn.agen_type from m_agen agn where agn.agen_id = dcb.m_agen_id and agn.agen_company_id = :company) ] path2 \r\n" + 
				"FROM\r\n" + 
				"	m_agen ma\r\n" + 
				"	INNER JOIN m_agendetail mad ON ma.agen_id = mad.m_agen_id \r\n" + 
				"WHERE\r\n" + 
				"	ma.agen_company_id = :company \r\n" + 
				"	AND mad.agendetail_status = TRUE \r\n" + 
				"	AND ma.agen_status = TRUE \r\n" + 
				"	) w\r\n" + 
				"	INNER JOIN findroute f ON \r\n" + 
				"	( f.m_kotadetail_id = w.m_kota_id ) \r\n" + 
				"\r\n" + 
				"WHERE\r\n" + 
				"	NOT w.m_kotadetail_id = ANY ( f.PATH ) \r\n" + 
				"	AND f.m_kotadetail_id <>  "+tokota+" \r\n" + 
				"	) \r\n" + 
				"	SELECT \r\n" + 
				"-- 	cast ( findroute.path3 AS TEXT ),\r\n" + 
				"	CAST ( findroute.path AS TEXT ),\r\n" + 
				"	CAST ( findroute.path2 AS TEXT ) \r\n" + 
				"\r\n" + 
				"FROM\r\n" + 
				"	findroute \r\n" + 
				"WHERE\r\n" + 
				"	m_kotadetail_id = "+tokota+" \r\n" + 
				"	LIMIT 20")
//				.setParameter("from", getmKotaId)
//				.setParameter("to", tokota)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public void saveTracking(TrxTracking trxTracking) {
		// TODO Auto-generated method stub
		
//		trxTracking.setTrxTrackingEstimetedTime(null);
		
		trxTrackingRepository.save(trxTracking);
	}

	@Override
	public List<Object[]> getAgen(Integer fromkota, Integer tokota, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select *\r\n" + 
				" from (select null m_cabang_id, null m_cabangdetail_id, magd.m_agen_id, magd.agendetail_id, mac.agen_cover_area_id, mac.m_kota_id as cover_kota, magd.m_kota_id, magd.agendetail_company_id company_id, null m_parent_id, magd.kode  from m_agendetail magd\r\n" + 
				"left join m_agen_cover_area mac on mac.m_agen_id = magd.m_agen_id and magd.agendetail_id = mac.m_agendetail_id and magd.agendetail_company_id = mac.agen_cover_area_company_id\r\n" + 
				"union\r\n" + 
				"select c.cabang_id m_cabang_id, cd.cabangdetail_id m_cabangdetail_id, null m_agen_id, null agendetail_id, null agen_cover_area_id, cd.m_kotadetail_id cover_kota ,  c.m_kota_id m_kota_id, c.cabang_company_id company_id, cd.m_parent_id, c.kode from m_cabang c \r\n" + 
				"left join m_cabangdetail cd on c.cabang_id = cd.m_cabang_id and c.cabang_company_id = cd.cabangdetail_company_id) p\r\n" + 
				"where\r\n" + 
				"(p.cover_kota = :city or p.m_kota_id = :city)\r\n" + 
				"and\r\n" + 
				"p.m_agen_id is not null\r\n" + 
				"and p.company_id = :company ")
				.setParameter("city", fromkota)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public List<Object[]> getCabang(Integer fromkota, Integer tokota, String string) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select *\r\n" + 
				" from (select null m_cabang_id, null m_cabangdetail_id, magd.m_agen_id, magd.agendetail_id, mac.agen_cover_area_id, mac.m_kota_id as cover_kota, magd.m_kota_id, magd.agendetail_company_id company_id, null m_parent_id, magd.kode from m_agendetail magd\r\n" + 
				"left join m_agen_cover_area mac on mac.m_agen_id = magd.m_agen_id and magd.agendetail_id = mac.m_agendetail_id and magd.agendetail_company_id = mac.agen_cover_area_company_id\r\n" + 
				"union\r\n" + 
				"select c.cabang_id m_cabang_id, cd.cabangdetail_id m_cabangdetail_id, null m_agen_id, null agendetail_id, null agen_cover_area_id, cd.m_kotadetail_id cover_kota ,  c.m_kota_id m_kota_id, c.cabang_company_id company_id,cd.m_parent_id, c.kode from m_cabang c \r\n" + 
				"left join m_cabangdetail cd on c.cabang_id = cd.m_cabang_id and c.cabang_company_id = cd.cabangdetail_company_id) p\r\n" + 
				"where\r\n" + 
				"(p.cover_kota = :city or p.m_kota_id = :city)\r\n" + 
				"and\r\n" + 
				"p.m_cabang_id is not null\r\n" + 
				"and p.company_id = :company ")
				.setParameter("city", fromkota)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public List<Object[]> getLastTrackingId(String string, String string2) {
		// TODO Auto-generated method stub
		return this.em.createNativeQuery("select (trx_tracking_id ), trx_tracking_company_id num from trx_tracking\r\n" + 
				"where\r\n" + 
				"tm_trx_order_id = :orderId \r\n" + 
				"and trx_tracking_company_id = :company \r\n" + 
				"order by trx_tracking_id desc\r\n" + 
				"limit 1")
				.setParameter("orderId", string)
				.setParameter("company", string2).getResultList();
	}

	@Override
	@Transactional
	public void deleteTracking(String string, Integer typeTambahan, String string2) {
		// TODO Auto-generated method stub
		trxTrackingRepository.deleteById(new TrxTrackingSerializable(string, typeTambahan, string2));
	}

	@Override
	@Transactional
	public void updateId(Integer trxTracking, Integer sebelum, String company, String orderId) {
		// TODO Auto-generated method stub
		this.em.createNativeQuery("UPDATE trx_tracking\r\n" + 
				"SET trx_tracking_id = "+trxTracking+"\r\n" + 
				"WHERE trx_tracking_id = :sebelum and trx_tracking_company_id =:company and tm_trx_order_id = :orderId").setParameter("sebelum", sebelum)
		.setParameter("company", company)
		.setParameter("orderId", orderId)
		.executeUpdate();
	}
}
