package com.proacc.serviceimpl;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.hibernate.usertype.UserVersionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proacc.entity.TrxInvoiceHeader;
import com.proacc.repository.TrxInvoiceHeaderRepository;
import com.proacc.serializable.TrxInvoiceSerializable;
import com.proacc.service.TrxInvoiceHeaderService;

@Service
public class TrxInvoiceHeaderServiceImpl implements TrxInvoiceHeaderService{

	private static final Logger logger = LoggerFactory.getLogger(TrxInvoiceHeaderServiceImpl.class);
	@Autowired
	EntityManager em;
	
	@Autowired
	TrxInvoiceHeaderRepository TrxInvoiceHeaderRepository;

	@Override
	public List<Object[]> getId() {
		// TODO Auto-generated method stub
		return em.createNativeQuery("select nextval('invoice_header_sequence')").getResultList();
	}

	@Override
	public List<Object[]> listOpenInvoice(UUID user, String paramAktif, int i, String string) {
		// TODO Auto-generated method stub
		String status = "";
		if(paramAktif.equals("y"))
		{
			status = " and a.status = true";
		}
		else if ( paramAktif.equals("n") )
		{
			status = " and a.status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT a.invoice_nomor, cast(a.customer_name as varchar ), a.date_tanggal, a.gross_price, a.tax_nominal, a.nett_price, cast ( a.created_by as varchar ), a.status, a.tax_percent, a.bank_name, a.bank_account, a.account_name, a.name_signature, a.position_signature, a.text_signature,a.invoice_header_id  FROM \"trx_invoice_header\" a\r\n" + 
				"where\r\n" + 
				"1=1 "
				+ status + 
				" and a.created_by = :user "
				+ " and a.status_invoice = 0 "
				+ " and a.status_draft = :draft "
				+ " and a.company_id = :company" + 
				"" + 
				"").setParameter("user", user)
				.setParameter("draft", i)
				.setParameter("company", string)
				.getResultList();
	}

	@Override
	public void save(TrxInvoiceHeader data) {
		// TODO Auto-generated method stub
		TrxInvoiceHeaderRepository.save(data);
	}

	@Override
	public Optional<TrxInvoiceHeader> getDetail(int int1, String string) {
		// TODO Auto-generated method stub
		return TrxInvoiceHeaderRepository.findById(new TrxInvoiceSerializable(int1, string));
	}

	@Override
	public List<Object[]> listPaidInvoice(UUID user_uuid, String paramAktif,String string) {
		// TODO Auto-generated method stub
		String status = "";
		if(paramAktif.equals("y"))
		{
			status = " and a.status = true";
		}
		else if ( paramAktif.equals("n") )
		{
			status = " and a.status = false";
		}
		else
		{
			status = "";
		}
		return this.em.createNativeQuery("SELECT a.invoice_nomor, cast(a.customer_name as varchar ), a.date_tanggal, a.gross_price, a.tax_nominal, a.nett_price, cast ( a.created_by as varchar ), a.status, a.tax_percent, a.bank_name, a.bank_account, a.account_name, a.name_signature, a.position_signature, a.text_signature,a.invoice_header_id  FROM \"trx_invoice_header\" a\r\n" + 
				"where\r\n" + 
				"1=1 "
				+ status + 
				" and a.created_by = :user "
				+ " and a.status_invoice = 1 "
				+ " and a.status_draft = 1 "
				+ " and a.company_id = :company" + 
				"" + 
				"").setParameter("user", user_uuid)
				.setParameter("company", string)
				.getResultList();
	}

}
